/****************************************************************************/
/**! \mainpage TssJobs Classes used for defining jobs to run in the TSS server
 * \section intro_sec Introduction
 *
 * TssJobs.hpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * BNF
 * jobs     	 ::= queue_now 
 *           	 | scheduled
 *
 * queue_now      ::= sequence 
 *           	 |  parallell
 * 
 * sequence  	::= server {job}
 * 
 * parallell 	::= server {job}
 * 
 * node		::= rank i.e 0,1,2,3
 *		|   any
 *		|   all
 *
 * job       	::= id task {capability} {argument}
 * 
 * capability	::= opencl
 * 		| ...
 *
 * scheduled 	::= date_and_time
 * 		| repeat_minute
 * 		| repeat_hourly
 *           	| repeat_daily
 *           	| repeat_weekly
 *          	| repeat_monthly
 *
 * An XML file is used to define the jobs to be executed, either immidiately
 * or scheduled. An alternative is to use the Google format for cron
 * http://code.google.com/intl/sv-SE/appengine/docs/java/config/cron.html
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101219 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef TSS_COMMANDS
#define TSS_COMMANDS

namespace tss_commands {

/*----- sequence -----*/
 
class job {
public:
	static const int type = 1;

	std::string id;
	//Task - unsure how to specify - enum, #define or use some dynamic loading of class
	std::vector<std::string> args;
	//capability define a bit pattern - need to check...
};

 
/*----- server (node is confusing since it also si used to specify nodes in the tree) -----*/
 
class server {
public:
	static const int type = 2;
	// use enum etc for rank, any all
	int rank;
};

 
/*----- queue_now -----*/
 
class queue_now {
public:
	static const int type = 3;
};

class sequence_node : queue_now {
public:
	static const int type = 4;
	server		 s;
	std::vector<job>	js;
};

class parallell_node : queue_now {
public:
	static const int type = 5;
	server 		s;
	std::vector<job>	js;	
}; 


/*----- jobs -----*/
	  
class jobs {
public:
	static const int type = 6;
};

class queue_now_node : jobs {
public:
	static const int type = 7;
	queue_now* n;
};

class scheduled_node : jobs {
public:
	static const int type = 8;
	
	// NOT IMPLEMENTED YET
};

class jobs_node {
public:
	static const int type = 9;
	jobs* n;
};
 

} //namepaace tss_commands

#endif //TSS_COMMANDS

