/****************************************************************************/
/**
 *
 * TssProcess.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Wait for data to be ready and then acquire lock:
 *
 * boost::condition_variable cond;
 * boost::mutex mut;
 * bool data_ready;
 *
 * void process_data();
 * 
 * void wait_for_data_to_process()
 * {
 *    boost::unique_lock<boost::mutex> lock(mut);
 *    while(!data_ready)
 *    {
 *        cond.wait(lock);
 *    }
 *    process_data();
 * }
 *
 *
 * In order to make it possible for several threads to read and one thread to write
 *
 * boost::shared_mutex _access;
 * void reader()
 * {
 *     // get shared access
 *     boost::shared_lock lock(_access);
 *
 *     // now we have shared access
 * }
 *
 * void writer()
 * {
 *     // get upgradable access
 *     boost::upgrade_lock lock(_access);
 *
 *     // get exclusive access
 *     boost::upgrade_to_unique_lock uniqueLock(lock);
 *     // now we have exclusive access
 * }
 * 
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101206 Jonas C. Initial version
 *
 ****************************************************************************/

 

#include "TssProcess.hpp"

boost::condition_variable cond;
boost::mutex              mut;
bool                      stop_threads  = false;


/// TssProcess
/**
 * Empty contructor
 *
 *
 */ 	

tss_process::TssProcess::TssProcess() {
	this->m_pMPIProcessData     = NULL;
	this->m_pvAllMPIProcessData = NULL;
	this->m_pJobCreator	    = NULL;
	initMpiMembers();
}

/// initMpiMembers
/**
 * Initialize the MPI variables and distribute the data among all processes.
 *
 *
 */
void tss_process::TssProcess::initMpiMembers() {
	mpi::environment env; //(argc, argv);
	mpi::communicator world;
	
	this->m_pMPIProcessData = new mpi_process_data(world.rank(), 
							world.size(),
							env.processor_name());
	
	std::vector<mpi_process_data> in_values(this->m_pMPIProcessData->get_num_processes());
	this->m_pvAllMPIProcessData = new std::vector<mpi_process_data>(this->m_pMPIProcessData->get_num_processes());


	for(int i=0; i<this->m_pMPIProcessData->get_num_processes(); i++)
		in_values[i] = *m_pMPIProcessData;

	/// MPI function for sharing data among processes
	all_to_all(world, in_values, *(this->m_pvAllMPIProcessData));
	
}


/// ~TssProcess
/**
 * Empty destructor
 *
 *
 */ 	
tss_process::TssProcess::~TssProcess() {
	if (this->m_pMPIProcessData != NULL) 		delete this->m_pMPIProcessData;
	if (this->m_pvAllMPIProcessData != NULL) 	delete this->m_pvAllMPIProcessData;
	if (this->m_pJobCreator != NULL) 		delete this->m_pJobCreator;
}


/// addThread
/**
 * Get the contents once it has been parsed.
 *
 * @param thread The thread to add to the process
 *
 * \return void
 *
 */ 	
void tss_process::TssProcess::addThread(TssThreadWorker* thread) { 
	this->m_ThreadWorkers.push_back(thread); 
}


/// startThreads
/**
 * Get the contents once it has been parsed.
 *
 * @param thread The thread to add to the process
 *
 * \return void
 *
 */ 	
void tss_process::TssProcess::startThreads() { 
	std::vector<TssThreadWorker*>::const_iterator iterator;
	
	for(iterator=m_ThreadWorkers.begin(); iterator!=m_ThreadWorkers.end(); iterator++) {
		 (*iterator)->start();
	}

} 

/// joinThreads
/**
 * Get the contents once it has been parsed.
 *
 * @param thread The thread to add to the process
 *
 * \return void
 *
 */ 	
void tss_process::TssProcess::joinThreads() { 
	std::vector<TssThreadWorker*>::const_iterator iterator;
	
	for(iterator=m_ThreadWorkers.begin(); iterator!=m_ThreadWorkers.end(); iterator++) {
		 (*iterator)->join();
	}

} 


void tss_process::TssProcess::createJobsAndThreads(tss_commands::jobs_node* jobs) {
	if( m_pJobCreator == NULL ) {
		//throw some exception and log it
	}
	
	if( m_pJobCreator == NULL ) {
		//throw some exception and log it
	}
	
	switch( jobs->n->type ) {
		
		case tss_commands::queue_now_node::type:
		{
			tss_commands::queue_now_node* qn = (tss_commands::queue_now_node*) jobs->n;
			
			switch( qn->n->type ) {
				
				// run jobs in sequence	
				case tss_commands::sequence_node::type:
				{
					std::cout << "SEQUENCE" << std::endl;
					tss_commands::sequence_node* sn = (tss_commands::sequence_node*) qn->n;
					
					// should check sn.server that this is the right server
					
					//iterate over the jobs in the vector
					for ( std::vector<tss_commands::job>::iterator it=sn->js.begin(); it < sn->js.end(); it++ ) {
						tss_commands::job job = (tss_commands::job) *it;
						
						TssJob*		 pTssJob 	= m_pJobCreator->createJob(job);
						TssThreadWorker* pThreadWorker 	= new TssThreadWorker();
						pThreadWorker->setJob(pTssJob);
						
						//run threads in sequence
						this->addThread(pThreadWorker);
						this->startThreads();
						//wait for threads to end
						this->joinThreads();
					}
				}
				break;
				
				// run jobs in parallell	
				case tss_commands::parallell_node::type:
				{
					std::cout << "PARALLELL" << std::endl;
					tss_commands::parallell_node* pn = (tss_commands::parallell_node*) qn->n;

					// should check sn->server that this is the right server
										
					//iterate over the jobs in the vector
					for ( std::vector<tss_commands::job>::iterator  it=pn->js.begin(); it < pn->js.end(); it++ ) {
						tss_commands::job job = (tss_commands::job) *it;
						
						TssJob*		 pTssJob 	= m_pJobCreator->createJob(job);
						TssThreadWorker* pThreadWorker 	= new TssThreadWorker();
						pThreadWorker->setJob(pTssJob);
						
						this->addThread(pThreadWorker);
					}
					
					//run threads in parallell
					this->startThreads();
					//wait for threads to end
					this->joinThreads();
					break;
					
					//default:
					//throw exception - unkonow command
				}
				break;
			}
		}
		break;
		
		case tss_commands::scheduled_node::type:	
			// NOT IMPLMENTED YET
			break;
			
		//default:
			//throw exception - unknown command
			
	}
}


/******************************************************************************/




/// TssThreadWorker
/**
 * Empty contructor
 *
 *
 */ 	
tss_process::TssThreadWorker::TssThreadWorker() {
	m_pJob = NULL;
	// the thread is not-a-thread until we call start()
}


/// ~TssThreadWorker
/**
 * Empty destructor
 *
 *
 */ 	
tss_process::TssThreadWorker::~TssThreadWorker() {
	if(m_pJob != NULL) delete m_pJob;
}


/// run
/**
 * This fucntion does the actual work. It should be overloaded in the
 * class that inherits this class.
 *
 */ 	

void tss_process::TssThreadWorker::run() {
	// continue until stop_threads is set
	mut.lock();
	while(!stop_threads) {
		mut.unlock();
		this->work();
		mut.lock();
	}

	mut.unlock();
}	

/// work
/**
 * This fucntion does the actual work. It should be overloaded in the
 * class that inherits this class.
 *
 */ 	

void tss_process::TssThreadWorker::work() {
	if(this->m_pJob != NULL)
		this->m_pJob->run();
}

void tss_process::TssThreadWorker::start() {
	m_Thread = boost::thread(&TssThreadWorker::run, this);
}


void tss_process::TssThreadWorker::join() {
	m_Thread.join();
}

/// stopThreads
/**
 * Stop all threads in this by setting the stop_threads flag
 *
 *
 * \return void
 *
 */ 	
void tss_process::TssThreadWorker::stopThreads() { 
	mut.lock();
	stop_threads = true;
	mut.unlock();
} 





