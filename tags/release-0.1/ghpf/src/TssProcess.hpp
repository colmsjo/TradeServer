/****************************************************************************/
/**! \mainpage TssServer Time series server
 * \section intro_sec Introduction
 *
 * TssProcess.hpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Base classes for building a applications composed of a set of processes  
 * each containing a set of threads.
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101215 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef TSS_PROCESS
#define TSS_PROCESS

#include <iostream>

#include <boost/thread.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
namespace mpi = boost::mpi;

#include "TssCommands.hpp"

#ifdef _DEBUG
#include "../../tools/nvwa-0.8.2/nvwa/debug_new.h"
#endif

namespace tss_process {

/// mpi_process_data class
/**
 * 
 * Some basic data about the running process.
 * This is shared among the other processes using MPI_all_to_all
 *
 */
class mpi_process_data {
private:
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version) {
		ar & m_irank;
		ar & m_inum_processes;
		ar & m_strname;
	}

	int 		m_irank;
	int 		m_inum_processes;
	std::string 	m_strname;
	
public:
	mpi_process_data() : m_irank(-1), m_inum_processes(-1), m_strname(""){}
	mpi_process_data(int r, int s, std::string n) : 
		m_irank(r), m_inum_processes(s), m_strname(n) {}
	
	int    		get_rank()  	    { return m_irank; }
	int    		get_num_processes() { return m_inum_processes; }
	std::string 	get_name() 	    { return m_strname; }

	/// Make it possible to assign variables
	mpi_process_data& operator=(const mpi_process_data& m) {
		m_irank 		= m.m_irank;
		m_inum_processes 	= m.m_inum_processes;
		m_strname 		= m.m_strname;
	
	    return *this;
	}
	
	// std::stream& operator<<(std::stream& s, const mpi_process_data &m)
	//void operator<<(std::ostream& s, const mpi_process_data &m) {
	//	s << m.get_rank() << "," << m.get_num_processes() << "," << m.get_name();
	//}
};

///optimize serialization and transmission since the class has fixed size of its fields
//namespace boost { namespace mpi {
//  template <>
//  struct is_mpi_datatype<mpi_process_data> : mpl::true_ { };
//} }


/// TssMonitor class
/**
 * Process intended to run on a separate machine used for monitoring the 
 * running proceses and their threads.
 *
 */

class TssMonitor {
public:	

 	TssMonitor();
};


/// TssSharedMemory class
/**
 * Base class for shared memory object. Should use mutexes to ensure that 
 * access is thread safe.
 *
 */

class TssSharedMemory {
public:	

 	TssSharedMemory();
};



/// TssJob class
/**
 * Base class for Jobs that are executed by a Thread
 *
 *
 */

class TssJob {
public:
	virtual ~TssJob();
	virtual void run();
};

/// TssJobCreator class
/**
 * Base class for creating Jobs. This class is inherited in order to create 
 * creators for specialized Job classes (using the FactoryMethod pattern).
 *
 */

class TssJobCreator {
public:
	virtual ~TssJobCreator();
	virtual TssJob* createJob(tss_commands::job job);
};

/// TssThreadWorker class
/**
 * Represents one thread running in the process
 *
 *
 */

class TssThreadWorker {
private:	
	boost::thread 	m_Thread;
	TssJob*		m_pJob;

public:	


	/// TssThreadWorker
	/**
	 *  Contructor
	 *
	 * the thread is not-a-thread until we call start()
	 */ 	
	TssThreadWorker();
	

	/// ~TssThreadWorker
	/**
	 *  Destructor
	 *
	 *
	 */ 	
	virtual ~TssThreadWorker();

	
	/// run
	/**
	 * Loops until stop_threads is set and calls the work method.
	 *
	 */ 	
	
 	void run();
	
	/// start
	/**
	 * Creates the thread. This object isn't a thread until start is called.
	 *
	 */ 	
	void start();
	
	/// join
	/**
	 * Stops the thread by invoking join on the thread.
	 *
	 */ 	
	void join();

	/// work
	/**
	 * Calls the run method of the TssJob object. The Job object contains the 
	 * actaul work to be performed.
	 *
	 */ 	

	virtual void work();

	/// stopThreads
	/**
	 * Stop all threads in this process by calling the join method.
	 *
	 *
	 * \return void
	 *
	 */ 	
 	void stopThreads();
 	
	/// setJob
	/**
	 * Set the job to be executed by the thread.
	 *
	 * @param pJob The Job to be executed by this Thread
	 *
	 * \return void
	 *
	 */ 	
 	void setJob(TssJob* pJob){ this->m_pJob = pJob; }
 	
			
protected:
	static boost::condition_variable m_cond;
	static boost::mutex              m_mut;
	static bool                      m_stop_threads;
	
};




/// TssProcess class
/**
 * The root container for the different threads running within this process
 *
 *
 */

class TssProcess {
public:	

	/// TssProcess
	/**
	 * Contructor
	 *
	 *
	 */ 	
	TssProcess();
 
	
	/// ~TssProcess
	/**
	 * Destructor
	 *
	 *
	 */ 	
	~TssProcess();

	
	/// addThread
	/**
	 * Get the contents once it has been parsed.
	 *
	 * @param thread The thread to add to the process
	 *
	 * \return void
	 *
	 */ 	
 	void addThread(TssThreadWorker* thread);
 
	
	/// startThreads
	/**
	 * Start the threads added with addThread.
	 *
	 *
	 * \return void
	 *
	 */ 	
 	void startThreads();

	/// createJobsAndThreads
	/**
	 * Create Jobs and Threads using a set of commands.
	 *
	 * @param queue_now A queue of jobs to run
	 *
	 * \return void
	 *
	 */ 	
	 void createJobsAndThreads(tss_commands::jobs_node* jobs);

	/// joinThreads
	/**
	 * Stop all threads by calling their respective join methods.
	 *
	 * \return void
	 *
	 */ 	
 	void joinThreads();
 	
 	mpi_process_data* 		getMPIProcessData() {return m_pMPIProcessData; }

 	std::vector<mpi_process_data>* 	getAllMPIProcessData() {return m_pvAllMPIProcessData; }
	
 	void setJobCreator(TssJobCreator* pjc) { m_pJobCreator = pjc; }
 	
 	
private:
	void 				initMpiMembers();
	
	TssJobCreator*			m_pJobCreator;

	std::vector<TssThreadWorker*>   m_ThreadWorkers;
	mpi_process_data*		m_pMPIProcessData;
	std::vector<mpi_process_data>*  m_pvAllMPIProcessData;

};

} // namespace tss_process

#endif //TSS_PROCESS
