/****************************************************************************/
/*! \mainpage testTssProcess Test suite for TssProcess classes
 * \section intro_sec Introduction
 *
 * itch.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Test suite
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101215 Jonas C. Initial version
 *
 ****************************************************************************/

 
#include <fstream>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


// Note that the following header is needed!!!
// Without this, you must compile like:
// g++ -lboost_unit_test_framework foo.cpp
#include <boost/test/included/unit_test_framework.hpp> 

#include "../src/TssProcess.hpp"

using namespace std;
using namespace boost;
using namespace boost::unit_test;

using namespace tss_process;

class TssThreadWorker1 : public TssThreadWorker {
	
	// This is where something gets done
 	void work() {
		// Three seconds of pure, hard work!  
		boost::posix_time::seconds workTime(3);		
		boost::this_thread::sleep(workTime);
			
		std::cout << "Working really hard:" << boost::this_thread::get_id() << endl;
 	
 	}
	
};

class TssThreadWorker2 : public TssThreadWorker {
	
	// This is where something gets done
 	void work() {
 		static int i_counter = 0;
		boost::posix_time::seconds workTime(3);
		
		// run 5 iterations
 		if(i_counter++ < 5) {
 
			// Three seconds of pure, hard work!  
			boost::this_thread::sleep(workTime);	
			std::cout << "Working really hard:" << boost::this_thread::get_id() << endl;
 		}
 		else {
 			std::cout << "End of thread, before lock:" << boost::this_thread::get_id() << endl;
			this->stopThreads();
			std::cout << "End of thread, after lock:" << boost::this_thread::get_id() << endl;
 		}
 	}
	
};


class TestTssProcess {
	TssProcess _tssProcess;

public:
	
	
	// test mpi_process_data::serialize
	void test_case0() {
		// create and open a character archive for output
		std::ofstream ofs("mpi_process_data");
		
		// create class instance
		mpi_process_data pd(1, 2, "three");
		
		// save data to archive
		{
			boost::archive::text_oarchive oa(ofs);
			// write class instance to archive
			oa << pd;
			// archive and stream closed when destructors are called
		}
		
		// ... some time later restore the class instance to its orginal state
		mpi_process_data pd2;
		{
			// create and open an archive for input
			std::ifstream ifs("mpi_process_data");
			boost::archive::text_iarchive ia(ifs);
			// read class state from archive
			ia >> pd2;
			// archive and stream closed when destructors are called
		}
		
		BOOST_REQUIRE(pd.get_rank()          == pd2.get_rank());
		BOOST_REQUIRE(pd.get_num_processes() == pd2.get_num_processes());
		BOOST_REQUIRE(pd.get_name()          == pd2.get_name());	
	}
	
	
	// test mpi_process_data::operator=	
	void test_case01() {
		mpi_process_data pd(1, 2, "three");		
		{
			mpi_process_data pd2(4, 5, "six");
			pd = pd2;
			
			//Make sure that the pointer really differs
			BOOST_REQUIRE(&pd != &pd2);
		}
		
		BOOST_REQUIRE(pd.get_rank()          == 4);
		BOOST_REQUIRE(pd.get_num_processes() == 5);
		BOOST_REQUIRE(pd.get_name()          == "six");
		
	}
		
	void test_case1() {
		TssThreadWorker1* wk1 = new TssThreadWorker1();
		TssThreadWorker1* wk2 = new TssThreadWorker1();
		TssThreadWorker2* wk3 = new TssThreadWorker2();
		
		_tssProcess.addThread( wk1 );
		_tssProcess.addThread( wk2 );
		_tssProcess.addThread( wk3 );
		std::cout << "Threads added" << endl;
		
		_tssProcess.startThreads();
		std::cout << "Threads started" << endl;
		
		_tssProcess.joinThreads();
		std::cout << "Threads stopped" << endl;
		
		//BOOST_REQUIRE(_tssProcess.isOdd(3) == true);
		
		delete wk1;
		delete wk2;
		delete wk3;
	}
	
	
	void test_case2() {
		mpi_process_data* pd		   = _tssProcess.getMPIProcessData();
		std::vector<mpi_process_data>* opd = _tssProcess.getAllMPIProcessData();

		
		BOOST_REQUIRE((*opd)[0].get_rank() == 0);
		BOOST_REQUIRE((*opd)[1].get_rank() == 1);
		BOOST_REQUIRE((*opd)[1].get_num_processes() == pd->get_num_processes());	
	}

};


class TestSuiteTssProcess: public test_suite {
public:
	
    TestSuiteTssProcess(): test_suite("test_suite_tss_process")
    {
        shared_ptr<TestTssProcess> instance(new TestTssProcess());
        
        test_case *testCase0  = BOOST_CLASS_TEST_CASE(&TestTssProcess::test_case0, instance);
        test_case *testCase01 = BOOST_CLASS_TEST_CASE(&TestTssProcess::test_case01, instance);
        test_case *testCase1  = BOOST_CLASS_TEST_CASE(&TestTssProcess::test_case1, instance);
        test_case *testCase2  = BOOST_CLASS_TEST_CASE(&TestTssProcess::test_case2, instance);
        add(testCase0);
        add(testCase01);
        add(testCase1);
        add(testCase2);
    }
    
};


test_suite* init_unit_test_suite(int argc, char** argv)
{
    test_suite* suite(BOOST_TEST_SUITE("TssProcess Master Suite"));
    suite->add(new TestSuiteTssProcess());
    return suite;
}
