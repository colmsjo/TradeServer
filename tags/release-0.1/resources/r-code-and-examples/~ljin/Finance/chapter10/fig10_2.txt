#10.2

GetW<-function(mu,sigma,ngrid)
{
library("quadprog")
N<-length(mu)
one<-rep(1,N)
x<-c(one,mu)
Aeq<-matrix(x,ncol=2)

muP<-seq(min(mu),max(mu),length.out=ngrid)

wP<-matrix(rep(0,N*ngrid),nrow=ngrid)

for(i in 1:ngrid)
{
   f<-rep(0,N)
   b0<-c(1,muP[i])
   sol<-solve.QP(sigma,f,Aeq,b0,meq=2) #without constraint
   wP[i,]<-sol$solution
}
return(wP)
}

GetPW<-function(mu,sigma,xpoint)
{
library("quadprog")
N<-length(mu)
one<-rep(1,N)
x<-c(one,mu)
Aeq<-matrix(x,ncol=2)

wP<-rep(0,N)

f<-rep(0,N)
b0<-c(1,xpoint)
sol<-solve.QP(sigma,f,Aeq,b0,meq=2) #without constraint
wP<-sol$solution
return(wP)
}


GetW2<-function(mu,sigma,ngrid)
{
library("quadprog")
N<-length(mu)
one<-rep(1,N)
x<-c(one,mu)
Aeq<-matrix(x,ncol=2)

muP<-seq(min(mu),max(mu),length.out=ngrid)

wP2<-matrix(rep(0,N*ngrid),nrow=ngrid)

IMatrix<-matrix(0,N,N)
diag(IMatrix)<-1
A<-cbind(Aeq,IMatrix)

for(i in 1:ngrid)
{
   f<-rep(0,N)
   b0<-c(1,muP[i])
   b<- c(b0,rep(-0.0000001,N))
   sol2<-solve.QP(sigma,f,A,b,meq=2)   #with constraint
   wP2[i,]<-sol2$solution
}
return(wP2)
}

minIndex<-function(v)
{
   minv<-10000000000
   minindex<-0
   for(i in 1:length(v))
   {
        if(minv>v[i])
          {
             minv<-v[i]
             minindex<-i
          }
   }
   return (minindex)
}
 
maxIndex<-function(v)
{
   maxv<--10000000000
   maxindex<-0
   for(i in 1:length(v))
   {
        if(maxv<v[i])
          {
             maxv<-v[i]
             maxindex<-i
          }
   }
   return (maxindex)
}
 
countries<-read.table("d:\\S689RC\\dat\\countries.dat",skip=15)
Names<-c('Hong Kong','Singapore' ,'Brazil', 'Argentina', 'UK', 'Germany', 'Canada', 'France', 'Japan', 'US')
 
return_mat<-as.matrix(countries)
N<-length(return_mat[,1])-1
NStock<-length(Names)
return<-matrix(rep(0,N*NStock),ncol=NStock)
u.s<-rep(0,NStock)
var.s<-matrix(rep(0,NStock*NStock),nrow=NStock)

for(i in 1:NStock)
{
return[,i]<-diff(log(return_mat[,i+3]))
u.s[i]<-mean(return[,i])
}

for(i in 1:NStock)
{
 for(j in 1:NStock)
   {
     var.s[i,j]<-var(return[,i],return[,j])
   }
}

pdf("d:\\S689RC\\fig10_2.pdf")
par(mfrow=c(3,2))
for(i in 1:NStock)
{
plot(return[,i],xlab="",type='l',main=Names[i])
}
dev.off()

pdf("d:\\S689RC\\fig10_3.pdf")
par(mfrow=c(3,2))
for(i in 1:NStock)
{
acf(return[,i],type="partial", xlab="", ylab="cor", main=Names[i])
}
dev.off()

pdf("d:\\S689RC\\fig10_4.pdf") 
par(mfrow=c(3,2))
for(i in 1:NStock)
{
qqnorm(return[,i],main=Names[i])
qqline(return[,i])
}
dev.off()

INDEX<-1:N
R<-6
nGrid<-50

i<-1
u.b<-rep(0,NStock)
var.b<-matrix(0,NStock,NStock)

w.s<-GetW(u.s,var.s,nGrid)

u.s.grid<-seq(min(u.s),max(u.s),length.out=nGrid)
sigma.ws<-rep(0,nGrid)
sigma.wb<-rep(0,nGrid)
for(i in 1:nGrid)
{
sigma.ws[i]<-sqrt(w.s[i,]%*%var.s%*%w.s[i,])
}
Index.min.s<-minIndex(sigma.ws)

pdf("d:\\S689RC\\fig10_5.pdf")
par(mfrow=c(3,2))
for(i in 1:R)
{
Index.b<-sample(INDEX,size=N, replace = TRUE)
Return.b<-return[Index.b,]

for(j in 1:NStock)
{
  u.b[j]<-mean(Return.b[,j])
  for(k in 1:NStock)
  {
    var.b[j,k]<-var(Return.b[,j],Return.b[,k])
  }
}

w.b<-GetW(u.b,var.b,nGrid)
for(i in 1:nGrid)
{
sigma.wb[i]<-sqrt(w.b[i,]%*%var.s%*%w.b[i,])
}
#Plot Optimal
sigma.ws1<-sigma.ws[Index.min.s:nGrid]
u.s.grid1<-u.s.grid[Index.min.s:nGrid]
plot(sigma.ws1,u.s.grid1,type='l',col=10,xlab="risk",ylab="reward",xlim=c(min(sigma.ws1),max(sigma.ws1)),ylim=c(min(u.s.grid1),max(u.s.grid1)))
#Plot achieved
Index.min.b<-minIndex(sigma.wb)
u.b.grid<-w.b[Index.min.b:nGrid,]%*%u.s
lines(sigma.wb[Index.min.b:nGrid],u.b.grid,lty=2)

}
dev.off()



#What if we knew the covariance matrix
u00<-0.012

pdf("d:\\S689RC\\fig10_6.pdf")
par(mfrow=c(3,1))

#Plot Optimal
sigma.ws1<-sigma.ws[Index.min.s:nGrid]
u.s.grid1<-u.s.grid[Index.min.s:nGrid]
plot(sigma.ws1,u.s.grid1,type='l',col=10,xlab="risk",ylab="reward",xlim=c(min(sigma.ws1),max(sigma.ws1)),ylim=c(min(u.s.grid1),max(u.s.grid1)))
w00<-GetPW(u.s,var.s,u00)
sigma.s.00<-sqrt(w00%*%var.s%*%w00)
points(sigma.s.00,u00,cex=1.2,col=1)

R0<-400
uratio<-rep(0,R0)
sigmaratio<-rep(0,R0)
sigma.fix.u<-rep(0,R0)
sigmaratio.fix.u<-rep(0,R0)

for(i in 1:R0)
{
Index.b<-sample(INDEX,size=N, replace = TRUE)
Return.b<-return[Index.b,]

for(j in 1:NStock)
{
  u.b[j]<-mean(Return.b[,j])
  for(k in 1:NStock)
  {
    var.b[j,k]<-var(Return.b[,j],Return.b[,k])
  }
}

w00.b<-GetPW(u.b,var.s,u00)
w11.b<-GetPW(u.s,var.b,u00)

u00.b.grid<-w00.b%*%u.s
sigma.b.00<-sqrt(w00.b%*%var.s%*%w00.b)
sigma.b.11<-sqrt(w11.b%*%var.s%*%w11.b)

uratio[i]<-u00.b.grid/u00
sigmaratio[i]<-sigma.b.00/sigma.s.00
sigma.fix.u[i]<-sigma.b.11
sigmaratio.fix.u[i]<-sigma.b.11/sigma.s.00

points(sigma.b.00,u00.b.grid,cex=0.1)
}
hist(sigmaratio,xlab="sigma/sigma optimal")
hist(uratio,xlab="up/0.012")
dev.off()

pdf("d:\\S689RC\\fig10_7.pdf")
par(mfrow=c(2,1))
#Plot Optimal
sigma.ws1<-sigma.ws[Index.min.s:nGrid]
u.s.grid1<-u.s.grid[Index.min.s:nGrid]
plot(sigma.ws1,u.s.grid1,type='l',col=10,xlab="risk",ylab="reward",xlim=c(min(sigma.ws1),max(sigma.ws1)),ylim=c(min(u.s.grid1),max(u.s.grid1)))
w00<-GetPW(u.s,var.s,u00)
sigma.s.00<-sqrt(w00%*%var.s%*%w00)
points(sigma.s.00,u00,cex=1.2,col=1)
points(sigma.fix.u,rep(u00,R0),cex=0.1)
hist(sigmaratio.fix.u,xlab="sigma_p/sigma_optimal")
dev.off()


