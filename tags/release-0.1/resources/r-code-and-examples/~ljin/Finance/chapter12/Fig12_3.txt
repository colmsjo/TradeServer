#Fig 12.3 
#Page 369
#simulations of 600 observations from an ARCH (1) process and an AR(1)/ARCH(1) process.

library("fSeries")

a0<-1
a1<-0.95

u<-0.1
phi<-0.8

N<-600
seed<-2^7-1
set.seed(seed)

whitenoise<-rnorm(N)

##      GARCH -
# Simulate ARCH(1):
at = garchSim(model = list(omega = a0, alpha = a1), N)

#conditional std dev
st<-rep(1,N)
for(i in 2:N)
{
st[i]<-a0+a1*(at[i-1]*at[i-1])
}

#simulate AR(1)/ARCH(1)
arat<-rep(0,N)
arat[1]<-at[1]
for(i in 2:N)
{
arat[i]<- u+ phi*(arat[i-1]-u)+at[i]
}

pdf("D:\\S689RC\\Fig12_3.pdf")
par(mfrow=c(2,2))
plot(whitenoise,type='l',xlab=" ",ylab=" ", main="White Noise")
plot(st,type='l',xlab=" ",ylab=" ", main="conditional std dev")
plot(at,xlab=" ",ylab=" ", main="ARCH(1)")
plot(arat,type='l',xlab=" ",ylab=" ", main="AR(1)/ARCH(1)")
qqnorm(at)
qqline(at)
dev.off()
