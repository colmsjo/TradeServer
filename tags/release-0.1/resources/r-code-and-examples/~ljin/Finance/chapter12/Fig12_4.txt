#Fig 12.4 
#Page 371
#simulations of 600 observations from an GARCH (1,1) process and an AR(1)/GARCH(1,1) process.

library("fSeries")

a0<-1
a1<-0.08
b1<-0.9

phi<-0.8

N<-600

seed<-2^11-1
set.seed(seed)

whitenoise<-rnorm(N)

##      GARCH -
# Simulate GARCH(1,1):
gat = garchSim(model = list(omega = a0,alpha = a1,beta=b1), N)

vart<-rep(1,N)
for(i in 2:N)
{
vart[i]<-a0+a1*(gat[i-1]*gat[i-1])+b1*(vart[i-1])
}
st<-sqrt(vart)

#simulate AR(1)/gARCH(1,1)
garat<-rep(0,N)
garat[1]<-gat[1]
for(i in 2:N)
{
garat[i]<- phi*garat[i-1]+gat[i]
}

pdf("D:\\S689RC\\Fig12_4.pdf")
par(mfrow=c(3,2))
plot(whitenoise,type='l',xlab=" ",ylab=" ", main="White Noise")
plot(st,type='l',xlab=" ",ylab=" ", main="conditional std dev")
plot(at,xlab=" ",ylab=" ", main="GARCH(1,1)")
plot(arat,type='l',xlab=" ",ylab=" ", main="AR(1)/GARCH(1,1)")
qqnorm(at)
qqline(at)
dev.off()
