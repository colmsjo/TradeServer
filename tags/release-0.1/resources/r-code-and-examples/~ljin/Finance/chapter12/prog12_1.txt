#Page 373
#Model fit, AR(1)/GARCH(0,1) process.

library("stats")
library("fSeries")

gat<-scan("D:\\S689RC\\dat\\garch02.dat", skip = 2)
# fit AR(1)/GARCH(0,1):
fit = garchFit(formula.mean = ~arma(1, 0),formula.var = ~garch(0, 1),series = ts(gat))

pdf("D:\\S689RC\\prog12_1.pdf")
sink("D:\\S689RC\\out12_1.txt")
par(mfrow=c(3,2))
summary(fit)
dev.off()

#Page 375
#Model fit, AR(1)/GARCH(1,1) process.
tab<-scan("D:\\S689RC\\dat\\ex105.txt",what = list(DATE="",RETURNNSP="",FSPCOM="",FSDXP="",R3="",PW="",GPW=""), skip = 1)

RETURNNSP<-as.numeric(tab$RETURNNSP)
GPW<-as.numeric(tab$GPW)
R3<-as.numeric(tab$R3)

DR3<-diff(R3)


# fit AR(1)/GARCH(1,1):
fit = garchFit(formula.mean = ~DR3+GPW+arma(1, 0),formula.var = ~garch(1, 1),series = ts(RETURNNSP))