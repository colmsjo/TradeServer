#Fig 2.13
#Sample correlation coefficients for eight random samples

N<-101
t<-seq(-2,2,length.out=N)
x<-t
xnorm<-rnorm(t,0,1)

pdf("D:\\S689RC\\Fig2_13.pdf")   
par(mfrow=c(2,2))

a<-0.02
sigma<-0.85
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 8), bg='gray90')


a<-0.20
sigma<-0.7
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 7), bg='gray90')

a<-0.35
sigma<-0.5
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 7), bg='gray90')

a<-0.45
sigma<-0.2
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 7), bg='gray90')

a1<-0.30
sigma<-0.15
y<-a*x*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 8), bg='gray90')

a1<-0.35
sigma<-0.2
y<-a*x*x*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 7), bg='gray90')

a<--0.33
sigma<-0.3
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 7), bg='gray90')

a<--0.45
sigma<-0.00000
y<-a*x+xnorm*sigma
r<-cor(x,y)
plot(-4:4, -4:4, xlab= 'x', ylab= 'y', main= "", sub = "",type = "n")
points(x,y,pch=19,cex=0.2)
legend(-3.9, 3.8,substr(paste("r=",r), 1, 8), bg='gray90')

dev.off()
