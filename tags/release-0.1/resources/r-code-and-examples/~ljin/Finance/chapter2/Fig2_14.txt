#Fig 2.14
#Prior and posterior densities in Examples 2.17. 

N<-100

t<-seq(0,1,length.out=N)
pri<-6*t*(1-t)
pos<-30*t*t*t*t*(1-t)

pdf("D:\\S689RC\\Fig2_14.pdf")   
plot(t,pos,type='l',ylim=c(0,2.5),xlab="x",ylab="density")
lines(t,pri,lty=2)
legend(0.1, 2.46,c("posterior","prior"),lty = c(1, 2), merge = TRUE, bg='gray90')
dev.off()
