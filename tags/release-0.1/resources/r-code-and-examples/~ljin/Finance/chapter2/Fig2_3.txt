#Example of Normal Probability densities

t<-seq(-5,5,length.out=100)

max_x<-max(t)

d1<-dnorm(t,0,1)
d2<-dnorm(t,0,2)
d3<-dnorm(t,1,1)

max_y<-max(c(d1,d2,d3))

pdf("D:\\S689RC\\R\\Fig2_3.pdf")
plot(t,d1,type='l',xlab="x",ylab="density",main="Normal Probability Densities")
lines(t,d2,lty=2)
lines(t,d3,lty=3)

leg.txt <- c("N(0,1)", "N(0,2)","N(1,1)")
legend(max_x-2, max_y-0.005,c("N(0,1)", "N(0,2)","N(1,1)"), col = c(3,4,6), text.col= "green4",lty = c(1, 2, 3), merge = TRUE, bg='gray90')
dev.off()