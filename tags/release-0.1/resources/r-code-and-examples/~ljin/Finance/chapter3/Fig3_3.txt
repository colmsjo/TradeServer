#Fig 3.3
#Two indepedent simulations of a geometric random walk(GRW) with u=0.1 and sigma=0.2. 

u<-0.1
s<-0.2
P0<-100
lP0<-log(100)

N<-20
t<-1:N
r1<-rnorm(t,u,s)
r2<-rnorm(t,u,s)

logprice1<-rep(lP0,(N+1))
logprice2<-rep(lP0,(N+1))

for(i in 1:N)
{
logprice1[i]<-logprice1[i]+r1[i]
logprice1[i+1]<-logprice1[i]
logprice2[i]<-logprice2[i]+r2[i]
logprice2[i+1]<-logprice2[i]
}
logprice1<-logprice1[1:N]
logprice2<-logprice2[1:N]

pdf("D:\\S689RC\\Fig3_3.pdf")
par(mfrow=c(3,1)) 
plot(t,rep(u,N),type='l',xlab="year",ylab="log return",xlim=c(-10,20),ylim=c(-0.5,1),main="log returns are N(.1,(.2)^2)")
lines(t,r1,lty=2)
lines(t,r2,lty=3)
legend(-9.5,1,c("Median","GRM1","GRM2"),lty = c(1,2,3), merge = TRUE, bg='gray90')

mlogprice<-lP0+t*u
ymax<-max(logprice1,logprice2,mlogprice)
ymin<-min(logprice1,logprice2,mlogprice)

plot(t,mlogprice,type='l',xlab="year",ylab="log price",xlim=c(-10,20),ylim=c(ymin,ymax))
lines(t,logprice1,lty=2)
lines(t,logprice2,lty=3)
legend(-9.5,ymax,c("Median","GRM1","GRM2"),lty = c(1,2,3), merge = TRUE, bg='gray90')

price1<-exp(logprice1)
price2<-exp(logprice2)

medianprice<-exp(mlogprice)
meanprice<-P0*exp((u+s*s/2)*t)

ymax<-max(price1,price2,medianprice,meanprice)
ymin<-min(price1,price2,medianprice,meanprice)

plot(t,medianprice,type='l',xlab="year",ylab="price",xlim=c(-10,20),ylim=c(ymin,ymax))
lines(t,price1,lty=2)
lines(t,price2,lty=3)
lines(t,meanprice,lty=4)
legend(-9.5,ymax,c("Median","GRM1","GRM2","mean"),lty = c(1,2,3,4), merge = TRUE, bg='gray90')

dev.off()
