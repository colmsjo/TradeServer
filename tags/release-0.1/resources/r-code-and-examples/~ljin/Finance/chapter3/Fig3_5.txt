#Fig 3.5
#Five indepedent simulations of a geometric random walk(GRW) and GE daily log returns. 

Price<-scan("D:\\S689RC\\dat\\ge.dat",skip=2)
N<-length(Price)

Return<-Price[2:N]/Price[1:(N-1)]-1
LogReturn<-log(1+Return)

ulr<-mean(LogReturn)
slr<-sqrt(var(LogReturn))
N<-length(LogReturn)  
rlr1<-rnorm(N,ulr,slr)
rlr2<-rnorm(N,ulr,slr)
rlr3<-rnorm(N,ulr,slr)
rlr4<-rnorm(N,ulr,slr)
rlr5<-rnorm(N,ulr,slr)

N<-length(Price) 
Price1<-Price
Price2<-Price
Price3<-Price
Price4<-Price
Price5<-Price

for(i in 2:N)
{
Price1[i]<-Price1[i-1]*exp(rlr1[i-1])
Price2[i]<-Price2[i-1]*exp(rlr2[i-1])
Price3[i]<-Price3[i-1]*exp(rlr3[i-1])
Price4[i]<-Price4[i-1]*exp(rlr4[i-1])
Price5[i]<-Price5[i-1]*exp(rlr5[i-1])
}

pdf("D:\\S689RC\\Fig3_5.pdf")
par(mfrow=c(2,2))
t<-1:N
plot(t,Price1,type='l',xlab="t",ylab="Price",main="Geometric random walk")
plot(t,Price2,type='l',xlab="t",ylab="Price",main="Geometric random walk")
plot(t,Price3,type='l',xlab="t",ylab="Price",main="Geometric random walk")
plot(t,Price4,type='l',xlab="t",ylab="Price",main="Geometric random walk")
plot(t,Price5,type='l',xlab="t",ylab="Price",main="Geometric random walk")
plot(t,Price,type='l',xlab="t",ylab="Price",main="GE,daily 2004")
dev.off()
