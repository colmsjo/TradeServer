#Fig 4.1
#Five indepedent simulations of a geometric random walk(GRW) and GE daily log returns. 

r1<-0.95
r2<-0.75
r3<-0.2
r4<--0.9
N<-16

rho1<-rep(1,N)
rho2<-rep(1,N)
rho3<-rep(1,N)
rho4<-rep(1,N)

for(i in 1:(N-1))
{
rho1[i+1]<-rho1[i]*r1
rho2[i+1]<-rho2[i]*r2
rho3[i+1]<-rho3[i]*r3
rho4[i+1]<-rho4[i]*r4
}

pdf("D:\\S689RC\\Fig4_1.pdf")
par(mfrow=c(2,2))
t<-0:(N-1)
Zero<-rep(0,N)
plot(t,rho1,type='l',xlab="t",ylab="r",ylim=c(-1,1))
points(t,rho1,cex=0.5)
lines(t,Zero)
plot(t,rho2,type='l',xlab="t",ylab="r",ylim=c(-1,1))
points(t,rho2,cex=0.5)
lines(t,Zero)
plot(t,rho3,type='l',xlab="t",ylab="r",ylim=c(-1,1))
points(t,rho3,cex=0.5)
lines(t,Zero)
plot(t,rho4,type='l',xlab="t",ylab="r",ylim=c(-1,1))
points(t,rho4,cex=0.5)
lines(t,Zero)
dev.off()
