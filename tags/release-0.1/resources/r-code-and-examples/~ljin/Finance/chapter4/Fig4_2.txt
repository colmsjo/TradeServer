#Fig 4.2
#Simulations of 200 Observations from AR(1) processes with various parameters. The white noise process is the same for all four AR(1) Processes. 
# Yt = a * Y(t-1) + et

N<-200
a<-c(0.9,-0.6,1,1.02)
e<-rnorm((N+1),0,1)
u<-0
p1<-rep(u,(N+1))
p2<-rep(u,(N+1))
p3<-rep(u,(N+1))
p4<-rep(u,(N+1))

for(i in 2:(N+1))
{
p1[i]<-p1[i-1]*a[1] + e[i]
p2[i]<-p2[i-1]*a[2] + e[i]
p3[i]<-p3[i-1]*a[3] + e[i]
p4[i]<-p4[i-1]*a[4] + e[i]
}

pdf("D:\\S689RC\\Fig4_2.pdf")
par(mfrow=c(2,2))
t<-0:N
t1<-paste("AR(1): a = ",a[1])
t2<-paste("AR(1): a = ",a[2])
t3<-paste("AR(1): a = ",a[3])
t4<-paste("AR(1): a = ",a[4])

plot(t,p1,type='l',xlab="t",ylab="y",main=t1)

plot(t,p2,type='l',xlab="t",ylab="y",main=t2)

plot(t,p3,type='l',xlab="t",ylab="y",main=t3)

plot(t,p4,type='l',xlab="t",ylab="y",main=t4)

dev.off()
