#Fig 4.3
#Simulations of 30 Observations from AR(1) processes with various parameters. The white noise process is the same for all four AR(1) Processes. 
# Yt = a * Y(t-1) + et

N<-30
a<-c(1,1.02)
e<-rnorm((N+1),0,1)
u<-0
p1<-rep(u,(N+1))
p2<-rep(u,(N+1))

for(i in 2:(N+1))
{
p1[i]<-p1[i-1]*a[1] + e[i]
p2[i]<-p2[i-1]*a[2] + e[i]
}

pdf("D:\\S689RC\\Fig4_3.pdf")
par(mfrow=c(1,2))
t<-0:N
t1<-paste("AR(1): a = ",a[1])
t2<-paste("AR(1): a = ",a[2])

ymax<-max(c(p1,p2))
ymin<-min(c(p1,p2))
plot(t,p1,type='l',xlab="t",ylab="y",ylim=c(ymin,ymax),main=t1)

plot(t,p2,type='l',xlab="t",ylab="y",ylim=c(ymin,ymax),main=t2)

dev.off()
