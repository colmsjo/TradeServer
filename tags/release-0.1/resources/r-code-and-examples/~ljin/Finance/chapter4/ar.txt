
Title:
 ARIMA Modelling 

Call:
 armaFit(formula = LogReturn ~ arima(1, 0, 0))

Model:
 ARIMA(1,0,0) with method: CSS-ML

Coefficient(s):
       ar1   intercept  
 2.289e-01  -4.028e-05  

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0484833 -0.0111875  0.0002222  0.0117606  0.0420648 

Moments: 
Skewness Kurtosis 
-0.06514 -0.17721 

Coefficient(s):
            Estimate  Std. Error  t value Pr(>|t|)    
ar1        2.289e-01   6.178e-02    3.706 0.000211 ***
intercept -4.028e-05   1.396e-03   -0.029 0.976984    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

sigma^2 estimated as: 0.0002921
log likelihood:       667.86
AIC Criterion:        -1329.72

Description:
 Thu Dec 22 20:58:04 2005 

null device 
          1 
