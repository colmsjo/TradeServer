
Title:
 ARIMA Modelling 

Call:
 armaFit(formula = LogReturn ~ arima(6, 0, 0))

Model:
 ARIMA(6,0,0) with method: CSS-ML

Coefficient(s):
       ar1         ar2         ar3         ar4         ar5         ar6  
 2.566e-01  -1.228e-01   6.861e-02  -7.265e-02   4.800e-02  -2.283e-01  
 intercept  
-1.827e-06  

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0456931 -0.0111542  0.0006351  0.0101761  0.0408757 

Moments: 
Skewness Kurtosis 
-0.08454 -0.02426 

Coefficient(s):
            Estimate  Std. Error  t value Pr(>|t|)    
ar1        2.566e-01   6.160e-02    4.165 3.12e-05 ***
ar2       -1.228e-01   6.365e-02   -1.929 0.053672 .  
ar3        6.861e-02   6.401e-02    1.072 0.283769    
ar4       -7.265e-02   6.410e-02   -1.133 0.257034    
ar5        4.800e-02   6.365e-02    0.754 0.450807    
ar6       -2.283e-01   6.185e-02   -3.691 0.000223 ***
intercept -1.827e-06   9.971e-04   -0.002 0.998538    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

sigma^2 estimated as: 0.0002729
log likelihood:       676.24
AIC Criterion:        -1336.48

Description:
 Thu Dec 22 20:58:04 2005 

null device 
          1 
