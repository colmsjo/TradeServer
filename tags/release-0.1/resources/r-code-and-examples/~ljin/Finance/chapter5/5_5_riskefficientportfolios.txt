
mu<-c(0.08,0.03,0.05)
sigma<-c(0.3,0.02,0.01,0.02,0.15,0.03,0.01,0.03,0.18)
N<-length(mu)
sigma<-matrix(sigma,nrow=3)
one<-rep(1,N)

invsigma<-solve(sigma)
a<-one%*%invsigma%*%mu
A<-diag(rep(a,N))
b<-mu%*%invsigma%*%mu
B<-diag(rep(b,N))
c<-one%*%invsigma%*%one
C<-diag(rep(c,N))
d<-b*c-a^2
INVD<-diag(rep(1/d,N))
bg<-INVD%*%(B%*%invsigma%*%one-A%*%invsigma%*%mu)
bh<-INVD%*%(C%*%invsigma%*%mu-A%*%invsigma%*%one)
bg<-as.vector(bg)
bh<-as.vector(bh)
gg<-bg%*%sigma%*%bg
hh<-bh%*%sigma%*%bh
gh<-bg%*%sigma%*%bh
mumin<- -gh/hh
sdmin<-sqrt(gg*(1-(gh*gh/(gg*hh))))

Len<-50
muP<-seq(min(mu),max(mu),length.out=Len)
sigmaP<-rep(0,Len)
for(i in 1:Len)
{
omegaP<-bg+muP[i]*bh
sigmaP[i]<-sqrt(omegaP%*%sigma%*%omegaP)
}

for(i in 1:Len)
{
if(muP[i+1]>mumin & muP[i]<mumin ) indicate<-(i+1)
}
# for figure 5_4
pdf("c:\\S689RC\\Fig5_4.pdf")
plot(sigmaP[indicate:Len],muP[indicate:Len],type='l',lty=1,xlab="standard deviation of return",ylab="expected return",ylim=c(min(mu),max(mu)))
lines(sigmaP[1:indicate],muP[1:indicate],lty=2)
dev.off()
# for figure 5_5
pdf("c:\\S689RC\\Fig5_5.pdf")
w1<-bg[1]+muP[indicate:Len]*bh[1]
w2<-bg[2]+muP[indicate:Len]*bh[2]
w3<-bg[3]+muP[indicate:Len]*bh[3]
miny<-min(c(w1,w2,w3))
maxy<-max(c(w1,w2,w3))
plot(muP[indicate:Len],w1,type='l',lty=1,xlab="mu_p",ylab="weight",xlim=c(mumin,max(mu)),ylim=c(miny,maxy))
lines(muP[indicate:Len],w2,lty=2)
lines(muP[indicate:Len],w3,lty=3)
legend(mumin,maxy,c("w1", "w2","w3"), lty = c(1, 2, 3), merge = TRUE, bg='gray90')
dev.off()

#for figure 5_7
pdf("c:\\S689RC\\Fig5_7.pdf")
Len1<-length(w2)
for(i in 1:Len1)
{
if(w2[i]<0)
{
w1[i]<-(muP[indicate+i-1]-mu[3])/(mu[1]-mu[3])
w3[i]<-1-w1[i]
w2[i]<-0
}
}
plot(muP[indicate:Len],w1,type='l',lty=1,xlab="mu_p",ylab="weight",xlim=c(mumin,max(mu)),ylim=c(0,maxy))
lines(muP[indicate:Len],w2,lty=2)
lines(muP[indicate:Len],w3,lty=3)
legend(mumin,maxy,c("w1", "w2","w3"), lty = c(1, 2, 3), merge = TRUE, bg='gray90')
dev.off()

#Figure 5_8
pdf("c:\\S689RC\\Fig5_8.pdf")
muf<-0.02
wbar<-invsigma%*%(mu-muf*one)
wbar<-as.vector(wbar)
wt<-wbar/(one%*%wbar)
sigmaT<-sqrt(wt%*%sigma%*%wt)
muT<-mu%*%wt

wP2<-c(0,0.3,0.7)
sigmaP2<-sqrt(wP2%*%sigma%*%wP2)
muP2<-mu%*%wP2

plot(sigmaP[indicate:Len],muP[indicate:Len],type='l',lty=1,xlab="standard deviation of return",ylab="expected return",xlim=c(0,0.5),ylim=c(muf,max(mu)))
lines(sigmaP[1:indicate],muP[1:indicate],lty=2)
points(sigmaP2,muP2,cex=1,pch='P')
points(sigmaT,muT,cex=1,pch='T')
points(0,muf,cex=1,pch='F')
lamda<-seq(0,1,length.out=30)
y<-muf*lamda+muT*(1-lamda)
x<-(1-lamda)*sigmaT
lines(x,y,lty=3)
dev.off()