#Fig 8.11, page 285

Vol<-scan("D:\\S689RC\\dat\\ImpVolRegData.txt", what=list(K="",T="",ImpVol=""), skip = 5)
RK<-as.numeric(Vol$K)
K<-RK-mean(RK)
RT<-as.numeric(Vol$T)
T<-RT-mean(RT)
ImpVol<-as.numeric(Vol$ImpVol)

T2<-T*T
K2<-K*K
KT<-K*T
K3<-K*K*K
T3<-T*T*T

N<-20

k = seq(min(K), max(K), length.out=N)
t<-seq(min(T),max(T),length.out=N)
z1<-matrix(rep(0,N*N),nrow=N)
z2<-matrix(rep(0,N*N),nrow=N)

for(i in 1:N)
{
new_K <- data.frame(K=k,K2=k*k,K3=k*k*k,T=t[i],T2=t[i]*t[i],T3=t[i]*t[i]*t[i])
prefit_K<-predict(lm(ImpVol~K+K2+K3+T+T2+T3), new_K, se.fit = TRUE)
z1[,i]<-prefit_K$fit
z2[,i]<-prefit_K$se
}

pdf("D:\\S689RC\\Fig8_11.pdf")   
persp(k+mean(RK),t+mean(RT),z1,theta = -30, phi = 30, expand = 0.5, col = "lightblue",xlab="Exercise Price",ylab="Maturity",zlab='implied volatility')
dev.off()

pdf("D:\\S689RC\\Fig8_12.pdf")   
persp(k+mean(RK),t+mean(RT),z2,theta = -30, phi = 30, expand = 0.5, col = "lightblue",xlab="Exercise Price",ylab="Maturity",zlab='SE of implied volatility')
dev.off()



