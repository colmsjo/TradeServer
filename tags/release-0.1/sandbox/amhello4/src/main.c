/* Copyright (C) 2006 Free Software Foundation, Inc.
   This program is free software; the Free Software Foundation
   gives unlimited permission to copy, distribute and modify it.  */

#include <config.h>
#include <stdio.h>

#include "version.h"

int
main (void)
{
  puts ("Hello World!");
	fputs ("This is " PACKAGE " ", stdout);
  puts (version());
  return 0;
}
