http://markuskimius.wikidot.com/programming:tut:autotools:4

1. run autoscan
2. rename configure.scan to configure.ac
3. run autoconf
4. Move Makefile to Makefile.in
5. run configure
6. make clean all

7. run autoheader 
8. and configure to generate config.h


9. Create a file named �Makefile.am�
	example:
	bin_PROGRAMS=hello
	hello_SOURCES=hello.c
10. run automake

11. Deal with the errors and warnings

12. run aclocal

13. run autoconf