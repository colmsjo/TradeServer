#include <stdio.h>
#include <mpi.h>


/**

Compile with:
mpicc hello2.c -o hello2

*/

int main(int argc, char *argv[]) {
  FILE *pFile = fopen("hello.txt", "w");

  int numprocs, rank, namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(processor_name, &namelen);

  fprintf(pFile, "Process %d on %s out of %d\n", rank, processor_name, numprocs);

  MPI_Finalize();
}
