/* atol example */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;

const int MSG1[] = {0, 5, 6, 12};
const char szStr[] = "12345T12345M12345";

int main ()
{
  long int li;
  char buffer[256];

  strncpy(buffer, szStr, MSG1[1]); 
  li = atol (buffer);
  cout << li << endl;

  strncpy(buffer, szStr+MSG1[2], MSG1[3]); 
  li = atol (buffer);
  cout << li << endl;
  
  
return 0;
}
