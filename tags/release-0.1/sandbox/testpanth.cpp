/**
 * 101119 Jonas C.
 *
 * Written for testing purposes
 *
 * Code copied from tutorial at http://dinhngocson.blogspot.com/2010/11/pantheios-logging-library-basic.html
 *
*/



#include "pantheios/pantheios.hpp" 				//primary header file, always be included
#include "pantheios/frontends/stock.h"

//Specify process identity
const PAN_CHAR_T PANTHEIOS_FE_PROCESS_IDENTITY[] = "tpanth.exe";

// Headers for main()
#include <pantheios/pantheios.hpp> 
#include <pantheios/backends/bec.file.h> 

// Headers for implicit linking
#include <pantheios/implicit_link/core.h>
#include <pantheios/implicit_link/fe.simple.h>
#include <pantheios/implicit_link/be.file.h>
#include <pantheios/unixem/implicit_link.h>

#define PSTR(x)         PANTHEIOS_LITERAL_STRING(x)

int log_to_file() {
  pantheios::log_NOTICE("log-1"); 				// save until log file set
  pantheios_be_file_setFilePath(PSTR("mylogfile"), PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BEID_LOCAL); 		// sets log file; write "log-1" stmt
  pantheios::log_NOTICE("log-2"); 				// write "log-2" stmt
  pantheios_be_file_setFilePath(NULL); 				// close "mylogfile"

  pantheios::log_NOTICE("log-3"); 				// save until log file set
  pantheios_be_file_setFilePath(PSTR("mylogfile2"), PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BEID_LOCAL); 		// sets log file; write "log-3" stmt
  pantheios::log_NOTICE("log-4"); 				// write "log-4" stmt
  
  return 2;
} 								// closes "mylogfile2" during program closedown

int main(int argc, char** argv){

  //Use pantheios::log_xxx() or pantheios::log(xxx, ) with xxx is severity level

  log_to_file();


  try
  {
    // pantheios::log(pantheios::debug, "Entering main(", pantheios::args(argc, argv, pantheios::args::arg0FileOnly), ")");
    pantheios::log_DEBUG("debug");  
    pantheios::log_INFORMATIONAL("informational");
    pantheios::log_NOTICE("notice");
    pantheios::log_WARNING("warning");
    pantheios::log_ERROR("error");
    pantheios::log_CRITICAL("critical");
    pantheios::log_ALERT("alert");
    pantheios::log_EMERGENCY("emergency");

    return 1;
  }
    catch(std::bad_alloc&){
    pantheios::log_ALERT("out of memory");
  }
    catch(std::exception& x){
    pantheios::log_CRITICAL("Exception: ", x);
  }
  catch(...){
    pantheios::puts(pantheios::emergency, "Unknown error");
  }

  
  return 2;
}


/* /////////////////////////////////////////////////////////////////////////
 * File:        examples/cpp/example.cpp.file/example.cpp.file.cpp
 *
 * Purpose:     C++ example program for Pantheios. Demonstrates:
 *
 *                - use of pantheios_be_file_setFilePath()
 *                - use of pantheios::logputs() in bail-out conditions
 *
 * Created:     29th November 2006
 * Updated:     23rd March 2010
 *
 * www:         http://www.pantheios.org/
 *
 * License:     This source code is placed into the public domain 2006
 *              by Synesis Software Pty Ltd. There are no restrictions
 *              whatsoever to your use of the software.
 *
 *              This software is provided "as is", and any warranties,
 *              express or implied, of any kind and for any purpose, are
 *              disclaimed.
 *
 * ////////////////////////////////////////////////////////////////////// */


/* Pantheios Header Files */
#include <pantheios/pantheios.hpp>            // Pantheios C++ main header
#include <pantheios/inserters/args.hpp>       // for pantheios::args
#include <pantheios/inserters/exception.hpp>  // for pantheios::exception

#include <pantheios/backends/bec.file.h>      // be.file header

/* Standard C/C++ Header Files */
#include <exception>                          // for std::exception
#include <new>                                // for std::bad_alloc
#include <string>                             // for std::string
#include <stdlib.h>                           // for exit codes

/* ////////////////////////////////////////////////////////////////////// */

/* Define the stock front-end process identity, so that it links when using
 * fe.N, fe.simple, etc. */
PANTHEIOS_EXTERN_C const PAN_CHAR_T PANTHEIOS_FE_PROCESS_IDENTITY[] = PANTHEIOS_LITERAL_STRING("example.cpp.file");

/* ////////////////////////////////////////////////////////////////////// */

#define PSTR(x)         PANTHEIOS_LITERAL_STRING(x)

/* ////////////////////////////////////////////////////////////////////// */

int main(int argc, char **argv)
{
  try
  {
#ifndef PANTHEIOS_USE_WIDE_STRINGS
    pantheios::log_DEBUG("main(", pantheios::args(argc, argv), ")");
#else /* ? !PANTHEIOS_USE_WIDE_STRINGS */
    STLSOFT_SUPPRESS_UNUSED(argc); STLSOFT_SUPPRESS_UNUSED(argv);
#endif /* !PANTHEIOS_USE_WIDE_STRINGS */

    pantheios::log_NOTICE(PSTR("stmt 1"));

    // Set the file name for the local back-end, truncating the
    // file's existing contents, if any.
    pantheios_be_file_setFilePath(PSTR("log.local"), PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BE_FILE_F_TRUNCATE, PANTHEIOS_BEID_LOCAL);

    pantheios::log_NOTICE(PSTR("stmt 2"));

    // Set the file name for the remote back-end.
    pantheios_be_file_setFilePath(PSTR("log.remote"), PANTHEIOS_BEID_REMOTE);

    pantheios::log_NOTICE(PSTR("stmt 3"));

    // Set the file name for all back-ends.
    pantheios_be_file_setFilePath(PSTR("log.all"));

    pantheios::log_NOTICE(PSTR("stmt 4"));

    pantheios::log_DEBUG(PSTR("exiting main()"));

    return EXIT_SUCCESS;
  }
  catch(std::bad_alloc&)
  {
    pantheios::log(pantheios::alert, PSTR("out of memory"));
  }
  catch(std::exception& x)
  {
  pantheios::log_CRITICAL(PSTR("Exception: "), pantheios::exception(x));
  }
  catch(...)
  {
    pantheios::logputs(pantheios::emergency, PSTR("Unexpected unknown error"));
  }

  return EXIT_FAILURE;
}

/* ///////////////////////////// end of file //////////////////////////// */
