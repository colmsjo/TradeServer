/****************************************************************************/
/*! \mainpage tparser test files
 * \section intro_sec Introduction
 *
 * Simple test of fetching data using curl and parsing useing a parser from
 * Business-Insight <a href="http://www.Business-Insight.com">Business-Insight</a>
 * licensed under a AFPL-license.txt
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * \section Also testing the doxygen utility
 * 
 * This is a sextion
 *
 * \subsection debugwin Debugging under WINDOWS
 *
 * This is a subsection
 *
 ****************************************************************************/

#include <stdio.h>    // to get "printf" function
#include <stdlib.h>   // to get "free" function

#include "../tools/xmlParser/xmlParser.h"
// #include "../../tools/xmlParser.h"
 
#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include "/usr/include/curl/curl.h"
#include "/usr/include/curl/easy.h"

#define URL_TO_FETCH "http://localhost:8678/expanded-mbp/ABB.ST"
// #define URL_TO_FETCH "http://ichart.finance.yahoo.com/table.csv?s=DAI.DE&a=NaN&b=02&c=pr-2&g=d&ignore=.csv"

using namespace std;
 
/** @defgroup testparser The testparser group.
 * @ingroup sandbox
 * @{ */ 

 
int writer(char *data, size_t size, size_t nmemb, string *buffer){
  fprintf(stderr,"Hello I am a function pointer\n");
  int result = 0;
  if(buffer != NULL) {
    buffer -> append(data, size * nmemb);
    result = size * nmemb;
  }
  return result;
} 
 
/// The fetch_data function will feth the URL_TO_FETCH and write its contents to t string buffer
/**
 *
 *
 * @param buffer string to put the contents of the URL in
  *
 * \code Testing some code stuff in doxy gen
 *        x.getText(0) -> "foobar"
 *        x.getText(1) -> "chu"
 *        x.getClear(0) --> "<!DOCTYPE world >"
 * \endcode
 *
 * \return return value
 *
 * \note Testing a note in doxygen
 *
 */
int fetch_data (string buffer)
{
  /* (A) Variable Declaration */
  CURL *curl;		/* That is the connection, see: curl_easy_init */
  //CURLcode res;		/* Not needed, see: curl_easy_cleanup */
 
  /* (B) Initilise web query */
  curl = curl_easy_init();
 
  /* (C) Set Options of the web query 
   * See also:  http://curl.haxx.se/libcurl/c/curl_easy_setopt.html  */
  if (curl){
    curl_easy_setopt(curl, CURLOPT_URL, URL_TO_FETCH);
    curl_easy_setopt(curl, CURLOPT_HEADER, 0);	 /* No we don't need the Header of the web content. Set to 0 and curl ignores the first line */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0); /* Don't follow anything else than the particular url requested*/
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer ); /* Data Pointer &buffer stores downloaded web content */	
  }
  else{
    fprintf(stderr,"Error 1\n");	/* Badly written error message */
    return 0;											
  }
 
  /* (D) Fetch the data */
  curl_easy_perform(curl);
  /* res = curl_easy_perform(curl); */
  /* There is no need for "res" as it is just a flag */	
 
  /* (E) Close the connection */
  curl_easy_cleanup(curl);
 
  /* (F) Transform &buffer into a istringstream object */
  std::istringstream iss(buffer);
 
  string line, item;	
  int linenum = 0;
  while (getline (iss, line)){
    linenum++;								/* Move to Next Line */
    cout << "\nLine #" << linenum << ":" << endl;			/* Terminal Printout */
    std::istringstream linestream(line);				/* Read Next Line */
 
    int itemnum = 0;
    while (getline (linestream, item, ',')){
      itemnum++;
      cout << "Item #" << itemnum << ": " << item << endl;		/* Terminal Printout */
    } // End WHILE (items)
  } //End WHILE (lines)
 
  return 0;	
}

/// The parse_xml function will parse a XML file
/**
 * Uses the parser defined in xmlParser.cpp
 *
 * \return returns zero
 *
 */

int parse_xml()
{
  // this open and parse the XML file:
  XMLNode xMainNode=XMLNode::openFileHelper("xmlParser/PMMLModel.xml","PMML");

  // this prints "<Condor>":
  XMLNode xNode=xMainNode.getChildNode("Header");
  printf("Application Name is: '%s'\n", xNode.getChildNode("Application").getAttribute("name"));
  
  // this prints "Hello world!":
  printf("Text inside Header tag is :'%s'\n", xNode.getText());

  // this gets the number of "NumericPredictor" tags:
  xNode=xMainNode.getChildNode("RegressionModel").getChildNode("RegressionTable");
  int n=xNode.nChildNode("NumericPredictor");

  // this prints the "coefficient" value for all the "NumericPredictor" tags:
  for (int i=0; i<n; i++)
    printf("coeff %i=%f\n",i+1,atof(xNode.getChildNode("NumericPredictor",i).getAttribute("coefficient")));

  // this prints a formatted ouput based on the content of the first "Extension" tag of the XML file:
  char *t=xMainNode.getChildNode("Extension").createXMLString(true);
  printf("%s\n",t);
  free(t);
  return 0;
}

int parse_xml2()
{
  // open and parse the XML file:
  XMLNode xMainNode=XMLNode::openFileHelper("quote.abb.st.xml","body");

  // Get first row
  XMLNode xNode=xMainNode.getChildNodeByPath("html/table/tr");
  
  // Get the number of cells
  int n=xNode.nChildNode("td");
  printf("Number of cells: %i\n", n);
  
  // print the contents of all cells
  for (int i=0; i<n; i++)
  	  printf("cell: %s\n",xNode.getChildNode("td",i).getText());

  return 0;
}


int main ()
{
  string buffer;		/* See: CURLOPT_WRITEDATA */
  int i;
  
  // fetch the data over http using the curl library
  //i = fetch_data(buffer);

  // parse the fetched xml using a simple XML parser that I've borrowed
  parse_xml2();
  
  return i;
}

/**
* @} */ 
