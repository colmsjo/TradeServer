#
# Jonas C. 101212
#
# Script for building CppTest and Pantheios
#
gunzip cpptest-1.1.1.tar.gz.gz
tar -xf cpptest-1.1.1.tar.gz
unzip xmlParser.zip
unzip pantheios-1.0.1-beta201.zip
unzip nvwa-0.8.2.zip
unzip stlsoft-1.9.103-hdrs.zip

gunzip boost_1_45_0.tar.gz 
tar -xvf boost_1_45_0.tar 

cd boost_1_45_0
./bootstrap.sh --prefix=/usr/local --with-libraries=date_time,filesystem,math,iostreams,serialization,mpi,thread,test | tee -a build-tools.log
./bjam variant=release link=static threading=multi runtime-link=static stage 2>&1 | tee -a build-tools.log
#./bjam --without-python variant=release link=static threading=multi runtime-link=static stage 2>&1 | tee -a build-tools.log
cd ..

cd cpptest-1.1.1 
./configure | tee -a build-tools.log
make | tee -a build-tools.log
cd ..

cd pantheios-1.0.1-beta201
cd build/gcc41.unix/
make build.libs.core | tee -a build-tools.log
make build | tee -a build-tools.log
make test | tee -a build-tools.log
cd ../../../
