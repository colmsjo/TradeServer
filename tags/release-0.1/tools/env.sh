#
# Make sure tat svn works
#

export EDITOR=vi

#
# Set root directory for source code
#

export SRC_ROOT=/root/svn/TradeServer/trunk


#
# Path for Boost
#
export BOOST_ROOT=$SRC_ROOT/tools/boost_1_45_0

#
# Setup pantheios
#

export FASTFORMAT_ROOT=$SRC_ROOT/tools/pantheios-1.0.1-beta201
export STLSOFT=$SRC_ROOT/tools/stlsoft-1.9.103
export GLIBCXX_FORCE_NEW=1


#
# OpenMPI is install in /usr/local
#

export PATH=$PATH:/usr/local/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
