/****************************************************************************/
/*! \mainpage testFetchPTData Test suite for Fetch PrimeTrader data
 * \section intro_sec Introduction
 *
 * testTestparser.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Test suite
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101118 Jonas C. Initial version
 *
 ****************************************************************************/

#include <cstdlib>
#include <cstring>
#include <iostream>

#include "FetchPTData.h"

#include "cpptest.h"

//correct url
#define TEST_PT_URL_1 "http://localhost:8678/quotes/NHY.OSE&PEP.N&ABB.ST"				
//incorret url
#define TEST_PT_URL_2 "http://localhost:8677/quotes/NHY.OSE&PEP.N&ABB.ST"
// site always should be up
#define TEST_PT_URL_3 "http://di.se"

#define CSV_FILE "test_output.csv" 
#define HTML_STRING "<body><html><table><tr><td>SEQUENCE</td> <td>EXCHANGE</td> <td>BOARD</td> <td>TIME</td> <td>PAPER</td> <td>BID</td> <td>BID-DEPTH</td> <td>BID-DEPTH-TOTAL</td> <td>BID-NUMBER</td> <td>OFFER</td> <td>OFFER-DEPTH</td> <td>OFFER-DEPTH-TOTAL</td> <td>OFFER-NUMBER</td> <td>OPEN</td> <td>HIGH</td> <td>LOW</td> <td>LAST</td> <td>CHANGE</td> <td>CHANGE-PERCENT</td> <td>VOLUME</td> <td>VALUE</td> <td>TRADES</td> <td>STATUS</td> <td>QUOTE-ID</td></tr> \
			<tr><td>1628316</td><td>OSE</td><td></td><td>120413</td><td>NHY</td><td>37.9200</td><td>5078</td><td>761090</td><td>194</td><td>37.9600</td><td>4050</td><td>1565815</td><td>294</td><td>38.2800</td><td>38.2800</td><td>37.6900</td><td>37.9100</td><td>-0.0700</td><td>-0.1800</td><td>4703760</td><td>177044943.1300</td><td>1597</td><td></td><td></td></tr> \ 
			<tr><td>236385</td><td>N</td><td></td><td>121133</td><td>PEP</td><td>63.5900</td><td>200</td><td></td><td></td><td>67.9700</td><td>200</td><td></td><td></td><td>64.7700</td><td>64.7700</td><td>64.7700</td><td>64.7700</td><td>0.8300</td><td>1.2981</td><td>6983936</td><td>452349534</td><td>0</td><td></td><td></td></tr> \
			<tr><td>14130667</td><td>ST</td><td></td><td>120411</td><td>ABB</td><td>140.0000</td><td>26406</td><td>307809</td><td>198</td><td>140.1000</td><td>2000</td><td>604473</td><td>477</td><td>142.2000</td><td>142.4000</td><td>140.0000</td><td>140.1000</td><td>-1.2000</td><td>-0.8500</td><td>607560</td><td>85764971.6000</td><td>880</td><td></td><td></td></tr> \
			</table></html></body>"

#ifdef _MSC_VER
	#pragma warning (disable: 4290)
#endif


using namespace std;

// Class for testing the FetchPTData class
//
class testFetchPTData : public Test::Suite
{
public:
	// Add all functions that should be executed
	testFetchPTData()
	{
		TEST_ADD(testFetchPTData::testPTableRows)
		TEST_ADD(testFetchPTData::testPTConnection)
		TEST_ADD(testFetchPTData::testReadURL)
		TEST_ADD(testFetchPTData::testGetCell)
		TEST_ADD(testFetchPTData::testParseContents)
		TEST_ADD(testFetchPTData::testSaveAsCSV)
	}
	
private:

	void testPTConnection(){	
		cout << "\n>>>>> Test suite: testFetchPTData::testPTConnection >>>>>" << endl;

		FetchURL fetchURL;

		try {
			// Read from port localhost:8678 (need to make sure PrimeTrader is started)
			TEST_ASSERT( fetchURL.readURL(TEST_PT_URL_1) == TS_OK );
		} catch (TSException& e) {
			cout << "Unexpected exception:" + e.getErrorMsg() << endl;
		}
	}

	void testReadURL() {
		cout << "\n>>>>> Test suite: testFetchPTData::testReadURL >>>>>" << endl;

		FetchURL fetchURL;		
		
		// Shouldn't fail, di.se always up
		TEST_ASSERT( fetchURL.readURL(TEST_PT_URL_3) == TS_OK );
		
		// Test error handling
		TEST_THROWS_MSG( fetchURL.readURL(TEST_PT_URL_2), TSException, "fetchURL.readURL does not throw, expected TSException" );
		TEST_THROWS_MSG( fetchURL.readURL(""), 		  TSException, "fetchURL.readURL does not throw, expected TSException" );
		// doesn't throw, can't understand why TEST_THROWS_MSG( fetchURL.readURL(NULL), 	  TSException, "fetchURL.readURL does not throw, expected TSException" );
	}
	
	void testSaveAsCSV() {
		cout << "\n>>>>> Test suite: testFetchPTData::testSaveAsCSV >>>>>" << endl;

		FetchURL fetchURL;
	
		fetchURL.setContents(HTML_STRING);
		TEST_ASSERT( fetchURL.parseContents() == TS_OK );
		TEST_ASSERT( fetchURL.saveAsCSV( CSV_FILE) == TS_OK );
	}
	
	void testParseContents() {
		cout << "\n>>>>> Test suite: testFetchPTData::testParseContents >>>>>" << endl;

		FetchURL fetchURL;
	
		fetchURL.setContents(HTML_STRING);
		TEST_ASSERT( fetchURL.parseContents() == TS_OK );

		TEST_ASSERT( fetchURL.getNumRows() == 4 );
		TEST_ASSERT( fetchURL.getNumCols(0) == 24 );
		TEST_ASSERT( fetchURL.getNumCols(1) == 24 );
		TEST_ASSERT( fetchURL.getNumCols(2) == 24 );
		TEST_ASSERT( fetchURL.getNumCols(3) == 24 );
		
		TEST_ASSERT( fetchURL.getCell(1,0) == "1628316" );
		TEST_ASSERT( fetchURL.getCell(1,1) == "OSE" );
		TEST_ASSERT( fetchURL.getCell(1,21) == "1597" );
		
		TEST_ASSERT( fetchURL.getCell(3,0) == "14130667" );
		TEST_ASSERT( fetchURL.getCell(3,1) == "ST" );
		TEST_ASSERT( fetchURL.getCell(3,21) == "880" );
	}

	void testGetCell() {
		cout << "\n>>>>> Test suite: testFetchPTData::testGetCell >>>>> " << endl;

		FetchURL fetchURL;
	
		// test error handling, nothing has been fetched so an exception should be thrown
		TEST_THROWS_MSG( fetchURL.getCell(1,1), TSException, "fetchURL.readURL does not throw, expected TSException" );
	}

	void testPTableRows() {
		cout << "\n>>>>> Test suite: testFetchPTData::testAstrCells >>>>>" << endl;

		FetchURL fetchURL;
	
		TEST_ASSERT( fetchURL.testPTableRows() == TS_OK );
	}
};


// Main test program
//
int
main(int argc, char* argv[])
{
	try
	{
		// Create test suite and add tests
		Test::Suite ts;
		ts.add(auto_ptr<Test::Suite>(new testFetchPTData));

		// Run the tests
		Test::TextOutput output(Test::TextOutput::Verbose);		
		ts.run(output, true);
	}
	catch (...)
	{
		cout << "unexpected exception encountered\n";
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
 
 
 
 

