/****************************************************************************/
/**
 *
 * itch.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101206 Jonas C. Initial version
 *
 ****************************************************************************/


#include "itch.h"

//Specify process identity
const PAN_CHAR_T PANTHEIOS_FE_PROCESS_IDENTITY[] = "itch.exe";

Itch::Itch() {
	//this->itch_message.message = NULL;	
}

Itch::~Itch() {
	
	// Free allocated memory
	//if(this->itch_message.message)
	//	free(this->itch_message.message);
	
}


ItchMessage* Itch::parse(const char *pSz) {
	char szBuffer[20];				//the longest string is 10 chars
	ItchMessage* pItchMesg = NULL;
	
	// The first character indicates the message type
	//this->itch_message.message_type = *pSz;
	
	switch(*pSz) {
		case TIME_SEC_ID:
			strncpy(szBuffer, pSz+TIME_SEC_OFFSET[1], TIME_SEC_OFFSET[2]);
			
			pItchMesg 			= new TimeSecMsg();
			((TimeSecMsg*)pItchMesg)->secs 	= atol(szBuffer);
			
			break;
			
		case TIME_MSEC_ID:
			break;
			
		case SYSTEM_EVENT_ID:		
			break;
	
		case MARKET_SEGMENT_STATE_ID:
			break;
	
		case ORDER_BOOK_DIRECTORY_ID:
			break;
	
		case ORDER_BOOK_TRADING_ACTION_ID:
			break;
	
		case ADD_ORDER_ID:
			break;
	
		case ADD_ORDER_MPID_ID:
			break;
	
		case ORDER_EXECUTED_ID:
			break;
	
		case ORDER_EXECUTED_WITH_PRICE_ID:
			break;
	
		case ORDER_CANCEL_ID:
			break;
	
		case ORDER_DELETE_ID:
			break;
	
		case TRADE_ID:
			break;
	
		case CROSS_TRADE_ID:
			break;
	
		case BROKEN_TRADE_ID:
			break;
	
		case NOII_ID:
			break;
			
		default:
			string s = string("ItchMessage::parse - unrecognized message") + *pSz;		
			this->logger->log_ERROR(TSERR_INTERNAL_ERROR, s);
			throw  TSException(s);
	}
	
	
	return pItchMesg;	
}

