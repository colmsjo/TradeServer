/****************************************************************************/
/*! \mainpage testItch Test suite for Itch classes
 * \section intro_sec Introduction
 *
 * itch.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Test suite
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101206 Jonas C. Initial version
 *
 ****************************************************************************/

#include <cstdlib>
#include <cstring>
#include <iostream>

#include "itch.h"

#include "cpptest.h"

//Test messages           012345678901234567890123456789
const char *TEST_MSG_1 = "T86400";				
const char *TEST_MSG_2 = "T86400M999";
const char *TEST_MSG_3 = "T86400";		

#ifdef _MSC_VER
	#pragma warning (disable: 4290)
#endif


using namespace std;

// Class for testing the FetchPTData class
//
class testItch : public Test::Suite
{
public:
	// Add all functions that should be executed
	testItch()
	{
		TEST_ADD(testItch::test1)
	}
	
private:

	void test1(){	
		cout << "\n>>>>> Test suite: testItch::test1 >>>>>" << endl;

		ItchMessage itch;
		
		/**
		 * NasDaq OMX ITCH message types:
		 * TIME_SEC_ID				'T'
		 * TIME_MSEC_ID				'M'
		 * SYSTEM_EVENT_ID			'S'
		 * MARKET_SEGMENT_STATE_ID		'O'
		 * ORDER_BOOK_DIRECTORY_ID		'R'
		 * ORDER_BOOK_TRADING_ACTION_ID		'H'
		 * ADD_ORDER_ID				'A'
		 * ADD_ORDER_MPID_ID			'F'
		 * ORDER_EXECUTED_ID			'E'
		 * ORDER_EXECUTED_WITH_PRICE_ID		'C'
		 * ORDER_CANCEL_ID			'X'
		 * ORDER_DELETE_ID			'D'
		 * TRADE_ID				'P'
		 * CROSS_TRADE_ID			'Q'
		 * BROKEN_TRADE_ID			'B'
		 * NOII_ID				'I'
		 */		
		
		try {
			TEST_ASSERT( itch.parse(TEST_MSG_1) == TS_OK );
			TEST_ASSERT( itch.itch_message.message_type == TIME_SEC_ID );
			TEST_ASSERT( !strcmp(itch.itch_message.message->sec_msg.time, "86400" ) );
			
			printf( "itch.itch_message.message->sec_msg.time[0]:%c", itch.itch_message.message->sec_msg.time[0] );
			cout << "itch.itch_message.message_type" + itch.itch_message.message_type << endl;
			
		} catch (TSException& e) {
			cout << "Unexpected exception:" + e.getErrorMsg() << endl;
		}
	}

};


// Main test program
//
int
main(int argc, char* argv[])
{
	try
	{
		// Create test suite and add tests
		Test::Suite ts;
		ts.add(auto_ptr<Test::Suite>(new testItch));

		// Run the tests
		Test::TextOutput output(Test::TextOutput::Verbose);		
		ts.run(output, true);
	}
	catch (...)
	{
		cout << "unexpected exception encountered\n";
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
