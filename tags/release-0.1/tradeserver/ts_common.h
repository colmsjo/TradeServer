/****************************************************************************/
/*! \mainpage Common classes Classes used to parser ITCH data
 * \section intro_sec Introduction
 *
 * itch.h 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Classes use for parsing itch data.
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101206 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef TS_COMMON
#define TS_COMMON

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <exception>

#include <curl/curl.h>
#include <curl/easy.h> 

#include "xmlParser.h"
#include "pantheios/pantheios.hpp"
#include "pantheios/frontends/stock.h"
#include "pantheios/backends/bec.file.h" 

#ifdef DEBUG_MODE
#include "../tools/nvwa-0.8.2/nvwa/debug_new.h"
#endif


using namespace std;


// Define error codes
// code > 0 everything is ok
// vode < 0 some type of error
#define TS_OK 1				// All good
#define TS_NOT_OK -1			// Generic error

// Logging setup
static const string TS_LOG_FILE = "tradeserver.log";

#define PSTR(x)         PANTHEIOS_LITERAL_STRING(x)

// Define texts for errors used for logging
#define TSERR_INTERNAL_ERROR 		"ERROR 1. Internal error"
#define TSERR_CURL_ERROR 		"ERROR 2. Generic curl error"



/// TSLog
/**
 * Wrapper class for the Pantheios loggin framework. Should always be used
 *
 *
 */

class TSLog {
public:
	TSLog(string strLogFile) {
		pantheios_be_file_setFilePath(strLogFile.c_str()); 
					      //PANTHEIOS_BE_FILE_F_TRUNCATE, 
					      //PANTHEIOS_BE_FILE_F_TRUNCATE, 
					      //PANTHEIOS_BEID_LOCAL);	
	}
	
	void log_DEBUG(string s1, string s2) {	
		string s = s1 + ":" + s2;
		pantheios::log_DEBUG(s);
	}
	
	void log_INFORMATIONAL(string s1, string s2) {	
		string s = s1 + ":" + s2;
		pantheios::log_INFORMATIONAL(s);
	}
	
	void log_NOTICE(string s1, string s2) {	
		string s = s1 + ":" + s2;
		pantheios::log_NOTICE(s);
	}
	
	void log_WARNING(string s1, string s2) {
		string s = s1 + ":" + s2;
		pantheios::log_WARNING(s);
	}
	
	void log_ERROR(string s1, string s2) {
		string s = s1 + ":" + s2;
		pantheios::log_ERROR(s);
	}
	
	void log_CRITICAL(string s1, string s2) {
		string s = s1 + ":" + s2;
		pantheios::log_CRITICAL(s);
	}
	
	void log_ALERT(string s1, string s2) {
		string s = s1 + ":" + s2;
		pantheios::log_ALERT(s);
	}
	
	void log_EMERGENCY(string s1, string s2) {
		string s = s1 + ":" + s2;
		pantheios::log_EMERGENCY(s);
	}	
};

/// TSBase
/**
 * A generic TradeServer Root class that all classes should inherit.
 *
 * Currently empty, can be used for generic constructors, destructors etc.
 *
 */

class TSBase {
protected:
	//static TSLog* logger;
	TSLog* logger;
public:
	TSBase() {
		//if(!TSBase::logger) TSBase::logger = new TSLog(TS_LOG_FILE); 
		logger = new TSLog(TS_LOG_FILE); 
	}

	~TSBase() {
		delete logger; 
	}

};

/// TSException generic exception class
/**
 * A generic exception class. Inherit from this class for spcecialized exceptions
 *
 *
 *
 *
 * Example on how to catch exceptioins, NOT THE & FOLLOWING catch (exception&)
 *
 * int main () {
 *  try
 *  {
 *    throw myex;
 *  }
 * catch (exception& e)
 * {
 *   cout << e.what() << endl;
 * }
 *  return 0;
 * }
 *
 *
 *
 *
 */

/** class TSException : public exception {
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
}; */
 
 
class TSException : public exception {
public:
	TSException(string s) {
		strErrorMsg = s;
	}
	
	~TSException() throw() {};
	
	string getErrorMsg() throw() {
		return strErrorMsg;
	}

private:
	string strErrorMsg;
};




#endif //TS_COMMON
