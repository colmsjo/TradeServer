#!/bin/sh
if [ ! -d "m4" ]; then
    mkdir m4
fi

chmod +x bootstrap && ./bootstrap
rm -rf build && mkdir build && cd build
../configure
cp ../doc/Doxyfile doc
make
cd ../test

if [ ! -d "m4" ]; then
    mkdir m4
fi


chmod +x bootstrap && ./bootstrap
rm -rf build && mkdir build && cd build
../configure
make
cd ..

echo "RUNNING TESTS..."
./run_tests.sh

echo "RUNNING ghpf..."
cd .. && sudo build/src/app_server
