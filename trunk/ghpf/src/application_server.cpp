/****************************************************************************
 *
 * @file 		application_server.cpp
 * @brief		Initializes the server.
 * @author      Jonas Colmsj��
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/



// The only include allowed by the coding standards in cpp-files 
#include "application_server.hpp"


// Override log file name
static const std::string _LOG_FILE_ = "/var/log/ghpf";


int main(int argc, char* argv[]) 
{

	// Initialize the logging functionlity
	ghpf::Log::init(_LOG_FILE_);

	// Turn the process into a daemon. Parent will receive the process id of the child
	// while the child will get 0
	
	// TODO: do a ifdef to check for windows or unix
	if( _ghpf::daemonize() > 0) {
		
		// ******* THE PARENT PROCESS
		
    	ghpf::Log::log_INFORMATIONAL("application_server.cpp","Have forked the process and is exiting parent process...");
		ghpf::Log::destroy();
        exit(true);
	}

	// ******** NOW IN THE CHILD/DAEMON PROCESS

	try {

		// Close the logging and do a full/proper initialization of the process
		ghpf::Log::destroy();
		_ghpf::init(argc, argv, _LOG_FILE_);
		
		// Should do something usefull
		while(! ghpf::Context::Instance()->exitProcess() ) {
			sleep(10);

			//ghpf::Log::log_DEBUG("main.cpp", "RESTServer still running...");
		}
				
		// Exit the process
    	ghpf::Log::log_INFORMATIONAL("application_server.cpp","Exiting the process...");
		_ghpf::exit(true);

	} 
	// Catch all exceptions that haven't been caugt previously and exit gracefully
	catch (ghpf::Exception& e) {
		std::stringstream ss;
		ss << "Exception caught:" << e.what();
		ghpf::Log::log_ERROR("main.cpp", ss.str() );
		_ghpf::exit(true);
	}

}
