/****************************************************************************
 *
 * @file 		application_server.hpp
 * @brief		GHPF Application Server
 * @author      Jonas Colmsjö
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef APPLICATION_SERVER
#define APPLICATION_SERVER

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>

#include "ghpf.hpp"

/// application_server
/**
 * GHPF Application Server
 * 
 */
 
namespace application_server {

	static const int kApplicationServerProcess = 0;


/************************************************ ENTITY CLASSES *********************************************************/




/************************************************ ACTIVE CLASSES *********************************************************/



	/// ApplicationServer class
	/**
	 * The ApplicationServer controls all GHJPF applications running on a specific host or
	 * a set of hosts in a MPI cluster. It contains a RESTful server used to communicate 
	 * with the outside world.
	 *  
	 */

	 class ApplicationServer : public ghpf::ActiveObject {
	 public:	
	
		/// ApplicationServer
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	ApplicationServer() {
			string constant_uuid("67ece8c3-8581-48c3-b727-a3aefe6a54f7");
	 		uuid_ = ghpf::uuid::create(constant_uuid);
	 	}
	
		 /// ~ApplicationServer
		 /**
		  *  Destructor
		  *
		  * 
		  */ 	
		 
		 ~ApplicationServer() {
		 	if(uuid_) delete uuid_;	
		 }
	
		/// getUUID
		/**
		 *  Get the UUID of the ActiveObject
	 	 *
	 	 * \return uuid of the currenbt object
		 * 
		 */ 	
	 	ghpf::uuid* getUUID(void) {
			return uuid_;
	 	}

		/// init
		/**
		 *  Initilize the ActiveObject
		 *
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	void init(void) {}; 

		/// receive
		/**
		 *  Receive MPI message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	void receive(ghpf::MPIMessage* msg) {};

		/// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	void receive(ghpf::RESTMessage* msg) {}; 
		 
	private:
		ghpf::uuid* uuid_;
	};


	/// AppServerProcessInitiator class
	/**
	 * Initiates the a Application Server Process. The same object is always created.
	 *  
	 */
	class AppServerProcessInitiator : public ghpf::ProcessInitiator {
	public:
	 	
		/// AppServerProcessInitiator
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	 	AppServerProcessInitiator();
	
		/// AppServerProcessInitiator
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	~AppServerProcessInitiator();
	 	
	 	/// init()
	 	/**
	 	 * Initialize the process and create the Active Object for this particular 
	 	 * process.
	 	 *
	 	 */
	 	void init() {};
	
	 };
	



} // namespace application_server


#endif // APPLICATION_SERVER
