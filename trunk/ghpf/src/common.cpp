/*****************************************************************************
 *
 * @file 		common.cpp
 * @brief		Common classes used throughout the framework
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ******************************************************************************/


// The only include allowed by the coding standards in cpp-files 
#include "common.hpp"

using namespace std;

// only used by simple homemade logger but needed for the compile in all cases
ofstream 			ghpf::Log::log_file_;


// Check if log4cplus is installed, simple logging will be used otherwise (see below)
#ifdef ENABLE_LOG4CPLUS

/*
 * SUPPORT FOR LOG4CPLUS NOT IMPLEMENTED YET
 *
 */
 
using namespace log4cplus;
BasicConfigurator 	ghpf::logger_config_;
string 				ghpf::log_filename_;

void ghpf::Log::init(string log_file) {	
	pid_t pid = getpid();
	stringstream log_file2;
	log_file2 << log_file << "." << pid << ".log"; 

	log_filename_ = log_file2.str();
    logger_config_.configure();
}

void ghpf::Log::destroy() {
}	

void ghpf::Log::log_WARNING(string s1, string s2) {
    Logger logger = Logger::getInstance(log_filename_.c_str());
	string s = "WARNING:" + s1 + ":" + s2;
    LOG4CPLUS_WARN(logger, s);
}

#elif ENABLE_PANTHEIOS // end of ENABLE_LOG4CPLUS

/*
 * Use Pantheios logger - http://pantheios.sourceforge.net/
 *
 * Also need STLSoft libraries - http://www.stlsoft.org/
 *
 */

#include <pantheios/pantheios.hpp>
#include <pantheios/frontends/stock.h>
#include <pantheios/backends/bec.file.h>
 
#define PSTR(x)         PANTHEIOS_LITERAL_STRING(x)
 
/*
 * Was defined in the class definition, not sure if really needed
 */

//PANTHEIOS_BE_FILE_F_TRUNCATE, 
//PANTHEIOS_BE_FILE_F_TRUNCATE, 
//PANTHEIOS_BEID_LOCAL);	


void ghpf::Log::init(string log_file) {	
	pid_t pid = getpid();
	stringstream log_file2;
	log_file2 << log_file << "." << pid << ".log"; 

	log_filename_ = log_file2.str();
    
	pantheios_be_file_setFilePath(log_filename_.c_str()); 
}

void ghpf::Log::destroy() {
}	
  
 
static void ghpf::Log::log_DEBUG(string s1, string s2) {	
	 string s = s1 + ":" + s2;
	 pantheios::log_DEBUG(s);
}
 
static void ghpf::Log::log_INFORMATIONAL(string s1, string s2) {	
	 string s = s1 + ":" + s2;
	 pantheios::log_INFORMATIONAL(s);
}
 
static void ghpf::Log::log_NOTICE(string s1, string s2) {	
	 string s = s1 + ":" + s2;
	 pantheios::log_NOTICE(s);
}
 
static void ghpf::Log::log_WARNING(string s1, string s2) {
	 string s = s1 + ":" + s2;
	 pantheios::log_WARNING(s);
}
 
static void ghpf::Log::log_ERROR(string s1, string s2) {
	 string s = s1 + ":" + s2;
	 pantheios::log_ERROR(s);
}
 
static void ghpf::Log::log_CRITICAL(string s1, string s2) {
	 string s = s1 + ":" + s2;
	 pantheios::log_CRITICAL(s);
}
 
static void ghpf::Log::log_ALERT(string s1, string s2) {
	 string s = s1 + ":" + s2;
	 pantheios::log_ALERT(s);
}
 
static void ghpf::Log::log_EMERGENCY(string s1, string s2) {
	 string s = s1 + ":" + s2;
	 pantheios::log_EMERGENCY(s);
}	

#else // end of ENABLE_PANTHEIOS

/*
 * Simples possible logger, just log everything to a file.
 *
 * No support for defferent logging levels.
 *
 */

void ghpf::Log::init(string log_file) {	
	pid_t pid = getpid();
	stringstream log_file2;
	log_file2 << log_file << "." << pid << ".log"; 
	log_file_.open(log_file2.str().c_str());
	
	if(log_file_.fail())
		cout << "ERROR: Could not open logfile:" + log_file2.str() << endl;
}

void ghpf::Log::destroy() {
	log_file_.close();
}	

void ghpf::Log::log_DEBUG(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "DEBUG:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_INFORMATIONAL(string s1, string s2) {	
 	pid_t pid = getpid();
	stringstream ss;
	ss << "INFO:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_NOTICE(string s1, string s2) {	
 	pid_t pid = getpid();
	stringstream ss;
	ss << "NOTICE:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_WARNING(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "WARNING:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_ERROR(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "ERROR:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_CRITICAL(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "CRITICAL:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_ALERT(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "ALERT:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}

void ghpf::Log::log_EMERGENCY(string s1, string s2) {
 	pid_t pid = getpid();
	stringstream ss;
	ss << "EMERGENCY:" << pid << ":" << s1 << ":" << s2;
	log(ss.str());
}
	
void ghpf::Log::log(string s) {
 	cout << s << endl;
	log_file_ << s << "\n";
	log_file_.flush();
}

#endif
