/****************************************************************************/
/*! \mainpage Gizur High Performance Framework 
 * \section intro_sec Introduction
 *
 * Gizur High Performance Framework makes it easy to build performance critical 
 * applications using C++. Using ghpf it becomes easy to build applications 
 * consisting of multiple communicating processes. RESTful services and web 
 * applications can be built.
 *
 * The implementation of the Framework is layered. 
 *
 * The application layer must not use the OS Layer directly, only classes defined
 * in the ghpf namespace. These abstract classes in ghpf are implemented by the OS Layer.
 * The ghpf::Context contains objects and factories used to instantiate the classes.
 *
 * <pre>
 *                      |--------------------------------------------------------------|
 * Applications layer   |  application_server | custom applications  | ...             |
 *                      |--------------------------------------------------------------|
 * Public layer         |                           ghpf                               |
 *                      |--------------------------------------------------------------|
 * Abstration layer     |                        os_facade                         | c |
 *                      |----------------------------------------------------------| o |
 * OS Layer             | unix/windows (windows not implemented) |  MPI   | http   | m |
 *                      |                                        |        |        | n |
 *                      |----------------------------------------------------------|---|
 * </pre>
 *
 * The framework for communication is centered around a set of abstract classes. These
 * classes are implemented using different communication techniques, currently REST (HTTP)
 * and MPI. 
 *
 * The classes are:
 * <ul>
 *   <li> uuid - univerally unique identifier. Used to identify ActiveObejcts etc.
 *   <li> Message - contains the information to be sent from one process to antoher
 *   <li> ActiveObject - each process contains one or more ActiveObjects that performs the 
 *                 actual work
 *   <li> Communicator - each process has one, and only one, Communictor that
 *                  ActiveObjects use when communicating with each other
 *   <li> Adapter - contains the logic for sending messages using a communication technique.
 *             There are currently two Adapters, one for MPI and one for REST (HTTP).
 *             ActiveObjects are identified using uuid.
 * </ul>
 *
 * There are to additional classes:
 * <ul>
 *   <li> Context (Singleton) - contains reference to the Communicator and other generic
 *                         information about the running process. Also contains 
 *                         references to all ActiveObjects (TO BE IMPLMENTED).
 *   <li> ProcessInitiator - performs the initial process setup, i.e creates the Context and
 *                      ActiveObjects. The object serves no purpose after calling the 
 *                      init funcation.
 * </ul>
 *
 * The framework is typically initilized this way:
 * <ul>
 *   <li> ProcessInitiator - calls _ghpf::init which creates the Context object
 *   <li> -> Context - createa a communicator
 *   <li> --> Communicator - creates a RESTAdapter and a MPIAdapter
 *   <li> ---> RESTAdapter - create a RESTServer and a RESTClient
 *   <li> ----> RESTServer
 *   <li> ----> RESTClient
 *   <li> ---> MPIAdapter
 *   <li> Process Initiator creates the ActiveObjects for this process
 *   <li> -> ActiveObject 1
 *   <li> ProcessInitiator - registers the ActiveObjects with the Communicator
 *   <li> ->Communicator::register - stores the uuid together with a reference to the ActiveObject
 * </ul>
 *
 *
 *
 * Typical scenarios
 * Sending:
 * <ol>
 *  <li> ActiveObject - An ActiveObject requests the Communicator from the Context singleton
 *  <li> The ActiveObeject creates a Message, currently be MPIMessage or CommandMessage
 *    and populates it with data
 *  <li> The ActiveObeject calls Communicator.send(Message)
 *  <li> -> Communicator - The Communicator get the Adapter calling Message.getAdapter
 *  <li> --> Adapter - The Comminicator calls Adapter.send(Message)
 * </ol>
 *
 * Receiving using REST:
 * <ol>
 *  <li> RESTServer - The callback funtion of the RESTServer stores a CommandMessage in the FIFO queue
 *  <li> RESTAdapter - The Adapter is looping forever in the run function (needs to be started using Context::run)
 *                        RESTServer::getNextMessage is called in order to check for new messages
 *  <li> -> RESTServer::getNextMessage - the FIFO queue is checked for new messages. Communicator::receive is called
 *                                       for each CommandMessage found.
 *  <li> -> Communicator::receive - Check the receiver of the message and pass it on
 *  <li> --> ActiveObject::receive - process the message
 * </ol>
 *
 * Detailed view of the layers:
 * <pre>
 *                      |---------------------------------------------------------------------------------|
 * Applications layer   |  application_server | custom applications  | ...                                |
 *                      |---------------------------------------------------------------------------------|
 * Public layer         |                                       ghpf                                      |
 *                      |  Context Communicator Message CommandMessage SimpleXMLParser Url                |
 *                      |---------------------------------------------------------------------------------|
 * Abstration layer     |                             os_facade                             | c           |
 *                      |  ContextImp CommunicatorImpl SimpleXMLParserImpl                  | o Adapter   |
 *                      |-------------------------------------------------------------------| m FIFOQueue |
 * OS Layer             | unix/windows (windows not implemented) |     MPI    |    http     | m           |
 *                      |                                        | MPIAdapter | RESTAdapter | n           |
 *                      |                                        |            | RESTserver  | n           |
 *                      |                                        |            | RESTClient  | n           |
 *                      |---------------------------------------------------------------------------------|
 * </pre>
 *
 * @file 		common.hpp
 * @brief		Common classes used throughout the framework
 * @author      Jonas Colmsj��
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef COMMON
#define COMMON

// \todo: NOT SURE THIS IS THE RIGHT PLACE, SEAMS TO BE VERY SENSITIVE TO WHERE THIS IS PLACED
// Don't move this if it compiles without mpi errors!!
// Need serialization in order to send message with boost::mpi
#ifndef DISABLE_MPI
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/string.hpp>
#endif


// contains information about the current platform that the framework is built on
// is generated by the configure script (autotools)
// \todo: Need to find another solution for windows
#ifdef HAVE_CONFIG_H
#include "ghpf_config.h"			
#endif

// All includes of cross plattform standard libraries should be placed here
#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
#include <iomanip>
#include <queue>

#include <signal.h>
#include <inttypes.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdint.h>				//needed by microhttpd

#include <stdio.h>

#ifndef DISABLE_CURL
#include <curl/curl.h>
#endif

// For strtok used in http_server::UrlParser
#include <stddef.h>

// used in order to Universal Unique ID:s
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

// for instance case insensitive string comparison
#include <boost/algorithm/string.hpp>

// use boost threads to implement threads, does this work on Windows?
#include <boost/thread.hpp>


// Inform microhttpd to include platform.h
// \todo: Remove this since it requires platform.h to be manually copied ti /usr/local/include
//       during installation
// Make sure that MPI should be enabled
#ifndef DISABLE_HTTPD
#define MHD_PLATFORM_H
#include <platform.h>
#include <microhttpd.h>
#endif

// Simple XML Parser
#include "xmlParser.h"

// Classes that also are used by applications using the framework
#include "ghpf.hpp"

// All forward declarations should be put here
// Common cannot inlude any other modules in the framework, forward declarations must be used in stead
// This is necessary in order to avoid circular dependencies
namespace http {
	class RESTAdapter;
}

namespace mpi {
	// \todo: Should be changed to MPIAdapter
	class MPIProcess;
}

// Check if log4cplus is installed
#ifdef ENABLE_LOG4CPLUS
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#endif

#ifdef ENABLE_LOG4CPP
#include <log4cpp/Category.hh>
#endif 
 
// Show source file for leaked objects
#ifdef USE_DEBUG_NEW
#include "../../tools/nvwa-0.8.2/nvwa/debug_new.h"
#endif


using namespace std;


/// common
/**
 * Contains classes used throughout the framework. 
 *
 *
 */
namespace common {


	/// map class
	/**
	 * map, A map that will throw exceptions if members not defined are accessed.
	 * 
	 * The standard map returns zero if keys that haven't been defined are accessed.
	 *
	 */
	template <typename KeyType, typename ValueType>
	class map {
	public:
		void set(KeyType key, ValueType value) {
			map_[key] = value;
		}
		
		ValueType get(KeyType key) {
			if(map_.count(key)) return map_[key];
			else {
				stringstream ss;
				ss << "common::map::get(" + key + ") not defined";
				throw ghpf::Exception( ss.str() );
			}
		}

		int size(void) { return map_.size(); }

	protected:
		std::map<KeyType, ValueType> map_;
	};

	/// map class
	/**
	 * map, A map that will throw exceptions if members not defined are accessed.
	 * 
	 * The standard map returns zero if keys that haven't been defined are accessed.
	 *
	 */
	class strings_map : public map<std::string, std::string> {
	public:
		void                      resetIterator(void)       { iterator_ = map_.begin(); }
		bool                      iteratorReachedEnd(void)  { return iterator_ == map_.end(); }
		std::pair<string, string> nextIteratorElement(void) { return *iterator_++; }
		
	private:
		std::map<std::string, std::string>::const_iterator iterator_;
	};
	
	/// FIFOQueue
	/**
	 * A Thread safe queue
	 *
	 *
	 * Messages are saved in a fifo queue. The qeueue needs to be thread safe though.
	 * Two alternatives:
	 * 1. Boost
	 * 2. Homemade using article: http://drdobbs.com/high-performance-computing/212201163?pgno=1
	 *
	 * 1. Boost
	 * Wait for data to be ready and then acquire lock:
	 *
	 * boost::condition_variable cond;
	 * boost::mutex mut;
	 * bool data_ready;
	 *
	 * void process_data();
	 * 
	 * void wait_for_data_to_process()
	 * {
	 *    boost::unique_lock<boost::mutex> lock(mut);
	 *    while(!data_ready)
	 *    {
	 *        cond.wait(lock);
	 *    }
	 *    process_data();
	 * }
	 *
	 *
	 * In order to make it possible for several threads to read and one thread to write
	 *
	 * boost::shared_mutex _access;
	 * void reader()
	 * {
	 *     // get shared access
	 *     boost::shared_lock lock(_access);
	 *
	 *     // now we have shared access
	 * }
	 *
	 * void writer()
	 * {
	 *     // get upgradable access
	 *     boost::upgrade_lock lock(_access);
	 *
	 *     // get exclusive access
	 *     boost::upgrade_to_unique_lock uniqueLock(lock);
	 *     // now we have exclusive access
	 * }
	 * 
	 *------------ CORRECTION - http://stackoverflow.com/questions/989795/example-for-boost-shared-mutex-multiple-reads-one-write
	 *	boost::shared_mutex _access;
	 *	void reader()
	 *	{
	 *	  boost::shared_lock< boost::shared_mutex > lock(_access);
	 *	  // do work here, without anyone having exclusive access
	 *	}
	 *	
	 *	void conditional_writer()
	 *	{
	 *	  boost::upgrade_lock< boost::shared_mutex > lock(_access);
	 *	  // do work here, without anyone having exclusive access
	 *	
	 *	  if (something) {
	 *	    boost::upgrade_to_unique_lock< boost::shared_mutex > uniqueLock(lock);
	 *	    // do work here, but now you have exclusive access
	 *	  }
	 *	
	 *	  // do more work here, without anyone having exclusive access
	 *	}
	 *	
	 *	void unconditional_writer()
	 *	{
	 *	  boost::unique_lock< boost::shared_mutex > lock(lock);
	 *	  // do work here, with exclusive access
	 *	}
	 */
	template <typename T>
 	class FIFOQueue {
	public:
		T* read(void) {
			T *element = NULL;
			
			// get shared access
			boost::shared_lock< boost::shared_mutex > lock(access_);
			// now we have shared access
			
			if(!queue_.empty()) {
				element = queue_.front();
				queue_.pop();
			}
			
			// Not sure if this is needed or the right way of doing it??
			access_.unlock();
			
			return element;
		}
	 
		void write(T* element) {
			// get upgradable access
			boost::upgrade_lock< boost::shared_mutex > lock(access_);
			
			// get exclusive access
			boost::upgrade_to_unique_lock< boost::shared_mutex > uniqueLock(lock);
			// now we have exclusive access
			
			this->queue_.push(element);
			
			// Not sure if this is needed or the right way of doing it??
			access_.unlock();
		}

	private:
		boost::shared_mutex access_;	
	 	queue<T*>			queue_;
		
	};


	/// Adapter class
	/**
	 * Adapter, virtual class to be implemented in MPI etc.
	 * 
	 * Adapters are never created directly. This is always performed by a Message of the
	 * same type as the Adapter. 
	 *
	 */
	 class Adapter {
	 public:
	 
		/// ~Adapter
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		virtual ~Adapter() {};
		
	 	/// send
	 	/**
	 	 * Send using MPI
	 	 *
	 	 * @param message the message to send. It contains the UUID of the receiving ActiveObject.
	 	 *
	 	 * \return void 
	 	 */
		virtual void send(int rank_receiver, ghpf::MPIMessage *message) = 0;

	 	/// send
	 	/**
	 	 * Send using REST
	 	 *
	 	 * @param message the message to send. It contains the UUID of the receiving ActiveObject.
	 	 *
	 	 * \return void 
	 	 */
		virtual void send(string host, int port, ghpf::RESTMessage *message) = 0;

	 };

	/// OSProcess class
	/**
	 * OSProcess, encapsulates the operating system
	 * 
	 *  \todo: Remove this class, not used in the framework 
	 */
	class OSProcess {
	public:	
			
		/// OSProcess
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	OSProcess() {} ;

		/// ~OSProcess
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~OSProcess() {};
	 			
	};

	/******************************* HTTP related classes ******************************************/


	/// http class
	/**
	 * http contains defintions used by http-related classes (such as Url, RESTServer and RESTClient)
	 * 
	 *  
	 */
	class http_definitions {
	public:	
		static const string kStrStart;
		static const string kStrStop ;
		static const string kStrStatus ;
		static const string kStrGetUUID ;
		static const string kStrServer ;	

		static const int kError			= -1;
		static const int kNoCommand		= 0;
		static const int kStart 		= 1;
		static const int kStop 			= 2;
		static const int kStatus		= 3;
		static const int kGetUUID		= 4;
	};



} // namespace common

#endif //COMMON
