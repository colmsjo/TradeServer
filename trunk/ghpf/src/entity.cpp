/*****************************************************************************
 *
 * @file 		entity.cpp
 * @brief		Implements the Entities and Entity classes
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ******************************************************************************/



#include "entity.hpp"


/**
 * This function performs the actual work of translating the message
 * into something can can be sent using MPI
 *
 * 
 */ 	
template<class Archive> 
void entity::EntityImpl::serialize(Archive & ar, const unsigned int version) {
	std::map<string, attribute_type>::const_iterator cii;

	std::map<string, attribute_type>* names_and_types = entities_->getNamesAndTypes();

	for(cii  = names_and_types->begin(); 
	    cii != names_and_types->end(); 
	    cii++) {
		
		string attribute_name = cii->first;
		ar & attribute_name;			// the name
		switch(cii->second) {		    // the type
			case _CHAR:
				ar & entities_->getChar(uuid_, attribute_name);
				break;

			case _LONG:
				ar & entities_->getLong(uuid_, attribute_name);
				break;
			
			case _BOOL:
				ar & entities_->getBool(uuid_, attribute_name);
				break;

			case _DOUBLE:
				ar & entities_->getDouble(uuid_, attribute_name);
				break;

			case _WSTRING:
				ar & entities_->getWstring(uuid_, attribute_name);
				break;
			 
		} // switch
	} // for
}

/**
 * This function performs the actual work of translating the message
 * into something can can be sent using MPI
 *
 * \todo: Not sure that this function is needed/good idea to have
 */ 	
template<class Archive> 
void entity::EntitiesImpl::serialize(Archive & ar, const unsigned int version) {
	
	std::map<string, char>::const_iterator char_iterator;
	for(char_iterator=chars_.begin(); char_iterator!=chars_.end(); char_iterator++)
		ar & *char_iterator;
	 
	std::map<string, long>::const_iterator long_iterator;
	for(long_iterator=longs_.begin(); long_iterator!=longs_.end(); long_iterator++)
		ar & *long_iterator;

	std::map<string, bool>::const_iterator bool_iterator;
	for(bool_iterator=bools_.begin(); bool_iterator!=bools_.end(); bool_iterator++)
		ar & *bool_iterator;

	std::map<string, double>::const_iterator double_iterator;
	for(double_iterator=doubles_.begin(); double_iterator!=doubles_.end(); double_iterator++)
		ar & *double_iterator;

	std::map<string, wstring>::const_iterator wstring_iterator;
	for(wstring_iterator=wstrings_.begin(); wstring_iterator!=wstrings_.end(); wstring_iterator++)
		ar & *wstring_iterator;
}


entity::Entities* entity::Entities::create(std::string entity_name, std::map<string, entity::attribute_type>* attribute_names_and_types) {
	entity::EntitiesImpl* entities = new entity::EntitiesImpl(entity_name, attribute_names_and_types);
	
	return entities;
}


/**
 *  Constructur
 *
 */ 	
entity::EntitiesImpl::EntitiesImpl(std::string entity_name, std::map<string, entity::attribute_type>* attribute_names_and_types) {
	entities_name_ 				= entity_name;
	attribute_names_and_types_ 	= attribute_names_and_types;
}
		

/**
 *  getNamesAndTypes
 *
 */ 	
std::map<string, entity::attribute_type>* entity::EntitiesImpl::getNamesAndTypes(void) {
	return attribute_names_and_types_;
}


/**
 *  Create a new entity
 *
 */ 	
ghpf::uuid* entity::EntitiesImpl::createEntity(void) {
	return ghpf::uuid::create();
}


/**
 *  Create a new entity with a specific identifier
 *
 */ 	
void entity::EntitiesImpl::createEntity(ghpf::uuid* u) {
	// Nothing needs to be done
	// \todo: add a check that this uuid doesn't already exist
}


void entity::EntitiesImpl::check_type(ghpf::uuid* u, string attribute_name, entity::attribute_type expected_type) {
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;

	// \todo: Need to redo, no exceptions will be thrown. Should use map::count instead!
	switch(expected_type) {
		case _CHAR:
			try {
				chars_[ss.str()];
			}
			catch(exception &e) {
				string s = string("Entities::check_type - excepted char:") + e.what();
				throw ghpf::Exception(s);	
			}
			break;
		
		case _LONG:
			try {
				longs_[ss.str()];
			}
			catch(exception &e) {
				string s = string("Entities::check_type - excepted long:") + e.what();
				throw ghpf::Exception(s);	
			}
			break;
			
		case _BOOL:
			try {
				bools_[ss.str()];
			}
			catch(exception &e) {
				string s = string("Entities::check_type - excepted bool:") + e.what();
				throw ghpf::Exception(s);	
			}
			break;
		
		case _DOUBLE:
			try {
				doubles_[ss.str()];
			}
			catch(exception &e) {
				string s = string("Entities::check_type - excepted double:") + e.what();
				throw ghpf::Exception(s);	
			}
			break;
			
		case _WSTRING:
			try {
				wstrings_[ss.str()];
			}
			catch(exception &e) {
				string s = string("Entities::check_type - excepted wstring:") + e.what();
				throw ghpf::Exception(s);	
			}
			break;	
		
	}
}


/**
 *  Set the value of a attribute of a specific type. Nothing is saved until commit is called.
 *
 * Five types are supported: char, long (use for short and int also), bool, 
 * double (use also for float) and wstring (use also for string).
 * wstring are used in order to represent unicode strings on windows and unix correctly
 *
 * Write changes to the queue.
 *
 */
 
void entity::EntitiesImpl::set(ghpf::uuid* u, string attribute_name, char attribute_value) {
	check_type(u, attribute_name, _CHAR);
	queue_.write(new EntityChange(u, attribute_name, attribute_value));
}

void entity::EntitiesImpl::set(ghpf::uuid* u, string attribute_name, long attribute_value) {
	check_type(u, attribute_name, _LONG);
	queue_.write(new EntityChange(u, attribute_name, attribute_value));
}

void entity::EntitiesImpl::set(ghpf::uuid* u, string attribute_name, bool attribute_value) {
	check_type(u, attribute_name, _BOOL);
	queue_.write(new EntityChange(u, attribute_name, attribute_value));
}

void entity::EntitiesImpl::set(ghpf::uuid* u, string attribute_name, double attribute_value) {
	check_type(u, attribute_name, _DOUBLE);
	queue_.write(new EntityChange(u, attribute_name, attribute_value));
}

void entity::EntitiesImpl::set(ghpf::uuid* u, string attribute_name, std::wstring attribute_value) {
	check_type(u, attribute_name, _WSTRING);
	queue_.write(new EntityChange(u, attribute_name, attribute_value));
}

/**
 *  Get the value of a attribute of a specific type
 *
 * Five types are supported: char, long (use for short and int also), bool, 
 * double (use also for float) and wstring (use also for string).
 * wstring are used in order to represent unicode strings on windows and unix correctly
 *
 *
 */ 
void entity::EntitiesImpl::check_type(size_t i, string key) {
	if( i == 0 ) {
		string s = string("Cannot find key:") + key;
		throw ghpf::Exception(s);
	}
}
  	
char    entity::EntitiesImpl::getChar(ghpf::uuid* u, string attribute_name) {
	check_type(u, attribute_name, _CHAR);
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;
	check_type( chars_.count(ss.str()), ss.str() );
	return chars_[ss.str()];
}

long    entity::EntitiesImpl::getLong(ghpf::uuid* u, string attribute_name) {
	check_type(u, attribute_name, _LONG);
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;
	check_type( longs_.count(ss.str()), ss.str() );
	return longs_[ss.str()];
}

bool    entity::EntitiesImpl::getBool(ghpf::uuid* u, string attribute_name) {
	check_type(u, attribute_name, _BOOL);
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;
	check_type( bools_.count(ss.str()), ss.str() );
 	return bools_[ss.str()];
}

double  entity::EntitiesImpl::getDouble(ghpf::uuid* u, string attribute_name) {
	check_type(u, attribute_name, _DOUBLE);
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;
	check_type( doubles_.count(ss.str()), ss.str() );
	return doubles_[ss.str()];
}

std::wstring entity::EntitiesImpl::getWstring(ghpf::uuid* u, string attribute_name) {
	check_type(u, attribute_name, _WSTRING);
	std::stringstream ss;
	ss << u->str() << "_" << attribute_name;
	check_type( wstrings_.count(ss.str()), ss.str() );
	return wstrings_[ss.str()];
}


/**
 *  Get a Entity object of a specific entity
 *
 *
 */ 	
entity::Entity* entity::EntitiesImpl::getEntity(ghpf::uuid* u) {
	return new EntityImpl(u, (entity::Entities*) this);
}

/**
 *  Save changes
 *
 *
 */ 	
void entity::EntitiesImpl::commit(void) {
	EntityChange *entity_change = queue_.read();
	
	while(entity_change) {
		
		std::stringstream ss;
		ss << entity_change->uuid_->str() << "_" << entity_change->attribute_name_;
				
		switch(entity_change->attribute_type_) {
			case _CHAR:
				chars_[ss.str()] = entity_change->char_;
				break;	

			case _LONG:
				longs_[ss.str()] = entity_change->long_;
				break;	

			case _BOOL:
				bools_[ss.str()] = entity_change->bool_;
				break;	

			case _DOUBLE:
				doubles_[ss.str()] = entity_change->double_;
				break;	

			case _WSTRING:
				wstrings_[ss.str()] = entity_change->wstring_;
				break;	
		} // switch
		
		// Deallocate the change objectr
		delete entity_change;
		
		// Get the next change object
		entity_change = queue_.read();
	} // while
}

/**
 *  Cancel all changes performed since the last commit.
 *
 *
 */ 	
void entity::EntitiesImpl::rollback(void) {
	EntityChange *entity_change = queue_.read();

	// just throw away all changes	
	while(entity_change) {
		entity_change = queue_.read();
	} // while
}
