/*****************************************************************************
 *
 * @file 		entity.cpp
 * @brief		Implements the Entities and Entity classes
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ******************************************************************************/


#ifndef ENTITY
#define ENTITY


// All modules must use the common module
#include "common.hpp"


/// entity
/**
 * Represents entities
 *
 * Currently support five types:
 * + char
 * + long - use also for short and int
 * + bool
 * + double - use also for float
 * + wstring - use also for strings
 *
 * It will be possbile to persist objects using two-phase commit in the future. Currently 
 * is the comit logic restriced to the objects in the memory. 
 *
 * There is also planned support for vectors of each types.
 *
 * \todo: Add support for date and time. Implement a new ghpf::DateTime class which encapsulates
 * boost.Date_Time
 * \todo: Should break out EntityChange from the Entities class and create a Transaction class
 *        that works cross Entities objects. This Transaction should instead be connected to 
 *        a sesion.
 * \todo: Implement snapshots. These should record all commits from the start of the snapshot
 *        for a specific Entities object.
 * \todo: Implement authorizations. For instannce allow/deny for (uuid, attribute_name, role)
 */ 
namespace entity {

	// To be implemented in the future: _CHAR_VECTOR, _LONG_VECTOR, _DOUBLE_VECTOR, _WSTRING_VECTOR
	typedef enum {_CHAR, _LONG, _BOOL, _DOUBLE, _WSTRING} attribute_type;


	/// Entity class
	/**
	 * Entity
	 * 
	 * Represents one entity.
	 *
	 */
	class Entity {
		// Has no public methods
	};


	/// Entities class
	/**
	 * Entities - contains a set of small "objects" that are called entities. These can be
	 * sent using REST and MPI and will also be possible to store to a presistent storage 
	 * using two-phase-commit.
	 * 
	 *  \todo: move to ghpf namespace
	 */
	class Entities {
	public:
				
		/// create
		/**
		 *  Create a Entities object with strong typing.
		 *
		 */ 	
		static Entities* create(string entity_name, std::map<string, attribute_type>* attribute_names_and_types);
		

		/// getNamesAndTypes
		/**
		 *  Get the definition of the object.
		 *
		 * \return: a map with the naes and types 
		 */ 	
		virtual std::map<string, attribute_type>* getNamesAndTypes(void) = 0;

		/// Destructur
		/**
		 *  Destructur
		 *
		 */ 	
		virtual ~Entities() {};

		/// createEntity
		/**
		 *  Create a new entity
		 *
		 * \returns: the identification of the new entity
		 */ 	
		virtual ghpf::uuid* createEntity(void) = 0;

		/// createEntity
		/**
		 *  Create a new entity with a specific identifier
		 *
		 * @param uuid - the identifier of the new object
		 */ 	
		virtual void createEntity(ghpf::uuid* u) = 0;

		/// set
		/**
		 *  Set the value of a attribute of a specific type. Nothing is saved until commit is called.
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 * @param attribute_value - the value of the attribute
		 *
		 */ 	
		virtual void set(ghpf::uuid* u, string attribute_name, char attribute_value) = 0;
		virtual void set(ghpf::uuid* u, string attribute_name, long attribute_value) = 0;
		virtual void set(ghpf::uuid* u, string attribute_name, bool attribute_value) = 0;
		virtual void set(ghpf::uuid* u, string attribute_name, double attribute_value) = 0;
		virtual void set(ghpf::uuid* u, string attribute_name, std::wstring attribute_value) = 0;

		/// get
		/**
		 *  Get the value of a attribute of a specific type
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 * \returns the value of the attribute
		 *
		 */ 	
		virtual char    getChar(ghpf::uuid* u, string attribute_name) = 0;
		virtual long    getLong(ghpf::uuid* u, string attribute_name) = 0;
		virtual bool    getBool(ghpf::uuid* u, string attribute_name) = 0;
		virtual double  getDouble(ghpf::uuid* u, string attribute_name) = 0;
		virtual std::wstring getWstring(ghpf::uuid* u, string attribute_name) = 0;

		
		/// get_vector
		/**
		 *  Get a pointer the vector for a attribute.
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 *	
		 * NOT IMPLEMENTED YET
		 *
		virtual vector<char>*    get_vector(uuid* u, string attribute_name) = 0;		
		virtual vector<long>*    get_vector(uuid* u, string attribute_name) = 0;		
		virtual vector<bool>*    get_vector(uuid* u, string attribute_name) = 0;		
		virtual vector<double>*  get_vector(uuid* u, string attribute_name) = 0;		
		virtual vector<wstring>* get_vector(uuid* u, string attribute_name) = 0;	*/
		
		/// getEntity
		/**
		 *  Get a Entity object of a specific entity
		 *
		 * @param u - the identifier for the attribute
		 *
		 * \returns a Entity object
		 *
		 */ 	
		virtual Entity* getEntity(ghpf::uuid* u) = 0;
		
		/// commit
		/**
		 *  Save changes
		 *
		 *
		 */ 	
		virtual void commit(void) = 0;

		/// rollback
		/**
		 *  Cancel all changes performed since the last commit.
		 *
		 *
		 */ 	
		virtual void rollback(void) = 0;
		
	};


	/// EntityImpl class
	/**
	 * EntityImpl
	 * 
	 * Represents one entity. 
	 *
	 */
	class EntityImpl : public Entity {
		// Boost need access to private funtions in order to send messages
		friend class boost::serialization::access;

	 private:
	
		/// serialize
		/**
		 * This function performs the actual work of translating the message
		 * into something can can be sent using MPI
		 *
		 * @param ar 
		 * @param version
		 *
		 */ 	
		template<class Archive> void serialize(Archive & ar, const unsigned int version);

		ghpf::uuid* uuid_;
		Entities* entities_;
	
	public:
		EntityImpl(ghpf::uuid* u, Entities* e) : uuid_(u), entities_(e) {};
		virtual ~EntityImpl() {};
	};


	class EntityChange {
	public:
		ghpf::uuid*      uuid_;
		attribute_type 	 attribute_type_;
		string			 attribute_name_;
		char             char_;
		long             long_;
		bool             bool_;
		double           double_;
		std::wstring     wstring_;
		
		EntityChange(ghpf::uuid* u, string	attr_name, char c)         
					: uuid_(u), attribute_type_(_CHAR),  attribute_name_(attr_name),  char_( c )  {};
					
		EntityChange(ghpf::uuid* u, string	attr_name, long l)         
					: uuid_(u), attribute_type_(_LONG),   attribute_name_(attr_name),  long_(l)    {};
		
		EntityChange(ghpf::uuid* u, string	attr_name, bool b)         
					: uuid_(u), attribute_type_(_BOOL),  attribute_name_(attr_name),   bool_(b)    {};
		
		EntityChange(ghpf::uuid* u,string	attr_name, double d)       
					: uuid_(u), attribute_type_(_DOUBLE), attribute_name_(attr_name),  double_(d)  {};
		
		EntityChange(ghpf::uuid* u,string	attr_name, std::wstring w) 
					: uuid_(u), attribute_type_(_WSTRING), attribute_name_(attr_name), wstring_(w) {};	
	};

	/// EntitiesImpl class
	/**
	 * EntitiesImpl
	 * 
	 *  
	 */
	class EntitiesImpl : public Entities {
		// EntityImpl needs access to private attributes
		friend class EntityImpl;
		friend class boost::serialization::access;

	public:
		/// Constructur
		/**
		 *  Constructur
		 *
		 */ 	
		EntitiesImpl(string entity_name, std::map<string, attribute_type>* attribute_names_and_types );
				
		/// getNamesAndTypes
		/**
		 *  Get the definition of the object.
		 *
		 * \return: a map with the naes and types 
		 */ 	
		std::map<string, attribute_type>* getNamesAndTypes(void);

		template<class Archive> void serialize(Archive & ar, const unsigned int version);


		/// Destructur
		/**
		 *  Destructur
		 *
		 */ 	
		virtual ~EntitiesImpl() {};

		/// createEntity
		/**
		 *  Create a new entity
		 *
		 * \returns: the identification of the new entity
		 */ 	
		ghpf::uuid* createEntity(void);

		/// createEntity
		/**
		 *  Create a new entity with a specific identifier
		 *
		 * @param uuid - the identifier of the new object
		 */ 	
		void createEntity(ghpf::uuid* u);

		/// set
		/**
		 *  Set the value of a attribute of a specific type. Nothing is saved until commit is called.
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 * @param attribute_value - the value of the attribute
		 *
		 */ 	
		void set(ghpf::uuid* u, string attribute_name, char attribute_value);
		void set(ghpf::uuid* u, string attribute_name, long attribute_value);
		void set(ghpf::uuid* u, string attribute_name, bool attribute_value);
		void set(ghpf::uuid* u, string attribute_name, double attribute_value);
		void set(ghpf::uuid* u, string attribute_name, std::wstring attribute_value);

		/// get
		/**
		 *  Get the value of a attribute of a specific type
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 * \returns the value of the attribute
		 *
		 */ 	
		char         getChar(ghpf::uuid* u, string attribute_name);
		long         getLong(ghpf::uuid* u, string attribute_name);
		bool         getBool(ghpf::uuid* u, string attribute_name);
		double       getDouble(ghpf::uuid* u, string attribute_name);
		std::wstring getWstring(ghpf::uuid* u, string attribute_name);

		
		/// get_vector
		/**
		 *  Get a pointer the vector for a attribute.
		 *
		 * Five types are supported: char, long (use for short and int also), bool, 
		 * double (use also for float) and wstring (use also for string).
		 * wstring are used in order to represent unicode strings on windows and unix correctly
		 *
		 * @param u - the identifier for the attribute
		 * @param attriute_name - the name of the attribute
		 *	
		 * NOT IMPLEMENTED YET
		 *
		vector<char>*    get_vector(uuid* u, string attribute_name);		
		vector<long>*    get_vector(uuid* u, string attribute_name);		
		vector<bool>*    get_vector(uuid* u, string attribute_name);		
		vector<double>*  get_vector(uuid* u, string attribute_name);		
		vector<wstring>* get_vector(uuid* u, string attribute_name);	*/
		
		/// getEntity
		/**
		 *  Get a Entity object of a specific entity
		 *
		 * @param u - the identifier for the attribute
		 *
		 * \returns an Entity object
		 *
		 */ 	
		Entity* getEntity(ghpf::uuid* u);
		
		/// commit
		/**
		 *  Save changes
		 *
		 *
		 */ 	
		void commit(void);

		/// rollback
		/**
		 *  Cancel all changes performed since the last commit.
		 *
		 *
		 */ 	
		void rollback(void);
		
	private:
		void check_type(ghpf::uuid* u, string attribute_name, entity::attribute_type expected_type);
		void check_type(size_t i, string key);
		
		common::FIFOQueue<EntityChange>   queue_;
		std::string					      entities_name_;
		std::map<string, attribute_type>* attribute_names_and_types_;
		
		//
		// the key in the maps are uuid concatenated with the attribute name
		//
		std::map<std::string, char>         chars_;
		std::map<std::string, long>         longs_;
		std::map<std::string, bool>         bools_;
		std::map<std::string, double>       doubles_;
		std::map<std::string, std::wstring> wstrings_;		
	};


} // namespace entity

#endif // ENTITY