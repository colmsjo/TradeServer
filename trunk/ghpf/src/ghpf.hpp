/*****************************************************************************
 *
 * @file 		ghpf.hpp
 * @brief		Include for applications/modules intended for the application server
 * @author      Jonas Colmsjö
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ******************************************************************************/


#ifndef GHPF
#define GHPF

#ifdef HAVE_CONFIG_H
#include "ghpf_config.h"			
#endif


#include <string>
#include <exception>


using namespace std;

/// ghpf
/**
 * Contains classes used by modules/applications using the GHPF framework. 
 * All logging and management of exceptions should be performed through these classes.
 *
 * Also abstracts the methods used to implement communication and processes from
 * the framework. The ProcessInitiator class controls the creation of objects
 * within each process. Subclass this class in order to implement new 
 * ActiveObejcts. The Communicator class is used in order to communicate with
 * objects in other processes wothj Message obejcts. The Context class is a 
 * singleton (only one instance can be created) theat contains references the 
 * Communicator etc.
 *
 *
 */
namespace ghpf {


	// Constants used in the framework
	static const int kRESTPort		= 8081;
	  

	// forward declarations
	class CommandMessage;

	
	/// uuid_class class
	/**
	 * uuid_class, manages Universal Unique Identifiers
	 * 
	 *  
	 *
	 */	 
	class uuid {
	public:
	    static uuid* create();
	    static uuid* create(string uuid_string);
	    virtual ~uuid() {};
	    virtual string str(void) = 0;
	};

	
	/// Exception generic exception class
	/**
	 * A generic exception class. Inherit from this class for spcecialized exceptions
	 *
	 *
	 * Example on how to catch exceptioins, NOTE THE & FOLLOWING catch (exception&)
	 *
	 * int main () {
	 *  try
	 *  {
	 *    throw myex;
	 *  }
	 * catch (exception& e)
	 * {
	 *   cout << e.what() << endl;
	 * }
	 *  return 0;
	 * }
	 *
	 *
	 *
	 *
	 */

	class Exception : public exception {
	public:
		Exception(string s1, string s2) {
			error_msg_ = s1 + ":" + s2;
		}

		Exception(string s) {
			error_msg_ = s;
		}
		
		~Exception() throw() {};
		
		const char* what() {
			return 	error_msg_.c_str();
		}
		
		string getErrorMsg() throw() {
			return error_msg_;
		}
		
	private:
		string error_msg_;
	};


	/// MPIMessage class
	/**
	 * MPIMessage, virtual class to be implemented in MPI etc.
	 *
	 * For classes that it should be possible to send using mpi (mpi::communicator::send/recv),
	 * a serialization function needs to be implemented.
	 *
	 *  // Boost need access to private funtions in order to send/receive messages
	 *  friend class boost::serialization::access;
	 *  template<class Archive> void serialize(Archive & ar, const unsigned int version) { …};
	 *
	 * For classes consisting that sends/receives messages of fixed size is optimization possible.
	 * Non template classes uses the macro BOOST_IS_MPI_DATATYPE(class) to enable this optimization.
	 *
	 * see http://www.boost.org/doc/libs/1_47_0/doc/html/mpi.html for details on boost and MPI communication.
	 */
	 class MPIMessage {
	 public:	
	
		/// create
		/**
		 *  Factory Method for creating parsers
		 *
		 * 
		 */ 	
		static MPIMessage* create(); 
		static MPIMessage* create(ghpf::uuid* sender, ghpf::uuid* receiver); 

		/// ~MPIMessage
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~MPIMessage() {};

		/// getCommandString
		/**
		 *  Get the command sent in the message
		 *
		 * \returns: The string containg the command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		virtual string getCommand() = 0;

		/// setCommand
		/**
		 *  Set the command sent in the message
		 *
		 * @param command The command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		virtual void setCommand(std::string command) = 0;
		
		/// getReceiver
		/**
		 *  Get the UUID of the receiver
		 *
		 * \return the UUID of the receiver
		 */ 	
		virtual ghpf::uuid* getReceiver(void) = 0;		
	 	
		/// getSender
		/**
		 *  Get the UUID of the sending ActiveObject
		 *
		 * \return the UUID of the receiver
		 */ 	
		virtual ghpf::uuid* getSender(void) = 0;
			
	};

	class RESTMessage {
	public:
		
		/// create
		/**
		 *  Factory Method for creating parsers
		 *
		 * @param sender ID of the sender
		 * @param sender ID of the receiver
		 * 
		 */ 	
		static RESTMessage* create(ghpf::uuid* sender, ghpf::uuid* receiver); 
		static RESTMessage* create(); 
		
		/// ~RESTMessage
		/**
		 *  Destructor
		 *
		 * 
		 */ 		
	 	virtual ~RESTMessage() {};

	 	/// addParamVal
	 	/**
	 	 * Add parameter and value pairs
		 *
	 	 * @param param key
	 	 * @param val the value
	 	 */
		virtual void addParamVal(string param, string val) = 0;

		/// getUrl
		/**
		 *  Get the command sent in the message
		 *
		 * \returns: The string containg the command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		virtual string getURL() = 0;

		/// getShortURL
		/**
		 *  Get the command sent in the message
		 *
		 * \returns: The string containg the command, typically command?param1=val1;param2=val2...
		 */ 	
		virtual string getShortURL() = 0;

		/// setCommand
		/**
		 *  Set the command sent in the message
		 *
		 * @param command The command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		virtual void setCommand(std::string command) = 0;
			 	
		/// getReceiver
		/**
		 *  Get the UUID of the receiver
		 *
		 * \return the UUID of the receiver
		 */ 	
		virtual ghpf::uuid* getReceiver(void) = 0;		
	 	
		/// getSender
		/**
		 *  Get the UUID of the sending ActiveObject
		 *
		 * \return the UUID of the receiver
		 */ 	
		virtual ghpf::uuid* getSender(void) = 0;
	};
	

	/// ActiveObject class
	/**
	 * ActiveObject, base class for all active objects
	 * 
	 *  
	 */
	 class ActiveObject {
	 public:	
	
		/// ActiveObject
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	 	ActiveObject();
	 	
		/// ~ActiveClass
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~ActiveObject();
	 	
		/// getUUID
		/**
		 *  Get the UUID of the ActiveObject
	 	 *
	 	 * \return uuid of the currenbt object
		 * 
		 */ 	
	 	virtual ghpf::uuid* getUUID(void) = 0; 

		/// init
		/**
		 *  Initilize the ActiveObject
		 *
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	virtual void init(void) = 0; 

		/// receive
		/**
		 *  Receive MPI message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	virtual void receive(MPIMessage* msg) = 0; 

		/// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
	 	virtual void receive(RESTMessage* msg) = 0; 
	};
	
	
	/// Communicator class
	/**
	 * Communicator, virtual class to be implemented in MPI etc.
	 * 
	 *  
	 */
	 class Communicator {
	 public:	
	
		/// create
		/**
		 *  create a Communicator instance 
		 *
		 * 
		 */ 	
		static Communicator* create(); 
	 	
		/// init
		/**
		 *  Initialize the communicator without MPI.
		 *
		 * 
		 */ 	
		virtual void init(void) = 0;

		/// init
		/**
		 *  Initialize the context with MPI.
		 *
	 	 * @param argc Number of arguments
	 	 * @param argv array of strings containing arguments
		 * 
		 */ 	
		virtual void init(int argc, char* argv[]) = 0;

		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		virtual void run(void) = 0; 

		/// getRank
		/**
		 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
		 *
	 	 * \return int The rank if this process 
		 * 
		 */ 	
		virtual int getRank() = 0;

		/// getNumProcesses
		/**
		 *  Return the number of processes
		 *
	 	 * \return int the number of processes in the MPI world
		 * 
		 */ 	
		virtual int getNumProcesses() = 0;

		/// ~Communicator
		/**
		 *  ~Destructor
		 *
		 * 
		 */ 		
		virtual ~Communicator() {};
	
	 	/// send
	 	/**
	 	 * Send 
	 	 *
	 	 * @param msg the Message to send using MPI
	 	 *
	 	 * \return void 
	 	 */
	 	virtual void send(int rank_receiver, MPIMessage* msg) = 0;
	
	 	/// receive
	 	/**
	 	 * Receive a message from another process using MPI
	 	 *
	 	 * @param msg the Message to receive
	 	 *
	 	 * \return void 
	 	 */
	 	virtual void receive(MPIMessage* msg) = 0;
	
	 	/// send
	 	/**
	 	 * Send 
	 	 *
	 	 * @param host receiving host
	 	 * @param port the port at the receving host
	 	 * @param msg the Message to send using REST
	 	 *
	 	 * \return void 
	 	 */
	 	virtual void send(string host, int port, RESTMessage* msg) = 0;
	
	 	/// receive
	 	/**
	 	 * Receive a message from another process using REST
	 	 *
	 	 * @param msg the Message to receive
	 	 *
	 	 * \return void 
	 	 */
	 	virtual void receive(RESTMessage* msg) = 0;

	 	/// registerActiveObject
	 	/**
	 	 * Register an ActiveObject as a receiver 
	 	 *
	 	 * @param obj The ActiveObject to register as a receiver
	 	 * @param uuid The UUID of the ActiveObject
	 	 *
	 	 * \return void 
	 	 */
	 	virtual void registerActiveObject(ActiveObject* obj, ghpf::uuid* uuid) = 0;
	};
	


	/// Context class
	/**
	 * Context, virtual class to be implemented in MPI etc.
	 * 
	 * Implemented as a Singleton, 
	 * os-facade.cpp we need Context* Context::m_pInstance = NULL;
	 *
	 * \todo: should move the implementation parts outside the header file
	 * \todo: need to split into Context and ContextImpl, can then make MPIProcess part of ContextImpl
	 */
	 class Context {
	 public:	
	
		/// Instance
		/**
		 *  Create an instance of this Singleton class.
		 *
	 	 *
	 	 * \return Contecxt*
		 * 
		 */ 	
		static Context* Instance();
	
		/// init
		/**
		 *  Initialize the context and MPI.
		 *
	 	 * @param argc Number of arguments
	 	 * @param argv array of strings containing arguments
		 * 
		 */ 	
		virtual void init(int argc, char* argv[]) = 0;
	
		/// init
		/**
		 *  Initialize the context without MPI.
		 *
		 * 
		 */ 	
		virtual void init(void) = 0;
	

		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		virtual void run(void) = 0; 

		/// getRank
		/**
		 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
		 *
	 	 * \return int The rank if this process 
		 * 
		 */ 	
		//virtual int getRank() = 0;
	
		/// getCommunicator
		/**
		 * Return the Communicator for this process. The Communicatitor should be created
		 * by the ProcessInitiator which is instanciated at startup. 
		 *
		 */
		virtual Communicator* getCommunicator() = 0;
	
	
		/// setExitProcess
		/**
		 *  Set the flag that indictes that the proces should be exited and the assiciated exit code.
		 *
	 	 * @param exit_code The rank if this process 
		 * 
		 */ 	
		virtual void setExitProcess(int exit_code) = 0;

		/// getExitCode
		/**
		 *  Get the exit code for the process. Typically used once exitProcess has returned true.
		 *
	 	 * \returns exit_code
		 * 
		 */ 	
		virtual int getExitCode(void) = 0;

		/// exitProcess
		/**
		 *  Returns true if the process should be terminated. Typically due to a TERM singal.
		 *
	 	 * \return exit_code  
		 * 
		 */ 	
		virtual bool exitProcess() = 0;

		/// destroy
		/**
		 *  Tear down everything.
		 *
		 */ 	
		virtual void destroy() = 0;
		
		 /// ~Context
		 /**
		  *  Destructor
		  *
		  * 
		  */
		virtual ~Context() {};
		 
	};
		
	
	/// ProcessInitiator class
	/**
	 * ProcessInitiator, create the active classes in each process
	 * 
	 * The implementation layer for the process and communiacation framework, such as
	 * MPI should implement one class that contains a ProcessInitiator. Each
	 * node will then have one ProcessInitiator that then creates the active objects
	 * for in each node. The rank for the node, as represented by the context,
	 * will be used in order to determine which objects to create.
	 *  
	 */
	 class ProcessInitiator {
	 public:	
	
		/// ProcessInitiator
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	 	ProcessInitiator();
	
		/// ProcessInitiator
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~ProcessInitiator();
	 	
	 	/// init()
	 	/**
	 	 * Initialize the process and create the Active Object for this particular 
	 	 * process.
	 	 *
	 	 */
	 	virtual void init() {};
	
	 protected:
		/// Each ProcessInitiator creates one active object and registers it 
		/// as the receiver for the communicator
		ActiveObject* active_object_;
	};
	
	
	// the types of the class factories. Used when dynamicaly loading classes from libraries (currently not used).
	typedef ProcessInitiator* 	create_t();
	typedef void 				destroy_t(ProcessInitiator*);



	/******************************* XMLParser ******************************************/

	/// SimpleXMLParser class
	/**
	 * SimpleXMLParser, encapsulates the XML Parser used, currently xmlParser from Business-Insight
	 * 
	 * Only simple one-level XML Tree are currently supported. Use the xmlParser  
	 * directly if more advanced features are necessary.
	 *
	 * Exmaple of XML structure.
	 * <xml>
	 *		<tag_1>text text</tag_1>
	 *		<tag_2>text text</tag_2>
	 *		<tag_3 param="..." value="..."/>
	 *		...
	 * </xml>
	 *
	 *
	 */
	 class SimpleXMLParser {
	 public:	

		// Constants	
		static const string kNameTopNode;
		static const string kNameKeyAttribute;
		static const string kNameValueAttribute;

		/// create
		/**
		 *  Factory Method for creating parsers
		 *
		 * 
		 */ 	
		static SimpleXMLParser* create(); 
	 	
		/// parse
		/**
		 *  Parse string with XML
		 *
		 * @param xml the string containing the XML to parse
		 * 
		 */ 	
		virtual void parse(string xml) = 0;

		/// getChildNodeText
		/**
		 * getChildNodeText find the child node with a specific tag/name
		 *
		 * @param node_name the tag/name of the child to find, <tag>...
		 * \returns the text of the current node <tag>text</tag>
		 * 
		 */ 	
		virtual string getChildNodeText(string node_name) = 0;
		virtual string getChildNodeText(string node_name, int index) = 0;
		 		 
		/// numChildNodes
		/**
		 * numChildNodes
		 *
		 * @param node_name the tag/name of the child to count
		 * \returns returns the number of child nodes of a specific tag/name
		 */ 	
		virtual int numNodes(string node_name) = 0;

		/// getAttributeNameOfCurrentNode
		/**
		 * getAttributeNameOfCurrentNode
		 *
		 * @param index the index of the attribute to get
		 * \returns returns the attribute name
		 */ 	
		virtual string getAttributeNameOfCurrentNode(int index) = 0;

		/// getAttributeValueOfCurrentNode
		/**
		 * getAttributeValueOfCurrentNode
		 *
		 * @param index the index of the attribute to get
		 * \returns returns the attribute value
		 */ 	
		virtual string getAttributeValueOfCurrentNode(int index) = 0;

		/// createTopNode
		/**
		 * createTopNode Creates a new top node of a XML Tree. The existing tree will be deleted
		 *               if one exists.
		 *
		 */ 	
		virtual void createTopNode(void) = 0;
		 
		/// addNode
		/**
		 * addNode Add a node in the XML tree
		 *
		 * @param node_name
		 */ 	
		virtual void addNode(string node_name) = 0;

		/// addTextToCurrentNode
		/**
		 * addTextToCurrentNode Add text to the current node, <tag>text</tag>
		 *
		 * @param text the text to add
		 */ 	
		virtual void addTextToCurrentNode(string text) = 0;

		/// addAttributeToCurrentNode
		/**
		 * addAttributeToCurrentNode Add text to the current node, <tag>text</tag>
		 *
		 * @param key the key of the paramater
		 * @param value the value of the paramater
		 *
		 */ 	
		virtual void addAttributeToCurrentNode(string key, string value) = 0;

		/// createXML
		/**
		 * createXML Create XML string from the current XML Tree
		 *
		 *
		 */ 	
		virtual string createXML(void) = 0;

		virtual ~SimpleXMLParser() {};

	};
	
	/// Log
	/**
	 * Wrapper class for the Boost loggin framework. Should always be used
	 *
	 * Using the syslog levels:
	 * emerg   – system is or will be unusable if situation is not resolved
     * alert   – immediate action required 
     * crit    – critical situations
     * error   - error conditions
     * warning – recoverable errors 
     * notice  – unusual situation that merits investigation; a significant 
     *           event that is typically part of normal day-to-day operation
     * info    – informational messages
     * debug   – verbose data for debugging
	 *
	 */
	class Log {
	public:
		/**
		 * Initialize logger
		 *
		 * @param log_file full path to log file
		 *
		 * \return void 
		 */
		static void init(string log_file);
		//static void init2(std::string log_file);
		
		/**
		 * Tear down logger 
		 *
		 *
		 * \return void 
		 */
		static void destroy();
		
		/**
		 * Log debug message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_DEBUG(string s1, string s2);
		
		/**
		 * Log info message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_INFORMATIONAL(string s1, string s2);
		
		/**
		 * Log notice message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_NOTICE(string s1, string s2);
		
		/**
		 * Log warning message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_WARNING(string s1, string s2);
		
		/**
		 * Log error message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_ERROR(string s1, string s2);
		
		/**
		 * Log critical message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_CRITICAL(string s1, string s2);
		
		/**
		 * Log alert message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */
		static void log_ALERT(string s1, string s2);

		/**
		 * Log emergency message
		 *
		 * @param s1 Header
		 * @param s2 Message to log
		 *
		 * \return void 
		 */		
		static void log_EMERGENCY(string s1, string s2);
		
		static ofstream 			log_file_;
		
	private:		
		static void log(string s);
	
	};



} //namespace ghpf

/// _ghpf
/**
 * Global funtions
 *
 * \todo: move implementation to os_facade.
 */
class _ghpf {
public:

	/// init
	/**
	 * init, initialize framework and MPI
	 *
	 * @param argc number of arguments (typically the same as in the main function)
	 * @param argv pointer to array of strings containg arguments (typically the same as in the main function)
	 * @param log_file path to the log file for the application
	 *
	 */	 
	static void init(int argc, char* argv[], string log_file);
		
	/// init
	/**
	 * init, initialize framework without MPI
	 *
	 * @param log_file path to the log file for the application
	 *
	 */	 
	static void init(string log_file);

	/// exit
	/**
	 * exit, tear down everything and exit application
	 * 
	 * @param exit_code exit code for the process
	 */	 
	static void exit(bool exit_process);

	/// daemonize
	/**
	 *  Turn the current process into a daemon.
	 *
	 */ 	
	static pid_t daemonize(void);	
	
};


#endif //GHPF
