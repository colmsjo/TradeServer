/****************************************************************************
 *
 * @file 		http.cpp
 * @brief		Implements a HTTP server using libmicrohttpd
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/



// The only include allowed by the coding standards in cpp-files 
#include "http.hpp"


const string common::http_definitions::kStrStart 		= "start";
const string common::http_definitions::kStrStop 		= "stop";
const string common::http_definitions::kStrStatus 		= "status";
const string common::http_definitions::kStrGetUUID 		= "getuuid";
const string common::http_definitions::kStrServer 		= "server";


/// ------------------------- RESTMessage & RESTMessageImpl

/// create
/**
 *  create new RESTMessage
 *
 * 
 */ 	
ghpf::RESTMessage* ghpf::RESTMessage::create(ghpf::uuid* sender, ghpf::uuid* receiver) {
	http::RESTMessageImpl *message = new http::RESTMessageImpl(sender, receiver);
	return message;
}


// Constants
const string http::RESTMessageImpl::kNameMessageNode 		= "message";
const string http::RESTMessageImpl::kNameParamNode 			= "param";
const string http::RESTMessageImpl::kNameHostAndPortNode	= "host_and_port";
const string http::RESTMessageImpl::kNameUUIDNode			= "uuid";

const string http::RESTMessageImpl::kNameSenderUUIDNode		= "sender_uuid";
const string http::RESTMessageImpl::kNameReceiverUUIDNode	= "receiver_uuid";



/**
 *  Constructor
 *
 * 
 */ 	

http::RESTMessageImpl::RESTMessageImpl(ghpf::uuid* sender, ghpf::uuid* receiver) {
	url_.addParamVal( kNameSenderUUIDNode,   sender->str()   );
	url_.addParamVal( kNameReceiverUUIDNode, receiver->str() );
	
	xml_parser_ = ghpf::SimpleXMLParser::create();
}

http::RESTMessageImpl::RESTMessageImpl() {
	xml_parser_ = ghpf::SimpleXMLParser::create();
}

/**
 *  Destructor
 *
 * 
 */ 	

http::RESTMessageImpl::~RESTMessageImpl() {
		if(xml_parser_) delete xml_parser_;
}


/**
 *  Get the url representation of the message 
 *
 * 
 */ 	
std::string http::RESTMessageImpl::getURL(void) { 
	return url_.getURL(); 
}

std::string http::RESTMessageImpl::getShortURL(void) { 
	return url_.getShortURL(); 
}


http::Url http::RESTMessageImpl::getCommandUrl(void) { 
	return url_; 
}


/**
 *  Get the command sent in the message
 *
 * 
 */ 	
/*string http::RESTMessageImpl::getCommand() { 
	return url_.getCommandString(); 
}*/


/**
 *  Set the command sent in the message
 *
 * 
 */ 	
void http::RESTMessageImpl::setCommand(std::string command) { 
	url_.setCommandString(command);
	
}

/*void http::RESTMessageImpl::setCommand(Url url) { 
	url_ = url;
}*/


/**
 * Add a parameter to the message
 *
 * 
 */ 	
void http::RESTMessageImpl::addParamVal(string param, string val) {
	url_.addParamVal(param, val);
}


/**
 *  Make it possible to assign variables
 *
 * 
 */ 	
http::RESTMessageImpl& http::RESTMessageImpl::operator=(const http::RESTMessageImpl& m) {
    this->url_ = m.url_;
    return *this;
}

/**
 *  Parse URL and store in the url
 *
 * 
 */ 	
void http::RESTMessageImpl::parseUrl(string url) {
	url_.parseUrl(url);
}

/**
 *  Parse URL and store in the url
 *
 * 
 */ 	
void http::RESTMessageImpl::parseShortUrl(string url) {
	url_.parseShortUrl(url);
}


/**
 *  Parse the XML string and create a Url object representing the command
 *
 * 
 */ 	
void http::RESTMessageImpl::parseXML(string xml) { 
	
	this->xml_parser_->parse(xml);

	// The command is represented by a Url object internally
	http::Url url;

	// Save hostname and port 'lovalhost:80' etc.
	url.setHostAndPort(this->xml_parser_->getChildNodeText(kNameHostAndPortNode));

	// save the command
	url.setCommandString(this->xml_parser_->getChildNodeText(kNameMessageNode));
	
	// save the parameters
	int num_params = this->xml_parser_->numNodes(kNameParamNode);
	for(int i=0; i<num_params; i++) {
		this->xml_parser_->getChildNodeText(kNameParamNode, i);
		
		string s1 = this->xml_parser_->getAttributeNameOfCurrentNode(0);
		string s2 = this->xml_parser_->getAttributeValueOfCurrentNode(0);
		
		string s3 = this->xml_parser_->getAttributeNameOfCurrentNode(1);
		string s4 = this->xml_parser_->getAttributeValueOfCurrentNode(1);
		
		url.addParamVal( (boost::iequals(s1,string(ghpf::SimpleXMLParser::kNameKeyAttribute))) ? s2 : s4, 
						 (boost::iequals(s1,string(ghpf::SimpleXMLParser::kNameValueAttribute))) ? s2 : s4);
	}

	// save the Url that was created
	this->url_ = url; 

}

/**
 *  Get the command sent in the message
 *
 * Message sent/received to/from RESTServer/RESTClient have XML format
 * 	<xml>
 *		<host>localhost</host>
 *		<port>8080</port>
 *		<uuid>server</uuid>
 *		<message>command</message>
 *		<param key="key1" value="val1"/>
 *		<param key="key2" value="val2"/>
 *	</xml>
 * 
 */ 	
string http::RESTMessageImpl::toXML() {
	// Create top node
	this->xml_parser_->createTopNode();

	// Create host_and_port node
	this->xml_parser_->addNode(kNameHostAndPortNode);
	this->xml_parser_->addTextToCurrentNode(this->url_.getHostAndPort());

	// Create message node
	this->xml_parser_->addNode(kNameMessageNode);
	this->xml_parser_->addTextToCurrentNode(this->url_.getCommandString());
	
	url_.resetIterator();
	
	while( !url_.iteratorReachedEnd() ) {
		std::pair<string, string> key_value = url_.nextIteratorElement();
		
		this->xml_parser_->addNode(kNameParamNode);
		this->xml_parser_->addAttributeToCurrentNode(key_value.first, key_value.second );
	}
	
	return this->xml_parser_->createXML(); 
}

/**
 *  Get the Adapter used to sending RESTMessages
 *
 * 
 */ 	
common::Adapter* http::RESTMessageImpl::getAdapter() { 
	return http::RESTAdapter::Instance(); 
}

/**
 *  Get the UUID of the receiver
 *
 * \return the UUID of the receiver
 */ 	
ghpf::uuid* http::RESTMessageImpl::getReceiver() {
	return ghpf::uuid::create( url_.getParamVal(kNameReceiverUUIDNode) ); //receiver_;
}		
	
/**
 *  Get the UUID of the sending ActiveObject
 *
 * \return the UUID of the receiver
 */ 	
ghpf::uuid* http::RESTMessageImpl::getSender() {
	return ghpf::uuid::create( url_.getParamVal(kNameSenderUUIDNode) ); //sender_;
}



//--------------------------------------- Url

// global url object that is used by the static functions (can't use member attributes)
//Not thread safe, can only be used with MHD_USE_SELECT_INTERNALLY and not with MHD_USE_THREAD_PER_CONNECTION

http::Url *global_url_;

/**
 * Overloaded >> operator
 *
 * Will output the attributes of the object. This is not a member but a GLOBAL function.
 * 
 */ 
std::ostream& http::operator<<(std::ostream& output,  http::Url& p) {

    output << p.host_port_;
    output << "/" 		<<  p.uuid_;
    output << "/" 		<<  p.str_command_;
    output << "?";
    	
	p.resetIterator();
	
	int i = 0;
	while(! p.iteratorReachedEnd() ) {
		if (i++>0) output << "&";
		std::pair<string, string> key_param = p.nextIteratorElement();
    	output <<  key_param.first << "=" << key_param.second;
	}
    
    output << " (command:" 			<<  p.getCommand();
    output << ",server_command:" 	<<  p.server_command_ << ")";

    return output;
}

/**
 * Overloaded == operator
 *
 * Compares two Url objects. This is not a member but a GLOBAL function.
 * 
 */ 
bool http::operator==( http::Url& u1,  http::Url& u2) {
	bool result = boost::iequals(u1.str_command_, u2.str_command_);
	
	// check that the number of parameters are the same
	if (result) result = (u1.parameters_.size() == u2.parameters_.size());

	u1.parameters_.resetIterator();
	
	while(!u1.parameters_.iteratorReachedEnd() && result) {
		std::pair<string, string> key_param1 = u1.parameters_.nextIteratorElement();
		
		result = key_param1.second == u2.parameters_.get(key_param1.first);
		
	}

	return result;	
}


/**
 * Constructor
 *
 * Initialize attributes.
 * 
 */ 
 http::Url::Url() {
 		//this->command_ 		= common::http_definitions::kNoCommand;
 }

 http::Url::Url(string url) {
 		//this->command_ 		= common::http_definitions::kNoCommand;
 		this->parseUrl(url);
 }

/**
 * Destructor
 *
 * 
 */ 
 http::Url::~Url() {
 }

/**
 * Helper function
 *
 * 
 */ 
int getCommandForString(std::string str_command) {
	int command;

	if(boost::iequals(str_command, common::http_definitions::kStrStart)) 		 
		command = common::http_definitions::kStart;
		
	else if(boost::iequals(str_command, common::http_definitions::kStrStop)) 	 
		command = common::http_definitions::kStop;
		
	else if(boost::iequals(str_command, common::http_definitions::kStrStatus))  
		command = common::http_definitions::kStatus;
		
	else if(boost::iequals(str_command, common::http_definitions::kStrGetUUID)) 
		command = common::http_definitions::kGetUUID;
		
	else command = common::http_definitions::kError;
	
	return command;
	
}

/**
 * Parse a url and set the attributes of the object
 *
 * Exmaple of url:
 * "http://localhost:8080/abcde-034k-ldskjf0-lkjdf/start?parm1=val;parm2=val2"
 * 
 */ 
int http::Url::parseUrl(string url) {
	char cp[255], *token;
	const char delimiters1[] = "/?";
	const char delimiters2[] = "=;&";
	
	//this->command_ = common::http_definitions::kError;

	// Copy string to new buffer (since it will be consumed)
	strcpy(cp, url.c_str());

	// should return 'http'
	token = strtok (cp, delimiters1);
	if(token == NULL) return common::http_definitions::kError;

	// should return 'host:port'
	token = strtok (NULL, delimiters1);
	if(token == NULL) return common::http_definitions::kError;
	this->host_port_ = string(token);

	// should return UUID 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
	token = strtok (NULL, delimiters1);
	if(token == NULL) return common::http_definitions::kError;
	this->uuid_ = string(token);

	// set the server_command_ variable
	this->server_command_ = boost::iequals(this->uuid_, common::http_definitions::kStrServer);

	// should return command
	token = strtok (NULL, delimiters1);
	if(token == NULL) return  common::http_definitions::kError;
	this->str_command_ = string(token);

	// the remaing string should be parameters
	while(token != NULL) {
		
		// should return parameter
		token = strtok (NULL, delimiters2);
		if(token != NULL) {
			string key = string(token);
			
			// should return value
			token = strtok (NULL, delimiters2);
			if(token == NULL) return common::http_definitions::kError;

			string value = string(token);

			addParamVal(key, value);

		}
	}

	return getCommandForString(str_command_);

}


/**
 * Parse a url and set the attributes of the object
 *
 * Exmaple of url:
 * "/abcde-034k-ldskjf0-lkjdf/start?parm1=val;parm2=val2"
 * 
 */ 
int http::Url::parseShortUrl(string url) {
	char cp[255], *token;
	const char delimiters1[] = "/?";
	const char delimiters2[] = "=;&";
	
	//this->command_ = common::http_definitions::kError;

	// Copy string to new buffer (since it will be consumed)
	strcpy(cp, url.c_str());

	// reset host_port_
	this->host_port_ = string("");

	// should return UUID 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
	token = strtok (cp, delimiters1);
	if(token == NULL) return common::http_definitions::kError;
	this->uuid_ = string(token);

	// set the server_command_ variable
	this->server_command_ = boost::iequals(this->uuid_, common::http_definitions::kStrServer);

	// should return command
	token = strtok (NULL, delimiters1);
	if(token == NULL) return common::http_definitions::kError;
	this->str_command_ = string(token);

	// the remaing string should be parameters
	while(token != NULL) {
		
		// should return parameter
		token = strtok (NULL, delimiters2);
		if(token != NULL) {
			string key = string(token);

			// should return value
			token = strtok (NULL, delimiters2);
			if(token == NULL) return common::http_definitions::kError;

			string value = string(token);
			
			addParamVal(key, value);
		}
	}
	
	return getCommandForString(str_command_);
}


/**
 * getShortURL
 *
 * 
 */ 
string http::Url::getShortURL(void) {
	string output("");

    if(this->server_command_ )			output += "/server";
    
    output += getURLParams();
    
    return output;
}

/**
 * getURL
 *
 * 
 */ 
string http::Url::getURL(void) {
	string output("http://");
	
    if(!this->host_port_.empty() )		output += 	 		   this->host_port_;
    if(!this->uuid_.empty() )			output += "/" 		+  this->uuid_;
    if(this->server_command_ )			output += "/server";
    
    output += getURLParams();
    
    return output;
}


/**
 * getCommand
 *
 * 
 */ 
int http::Url::getCommand(void) {return getCommandForString(str_command_);}


/**
 * getCommandString
 *
 * 
 */ 
string http::Url::getCommandString(void) {return this->str_command_;}


/**
 * getURLParams
 *
 * 
 */ 
string http::Url::getURLParams(void) {
	string output("");

    if(!this->str_command_.empty() )	output += "/" 		+  this->str_command_;
  
    if( parameters_.size() > 0) {
    	output += "?";
    
		parameters_.resetIterator();
		
		int i = 0;
		while(!parameters_.iteratorReachedEnd() ) {
			std::pair<string, string> key_value = parameters_.nextIteratorElement();
			
			if (i++>0) output += "&";
			
	    	output +=  key_value.first + "=" + key_value.second;
		}
	}
	
    return output;
}


/**
 *
 * @param command the command of the Url
 *
 */	
void http::Url::setCommandString(string command) {
	this->str_command_ = command;
}


/**
 * getParamVal
 *
 * 
 */ 
string http::Url::getParamVal(string key) { 

	return parameters_.get(key);

}


/**
 * addParamVal
 *
 * 
 */ 
void http::Url::addParamVal(string key, string val) { 
	parameters_.set(key, val);
}


/**
 * setHostAndPort
 *
 * 
 */ 
void http::Url::setHostAndPort(std::string host, int port) {
	stringstream ss;
	ss << host << port;
	host_port_ = ss.str();
}
void http::Url::setHostAndPort(string host_port) {
	this->host_port_ = host_port;
}

/**
 * getHostAndPort
 *
 * 
 */ 
string http::Url::getHostAndPort(void) {
	return this->host_port_;
}


/// ------------------------- RESTAdapter


// Initialize the attribute that manages the Singleton pattern 
http::RESTAdapter* http::RESTAdapter::instance_ = NULL;

// The thread that the Adpater runs in
// not needed, use in MPI though boost::thread thread_;

/**
 *  Create an instance of this Singleton class or return the existing instance.
 *
 * 
 */ 	
http::RESTAdapter* http::RESTAdapter::Instance() {
	if (!instance_) {
		instance_ = new RESTAdapter();
		// not needed, use in MPI though, boost::thread(&os_facade::RESTAdapter::::run, this);
		
	}
	return instance_;	
}

/**
 *  receiveMessage
 *
 * 
 */ 	
void http::RESTAdapter::receiveMessage(ghpf::RESTMessage* command_message) {
	
	// Testing if sender is defined, a exception will be thrown otherwise
	try {
		ghpf::uuid* sender   = command_message->getSender();
		delete sender;

	} 
	catch(ghpf::Exception &e) {
		stringstream ss;
		ss << "Sender not defined in REST message:" << command_message->getURL();
		ghpf::Log::log_ERROR("REST:", ss.str() );
				
		// deallocate the message
		delete command_message;	
		
		return;
	}


	// Testing if receiver is defined, a exception will be thrown otherwise
	try {
		ghpf::uuid* receiver = command_message->getReceiver();
		delete receiver;

	} 
	catch(ghpf::Exception &e) {
		stringstream ss;
		ss << "Receiver not defined in REST message:" << command_message->getURL();
		ghpf::Log::log_ERROR("REST:", ss.str() );
				
		// deallocate the message
		delete command_message;	
		
		return;
	}


	// receive the message in the Communicator
	communicator_->receive(command_message);
	
	
	// deallocate the message
	delete command_message;	
}


/**
 *  Destructor
 *
 * 
 */ 	
http::RESTAdapter::~RESTAdapter() {

	// Make sure that all messages are processed before exiting
	ghpf::RESTMessage* command_message = rest_server_->getNextMessage();

	while(command_message) {
		receiveMessage(command_message);

		// look for more messages
		command_message = rest_server_->getNextMessage();	
	}

	if(this->rest_server_) {
		delete this->rest_server_;
		this->rest_server_ = NULL;
	}
	
	if(this->rest_client_) {
		delete this->rest_client_;
		this->rest_client_ = NULL;
	}
	
	// Reset the instance_ attribute in order to make sure that a new object is
	// created the next time Instance() is called
	instance_ = NULL;
}


/**
 * Send a message using REST
 *
 *
 *
 */
void http::RESTAdapter::send(std::string host, int port, ghpf::RESTMessage *message) {
	std::stringstream ss;
	ss << "http://" << host << ":" << port << message->getShortURL();

	ghpf::Log::log_DEBUG("os_facade.cpp", string("http::RESTAdapter::send - ") + ss.str() );


	// Perform a HTTP GET using the REST client
	this->rest_client_->get( ss.str() );	
	
}

/**
 *  Hidden contructor
 *
 * 
 */
http::RESTAdapter::RESTAdapter() {
	this->communicator_ = ghpf::Context::Instance()->getCommunicator();
	
	// Create RESTServer
	this->rest_server_ = NULL;
	this->rest_server_ = http::RESTServer::create();

	// Create RESTClient
	this->rest_client_ = NULL;
	this->rest_client_ = http::RESTClient::create();

}


/**
 *  Initialize the adapter and start the HTTP deamon
 *
 * 
 */
void http::RESTAdapter::init() {
	// start the server on på kRESTPort (currently 8081) + the rank of the process
	this->rest_server_->start(ghpf::kRESTPort+ghpf::Context::Instance()->getCommunicator()->getRank());
	
}

/**
 *  Loop forever checking the message queue. Call the receive function of the Communicator
 *
 * 
 */
void http::RESTAdapter::run() {
	
	// Loop forever - in practice until exit is called
	while( !ghpf::Context::Instance()->exitProcess() ) {

		ghpf::RESTMessage* command_message = rest_server_->getNextMessage();
		
		//ghpf::Log::log_DEBUG("http.cpp", (command_message) ? "RESTAdapter found message" : "RESTAdapter found NO message" );
		
		// Pass on the message if there was any
		if(command_message) {

			// private helper function also used in the destructur
			receiveMessage(command_message);

		}
		
		// Sleep one second
		// \todo: Should this be removed, or set lower?
		sleep(1);
	}
	

	
}


//--------------------------------------- RESTServerImpl

// Make sure that microhttpd should be enabled
#ifndef DISABLE_HTTPD

common::FIFOQueue<ghpf::RESTMessage>	 http::RESTServerImpl::message_queue_;


/// create
/**
 *  create new RESTServer
 *
 * 
 */ 	
http::RESTServer* http::RESTServer::create() {
	http::RESTServerImpl *rest_server = new http::RESTServerImpl();
	return rest_server;
}

/**
 * Constructor
 *
 * Initialize attributes.
 * 
 */ 
http::RESTServerImpl::RESTServerImpl() { 
	this->daemon_   = NULL;
	this->port_num_	= 0;
	global_url_			= NULL;
 }

/**
 * Destructor
 *
 * Tear down the server.
 *
 */ 
http::RESTServerImpl::~RESTServerImpl() { 
	this->stop();
	if (global_url_) delete global_url_;
 }


/**
 * kv_cb
 *
 * Callback used to get the parameters of the call
 * 
 */ 

int http::RESTServerImpl::kv_cb(void *cls, enum MHD_ValueKind kind, const char *key, const char *value) {

	global_url_->addParamVal(key, value);

	return MHD_YES;
}

/**
 * execute_command
 *
 * 
 * 
 */ 

int http::RESTServerImpl::execute_command(void *cls,
          struct MHD_Connection *connection,
          const char *url,
          const char *method,
          const char *version,
          const char *upload_data, size_t *upload_data_size, void **ptr)
{
	struct MHD_Response *response;
	int ret;

	stringstream ss2;
	ss2 << "http::RESTServerImpl::execute_command - " << string(url);
	ghpf::Log::log_INFORMATIONAL("http.cpp", ss2.str() );

	// Reset the URL
	if (global_url_) delete global_url_;
	global_url_ = new http::Url();
	global_url_->parseShortUrl(string(url));
	
	// Get URI arguments
	ret = 0;
	MHD_get_connection_values(connection, MHD_GET_ARGUMENT_KIND, &http::RESTServerImpl::kv_cb, &ret);

	// Log the url that is to be executed
	stringstream ss;
	ss << "" << *global_url_;
	ghpf::Log::log_INFORMATIONAL("http.cpp", ss.str() );

	// Save the message to the queue
	http::RESTMessageImpl *command_message = new http::RESTMessageImpl();
	command_message->parseShortUrl( ss.str() );
	message_queue_.write(command_message);


	response = MHD_create_response_from_buffer( strlen(ss.str().c_str()), (void*) ss.str().c_str(), MHD_RESPMEM_MUST_COPY);
	
	ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
	MHD_destroy_response(response);

	return ret;
}

/**
 * echo_request
 *
 * 
 * 
 */ 

int http::RESTServerImpl::echo_request(void *cls,
          struct MHD_Connection *connection,
          const char *url,
          const char *method,
          const char *version,
          const char *upload_data, size_t *upload_data_size, void **ptr)
{
	struct MHD_Response *response;
	int ret;
	//static int aptr;
	
	//if (&aptr != *ptr) {
	//    // do never respond on first call
	//    *ptr = &aptr;
	//    return MHD_YES;
	//}
	// *ptr = NULL;                  // reset when done

	char echo_this[100];
	sprintf(echo_this, "<html><body><ul><li>url:%s<li>method:%s<li>version:%s</ul></body></html>", url, method, version);
	  
	response = MHD_create_response_from_buffer( strlen(echo_this), (void*) echo_this, MHD_RESPMEM_MUST_COPY);
	
	ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
	MHD_destroy_response(response);

	return ret;
}

/**
 * start
 *
 * Start the daemon.
 * 
 */ 

void http::RESTServerImpl::start(int port_num) {

	// Make sure that the daemon isn't running already
	if(this->daemon_) {
		this->stop();
		string s("http::RESTServerImpl::start called while HTTP server already running, stopping server first...");
		ghpf::Log::log_WARNING("http.cpp", s);
	}

	// Start the daemon on the requested port
	this->port_num_ = port_num;
	this->daemon_ = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY | MHD_USE_DEBUG,
                        port_num,
                        NULL, NULL, &http::RESTServerImpl::execute_command, NULL, MHD_OPTION_END);

	if (this->daemon_ == NULL) {
		stringstream ss;
		ss <<"http::RESTServerImpl::start MHD_start_daemon returned NULL, port:" << port_num;
		ghpf::Log::log_ERROR("http.cpp", ss.str());
		throw ghpf::Exception("http.cpp", ss.str());
	}
	  
}


/**
 * stop
 *
 * Stop the daemon.
 * 
 */ 

void http::RESTServerImpl::stop(void) {
	if(daemon_ == NULL) {
		ghpf::Log::log_WARNING("http.cpp", "http::RESTServerImpl::stop called with daemon==NULL...");
	 	return;
	}
		  
	MHD_stop_daemon(this->daemon_);
	this->daemon_ = NULL;
	
	return;
}



//--------------------------------------- WebServer


//
// Only used for testing libmicrohttpd so far
//

/**
 * Constructor
 *
 * Initialize attributes.
 * 
 */ 
 http::WebServer::WebServer() { 
	this->daemon_ = NULL;
 }


ssize_t http::WebServer::dataGenerator(void *cls, uint64_t pos, char *buf, size_t max) {
  if (max < 80)
    return 0;
  memset (buf, 'A', max - 1);
  buf[79] = '\n';
  return 80;
}

int http::WebServer::ahc_echo(void *cls,
          struct MHD_Connection *connection,
          const char *url,
          const char *method,
          const char *version,
          const char *upload_data, size_t *upload_data_size, void **ptr)
{
  static int aptr;
  struct MHD_Response *response;
  int ret;

  if (0 != strcmp (method, "GET"))
    return MHD_NO;              /* unexpected method */
  if (&aptr != *ptr)
    {
      /* do never respond on first call */
      *ptr = &aptr;
      return MHD_YES;
    }
  *ptr = NULL;                  /* reset when done */
  response = MHD_create_response_from_callback (MHD_SIZE_UNKNOWN,
                                                80,
                                                &http::WebServer::dataGenerator, NULL, NULL);
                                                
  ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  
  std::cout << "ahc_echo...\n";
  
  MHD_destroy_response(response);
  
  return ret;
}


int http::WebServer::start(int port_num) {

  this->daemon_ = MHD_start_daemon (MHD_USE_THREAD_PER_CONNECTION | MHD_USE_DEBUG,
                        port_num,
                        NULL, NULL, &http::WebServer::ahc_echo, NULL, MHD_OPTION_END);
                        
  if (this->daemon_ == NULL)
    return 1;
    
  return 0;
}


int http::WebServer::stop(void) {
	ghpf::Log::log_DEBUG("http.cpp", "About to stop MHD daemon...");
	  
	MHD_stop_daemon(this->daemon_);

	ghpf::Log::log_DEBUG("http.cpp", "MHD daemon stopped...");


  return 0;
}

#endif	//DISABLE_HTTPD


//--------------------------------------- RESTClientImpl

// Make sure that curl should be enabled
#ifndef DISABLE_CURL

/// create
/**
 *  create new RESTServer
 *
 * 
 */ 	
http::RESTClient* http::RESTClient::create() {
	http::RESTClientImpl *rest_client = new http::RESTClientImpl();
	return rest_client;
}

/**
 * Constructor
 *
 * Initialize attributes.
 * 
 */ 

http::RESTClientImpl::RESTClientImpl() {
	this->contents_ 	= NULL;
	
	// Curl related initialization
	this->curl_ 		= curl_easy_init();		// Initilise web query
 	this->res_ 			= CURLE_OK;				// Error code 
	this->err_[0] 		= '\0';					// String explaining error
	
}

/**
 * Destructor
 *
 * Tear down everything.
 * 
 */ 

http::RESTClientImpl::~RESTClientImpl() {

	if(contents_) delete contents_;
		
	// curl cleanup
	curl_easy_cleanup(curl_);			// (E) Close the connection
}


/**
 * Functions related to readURL
 *
 */

int http::RESTClientImpl::writer(char *data, size_t size, size_t nmemb, string *buffer) {
	int result = 0;
	if(buffer != NULL) {
		buffer -> append(data, size * nmemb);
		result = size * nmemb;
	}
	return result;
} 
 
  
int http::RESTClientImpl::fetch_url_main(string url, int method) {
 	
	if(contents_) delete contents_;
	contents_ = new string();
	
	if ( url.length()<=0 || url.empty() ){
		string s = string("http::RESTClientImpl::fetch_url_main - Invalid argument:") + url;		
		ghpf::Log::log_EMERGENCY("http.cpp", s);
		throw ghpf::Exception("http.cpp", s);											
	}
	
	// Set Options of the web query 
	if (curl_){
		curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());
		
		// No we don't need the Header of the web content. Set to 0 and curl ignores the first line 
		curl_easy_setopt(curl_, CURLOPT_HEADER, 0);
		
		switch(method) {
			case kGET:
				// No extra options needs to be set
				break;
				
			case kPUT:
				/* From: http://curl.haxx.se/libcurl/c/curl_easy_setopt.html
				 *
				 * CURLOPT_UPLOAD (should use instead of CURLOPT_PUT)
				 *	A parameter set to 1 tells the library to prepare for an upload. The CURLOPT_READDATA 
				 * and CURLOPT_INFILESIZE or 		CURLOPT_INFILESIZE_LARGE options are also interesting 
				 * for uploads. If the protocol is HTTP, uploading means using the PUT request unless you 
				 * tell libcurl otherwise.
				 *
				 * Using PUT with HTTP 1.1 implies the use of a "Expect: 100-continue" header. You can 
				 * disable this header with CURLOPT_HTTPHEADER as usual.
				 *
				 * If you use PUT to a HTTP 1.1 server, you can upload data without knowing the size before 
				 * starting the transfer if you use chunked encoding. You enable this by adding a header 
				 * like "Transfer-Encoding: chunked" with CURLOPT_HTTPHEADER. With HTTP 1.0 or without chunked 
				 * transfer, you must specify the size.
				 */
				
				//curl_easy_setopt(curl_, CURLOPT_UPLOAD, 1);
				throw ghpf::Exception("REST method PUT not implemented yet");	
				break;

			case kPOST:

				/* From: http://curl.haxx.se/libcurl/c/curl_easy_setopt.html
				 *
				 * CURLOPT_POST
				 *
				 * A parameter set to 1 tells the library to do a regular HTTP post. This will also make the 
				 * library use a "Content-Type: application/x-www-form-urlencoded" header. (This is by far 
				 * the most commonly used POST method).
				 *
				 * Use one of CURLOPT_POSTFIELDS or CURLOPT_COPYPOSTFIELDS options to specify what data to 
				 * post and CURLOPT_POSTFIELDSIZE or CURLOPT_POSTFIELDSIZE_LARGE to set the data size.
				 *
				 * Optionally, you can provide data to POST using the CURLOPT_READFUNCTION and CURLOPT_READDATA 
				 * options but then you must make sure to not set CURLOPT_POSTFIELDS to anything but NULL. When 
				 * providing data with a callback, you must transmit it using chunked transfer-encoding or you 
				 * must set the size of the data with the CURLOPT_POSTFIELDSIZE or CURLOPT_POSTFIELDSIZE_LARGE 
				 * option. To enable chunked encoding, you simply pass in the appropriate Transfer-Encoding header, 
				 * see the post-callback.c example.
				 *
				 * You can override the default POST Content-Type: header by setting your own with CURLOPT_HTTPHEADER.
				 *
				 * Using POST with HTTP 1.1 implies the use of a "Expect: 100-continue" header. You can disable 
				 * this header with CURLOPT_HTTPHEADER as usual.
				 *
				 * If you use POST to a HTTP 1.1 server, you can send data without knowing the size before starting 
				 * the POST if you use chunked encoding. You enable this by adding a header like "Transfer-Encoding: 
				 * chunked" with CURLOPT_HTTPHEADER. With HTTP 1.0 or without chunked transfer, you must specify 
				 * the size in the request.
				 *
				 * When setting CURLOPT_POST to 1, it will automatically set CURLOPT_NOBODY to 0 (since 7.14.1).
				 *
				 * If you issue a POST request and then want to make a HEAD or GET using the same re-used handle, 
				 * you must explicitly set the new request type using CURLOPT_NOBODY or CURLOPT_HTTPGET or similar.			
				 *
				 */
				//curl_easy_setopt(curl_, CURLOPT_POST, 1);
				throw ghpf::Exception("REST method POST not implemented yet");	
				break;

			case kDELETE:
				/* From: http://curl.haxx.se/libcurl/c/curl_easy_setopt.html
				 *
				 * CURLOPT_CUSTOMREQUEST
				 *
				 * Pass a pointer to a zero terminated string as parameter. It will be used instead of GET or 
				 * HEAD when doing an HTTP request, or instead of LIST or NLST when doing a FTP directory listing. 
				 * This is useful for doing DELETE or other more or less obscure HTTP requests. Don't do this at 
				 * will, make sure your server supports the command first.
				 *
				 * When you change the request method by setting CURLOPT_CUSTOMREQUEST to something, you don't 
				 * actually change how libcurl behaves or acts in regards to the particular request method, it 
				 * will only change the actual string sent in the request.
				 *
				 * For example: if you tell libcurl to do a HEAD request, but then change the request to a 
				 * "GET" with CURLOPT_CUSTOMREQUEST you'll still see libcurl act as if it sent a HEAD even when 
				 * it does send a GET.
				 * 
				 * To switch to a proper HEAD, use CURLOPT_NOBODY, to switch to a proper POST, use CURLOPT_POST 
				 * or CURLOPT_POSTFIELDS and so on.
				 *
				 * Restore to the internal default by setting this to NULL.
				 *
				 * Many people have wrongly used this option to replace the entire request with their own, 
				 * including multiple headers and POST contents. While that might work in many cases, it will 
				 * cause libcurl to send invalid requests and it could possibly confuse the remote server badly. 
				 * Use CURLOPT_POST and CURLOPT_POSTFIELDS to set POST data. Use CURLOPT_HTTPHEADER to replace 
				 * or extend the set of headers sent by libcurl. Use CURLOPT_HTTP_VERSION to change HTTP version.
				 *
				 */
				//curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "DELETE");
				throw ghpf::Exception("REST method DELETE not implemented yet");	
				break;
		
		}
		// Don't follow anything else than the particular url requested		
		curl_easy_setopt(curl_, CURLOPT_FOLLOWLOCATION, 0); 	
		
		// Function Pointer "writer" manages the required buffer size 	
		curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, http::RESTClientImpl::writer);
		
		// Data Pointer &buffer stores downloaded web content	
		curl_easy_setopt(curl_, CURLOPT_WRITEDATA, contents_ ); 	
		
		curl_easy_setopt(curl_, CURLOPT_ERRORBUFFER, err_ );
	}
	else{
		ghpf::Log::log_EMERGENCY("http.cpp", "FetchURL::fetch_url_main - curl_easy_init failed");
		return -1;											
	}
	
	// Fetch the data
	res_ = curl_easy_perform(curl_);
	
	if (res_ != CURLE_OK ) {
		string s = string("FetchURL::fetch_url_main failed! URL:") + url + string(" Error:") + err_;		
		ghpf::Log::log_EMERGENCY("http.cpp", s);
		throw ghpf::Exception(s);											
	}
 		
	return 1;
}


//int http::RESTClientImpl::get(http::Url url) {	 	 
int http::RESTClientImpl::get(std::string url) {	 	 
// 	return this->fetch_url_main( url.getURL(), kGET);
 	return this->fetch_url_main( url, kGET);
}

int http::RESTClientImpl::put(http::Url url) {
 	return this->fetch_url_main( url.getURL(), kPUT);
}

int http::RESTClientImpl::post(http::Url url) {
 	return this->fetch_url_main( url.getURL(), kPOST);
}

int http::RESTClientImpl::_delete(http::Url url) {
 	return this->fetch_url_main( url.getURL(), kDELETE);
}


string* http::RESTClientImpl::getContents(void) {	 	 
 	return this->contents_;
}

#endif // DISABLE_CURL


