/****************************************************************************
 *
 * @file 		http-server.hpp
 * @brief		Implementes classes for web and a RESTful server
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef HTTP
#define HTTP


// All modules must use the common module
#include "common.hpp"


// The following moudles are used - ONLY LAYERS BELOW CAN BE USED
// None...


namespace http {

	/// Url class
	/**
	 * Parse URL:s
	 * 
	 * \todo: Should change the representation of paramters and values to a map
	 *        instead of using two vectors. 
	 */
	class Url {
		// Give the << operator access to private attributes
		friend std::ostream& operator<<(std::ostream& output,  Url& p);
		friend bool operator==( Url& u1,  Url& u2);
	
	public:	
	
		/// UrlParser
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	 	Url();
		Url(string url);
		 	 	
		/// ~urlParser
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	~Url();
	 	
	 	/// parseUrl
	 	/**
	 	 *
	 	 * Parse url string with command
	 	 *
	 	 * @param url the string to parse
	 	 *
	 	 * Exmaple of url:
	 	 * "http://localhost:8080/abcde-034k-ldskjf0-lkjdf/start?parm1=val;parm2=val2"
	 	 *
	 	 * \todo: parseUrl and parseShortUrl duplicates a lot of code. Refactor...
	 	 */
	 	int parseUrl(string url);
	 	int parseShortUrl(string url);
	
	 	/// getURL
	 	/**
	 	 *
		 * \returns the full url including http://
		 *
	 	 */
		string getURL(void);
	
		/// getShortURL
		/**
		 *  Get the short url representation of the message
		 *
		 * 
		 */ 	
		string getShortURL(void);

	 	/// getCommand
	 	/**
	 	 *
		 * \returns the command of the url
		 *
	 	 */
		int getCommand(void);
	
	 	/// getCommand
	 	/**
	 	 *
		 * \returns the command of the url
		 *
	 	 */
		string getUUID(void) { return uuid_; }
	
	 	/// getCommandString
	 	/**
	 	 *
		 * \returns the command of the url
		 *
	 	 */
		string getCommandString(void);
	
	 	/// setCommandString
	 	/**
	 	 *
		 * @param command the command of the Url
		 *
	 	 */	
		void setCommandString(string command); 	
	
	 	/// addParamVal
	 	/**
	 	 * Add parameter and value pairs
		 *
	 	 * @param param key
	 	 * \returns: the value
	 	 */
		string getParamVal(string param);

	 	/// addParamVal
	 	/**
	 	 * Add parameter and value pairs
		 *
	 	 * @param param key
	 	 * @param val the value
	 	 */
		void addParamVal(string param, string val);
	
		void                      resetIterator(void)       { parameters_.resetIterator(); }
		bool                      iteratorReachedEnd(void)  { return parameters_.iteratorReachedEnd(); }
		std::pair<string, string> nextIteratorElement(void) { return parameters_.nextIteratorElement(); }
	   	int                       size(void)                { return parameters_.size(); }
	   	
	 	/// setHostAndPort
	 	/**
	 	 *
	 	 * Set the first parts of the url.
	 	 * 
	 	 * Example: host=di.se and port=""
	 	 *			url=http://di.se
	 	 *
	 	 * @param host the 
	 	 * @param port key
		 *
	 	 */
		void setHostAndPort(string host, int port);
		void setHostAndPort(string host_port);

	   	
	 	/// getHostAndPort
	 	/**
	 	 *
	 	 * Get Host and Port. 
	 	 * 
		 * \returns Example: 'localhost:80'
		 *
	 	 */
		string getHostAndPort(void);
	 	
	private:
		//int 			    command_;
		bool			    server_command_;
		string 			    host_port_, uuid_, str_command_;
		common::strings_map parameters_;
		
		string getURLParams(void);
	};
	
	/// operator<<
	/**
	 *
	 * Overloaded output operator for UrlParsser
	 *
	 * @param output stream to print to
	 * @param UrlParser the obejct to print
	 *
	 * \returns ostream
	 *
	 */ 
	std::ostream& operator<<(std::ostream& output,  Url& p);
	bool operator==( Url& u1,  Url& u2);


	/// RESTMessageImpl class
	/**
	 * RESTMessage, A message that the REST server can receive
	 * 
	 * Message sent/received to/from RESTServer/RESTClient have XML format
	 * 	<xml>
	 *		<message>command</message>
	 *		<param key="key1" value="val1"/>
	 *		<param key="key2" value="val2"/>
	 *	</xml>
	 *
	 *
	 * \todo: Need to move the implementations parts to os_facade.cpp
	 *  
	 */ 
	class RESTMessageImpl : public ghpf::RESTMessage {
	private:
		ghpf::SimpleXMLParser 	*xml_parser_;
		Url						url_;
		//ghpf::uuid *sender_, *receiver_;
		
	public:

		static const string kNameMessageNode;
		static const string kNameParamNode;
		static const string kNameHostAndPortNode;
		static const string kNameUUIDNode;
		static const string kNameSenderUUIDNode;
		static const string kNameReceiverUUIDNode;


		/// CommandMessage
		/**
		 *  Constructor
		 *
		 * 
		 */ 		
	 	RESTMessageImpl(ghpf::uuid* sender, ghpf::uuid* receiver);
	 	RESTMessageImpl();
		
		/// ~CommandMessage
		/**
		 *  Destructor
		 *
		 * 
		 */ 		
	 	~RESTMessageImpl();
		

		/// getCommand
		/**
		 *  Get the command sent in the message
		 *
		 * 
		 */ 	
		Url getCommandUrl(void);

		/// getUrl
		/**
		 *  Get the url representation of the message
		 *
		 * 
		 */ 	
		string getURL(void);

		/// getShortURL
		/**
		 *  Get the short url representation of the message
		 *
		 * 
		 */ 	
		string getShortURL(void);

	 	/// addParamVal
	 	/**
	 	 * Add parameter and value pairs
		 *
	 	 * @param param key
	 	 * @param val the value
	 	 */
		void addParamVal(string param, string val);

		/// setCommand
		/**
		 *  Set the command sent in the message
		 *
		 * 
		 */ 	
		void setCommand(std::string command);
		//void setCommand(Url url);
	

		/// operator=
		/**
		 *  Make it possible to assign variables
		 *
		 * 
		 */ 	
		RESTMessageImpl& operator=(const RESTMessageImpl& m);
	
		/// parseXML
		/**
		 *  Parse and store a XML string
		 *
		 * 
		 */ 	
		void parseXML(string xml);
	
		/// parseURL
		/**
		 *  Parse and store a URL
		 *
		 * 
		 */ 	
		void parseUrl(string url);
	
		/// parseShortUrl
		/**
		 *  Parse and store a URL
		 *
		 * 
		 */ 	
		void parseShortUrl(string url);

		/// toXML
		/**
		 *  Get the command sent in the message
		 *
		 * 
		 */ 	
		string toXML();		
	
		/// getReceiver
		/**
		 *  Get the UUID of the receiver
		 *
		 * \return the UUID of the receiver
		 */ 	
		ghpf::uuid* getReceiver(void);		
	 	
		/// getSender
		/**
		 *  Get the UUID of the sending ActiveObject
		 *
		 * \return the UUID of the receiver
		 */ 	
		ghpf::uuid* getSender(void);
		
		
		/// getAdapter
		/**
		 *  Get a Adapter for the Message
		 *
		 * \return a Adapter for REST Messages
		 */ 	
		common::Adapter* getAdapter(void);
		
	};
		
	
// Make sure that microhttpd should be enabled
#ifndef DISABLE_HTTPD

	/// RESTServer class
	/**
	 * Basic RESTServer. Implments a simple REST (Representational State Transer) Server. The server runs in a separate
	 * thread. Mesages received are stored in a queue from which they are fetched calling the getNExtMessage function.
	 *
	 * Uses the libmicrohttpd library in order to implement a web-server and RESTful
	 * web services.
	 *
	 *
	 * URI format: http://en.wikipedia.org/wiki/URI_scheme
	 *
	 * /<UUID>/command?param1=value1;param2=value2;...
	 * or
	 * /<UUID>/command?param1=value1&param2=value2&...
	 * or
	 * /server/command?param1=value1&param2=value2&...
	 *
	 * For more details on UUID, see for instance http://en.wikipedia.org/wiki/Universally_unique_identifier
	 *
	 * command ::= start | stop | status | getuuid
	 *
	 *
	 * UUID = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
	 *        123456789012345678901234567890123456
	 *                 1         2         3
	 *	36 characters
	 *
	 *
	 *
	 * GET, PUT, DELETE should alwaysbe idempotent (and therefore also without sideffects).
	 * POST - Creates new obejcts etc. calling twice will not give the same result
	 * For more details, see for instance http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
	 *
	 *
	 * Should use GET, PUT and DELETE as far as possible. Maybe only GET and PUT
	 *
	 * 200 OK						- Standard repsonse for sucessfull call
	 * 201 Created					- The request has been fulfilled and resulted in a new resource being created.[2]
	 * 202 Accepted					- The request has been accepted for processing,
	 * 400 Bad Request				- Bad syntax
	 * 403 Forbidden				- The request was a legal request, but the server is refusing to respond to it
	 * 405 Method Not Allowed		- A request was made of a resource using a request method not supported by that resource;[2] 
	 * 							  	  for example, using GET on a form which requires data to be presented via POST, or using PUT 
	 * 							  	  on a read-only resource.
	 * 500 Internal Server Error 	- on exceptions
	 * 501 Not Implemented			- The server either does not recognise the request method, or it lacks the ability to 
	 *                                fulfill the request.
	 * 503 Service Unavailable		- The server is currently unavailable (because it is overloaded or down for maintenance).[2] 
	 * 								  Generally, this is a temporary state.
	 *
	 * For more details, see for instance http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
	 *
	 * TESTING
	 * >telnet localhost 8080
	 * >GET /server/start HTTP/1.1
	 * >Host: myhost
	 * >[enter]
	 *
	 * >telnet localhost 8080
	 * >PUT /server/start?parm1=val1;parm2=val2 HTTP/1.1
	 * >Host: myhost
	 * >[enter]
	 *
	 * >telnet localhost 8080
	 * >PUT /server/stop?parm1=val1;parm2=val2 HTTP/1.1
	 * >Host: myhost
	 * >[enter]
	 *
	 *
	 * 
	 * References:
	 * - http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
	 * - http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods
	 *  
	 */
	class RESTServer {
	public:	
	
		/// create
		/**
		 *  Factory Method for creating REST servers
		 *
		 * 
		 */ 	
		static RESTServer* create(); 
	
		/// ~RESTServer
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		virtual ~RESTServer() {};
	
		/// start
		/**
		 *  Start the RESTful server
		 *
		 * @param port_num the port that the server should listen on
		 * 
		 */ 	
		virtual void start(int port_num) = 0;
	
		/// stop
		/**
		 *  Stop the RESTful sercer
		 *
		 * 
		 */ 	
		virtual void stop(void) = 0;
	

	
		/// getNextMessage
		/**
		 *  Get the next message the server has received. This implemented using a simple thread-safe FIFO queue.
		 *
		 * \return strint with the data fetched
		 */ 	
		virtual ghpf::RESTMessage* getNextMessage(void) = 0;

	};
	
	
	class RESTClient {
	public:	
		
		/// create
		/**
		 *  Factory Method for creating REST clients
		 *
		 * 
		 */ 	
		static RESTClient* create(); 
	
		/// Destructor
		/**
		 * 
		 *
		 *
		 */
		virtual ~RESTClient() {};
			 
		 /// get
		 /**
		 * Fetch the contens of a url
		 *
		 * @param url the URL to fetch
		 *
		 * \return return 1 if successfull, error code <0 if unsuccesfull
	 	 *
		 */
		 //virtual int get(Url url) = 0;
		 virtual int get(std::string url) = 0;

		 virtual int put(Url url) = 0;
		 virtual int post(Url url) = 0;
		 virtual int _delete(Url url) = 0;

	
		 /// getContent
		 /**
		 * Return the contens previously fetched with get
		 *
		 *
		 * \return strint with the data fetched
	 	 *
		 */
		 virtual string* getContents(void) = 0;
		 	 
	};


	/// RESTServerImpl
	class RESTServerImpl : public RESTServer {
	public:	
	
		/// RESTServerImpl
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	 	RESTServerImpl();
	
		/// ~RESTServerImpl
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~RESTServerImpl();
	
		/// start
		/**
		 *  Start the RESTful server
		 *
		 * @param port_num the port that the server should listen on
		 * 
		 */ 	
		void start(int port_num);
	
		/// stop
		/**
		 *  Stop the RESTful sercer
		 *
		 * 
		 */ 	
		void stop(void);
	
		/// execute_command
		/**
		 *  The callback funtion used by MHD for HTTP requests.
		 *
		 * 
		 */ 	
	 	static int execute_command(void *cls,
	          struct MHD_Connection *connection,
	          const char *url,
	          const char *method,
	          const char *version,
	          const char *upload_data, size_t *upload_data_size, void **ptr);
	
		/// kv_cb
		/**
		 *  Callbakc for processing arguments
		 *
		 * 
		 */ 	
		static int kv_cb(void *cls,
				enum MHD_ValueKind kind, 
				const char *key, 
				const char *value);
	
	
		/// echo_request
		/**
		 *  Used for testing from a browser. Will just return the url in a html message.
		 *
		 * 
		 */ 	
	 	static int echo_request(void *cls,
	          struct MHD_Connection *connection,
	          const char *url,
	          const char *method,
	          const char *version,
	          const char *upload_data, size_t *upload_data_size, void **ptr);



		/// getNextMessage
		/**
		 *  Return the next message in the queue.
		 *
		 * 
		 */ 	
		ghpf::RESTMessage* getNextMessage(void) {
			return this->message_queue_.read();
		}
	 	
	 	
	 private:
	 	// holds pointer to RESTMessages
	 	static common::FIFOQueue<ghpf::RESTMessage>	message_queue_;
		int 										port_num_;
	 
	 	///dataGenerator
	 	/**
	 	 *
	 	 * @param cls
	 	 * @param pos
	 	 * @param buf
	 	 * @param max
	 	 */ 
	 	static ssize_t dataGenerator(void *cls, uint64_t pos, char *buf, size_t max);
	
		struct MHD_Daemon *daemon_;
	
	};

	/// RESTAdapter class
	/**
	 * Adapter, Implements REST messaging.
	 * 
	 * Only HTTP GET is used at the moment.
	 *
	 */
	 class RESTAdapter : public common::Adapter {
	 public:
	 
		/// Instance
		/**
		 *  Create an instance of this Singleton class or return the existing instance.
		 *
	 	 * @param communicator Each adapter has a Communicator that is used to 
	 	 *
	 	 * \return RESTAdapter*
		 * 
		 */ 	
		static RESTAdapter* Instance();

		/// ~Adapter
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		virtual ~RESTAdapter();
		
		/// init
		/**
		 *  Initialize the adapter and start the REST server
		 *
		 * 
		 */ 	
		void init();
		
	 	/// run
	 	/**
	 	 * Start listening to REST messages 
	 	 *
	 	 *
	 	 */
		void run(void);


	 	/// send
	 	/**
	 	 * Send using MPI. Not implemented in this class.
	 	 *
	 	 * @param message the message to send. It contains the UUID of the receiving ActiveObject.
	 	 *
	 	 * \return void 
	 	 */
		void send(int rank_receiver, ghpf::MPIMessage *message) { 
			throw ghpf::Exception("RESTAdapter::send not implemented for MPIMessage"); 
		}

	 	/// send
	 	/**
	 	 * Send using REST
	 	 *
	 	 * @param message the message to send. It contains the UUID of the receiving ActiveObject.
	 	 *
	 	 * \return void 
	 	 */
		void send(string host, int port, ghpf::RESTMessage *message);

	 private:
	 
		/// Adapter
		/**
		 *  Hidden contructor
		 *
		 * 
		 */
	 	RESTAdapter();
	
	 	// used to implement the Singleton pattern
	 	static RESTAdapter* instance_;
	 	
	 	// Each adapter needs one Communicator
	 	ghpf::Communicator *communicator_;
	 	
	 	// The adapter receives messages in a thread, only one can exist
	 	// not needed, use in MPI though static boost::thread thread_;
	 	
	 	// The RESTServer that receives messages
	 	RESTServer *rest_server_;
	 	
	 	// Yhe RESTClient used to send messages
	 	RESTClient *rest_client_;
	 	
	 	void receiveMessage(ghpf::RESTMessage* command_message);

	 };


	/// WebServer class
	/**
	 * Basic WebServer. Only used for testing microhttpd.
	 * 
	 *  
	 */
	
	class WebServer {
	public:	
	
		/// WebServer
		/**
		 *  Constructor
		 *
		 * 
		 */ 
	 	WebServer();
	
		int start(int port_num);
	
		int stop(void);
	
	 	static int ahc_echo (void *cls,
	          struct MHD_Connection *connection,
	          const char *url,
	          const char *method,
	          const char *version,
	          const char *upload_data, size_t *upload_data_size, void **ptr);
	 	
	 private:
	 
	 	///dataGenerator
	 	/**
	 	 *
	 	 * @param cls
	 	 * @param pos
	 	 * @param buf
	 	 * @param max
	 	 */ 
	 	static ssize_t dataGenerator(void *cls, uint64_t pos, char *buf, size_t max);
	
		struct MHD_Daemon *daemon_;
	
	
	};
	
#endif	//DISABLE_HTTPD

// Make sure that curl should be enabled
#ifndef DISABLE_CURL
	
	class RESTClientImpl : public RESTClient {
	public:	
		
		static const int kGET 		= 0;
		static const int kPUT 		= 1;
		static const int kPOST 		= 2;
		static const int kDELETE 	= 3;
		
		
		/// RESTClientImpl
		/**
		 * Empty constructor
		 *
		 * @param no params
		 *
		 * \return return a instance of the vlass FetchURL
		 *
		 */
		 RESTClientImpl();
	
		/// ~RESTClientImpl
		/**
		 * 
		 *
		 *
		 */
		 virtual ~RESTClientImpl();
	
		 
		 /// get
		 /**
		 * Fetch the contens of a url
		 *
		 * @param url the URL to fetch
		 *
		 * \return return 1 if successfull, error code <0 if unsuccesfull
	 	 *
		 */
		 //int get(Url url);
		 int get(std::string url);

		 int put(Url url);
		 int post(Url url);
		 int _delete(Url url);

	
		 /// getContent
		 /**
		 * Return  the contens previously fetched with get
		 *
		 *
		 * \return strint with the data fetched
	 	 *
		 */
		 string* getContents(void);
	
	
		 /// writer
		 /**
		 * Callback funtion used by 
		 *
		 * @param strURL the URL to fetch
		 *
		 * \return return 1 if successfull, error code <0 if unsuccesfull
	 	 *
		 */
		static int writer(char *data, size_t size, size_t nmemb, string *buffer);
	
	 	 
	private:
	 	 string 	*contents_;						// string buffer for contents
	 	 
		 // curl related attributed
		 CURL 		*curl_;							// That is the connection, see: curl_easy_init  
	 	 CURLcode 	res_;							// Error code 
		 char 		err_[CURL_ERROR_SIZE];			// String explaining error
	 	 
	 	 int fetch_url_main(string strURLToFetch, int method);
	 	 
	};

#endif // DISABLE_CURL


} // namespace http

#endif // HTTP
