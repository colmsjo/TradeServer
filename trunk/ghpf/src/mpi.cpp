/****************************************************************************
 *
 * @file 		mpi.cpp
 * @brief		Implements the os-facade layer using MPI.
 * @author      Jonas Colmsj��
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/


// Make sure that MPI should be enabled
#ifndef DISABLE_MPI

 
// These cannot be included for the header file (don't know why??)
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>


// The only include allowed by the coding standards in cpp-files 
#include "mpi.hpp"


/// ------------------------- MPIMessage & MPIMessageImpl

ghpf::MPIMessage* ghpf::MPIMessage::create() {
	mpi::MPIMessageImpl *message = new mpi::MPIMessageImpl();
	return message;
}

ghpf::MPIMessage* ghpf::MPIMessage::create(ghpf::uuid* sender, ghpf::uuid* receiver) {
	mpi::MPIMessageImpl *message = new mpi::MPIMessageImpl(sender, receiver);
	return message;
}

/// Message
/**
 *  Constructor
 *
 * 
 */ 	
mpi::MPIMessageImpl::MPIMessageImpl() {
	sender_   = NULL;
	receiver_ = NULL;
}

mpi::MPIMessageImpl::MPIMessageImpl(ghpf::uuid* sender, ghpf::uuid* receiver) {
	sender_   = sender;
	receiver_ = receiver;
}

/**
 * This function performs the actual work of translating the message
 * into something can can be sent using MPI
 *
 * 
 */ 	
template<class Archive> void mpi::MPIMessageImpl::serialize(Archive & ar, const unsigned int version) {
	ar & command_;
}
 

/**
 *  Set the command sent in the message
 *
 * 
 */ 	
void mpi::MPIMessageImpl::setCommand(std::string command) { 
	this->command_ = command;
}

/**
 *  Set the command sent in the message
 *
 * 
 */ 	
string mpi::MPIMessageImpl::getCommand() { 
	return this->command_;
}

/**
 *  Get the UUID of the receiver
 *
 * \return the UUID of the receiver
 */ 	
ghpf::uuid* mpi::MPIMessageImpl::getReceiver() {
	return receiver_;	
}		
	
/**
 *  Get the UUID of the sending ActiveObject
 *
 * \return the UUID of the receiver
 */ 	
ghpf::uuid* mpi::MPIMessageImpl::getSender() {
	return sender_;	
}


/// ------------------------- MPIProcess


/**
 * Constructor
 *
 */ 	
mpi::MPIProcess::MPIProcess() {
} 


/**
 * Initialize process
 *
 * Create one ProcessInitiator object and call the init function.
 * 
 *  Should also call boost::mpi::environment env(argc, argv)??
 */ 	
void mpi::MPIProcess::init(int argc, char* argv[]) {
	
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;

	this->rank_			 = world.rank(); 
	this->num_processes_ = world.size();
	
} 


/**
 * Return the process rank
 *
 */ 	
int mpi::MPIProcess::getRank() {
	return this->rank_; 
}


/**
 * Return the number of processes in the world
 *
 */ 	
int mpi::MPIProcess::getNumProcesses() {
	return this->num_processes_;
}


/**
 * Destroy process
 *
 * Make sure that all obejct are deallocated properly.
 *
 */ 	

void mpi::MPIProcess::destroy() {
}

/// MPIProcess
/**
 *  Destructor
 *
 * 
 */ 	
mpi::MPIProcess::~MPIProcess() {
	this->destroy(); 
}

/**
 * Send a Message to the process with rank 
 *
 */
void mpi::MPIProcess::send(int rank_receiver, ghpf::MPIMessage* msg) {
	boost::mpi::communicator world;
	
	// cast the message so the serialize method is accessable
	MPIMessageImpl* msg2 = (MPIMessageImpl*) msg;
	
 	 // Currently always using the tag 0
	world.send(rank_receiver, 0, *msg2);
}


/**
 * Start receiving messages
 *
 */
void mpi::MPIProcess::run(void) {
	boost::mpi::communicator world;
	MPIMessageImpl msg;
	
	while(true) {
		// Blocking wait for the next message
		world.recv(boost::mpi::any_source, boost::mpi::any_tag, msg);

		// Process the message, \todo: receiving not implemented yet
		std::stringstream ss;
		ss << msg.getCommand();
		ghpf::Log::log_DEBUG("mpi.cpp", ss.str());

		// Wait a little
		// \todo: Should perhaps decrease or remove this?
		sleep(1);
	}

}


/// ------------------------- MPIAdapter


/**
 * Constructor
 *
 * Just call the constructor of the inherited class.
 */ 	
mpi::MPIAdapter::MPIAdapter() {
 	this->mpi_process_		= NULL;
} 

/**
 * init
 *
 * Initilize the MPI Process
 */ 	
void mpi::MPIAdapter::init(int argc, char* argv[]) {

	// Initialize MPI Communication
	this->mpi_process_ = new mpi::MPIProcess();
	mpi_process_->init(argc, argv);
}

/**
 * Start receiving messages
 *
 */
void mpi::MPIAdapter::run(void) {
	mpi_process_->run();
}

/**
 * Destructor
 *
 * Tear down the MPI Process
 */ 	
mpi::MPIAdapter::~MPIAdapter() {
	if(this->mpi_process_) delete this->mpi_process_;
} 
 
 
/**
 * Send a Message to the process with ran 
 *
 */
void mpi::MPIAdapter::send(int rank_receiver, ghpf::MPIMessage* msg) {
 	 mpi_process_->send(rank_receiver, msg);
}


/**
 * Receive a Message and pass it over to the ActiveObject
 *
 */
void mpi::MPIAdapter::receive(ghpf::MPIMessage* msg) {
 	 // Need to lookup the right ActiveObject this->active_object_->receive(msg);
 	 
}

/**
 * Register active object as receiver for messages in this process
 *
 * 
 */ 	
void mpi::MPIAdapter::registerActiveObject(ghpf::ActiveObject* object, ghpf::uuid* uuid) {
	this->map_active_objects_[(void*)uuid] = object;
}

/**
 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
 *
 * 
 */ 	
int mpi::MPIAdapter::getRank(){
 	 return mpi_process_->getRank();
}

/**
 *  Return the number of processes
 *
 * 
 */ 	
int mpi::MPIAdapter::getNumProcesses(){
 	 return mpi_process_->getNumProcesses();
}


#endif // DISABLE_MPI
