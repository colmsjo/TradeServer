/****************************************************************************
 *
 * @file 		mpi.cpp
 * @brief		Implementes process communicaton using OpenMPI
 * @author      Jonas Colmsj
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef MPI
#define MPI


// Make sure that MPI should be enabled
#ifndef DISABLE_MPI

// All modules must use the common module
#include "common.hpp"

// The following moudles are used - ONLY LAYERS BELOW CAN BE USED
// None...


/// mpi
/**
 * Classes that implements the communication framework defined by os-facade.
 * OpenMPI classes are used in order to achieve this. The boost library is
 * used in order to simplify the implementation.
 *
 */
namespace mpi {


	/// MPIMessageImpl class
	/**
	 * MPIMessageImpl, implements the virtual MPIMessage class
	 *
	 * For classes that it should be possible to send using mpi (mpi::communicator::send/recv),
	 * a serialization function needs to be implemented.
	 *
	 *  // Boost need access to private funtions in order to send/receive messages
	 *  friend class boost::serialization::access;
	 *  template<class Archive> void serialize(Archive & ar, const unsigned int version) { …};
	 *
	 * For classes consisting that sends/receives messages of fixed size is optimization possible.
	 * Non template classes uses the macro BOOST_IS_MPI_DATATYPE(class) to enable this optimization.
	 *
	 * see http://www.boost.org/doc/libs/1_47_0/doc/html/mpi.html for details on boost and MPI communication.
	 */
	 class MPIMessageImpl : public ghpf::MPIMessage {
		// Boost need access to private funtions in order to perform
		// send messages
		friend class boost::serialization::access;

	 private:
	
		/// serialize
		/**
		 * This function performs the actual work of translating the message
		 * into something can can be sent using MPI
		 *
		 * @param ar 
		 * @param version
		 *
		 */ 	
		template<class Archive> void serialize(Archive & ar, const unsigned int version);

		std::string 		command_;
		ghpf::uuid *sender_, *receiver_;
			
	 public:	

		/// MPIMessage
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	MPIMessageImpl();
	 	MPIMessageImpl(ghpf::uuid* sender, ghpf::uuid* receiver);

		/// ~MPIMessage
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	virtual ~MPIMessageImpl() {};

		/// getCommandString
		/**
		 *  Get the command sent in the message
		 *
		 * \returns: The string containg the command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		string getCommand();

		/// setCommand
		/**
		 *  Set the command sent in the message
		 *
		 * @param command The command, typically http://SERVER:PORT/command?param1=val1;param2=val2...
		 */ 	
		void setCommand(std::string command);
					 	
		/// getReceiver
		/**
		 *  Get the UUID of the receiver
		 *
		 * \return the UUID of the receiver
		 */ 	
		ghpf::uuid* getReceiver(void);	
	 	
		/// getSender
		/**
		 *  Get the UUID of the sending ActiveObject
		 *
		 * \return the UUID of the receiver
		 */ 	
		ghpf::uuid* getSender(void);
		
	};

	
	/// MPIAdapter class
	/**
	 * MPIAdapter, implement the virtual Adapter class.
	 * 
	 *  
	 */	
	// \todo: Convert MPIProcess to a MPIAdapter instead	
	class MPIAdapter : public common::Adapter {
	public:	
			
		/// MPIAdapter
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
		MPIAdapter();
		
		/// ~MPICommunicator
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		~MPIAdapter();

		/// init
		/**
		 *  Initialize the context with MPI.
		 *
	 	 * @param argc Number of arguments
	 	 * @param argv array of strings containing arguments
		 * 
		 */ 	
		void init(int argc, char* argv[]);
	
		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		void run(void); 

		/// getRank
		/**
		 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
		 *
	 	 * \return int The rank if this process 
		 * 
		 */ 	
		int getRank();

		/// getNumProcesses
		/**
		 *  Return the number of processes
		 *
	 	 * \return int the number of processes in the MPI world
		 * 
		 */ 	
		int getNumProcesses();
		
		/// send
		/**
		 * Send 
		 *
		 * @param rank the rank of the receiving process
		 * @param msg the Message to send
		 *
		 * \return void 
		 */
		void send(int rank_receiver, ghpf::MPIMessage* msg);
		
		/// send
		/**
		 * Send 
		 *
		 * @param rank the rank of the receiving process
		 * @param msg the Message to send
		 *
		 * \return void 
		 */
		void send(std::string host, int port, ghpf::RESTMessage* msg) { throw ghpf::Exception("MPIAdapter::send not implemented for RESTMessage"); }

		/// receive
		/**
		 * Receive a message from another process 
		 *
		 * @param msg the Message to receive
		 *
		 * \return void 
		 */
		void receive(ghpf::MPIMessage* msg);
			
	 	/// registerActiveObject
	 	/**
	 	 * Register an ActiveObject as a receiver 
	 	 *
	 	 * @param obj The ActiveObject to register as a receiver
	 	 * @param uuid The UUID of the ActiveObject
	 	 *
	 	 * \return void 
	 	 */
	 	void registerActiveObject(ghpf::ActiveObject* obj, ghpf::uuid* uuid);

	 protected:
	 
		/// The receiving ActiveObjects for this process
		std::map<void*, ghpf::ActiveObject*> map_active_objects_;
		
	private:
	 	/// One MPI Process for each process
		mpi::MPIProcess* mpi_process_;
	
	};
		
			
	/// MPIProcess class
	/**
	 * MPIProcess, all static methods 
	 * 
	 *  
	 */	
	class MPIProcess  {
	public:	
		
		
		/// MPIProcess
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
		MPIProcess();
			
			
		/// MPIProcess
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		virtual ~MPIProcess();
		
		/// init
		/**
		 *  Initialize process
		 *
		 * @param argc - number of arguments (from main(int argc, char* argv[])
		 * @param argv - array of strings with arguments (int argc, char* argv[])
		 *
		 */ 	
		void init(int argc, char* argv[]);
		
		
		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		void run(void); 

		/// destroy
		/**
		 *  Tear down process properly.
		 *
		 *
		 */ 	
		void destroy();
		
		
		/// getRank
		/**
		 *  Returns the rank of the MPI process.
		 *
		 *
		 */ 	
		int getRank();
		
		/// getNumProcesses
		/**
		 *  Return the number of processes in the world.
		 *
		 *
		 */ 	
		int getNumProcesses();
		
		/// send
		/**
		 * Send 
		 *
		 * @param rank the rank of the receiving process
		 * @param msg the Message to send
		 *
		 * \return void 
		 */
		void send(int rank_receiver, ghpf::MPIMessage* msg);

	private:
		int rank_, num_processes_;
	};
		
	
	

} // namespace mpi

#endif // DISABLE_MPI

#endif //MPI
