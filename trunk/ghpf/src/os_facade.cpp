/****************************************************************************
 *
 * @file 		os_facade.cpp
 * @brief		The os_facade layer
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

 
// The only include allowed by the coding standards in cpp-files 
#include "os_facade.hpp"


/// ------------------------- _ghpf (global static funtions)

/**
 * init, initialize framework and MPI
 *
 *
 */	 
void _ghpf::init(int argc, char* argv[], string log_file) {
	ghpf::Log::init(log_file);
	
	// Initialize the context for this process (MPI etc.)
	ghpf::Context *ctx = ghpf::Context::Instance();
	ctx->init(argc, argv); 
}
	
/**
 * init, initialize framework but not MPI
 *
 *
 */	 
void _ghpf::init(string log_file) {
	ghpf::Log::init(log_file);
	
	// Initialize the context for this process (MPI etc.)
	ghpf::Context *ctx = ghpf::Context::Instance();
	ctx->init(); 
}

/**
 * exit, tear down everything and exit application
 * 
 */	 
void _ghpf::exit(bool exit_process)  {
	
	// Get a handle to the Context
	ghpf::Context *ctx = ghpf::Context::Instance();

	int exit_code = ctx->getExitCode();

	// Delete the context and all object it contains
	delete ctx;

	// Close the log file
	ghpf::Log::destroy();
	
	// Exiting process
	if(exit_process) exit(exit_code);
}

/**
 *  Turn the current process into a daemon.
 *
 */ 	
pid_t _ghpf::daemonize(void) {
	
	// \todo: Need to do a ifdef and check if we are running UNIX
	return unix_os::UnixProcess::daemonize();
}


/// ------------------------- uuid & uuidImpl


/// create
/**
 *  create
 *
 * 
 */ 	
ghpf::uuid* ghpf::uuid::create() { 
	return new os_facade::uuidImpl(); 
}

ghpf::uuid* ghpf::uuid::create(string uuid_string) { 
	return new os_facade::uuidImpl(uuid_string); 
}

bool operator<(const os_facade::uuidImpl &u1, const os_facade::uuidImpl &u2) {
	return (boost::uuids::uuid)u1 < (boost::uuids::uuid)u2;	
}

string os_facade::uuidImpl::str(void) {
	stringstream ss;
	ss <<  *this;
	return ss.str();	
}



/// ------------------------- ActiveObject

/// ActiveObject
/**
 *  Constructor
 *
 * 
 */ 	
ghpf::ActiveObject::ActiveObject() {
}

/// ~ActiveObject
/**
 *  Destructor
 *
 * 
 */ 	
ghpf::ActiveObject::~ActiveObject() {
}


/// ------------------------- Communicator & CommunicatorImpl


/// create
/**
 *  create new Communicator
 *
 * 
 */ 	
ghpf::Communicator* ghpf::Communicator::create() {
	os_facade::CommunicatorImpl *communicator = new os_facade::CommunicatorImpl();
	return communicator;
}


/**
 * Register active object as receiver for messages in this process
 *
 * 
 */ 	
void os_facade::CommunicatorImpl::registerActiveObject(ghpf::ActiveObject* object, ghpf::uuid *uuid) {
	active_objects_.set( uuid->str(), object );
}

/**
 * Constructor
 *
 * Initialize attributes.
 * 
 */ 
os_facade::CommunicatorImpl::CommunicatorImpl() {
	
	//just in case any exceptions are thrown in the constructors of the Adapters
	rest_adapter_ = NULL;
	mpi_adapter_  = NULL;

}


/**
 * Initialize the process without MPI
 *
 * 
 */ 	
void os_facade::CommunicatorImpl::init(void) {
	
 	// Create a RESTAdapter and save the reference
	rest_adapter_ = http::RESTAdapter::Instance();

	// initilize the REST Adapter
 	rest_adapter_->init();
}

/**
 * Start listening for messages
 *
 * 
 */ 	
void os_facade::CommunicatorImpl::run(void) {
	
	// Start looping forever listening for messages
	rest_adapter_->run();
}

/**
 * Initialize the process
 *
 * 
 */ 	
void os_facade::CommunicatorImpl::init(int argc, char* argv[]) {

	// MPI needs to bi initilized before REST since the MPI rank is used as part of the port
	// the HPPT server listens to (8080+MPI rank)
	try {
		// Create the MPI Adapter
		mpi_adapter_  = new mpi::MPIAdapter();
	
	 	// Initialize the MPI Adapter
		mpi_adapter_->init(argc, argv);
	}
	catch(exception &e) {
		ghpf::Log::log_ERROR("os_facade.cpp","MPI initilization failed");
		delete mpi_adapter_;
		mpi_adapter_ = NULL;
	}

 	// Create a RESTAdapter and save the reference
	rest_adapter_ = http::RESTAdapter::Instance();

	// initilize the REST Adapter
 	rest_adapter_->init();
}


/**
 * Destructor
 *
 * Empty.
 * 
 */ 
os_facade::CommunicatorImpl::~CommunicatorImpl() { 
	if(rest_adapter_) {
		delete rest_adapter_;
		rest_adapter_ = NULL;
	}
	
	if(mpi_adapter_) {
		 delete mpi_adapter_;
		 mpi_adapter_ = NULL;
	}
}


/**
 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
 *
 * 
 */ 	
int os_facade::CommunicatorImpl::getRank(){
 	 return (mpi_adapter_) ? mpi_adapter_->getRank() : -1;
}


/**
 *  Return the number of processes in the MPI world
 *
 * 
 */ 	
int os_facade::CommunicatorImpl::getNumProcesses() {
 	 return (mpi_adapter_) ? mpi_adapter_->getNumProcesses() : 1;
}


/**
 * Send using MPI
 *
 */
void os_facade::CommunicatorImpl::send(int rank_receiver, ghpf::MPIMessage* message) {
	if(mpi_adapter_) mpi_adapter_->send(rank_receiver, message);
	else throw ghpf::Exception("os_facade.cpp","os_facade::CommunicatorImpl::send failed, MPI not initilized");
}

/**
 * Receive a message from another process using MPI
 *
 */
void os_facade::CommunicatorImpl::receive(ghpf::MPIMessage* msg) {
	
	// Get the uuid of the receiving object
	ghpf::uuid* u = msg->getReceiver();
	
	// Get the receiving active object
	ghpf::ActiveObject* active_object = active_objects_.get( u->str() );
	
	// Pass on the message to the receiving active_object
	active_object->receive(msg);
	
	delete u;
}

/**
 * Send using REST
 *
 */
void os_facade::CommunicatorImpl::send(string host, int port, ghpf::RESTMessage* message) {
	if (rest_adapter_) rest_adapter_->send(host, port, message);
	else throw ghpf::Exception("os_facade.cpp","os_facade::CommunicatorImpl::send failed, REST not initilized");
}

/**
 * Receive a message from another process using REST
 *
 */
void os_facade::CommunicatorImpl::receive(ghpf::RESTMessage* msg) {

	// Get the uuid of the receiving object
	ghpf::uuid* u = msg->getReceiver();
	
	// Get the receiving active object
	ghpf::ActiveObject* active_object = active_objects_.get( u->str() );
	
	// Pass on the message to the receiving active_object
	active_object->receive(msg);
	
	delete u;
	
}

	
/// ------------------------- Context

// Initialize the attribute that manages the Singleton pattern 
os_facade::ContextImpl* os_facade::ContextImpl::instance_ = NULL;
	
/**
 * Constructor
 *
 * 
 */ 	
os_facade::ContextImpl::ContextImpl() {
 	this->communicator_		= NULL;
 	this->exit_process_		= false; 	
}


/**
 * Initialize the process
 *
 * 
 */ 	
void os_facade::ContextImpl::init(int argc, char* argv[]) {
	
	// Create a communicator	
 	communicator_ 	= ghpf::Communicator::create();

	// Initialize MPI and REST Communication
	communicator_->init(argc, argv);
 	
}

/**
 * Initialize the process without MPI
 *
 * 
 */ 	
void os_facade::ContextImpl::init(void) {
	
 	// Create a communicator	
 	communicator_ 	= ghpf::Communicator::create();
 	
 	// Initilize communicator without MPI
 	communicator_->init();
 	
}


/**
 * Return a instance of the Singleton. Create one if it does not exist.
 *
 * 
 */ 	
ghpf::Context::Context* ghpf::Context::Instance() {
	return os_facade::ContextImpl::Instance();		
 }

/**
 * Return a instance of the Singleton. Create one if it does not exist.
 *
 * 
 */ 	
ghpf::Context::Context* os_facade::ContextImpl::Instance() {
	if (!instance_)
		instance_ = new os_facade::ContextImpl();
	return instance_;		
 }


/**
 * run
 *
 * 
 */ 	
void os_facade::ContextImpl::run() {
	
	// Start looping forever listening for messages
	communicator_->run();
}

/**
 * Destructor
 *
 * .
 * 
 */ 	
os_facade::ContextImpl::~ContextImpl() {
	this->destroy();
	
	// reset the pointer to the Singleton, a new object will be created if Instance() 
	// called again
	instance_ = NULL;
}

/**
 * Tear down everything
 *
 * .
 * 
 */ 	
void os_facade::ContextImpl::destroy() {
	if(this->communicator_ != NULL) {
		delete this->communicator_;
		this->communicator_ = NULL;
	}	
}


/**
 * Return the Communicator for this process. The Communicatitor should be created
 * by the ProcessInitiator which is instanciated at startup. 
 *
 */
ghpf::Communicator* os_facade::ContextImpl::getCommunicator() {
 	 return communicator_;
}


/**
 *  Set the flag that indictes that the proces should be exited and the assiciated exit code.
 *
 * 
 */ 	
void os_facade::ContextImpl::setExitProcess(int exit_code) {
 	 this->exit_process_ = true;
 	 this->exit_code_    = exit_code;
}

/**
 *  Get the exit code for the process. Typically used once exitProcess has returned true.
 *
 * 
 */ 	
int os_facade::ContextImpl::getExitCode(void) {
 	 return this->exit_code_;
}

/**
 *  Returns true if the process should be terminated. Typically due to a TERM singal.
 *
 * 
 */ 	
bool os_facade::ContextImpl::exitProcess() {
 	 return this->exit_process_;
}


/// ------------------------- ProcessInitiator

/**
 * Constructor
 *
 * 
 */ 	
ghpf::ProcessInitiator::ProcessInitiator() {

	/// Each process requires a Context object	
	ghpf::Context::Instance();

 	this->active_object_ 		= NULL;
}

/**
 * Destructor
 *
 * 
 */ 	
ghpf::ProcessInitiator::~ProcessInitiator() {
 	if(this->active_object_) delete this->active_object_;
}



/// ------------------------- SimpleXMLParserImpl & SimpleXMLParser


/**
 *  create new SimpleXMLParser
 *
 * 
 */ 	
ghpf::SimpleXMLParser* ghpf::SimpleXMLParser::create() {
	os_facade::SimpleXMLParserImpl *xml_parser = new os_facade::SimpleXMLParserImpl();
	return xml_parser;
}

 
// Constants
const string ghpf::SimpleXMLParser::kNameTopNode 			= "xml";
const string ghpf::SimpleXMLParser::kNameKeyAttribute 		= "key";
const string ghpf::SimpleXMLParser::kNameValueAttribute		= "val";


/**
 *  Constructor
 *
 * 
 */ 	
os_facade::SimpleXMLParserImpl::SimpleXMLParserImpl() {
	this->xml_results_		= NULL;
	this->xml_				= NULL;
}

	
/**
 *  Destructor
 *
 * 
 */ 	
os_facade::SimpleXMLParserImpl::~SimpleXMLParserImpl() {
	if(this->xml_results_) 	delete this->xml_results_;	
	if(this->xml_) 			freeXMLString(this->xml_);
}


/**
 *  Parse string with XML
 *
 * 
 */ 	
void os_facade::SimpleXMLParserImpl::parse(string xml) {
	
	// contains information about errors found
	if(this->xml_results_) delete this->xml_results_;
	this->xml_results_ = new XMLResults();

	// parse the string
	this->top_node_ = XMLNode::parseString((XMLCSTR) xml.c_str(), (XMLCSTR) kNameTopNode.c_str(), this->xml_results_);
	
	// check for errors
	if (this->xml_results_->error != eXMLErrorNone  ) {
		string s = string("ghpf::XMLParser::parseXML > XMLNode::parseString failed with error") + 
						XMLNode::getError(this->xml_results_->error);
							
		ghpf::Log::log_ERROR("os_facade.cpp", s);
		throw ghpf::Exception(s);											
	}
	
}

/**
 * getChildNodeText find the child node with a specific tag/name
 *
 * 
 */ 	
string os_facade::SimpleXMLParserImpl::getChildNodeText(string node_name) {
	XMLNode node = this->top_node_.getChildNodeByPath(node_name.c_str());
	this->current_node_ = node;
	
	// Check that the text isn't NULL
	if(!node.getText()) return string();

	return string(this->current_node_.getText());
}

/**
 * getChildNodeText find the i:th child node with a specific tag/name
 *
 * 
 */ 	
string os_facade::SimpleXMLParserImpl::getChildNodeText(string node_name, int index) {
	XMLNode node = this->top_node_.getChildNode(node_name.c_str(), index);
	this->current_node_ = node;
	
	// Check that the text isn't NULL
	if(!node.getText()) return string();

	return string(this->current_node_.getText());
}
 

/**
 * numNodes
 *
 */ 	
int os_facade::SimpleXMLParserImpl::numNodes(string node_name) {
	return this->top_node_.nChildNode(node_name.c_str());	
}


/**
 * getAttributeNameOfCurrentNode
 *
 */ 	
string os_facade::SimpleXMLParserImpl::getAttributeNameOfCurrentNode(int index) {
	XMLCSTR name = this->current_node_.getAttributeName(index);
	
	// return an empty string if nothing was found
	return (name) ? string(name) : string(); 	
}


/**
 * getAttributeValueOfCurrentNode
 *
 */ 	
string os_facade::SimpleXMLParserImpl::getAttributeValueOfCurrentNode(int index) {
	XMLCSTR value = this->current_node_.getAttributeValue(index);
	
	// return an empty string if nothing was found
	return (value) ? string(value) : string(); 	
}


/**
 * createTopNode Creates a new top node of a XML Tree. The existing tree will be deleted
 *               if one exists.
 *
 */ 	
void os_facade::SimpleXMLParserImpl::createTopNode(void) {
	this->top_node_	= XMLNode::createXMLTopNode(kNameTopNode.c_str());
}
 

/**
 * addNode Add a node in the XML tree
 *
 */ 	
void os_facade::SimpleXMLParserImpl::addNode(string node_name) {
	this->current_node_ = this->top_node_.addChild(node_name.c_str());
}


/**
 * addTextToCurrentNode Add text to the current node, <tag>text</tag>
 *
 */ 	
void os_facade::SimpleXMLParserImpl::addTextToCurrentNode(string text) {
	this->current_node_.addText(text.c_str());
}


/**
 * addAttributeToCurrentNode Add text to the current node, <tag>text</tag>
 *
 *
 */ 	
void os_facade::SimpleXMLParserImpl::addAttributeToCurrentNode(string key, string value) {
		this->current_node_.addAttribute(kNameKeyAttribute.c_str(),   key.c_str() );
		this->current_node_.addAttribute(kNameValueAttribute.c_str(), value.c_str() );
}


/**
 * createXML Create XML string from the current XML Tree
 *
 *
 */ 	
string os_facade::SimpleXMLParserImpl::createXML(void) {
	// store the result in a string
	if(this->xml_) freeXMLString(this->xml_);
	this->xml_ = this->top_node_.createXMLString();
	
	return string(this->xml_);
}




