/****************************************************************************
 *   
 * @file 		os_facade.hpp
 * @brief		Only for documentation. All class definitions are in ghpf.
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef OS_FACADE
#define OS_FACADE

// All modules must use the common module
#include "common.hpp"

// the following moudles are used by os_facade - ONLY LAYERS BELOW CAN BE USED
#include "mpi.hpp"
#include "http.hpp"

#ifdef __UNIX_OS__
#include "unix.hpp"
#endif

#ifdef __WINDOWS_OS__
#include "windows.hpp"
#endif



/// os_facade
/**
 *
 * Encapsulates all methods that are specific for a operating system.
 * This also applies to all includes that are OS specific.
 *
 * os_facade also implements classas and methods that a generic where
 * the implementation does that vary depending on OS.
 *
 * Could (should?) also add classes that just inherits the Message, Context, ActiveObject,
 * ProcessInitiator classes from the ghpf namespace.
 *
 */
namespace os_facade {


	/// uuidImpl class
	/**
	 * uuidImpl, implements UUID:s using boost
	 * 
	 * class uuid : public boost::uuids::uuid {
	 * public:
	 *    uuid()
	 *       : boost::uuids::uuid(boost::uuids::random_generator()())
	 *   {}
	 *   
	 *   explicit uuid(boost::uuids::uuid const& u)
	 *       : boost::uuids::uuid(u)
	 *   {}
	 * }; 
	 *
	 *
	 * boost::uuids::string_generator gen;
     * boost::uuids::uuid u1 = gen("{01234567-89ab-cdef-0123456789abcdef}");
	 *
	 */
	 class uuidImpl : public ghpf::uuid, public boost::uuids::uuid {
	 public:
	    uuidImpl(string uuid_string)
	       : boost::uuids::uuid(boost::uuids::string_generator()(uuid_string))
	   {}

	    uuidImpl()
	       : boost::uuids::uuid(boost::uuids::random_generator()())
	   {}
	   
	   explicit uuidImpl(boost::uuids::uuid const& u)
	       : boost::uuids::uuid(u)
	   {}
	   friend bool operator<(const uuidImpl &u1, const uuidImpl &u2);
	   
	   string str(void);
	 }; 



	/// Context class
	/**
	 * Context, virtual class to be implemented in MPI etc.
	 * 
	 * Implemented as a Singleton, 
	 * os-facade.cpp we need Context* Context::m_pInstance = NULL;
	 *
	 * \todo: should move the implementation parts outside the header file
	 * \todo: need to split into Context and ContextImpl, can then make MPIProcess part of ContextImpl
	 */
	
	 class ContextImpl : public ghpf::Context {
	 public:	
	
		/// Instance
		/**
		 *  Create an instance of this Singleton class.
		 *
	 	 *
	 	 * \return Contecxt*
		 * 
		 */ 	
		static Context* Instance();

		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		void run(void); 

		/// init
		/**
		 *  Initialize the context without MPI.
		 *
		 * 
		 */ 	
		void init(void);
	
		/// init
		/**
		 *  Initialize the context with MPI.
		 *
	 	 * @param argc Number of arguments
	 	 * @param argv array of strings containing arguments
		 * 
		 */ 	
		void init(int argc, char* argv[]);
	
		/// getCommunicator
		/**
		 * Return the Communicator for this process. The Communicatitor should be created
		 * by the ProcessInitiator which is instanciated at startup. 
		 *
		 */
		ghpf::Communicator* getCommunicator();
	
	
		/// setExitProcess
		/**
		 *  Set the flag that indictes that the proces should be exited and the assiciated exit code.
		 *
	 	 * @param exit_code The rank if this process 
		 * 
		 */ 	
		void setExitProcess(int exit_code);

		/// getExitCode
		/**
		 *  Get the exit code for the process. Typically used once exitProcess has returned true.
		 *
	 	 * \returns exit_code
		 * 
		 */ 	
		int getExitCode(void);

		/// exitProcess
		/**
		 *  Returns true if the process should be terminated. Typically due to a TERM singal.
		 *
	 	 * \return exit_code  
		 * 
		 */ 	
		bool exitProcess();

		/// destroy
		/**
		 *  Tear down everything.
		 *
		 */ 	
		void destroy();

		 /// ~Context
		 /**
		  *  Destructor
		  *
		  * 
		  */
		virtual ~ContextImpl();
		 
		 
	 private:
		/// Context
		/**
		 *  Hidden constructor
		 *
		 * 
		 */
	 	ContextImpl();

	 	/// used to implement the Singleton pattern
	 	static ContextImpl* instance_;
	
	 	/// The Communicator that is used to communicate with other processes
	 	ghpf::Communicator* communicator_;
	 		 	
	 	/// flag set by the signal handler etc. in order to exit the current process
		bool exit_process_;
	 	
	 	/// the return code when exiting the process
	 	int  exit_code_;
	};


	/// CommunicatorImpl class
	/**
	 * CommunicatorImpl, virtual class
	 * 
	 * Let Communicator use Adapters to communicate. Adapters can for instance be MPI
	 * or REST adapters.
	 *
	 * Communicator
	 * 		receive(Message &m)
	 * 		send(Message &m)
	 *
	 * Adapter (is a Singletons)
	 * 		Adapter(Communicator)
	 * 		send(Message &m)
	 *
	 * Message
	 *		getAdapter(Communicator)
	 *
	 *
	 *
	 * Use cases
	 *	Adapter receives message
	 *		1. Adapter receives message (from MPI or via HTTP etc.)
	 *		2. Adpater calls Communicator.receive(message) - the message contains the uuid of the receiving ActiveObject
	 *		
	 *	ActiveObject want's to send a message
	 *		1. ActiveObject calls Context.getCommunicator
	 *		2. ActiveObject calls Communicator.send(message)
	 *		3. Communicator calls getAdapter of the Message
	 *		4. Communicator calls Adapter.send(message)
	 *
	 *
	 */	
	 class CommunicatorImpl : public ghpf::Communicator {
	 public:	
	
		/// CommunicatorImpl
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
		CommunicatorImpl(); 
	 	
		/// ~CommunicatorImpl
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	
		virtual ~CommunicatorImpl();
	
		/// init
		/**
		 *  Initialize the communicator without MPI.
		 *
		 * 
		 */ 	
		void init(void);

		/// init
		/**
		 *  Initialize the context with MPI.
		 *
	 	 * @param argc Number of arguments
	 	 * @param argv array of strings containing arguments
		 * 
		 */ 	
		void init(int argc, char* argv[]);

		/// run
		/**
		 *  Start processing messages
		 *
		 * 
		 */ 	
		void run(void); 

		/// getRank
		/**
		 *  Return the rank, i.e. a unique identifier for the process that this Context represents.
		 *
	 	 * \return int The rank if this process 
		 * 
		 */ 	
		int getRank();
	
		/// getNumProcesses
		/**
		 *  Return the number of processes
		 *
	 	 * \return int the number of processes in the MPI world
		 * 
		 */ 	
		int getNumProcesses();

	 	/// send
	 	/**
	 	 * Send 
	 	 *
	 	 * @param rank the rank of the receiving process
	 	 * @param msg the Message to send
	 	 *
	 	 * \return void 
	 	 */
	 	void send(int rank_receiver, ghpf::MPIMessage* msg);
	
	 	/// receive
	 	/**
	 	 * Receive a message from another process 
	 	 *
	 	 * @param msg the Message to receive
	 	 *
	 	 * \return void 
	 	 */
	 	void receive(ghpf::MPIMessage* msg);
	
	 	/// send
	 	/**
	 	 * Send 
	 	 *
	 	 * @param host receiving host
	 	 * @param port the port at the receving host
	 	 * @param msg the Message to send using REST
	 	 *
	 	 * \return void 
	 	 */
	 	void send(string host, int port, ghpf::RESTMessage* msg);
	
	 	/// receive
	 	/**
	 	 * Receive a message from another process using REST
	 	 *
	 	 * @param msg the Message to receive
	 	 *
	 	 * \return void 
	 	 */
	 	void receive(ghpf::RESTMessage* msg);

	 	/// register
	 	/**
	 	 * Register an ActiveObject as a receiver 
	 	 *
	 	 * @param obj The ActiveObject to register as a receiver
	 	 *
	 	 * \return void 
	 	 */
	 	 void registerActiveObject(ghpf::ActiveObject* obj, ghpf::uuid* uuid);
	 	 	 
	 protected:
	 
		/// The receiving ActiveObjects for this process
		// std::map<std::string, ghpf::ActiveObject*> active_objects_;
		// using home made map that throws exceptions if key that haven't been defined are accessed
		common::map<std::string, ghpf::ActiveObject*> active_objects_;
		
	private:
		http::RESTAdapter* rest_adapter_;
		mpi::MPIAdapter*   mpi_adapter_; 
		
	};


	/// SimpleXMLParser class
	/**
	 * SimpleXMLParser, encapsulates the XML Parser used, currently xmlParser from Business-Insight
	 * 
	 * Only simple one-level XML Tree are currently supported. Use the xmlParser parser 
	 * directly if more advanced features are necessary.
	 * <xml>
	 *		<tag_1>text text</tag_1>
	 *		<tag_2>text text</tag_2>
	 *		<tag_3 param="..." value="..."/>
	 *		...
	 * </xml>
	 *
	 * Methods encapsulated for parsing XML strings:
	 * + static XMLNode::parseString
	 * + XMLNode::getChildNodeByPath
	 * + XMLNode::getText
	 * + XMLNode::nChildNode
	 * + XMLNode::getAttributeName
	 * + XMLNode::getAttributeValue
	 *
	 * Methods encapsulated for creating XML strings:
	 * + static XMLNode::createXMLTopNode
	 * + XMLNode::addChild
	 * + XMLNode::addText
	 * + XMLNode::addAttribute
	 * + XMLNode::createXMLString
	 *
	 * \see SimpleXMLParser for documentation on the functions.
	 *
	 * The pattern used is described here: 
	 * http://stackoverflow.com/questions/778802/c-header-file-that-declares-a-class-and-methods-but-not-members
	 *
	 */
	
	 class SimpleXMLParserImpl : public ghpf::SimpleXMLParser {
	 public:	
	
		/// SimpleXMLParserImpl
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
		SimpleXMLParserImpl(); 
	
		/// ~SimpleXMLParserImpl
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		virtual ~SimpleXMLParserImpl();
	
		/// parse
		/**
		 *  Parse string with XML
		 *
		 */ 	
		void parse(string xml);

		/// getChildNodeText
		/**
		 * getChildNodeText find the child node with a specific tag/name
		 *
		 * 
		 */ 	
		string getChildNodeText(string node_name);
		string getChildNodeText(string node_name, int index);
		 		 
		/// numChildNodes
		/**
		 * numChildNodes
		 *
		 */ 	
		int numNodes(string node_name);

		/// getAttributeNameOfCurrentNode
		/**
		 * getAttributeNameOfCurrentNode
		 *
		 */ 	
		 string getAttributeNameOfCurrentNode(int index);

		/// getAttributeValueOfCurrentNode
		/**
		 * getAttributeValueOfCurrentNode
		 *
		 */ 	
		 string getAttributeValueOfCurrentNode(int index);

		/// createTopNode
		/**
		 * createTopNode Creates a new top node of a XML Tree. The existing tree will be deleted
		 *               if one exists.
		 *
		 */ 	
		 void createTopNode(void);
		 
		/// addNode
		/**
		 * addNode Add a node in the XML tree
		 *
		 */ 	
		 void addNode(string node_name);

		/// addTextToCurrentNode
		/**
		 * addTextToCurrentNode Add text to the current node, <tag>text</tag>
		 *
		 */ 	
		 void addTextToCurrentNode(string text);

		/// addAttributeToCurrentNode
		/**
		 * addAttributeToCurrentNode Add text to the current node, <tag>text</tag>
		 *
		 *
		 */ 	
		 void addAttributeToCurrentNode(string key, string value);

		/// createXML
		/**
		 * createXML Create XML string from the current XML Tree
		 *
		 *
		 */ 	
		 string createXML(void);

	private:
		XMLNode    top_node_, current_node_;
		XMLResults *xml_results_;
		XMLSTR		xml_;

	};


	
} // namespace os_facade


#endif // OS_FACADE
