/****************************************************************************
 * * @file 		tss.cpp
 * @brief		Time Series Server implementation.
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

 
// The only include allowed by the coding standards in cpp-files 
#include "tss.hpp"

/**
 * Receive a Message and pass it over to the ActiveObject
 *
 */
void tss::CalculationEngine::receive(ghpf::MPIMessage* msg) {
 	 
}

 
/**
 * Receive a Message and pass it over to the ActiveObject
 *
 */
void tss::Monitor::receive(ghpf::MPIMessage* msg) {
 	 
}

/**
 * Receive a Message and pass it over to the ActiveObject
 *
 */
void tss::OrderExecutor::receive(ghpf::MPIMessage* msg) {
 	 
}

/**
 * Receive a Message and pass it over to the ActiveObject
 *
 */
void tss::TimeSeriesProcess::receive(ghpf::MPIMessage* msg) {
 	 
}


/// ------------------------- ProcessInitiator


/**
 * Constructor
 *
 * 
 */ 	
tss::TSSProcessInitiator::TSSProcessInitiator() {
	this->active_object_ 		= NULL;
}

/**
 * Destructor
 *
 * 
 */ 	
tss::TSSProcessInitiator::~TSSProcessInitiator() {
 	if(this->active_object_) delete this->active_object_;
}


/**
 * Initialize the process and create the ActiveObject  
 *
 * 
 */ 	
void tss::TSSProcessInitiator::init() {
 	 
 	 ghpf::Context *ctx = ghpf::Context::Instance();
 	 
 	 switch( ctx->getCommunicator()->getRank() ) {
 	 case TIME_SERIES_PROCESS:
 	 	 //this->active_object_ = new ...;
 	 	 break;
 	 	 
 	 case ORDER_EXECUTOR:
 	 	 //this->active_object_ = new ...;
 	 	 break;
 	 	 
 	 case MONITOR:
 	 	 //this->active_object_ = new ...;
 	 	 break;

 	 case CALCULATION_ENGINE:
 	 	 //this->active_object_ = new ...;
 	 	 break;
 	 }
 	 	 
 	 	 
}


// the class factories
extern "C" tss::TSSProcessInitiator* create(ghpf::Context* ctx) {
    tss::TSSProcessInitiator *pi = new tss::TSSProcessInitiator;
    pi->init();
    return pi;
}

extern "C" void destroy(tss::TSSProcessInitiator* p) {
    delete p;
}
