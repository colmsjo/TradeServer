/****************************************************************************
 *
 * @file 		tss.hpp
 * @brief		Time Series Server
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef TSS
#define TSS

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>

#include "ghpf.hpp"
// not allowed #include "common.hpp"


/// tss
/**
 * The Time Series Server
 * 
 * ActiveObjects:
 * <ul>
 *   <li> TimeSeriesProcess - contains the source of the time series that are used
 *   <li> CalculationEngine - performes calculations, can be several processes
 *   <li> Monitor - monitors the health of the other processes by pinging them
 *   <li> OrderExecutor - Executes work defined by the CalculationEngines
 * </ul>
 *
 * Entity classes:
 * <ul>
 *   <li> TimeSeries
 *   <li> TSRowData - Singleton with names for the rows (store ID:s etc.)
 * </ul>
 *
 */
namespace tss {

	static const int TIME_SERIES_PROCESS = 0;
	static const int ORDER_EXECUTOR		 = 1;
	static const int MONITOR 			 = 2;	
	static const int CALCULATION_ENGINE	 = 3;
	
	
	/************************************************ ENTITY CLASSES *********************************************************/
	
	
	/// Entity class
	/**
	 * Entity, base class for all entity classes
	 * 
	 *  
	 */
	
	class Entity {
	public:	
	
		/// Entity
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	
	 	Entity();
	};
	
	
	
	/// TimeSeries class
	/**
	 * TimeSeries, contains the time series data using a vector
	 * 
	 * IMPLEMENT A PROTYTYPE, TimeSeries<base_type>, IN ORDER TO MAKE THIS MORE FLEXIBLE
	 * WILL NEED BOTH A INT AND ADOUBLE TIME SERIES FOR THE REPLENISHMENT IMPLEMENTATION.
	 *  
	 */
	
	class TimeSeries : public Entity {
	public:	
	
		/// TimeSeries
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	
	 	TimeSeries();
	};
	
	
	/// TSRowData class
	/**
	 * TSRowData, names if the rows in the different time series
	 * 
	 * Implement as a Singleton? There should only be one!
	 * 
	 */
	
	class TSRowData : public Entity {
	public:	
	
		/// TSRowData
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	
	 	TSRowData();
	
	private:
		std::vector<std::string> identifiers;	/// NEED TO CHECK THE DATATYPES IN BOOST!!!!
	};
	
	
	/// MessageQueue class
	/**
	 * MessageQueue, a queue used by OrderExecutor and the OrderExecuterWorkerThread
	 *
	 * NOT IMPLEMENTED!!!
	 * 
	 */
	
	class MessageQueue : public Entity {
	public:	
	
		/// MessageQueue
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	
	 	MessageQueue();
	
		//void put(...);			//WHAT ARGUMENT DO I NEED?
		//... get();
	
	};
	
	
	/************************************************ ACTIVE CLASSES *********************************************************/
	
	
	
	/// CalculationEngine class
	/**
	 * CalculationEngine Performs the actual calculations
	 * 
	 *  
	 */
	
	 class CalculationEngine : public ghpf::ActiveObject {
	 public:	
	
		/// CalculationEngine
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	CalculationEngine();
	
		 /// ~CalculationEngine
		 /**
		  *  Destructor
		  *
		  * 
		  */ 	
		 
		 ~CalculationEngine();
	
		 /// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
		 void receive(ghpf::MPIMessage *msg); 
	
	};
	
	
	/// Monitor class
	/**
	 * Monitor, monitors the running processes by pinging them
	 * 
	 *  
	 */
	 class Monitor : public ghpf::ActiveObject {
	 public:	
	
		/// Monitor
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	Monitor();
	
		/// ~Monitor
		/**
		*  Destructor
		*
		* 
		*/ 	
		 
		~Monitor();
	
		 /// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
		 void receive(ghpf::MPIMessage *msg); 
	 };
	
	
	/// OrderExecutor class
	/**
	 * OrderExecutor, executes orders send from the CalculationEngines
	 * 
	 *  
	 */
	class OrderExecutor : public ghpf::ActiveObject {
	public:	
	
		/// OrderExecutor
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	OrderExecutor();
	
		 
		/// ~OrderExecutor
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		 
		~OrderExecutor();
		 
		 
	  	/// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
		 void receive(ghpf::MPIMessage *msg); 
	};
	
	
	/// TimeSeriesProcess class
	/**
	 * TimeSeriesProcess, maintains the source time series that all calculations are based on.
	 * The time series are divided into time series groups. Each CalculationEngine used one, and only one,
	 * time series group as input for the calculations.
	 *  
	 */
	 class TimeSeriesProcess : public ghpf::ActiveObject {
	 public:	
	
		/// TimeSeriesProcess
		/**
		 *  Constructor
		 *
		 * 
		 */ 	
	
	 	TimeSeriesProcess();
	
		/// ~TimeSeriesProcess
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
		 
		 ~TimeSeriesProcess();
		 
		 
	 	/// receive
		/**
		 *  Receive message
		 *
	 	 * @param msg The Message to receive
	 	 *
	 	 * \return void 
		 * 
		 */ 	
		 void receive(ghpf::MPIMessage *msg); 
	 };
	
	
	/// TSSProcessInitiator class
	/**
	 * Initiates the a Time Series Process with its processes:
	 * - CalculationEngine
	 * - OrderExecutor
	 * - Monitor
	 * - TimeSeriesProcess
	 *  
	 */
	 class TSSProcessInitiator : public ghpf::ProcessInitiator {
	 public:
	 	
		/// TSSProcessInitiator
		/**
		 *  Contructor
		 *
		 * 
		 */ 	
	 	TSSProcessInitiator();
	
		/// TSSProcessInitiator
		/**
		 *  Destructor
		 *
		 * 
		 */ 	
	 	~TSSProcessInitiator();
	 	
	 	/// init()
	 	/**
	 	 * Initialize the process and create the Active Object for this particular 
	 	 * process.
	 	 *
	 	 * @param ctx The Context for this process.
	 	 */
	 	void init();
	
	 };


} // namespace tss


#endif // TSS
