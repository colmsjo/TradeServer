/****************************************************************************
 *
 * @file 		unix.cpp
 * @brief		Implements the os-facade layer using unix/os x functions.
 * @author      Jonas Colmsjö
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/


// The only include allowed by the coding standards in cpp-files 
#include "unix.hpp"


/// UnixProcess
/**
 * Constructor
 *
 */ 	

unix_os::UnixProcess::UnixProcess() {
		
}

// ~UnixProcess
/**
 * Destructor
 *
 */ 	

unix_os::UnixProcess::~UnixProcess() {
		
}


/// signal_handler()
/**
 * Handle signals from the operating system. The signal handler is
 * registered when the process is daemonized.
 *
 *
 * @param sig the signal received
 * 
 */ 	
void signal_handler(int sig) {
 
    switch(sig) {
        case SIGHUP:
        	ghpf::Context::Instance()->setExitProcess(EXIT_SUCCESS);
            ghpf::Log::log_INFORMATIONAL("unix layer", "Received SIGHUP signal.");
            break;
        case SIGTERM:
        	ghpf::Context::Instance()->setExitProcess(EXIT_SUCCESS);
            ghpf::Log::log_INFORMATIONAL("unix layer", "Received SIGTERM signal.");
            break;
        default:
        	stringstream s;
        	s <<  "Unhandled signal: " << strsignal(sig);
            ghpf::Log::log_INFORMATIONAL("unix layer", s.str().c_str());
            break;
    }
    
    return;
}

/// daemonize()
/**
 * Make the process a daemon. The current process is forked and then
 * parent processes exites while the child becomes the daemon.
 *
 */ 	
pid_t unix_os::UnixProcess::daemonize(void) {
	
    /* Our process ID and Session ID */
    pid_t pid, sid;

    // Setup signal handling. This will also be used by the child process.
    ghpf::Log::log_DEBUG("Unix layer","Setting up signal handling...");
    signal(SIGHUP,  signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT,  signal_handler);
    signal(SIGQUIT, signal_handler);


    /* Fork off the parent process, the child gets 0 returned while the parent gets the PID of the child process */
    pid = fork();
    if (pid < 0) {
       	stringstream s;
       	s <<  "Error performing fork(), exiting process! PID/error code recived on fork:" << pid;
    	ghpf::Log::log_ERROR("Unix layer",s.str().c_str());
        exit(EXIT_FAILURE);
    }
    
    /* If we got a good PID, then just return. */
    if (pid > 0) {
		return pid;
    }
	
	//The following is only executed by the child process

    /* Change the file mode mask */
    ghpf::Log::log_DEBUG("Unix layer","Setting umask(0)...");
    umask(0);

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
       	stringstream s;
       	s <<  "Error performing setsid(), exiting process! SID/error code recived on setsid:" << sid;
    	ghpf::Log::log_ERROR("Unix layer",s.str().c_str());
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory */
    int i;
    if ((i = chdir("/")) < 0) {
       	stringstream s;
       	s <<  "Error performing chdir('/'), exiting process! Error code recived on setsid:" << i;
    	ghpf::Log::log_ERROR("Unix layer",s.str().c_str());
        exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    ghpf::Log::log_DEBUG("Unix layer","Closing stdin, stdout and stderr...");
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	return pid;
}

