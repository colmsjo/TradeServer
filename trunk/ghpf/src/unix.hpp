/****************************************************************************
 *   
 * @file 		unix.hpp
 * @brief		unix implementation of the operating System dependent classes
 * @author      Jonas Colmsjö
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/

#ifndef UNIX_OS
#define UNIX_OS

/// Header files that are unique for unix
#include <unistd.h>
#include <sys/stat.h>


// All modules must use the common module
#include "common.hpp"

// The following moudles are used - ONLY LAYERS BELOW CAN BE USED
// None...


/// unix
/**
 *
 *
 */ 
namespace unix_os {


	/// UnixProcess class
	/**
	 * UnixProcess, unix implementation of the process encapsulation
	 * 
	 *  
	 */
	class UnixProcess : public common::OSProcess {
	public:	
	
		/// UnixProcess
		/**
		 * Constructor
		 *
		 */ 		
		UnixProcess();
		
		// ~UnixProcess
		/**
		 * Destructor
		 *
		 */ 		
		~UnixProcess();
				
		/// daemonize()
		/**
		 * Make the process a daemon. The current process is forked and then
		 * parent processes exites while the child becomes the daemon.
		 *
		 * \return the process id of the new daemon process
		 */ 	
		static pid_t daemonize(void);
		
	};


} // namespace unix_os



#endif // UNIX_OS
