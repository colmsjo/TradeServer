#!/bin/bash


echo "Running test_suite, make sure there are no errors before continuing with the next step"
sudo ./build/test_suite

echo "Running 'sudo mpirun -np 2 build/test_mpi'"
sudo mpirun -np 2 build/test_mpi

