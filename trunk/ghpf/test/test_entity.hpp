/****************************************************************************
 *
 * @file 		test_entity.hpp
 * @brief		Unit tests.
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/


#include "../src/entity.hpp"


//-----------------------------------------
//				Entities and Entity tests


class EntityTestSuite : public Test::Suite {
public:
	EntityTestSuite() {
		TEST_ADD(EntityTestSuite::test1)

	}
		
private:

	void test1() {
		string NAME("Name"), AGE("Age"), GENDER("Gender");
		
		std::map<string, entity::attribute_type>* field_types = new std::map<string, entity::attribute_type>;
		(*field_types)[NAME]   = entity::_WSTRING;
		(*field_types)[AGE]    = entity::_LONG;
		(*field_types)[GENDER] = entity::_CHAR;
		
		entity::Entities* persons = entity::Entities::create("Persons", field_types);
		
		ghpf::uuid* kalle = persons->createEntity();
				
		persons->set(kalle, NAME,   wstring(L"Kalle Karlsson"));
		persons->set(kalle, AGE,    44L);
		persons->set(kalle, GENDER, 'M');
		
		// haven't commited yet
		TEST_THROWS_NOTHING( persons->getWstring(kalle, NAME) );
		TEST_THROWS_NOTHING( persons->getLong(kalle, AGE) 	  );
		TEST_THROWS_NOTHING( persons->getChar(kalle, GENDER)  );
		
		persons->commit();

		TEST_ASSERT( persons->getWstring(kalle, NAME) 	 == wstring(L"Kalle Karlsson"));
		TEST_ASSERT( persons->getLong(kalle, AGE) 	 == 44L);
		TEST_ASSERT( persons->getChar(kalle, GENDER) == 'M');
		
		delete persons;
		delete kalle;
		delete field_types;

	}
};

