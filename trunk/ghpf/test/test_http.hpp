/****************************************************************************
 *
 * @file 		test_http.hpp
 * @brief		Unit tests.
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/


#include "../src/ghpf.hpp"
#include "../src/http.hpp"
#include "../src/unix.hpp"


//-----------------------------------------
//				ghpf::uuid tests

class uuidTestSuite : public Test::Suite {
public:
	uuidTestSuite() {
		TEST_ADD(uuidTestSuite::test1)

	}
		
private:

	void test1() {
		ghpf::uuid* u1 = ghpf::uuid::create();
							
		//TEST_ASSERT( url_parser->parseUrl(url) 		== http::START );
		
		ghpf::Log::log_DEBUG("test_http.hpp", u1->str() );
		
		
		delete u1;
	}
};



//-----------------------------------------
//				ghpf::Url tests


class UrlTestSuite : public Test::Suite {
public:
	UrlTestSuite() {
		TEST_ADD(UrlTestSuite::valid_url_no_params)
		TEST_ADD(UrlTestSuite::valid_url_with_params)
		TEST_ADD(UrlTestSuite::valid_serverurl_with_params)
		TEST_ADD(UrlTestSuite::invalid_url1)

		TEST_ADD(UrlTestSuite::valid_shorturl_with_params)

	}
		
private:

	void valid_url_no_params() {
		string url("http://localhost:8080/abcde-034k-ldskjf0-lkjdf/start");
		http::Url *url_parser = new http::Url();
						
		TEST_ASSERT( url_parser->parseUrl(url) 		== common::http_definitions::kStart );
		TEST_ASSERT( url_parser->getCommand() 		== common::http_definitions::kStart );
		TEST_ASSERT( url_parser->getCommandString() == string("start") );
		
		//stringstream ss;
		//ss << "url_parser->getCommandString():" << url_parser->getCommandString();
		//ghpf::Log::log_DEBUG("test_http.hpp", ss.str() );
		
		TEST_ASSERT( url_parser->getUUID() 			== string("abcde-034k-ldskjf0-lkjdf") );
		
		delete url_parser;
	}

	void valid_url_with_params() {
		string url("http://localhost:8080/abcde-034k-ldskjf0-lkjdf/start?parm1=val1;parm2=val2");
		http::Url *url_parser = new http::Url();
		
		TEST_ASSERT( url_parser->parseUrl(url) 		== common::http_definitions::kStart);
		TEST_ASSERT( url_parser->getCommand() 		== common::http_definitions::kStart );
		TEST_ASSERT( url_parser->getCommandString() == string("start") );
		//TEST_ASSERT( url_parser->getUUID() 			== string("abcde-034k-ldskjf0-lkjdf") );
		
		TEST_ASSERT( url_parser->getParamVal("parm1")			== string("val1") );
		TEST_ASSERT( url_parser->getParamVal("parm2")			== string("val2") );

		delete url_parser;
		
	}

	void valid_shorturl_with_params() {
		string url("/abcde-034k-ldskjf0-lkjdf/start?parm1=val1;parm2=val2");
		http::Url *url_parser = new http::Url();
		
		TEST_ASSERT( url_parser->parseShortUrl(url)	== common::http_definitions::kStart);
		TEST_ASSERT( url_parser->getCommand() 		== common::http_definitions::kStart );
		TEST_ASSERT( url_parser->getCommandString() == string("start") );
		//TEST_ASSERT( url_parser->getUUID() 			== string("abcde-034k-ldskjf0-lkjdf") );
				
		TEST_ASSERT( url_parser->getParamVal("parm1")			== string("val1") );
		TEST_ASSERT( url_parser->getParamVal("parm2")			== string("val2") );

		delete url_parser;
		
	}

	void valid_serverurl_with_params() {
		string url("http://localhost:8080/server/start?parm1=val1;parm2=val2");
		http::Url *url_parser = new http::Url();
		
		TEST_ASSERT( url_parser->parseUrl(url) 		==  common::http_definitions::kStart);
		TEST_ASSERT( url_parser->getCommand() 		== common::http_definitions::kStart );
		TEST_ASSERT( url_parser->getCommandString() == string("start") );
				
		//TEST_ASSERT( url_parser->getUUID() 			== string("server") );
		
		TEST_ASSERT( url_parser->getParamVal("parm1")			== string("val1") );
		TEST_ASSERT( url_parser->getParamVal("parm2")			== string("val2") );

		delete url_parser;
		
	}

	void invalid_url1() {
		string url("http://localhost:8080/abcde-034k-ldskjf0-");
		http::Url *url_parser = new http::Url();
		TEST_ASSERT( url_parser->parseUrl(url) == common::http_definitions::kError );
		delete url_parser;
		
	}

	
};


//-----------------------------------------
//				http::RESTClient tests

class RESTClientTestSuite : public Test::Suite {
public:
	RESTClientTestSuite() {
		TEST_ADD(RESTClientTestSuite::simple_get_of_di)

	}
		
private:

	void simple_get_of_di() {
		string url("http://di.se");
		http::RESTClient *client = http::RESTClient::create();
						
		//TEST_ASSERT( url_parser->parseUrl(url) 		== http::START );
		
		int res = client->get(url);
		//http::Log::log_DEBUG("test_http.cpp", *(client->getContents()) );
		
		
		delete client;
	}
};


//-----------------------------------------
//				http::SimpleActiveObject, http::CommandMessage tests

class SimpleActiveObjectTestSuite : public Test::Suite {
public:
	SimpleActiveObjectTestSuite() {
		TEST_ADD(SimpleActiveObjectTestSuite::simple_command_message)

	}
		
private:

	void simple_command_message() {
		try {
			
			string url_string("http://localhost:8080/server/start?parm1=val1&parm2=val2");
			
			// Create a command with the contents of the url
			http::RESTMessageImpl *rest_message = new http::RESTMessageImpl();
			rest_message->parseUrl(url_string);		
	
			// Create a XML string		
			string xml = rest_message->toXML();
	
			// Try to parse the XML string just created		
			http::RESTMessageImpl *rest_message2 = new http::RESTMessageImpl();
	
			// TODO: should add a try clause or a ASSERT_DOES_NOT_THOW…
			rest_message2->parseXML(xml);		

			string url2 = rest_message2->getURL();
			
			// Check that the two commands are equal
			TEST_ASSERT( boost::iequals(url_string, url2) );
						
			delete rest_message;
			delete rest_message2;
		}
		catch (exception &e) {
			stringstream ss;
			ss << "Caught exception in simple_command_message:" << e.what();
			ghpf::Log::log_DEBUG("test_http.cpp", ss.str() );
		}

	}
};


//-----------------------------------------
//				ghpf::RESTServer and RESTClient tests


class RESTTestSuite : public Test::Suite {
public:
	RESTTestSuite() {
		TEST_ADD(RESTTestSuite::test1)

	}
		
private:

	// The test is performed in two processes, one for the RESTServer (the test process)
	// and one for the RESTClient (a new daemon process)
	void test1() {
		

		if( unix_os::UnixProcess::daemonize() > 0) {
			
			// ******* THE PARENT/SERVER PROCESS
			
		
			try {
	
		    	ghpf::Log::log_INFORMATIONAL("test_http_server.cpp","RESTTestSuite:Have forked the process, this is the parent/server process...");
				
				// log file name
				std::string _LOG_FILE_ = "/var/log/ghpf_test_http";
			
				_ghpf::init(_LOG_FILE_);
			
				// Create the ActiveObjects, should be Specialized in a real application
				// and one or more ActiveObjects created
				ghpf::ProcessInitiator::ProcessInitiator process_initiator;
			
				// Do some work...
				sleep(3);
					
				_ghpf::exit(true);
				
			}
			catch (exception &e) {
				stringstream ss;
				ss << "Caught exception in simple_command_message:" << e.what();
				ghpf::Log::log_DEBUG("test_http.cpp", ss.str() );
			}
	

		}

		// ******* THE CHILD/CLIENT PROCESS


    	ghpf::Log::log_INFORMATIONAL("test_http_server.cpp","RESTTestSuite:Have forked the process, this is the child/client process...");
		
		// Exit the child process
	    exit(true);
	}
};

