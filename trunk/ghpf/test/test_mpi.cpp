/****************************************************************************
 *
 * @file 		test_mpi.cpp
 * @brief		Implements test of MPI functions.
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Tests for the MPI implementation. Boost unit test framework does not work
 * together with MPI. Here is the Time Series Server stared and logging turned 
 * on.
 *
 * IMPORTANT: THIS EXECUTABLE MUST BE STARTED USING 'mpirun -np <number of proceses> test_mpi'
 *            It is possible to execute the program without MPI (using ./test_mpi)
 *            but only REST will be tested and the client and the server will be in the same
 *            process (but in different threads of course).
 *
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>

#include "../src/ghpf.hpp"


const static std::string kUuid = "7ad988c2-18c3-4d74-82bb-ef1a9d4266e3";


class TestActiveObject : public ghpf::ActiveObject {
public:	


 	TestActiveObject() {
		uuid_ = ghpf::uuid::create(kUuid);		
 	}
 	
 	~TestActiveObject() {
 		delete uuid_;	
 	}
 	
 	ghpf::uuid* getUUID(void) {
 		return uuid_;	
 	}

	// init is only used in process with rank 0
	void init(void) {
		
 		// Send a message to the process with rank 0
 		ghpf::Context* 		context 		= ghpf::Context::Instance();
		ghpf::Communicator* communicator   	= context->getCommunicator();

		// Check if there is more than one process running
		int port = (communicator->getNumProcesses() == 1) ? ghpf::kRESTPort : ghpf::kRESTPort+1;

		// construct the url
		//stringstream ss;
		//ss << "http://localhost:" << port << "/server/start?parm1=val1;parm2=val2";
		//string url_string = ss.str();

		//--------------------------- REST CLIENT
				

		// Create a command with the contents of the url, the same object is both sender and receiver
		ghpf::RESTMessage* rest_message = ghpf::RESTMessage::create(uuid_, uuid_);
		
		rest_message->setCommand("test_command");
		rest_message->addParamVal("param1","val1");
		rest_message->addParamVal("param2","val2");

		stringstream ss2;
		ss2 << "REST CLIENT:Trying to send message:" << rest_message->getShortURL() << " to localhost:" << port << 
														",\n MAKE SURE THE SERVER ACCEPTS MESSAGES";
		ghpf::Log::log_DEBUG("test_mpi.cpp", ss2.str() );
		
		sleep(5); 		// wait 5 sec
 
		communicator->send("localhost", port, rest_message); 		
  
		ghpf::Log::log_DEBUG("test_mpi.cpp", "REST CLIENT:Have sent the message");

		sleep(5); 		// wait 5 sec - in order to make sure that the server had tine to processes the message
						// Needed when server and client are in the same processes
		 	

		//--------------------------- MPI


		// Create MPI message with the contants of a url, sending and receiving object are the same (but in different processes)
		ghpf::MPIMessage* mpi_message = ghpf::MPIMessage::create(uuid_, uuid_);
		mpi_message->setCommand(rest_message->getURL());

		// send message to MPI process with rank 1
		//communicator->send(1, mpi_message); 		


		//--------------------------- Cleanup


 		delete rest_message;
		delete mpi_message;

	}
 	
 	// Not used in this test
 	void receive(ghpf::MPIMessage* msg) {
 		
 		
 	}


	// only used in process with rank 1
	void receive(ghpf::RESTMessage* msg) {
		stringstream ss;
		ss << "SERVER:Have received the message:" << msg->getShortURL();
		ghpf::Log::log_DEBUG("test_mpi.cpp", ss.str());
		
 		// Set exit process flag with return code 1
 		ghpf::Context::Instance()->setExitProcess(1);
		
	}
 	
private:
	ghpf::uuid* uuid_;
	
};


class TestProcessInitiator : public ghpf::ProcessInitiator {
public:
	TestProcessInitiator() {};
	~TestProcessInitiator() {};
	
	void init() {
		ghpf::Context      *context        = ghpf::Context::Instance();
		ghpf::Communicator *communicator   = context->getCommunicator();


		// Check if there is more than one process running
		if(communicator->getNumProcesses() <= 1) {
			ghpf::Log::log_DEBUG("test_mpi.cpp","At least two processes are needed, start with 'sudo mpirun -np 2 test_mpi'");
			_ghpf::exit(true);
		}

		
		TestActiveObject   *active_object = new TestActiveObject();
		
		// The processes does not know each others UUID, NEED TO FIX THIS...
		communicator->registerActiveObject(active_object, active_object->getUUID());
		
		switch(communicator->getRank()) {
			case 0:
			
				ghpf::Log::log_DEBUG("test_mpi.cpp","Process with rank 0");


				//--------------------------- REST CLIENT

				// This process will send messages to the process with rank 1
				active_object->init();
				break;
				
				
			case 1:
				ghpf::Log::log_DEBUG("test_mpi.cpp","Process with rank 1");

				// register the active object as a receiver for messages
				communicator->registerActiveObject(active_object, active_object->getUUID() );

				//--------------------------- REST SERVER

				// Start looping waiting for messages
				context->run();

				break;
			
		} //switch
		
		delete active_object;
		
	}  //init

private:
}; //class



        
int main(int argc, char* argv[]) 
{
	// log file name
	std::string _LOG_FILE_ = "/var/log/ghpf_test_mpi";

	try {
		
		// Initialize with MPI and REST
		_ghpf::init(argc, argv, _LOG_FILE_);
		
	} catch (ghpf::Exception& e) {
		std::cout << "EXCEPTION thrown by _ghpf::init(argc, argv, _LOG_FILE_..." << e.what() << std::endl;
	}

	// Create the ActiveObjects, should be Specialized in a real application
	// and one or more ActiveObjects created
	TestProcessInitiator process_initiator;

	process_initiator.init();
 	
	// Do some work...
	sleep(3);
	
	_ghpf::exit(true);

}


