/****************************************************************************
 *
 * @file 		test_suite.cpp
 * @brief		Unit tests.
 * @author      Jonas Colmsjö
 *
 * Uses Google C++ Style Guide
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml 
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/


// CppTest framework is used for unit tests
#include "cpptest.h"

// Use the Log function
#include "../src/ghpf.hpp"

// Override log file name
static const std::string _LOG_FILE_ = "/var/log/ghpf_test_suite";

// The tests that are executed
#include "../test_http.hpp"
#include "../test_entity.hpp"

using namespace std;


enum OutputType {
	Compiler,
	Html,
	TextTerse,
	TextVerbose
};

static void usage() {
	cout << "usage: mytest [MODE]\n"
		 << "where MODE may be one of:\n"
		 << "  --compiler\n"
		 << "  --html\n"
		 << "  --text-terse (default)\n"
		 << "  --text-verbose\n";
	exit(0);
}

static auto_ptr<Test::Output> cmdline(int argc, char* argv[]) {
	if (argc > 2)
		usage(); // will not return
	
	Test::Output* output = 0;
	
	if (argc == 1)
		output = new Test::TextOutput(Test::TextOutput::Verbose);
	else
	{
		const char* arg = argv[1];
		if (strcmp(arg, "--compiler") == 0)
			output = new Test::CompilerOutput;
		else if (strcmp(arg, "--html") == 0)
			output =  new Test::HtmlOutput;
		else if (strcmp(arg, "--text-terse") == 0)
			output = new Test::TextOutput(Test::TextOutput::Terse);
		else if (strcmp(arg, "--text-verbose") == 0)
			output = new Test::TextOutput(Test::TextOutput::Verbose);
		else
		{
			cout << "invalid commandline argument: " << arg << endl;
			usage(); // will not return
		}
	}
	
	return auto_ptr<Test::Output>(output);
}


// Main test program
//
int main(int argc, char* argv[]) {

	//_ghpf::init(argc, argv, _LOG_FILE_);
	_ghpf::init(_LOG_FILE_);				// initialize the framework without MPI
	
	try {	

		// Create test suite and add the test classes
		Test::Suite ts;
		
		// Add the test classes
		ts.add(auto_ptr<Test::Suite>(new uuidTestSuite));
		ts.add(auto_ptr<Test::Suite>(new UrlTestSuite));
		ts.add(auto_ptr<Test::Suite>(new RESTClientTestSuite));
		ts.add(auto_ptr<Test::Suite>(new SimpleActiveObjectTestSuite));
		ts.add(auto_ptr<Test::Suite>(new EntityTestSuite));
		// NOT FINISHED, USING test_mpi INSTEAD - ts.add(auto_ptr<Test::Suite>(new RESTTestSuite));

		// Run the tests
		auto_ptr<Test::Output> output(cmdline(argc, argv));

		ghpf::Log::log_DEBUG("test_suite.cpp", "Running unit tests" );
		ts.run(*output, true);
		ghpf::Log::log_DEBUG("test_suite.cpp", "Unit tests completed." );


		Test::HtmlOutput* const html = dynamic_cast<Test::HtmlOutput*>(output.get());
		if (html)
			html->generate(cout, true, "MyTest");
	}
	catch (...) {
		cout << "unexpected exception encountered\n";
		return EXIT_FAILURE;
	}
	
	_ghpf::exit(0);

	
	return EXIT_SUCCESS;
}
