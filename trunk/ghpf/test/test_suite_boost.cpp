/****************************************************************************
 *
 * @file 		test_suite.cpp
 * @brief		Unit tests.
 * @author      Jonas Colmsjö
 *
 * Copyright (c) 2011, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 ****************************************************************************/



#include <boost/test/unit_test.hpp>

// Note that the following header is needed!!!
// Without this, you must compile like:
// g++ -lboost_unit_test_framework foo.cpp
#include <boost/test/included/unit_test_framework.hpp> 


#include "../src/common.hpp"
#include "../src/mpi.hpp"
#include "../src/http-server.hpp"

// Show source file for leaked objects
#ifdef USE_DEBUG_NEW
#include "../../tools/nvwa-0.8.2/nvwa/debug_new.h"
#endif

// Override log file name
static const std::string _LOG_FILE_ = "/var/log/ghpf_test_suite";

	
using namespace std;
using namespace boost::unit_test;


class TestWebServer {
	http_server::WebServer* web_server_;

public:
    void test_init() {
    	web_server_ = new http_server::WebServer();
    	
		web_server_->start(8080);
		
		ghpf::Log::log_DEBUG("test_suite.cpp","Web server started on port 8080");
		ghpf::Log::log_DEBUG("test_suite.cpp","Will run for 20 seconds…");
		sleep(20);
		
		ghpf::Log::log_DEBUG("test_suite.cpp","Stopping web server…");
		web_server_->stop();
		ghpf::Log::log_DEBUG("test_suite.cpp","Deleting web server…");
		delete web_server_;
    }

};
	

class TestRESTServer {
	http_server::RESTServer* rest_server_;
	//http_server::WebServer* rest_server_;

public:
    void test_init() {
    	rest_server_ = new http_server::RESTServer();
    	//rest_server_ = new http_server::WebServer();
    	
		rest_server_->start(8080);
		
		ghpf::Log::log_DEBUG("test_suite.cpp","REST server started on port 8080");
		ghpf::Log::log_DEBUG("test_suite.cpp","Will run for 20 seconds…");
		
		sleep(20);
		
		ghpf::Log::log_DEBUG("test_suite.cpp","Stopping REST server…");
		rest_server_->stop();	
		ghpf::Log::log_DEBUG("test_suite.cpp","Deleting REST server…");
		delete rest_server_;
		ghpf::Log::log_DEBUG("test_suite.cpp","REST server deleted…");
    }

};

class TestSuite1_GHPF: public test_suite {
public:
	
    TestSuite1_GHPF() : test_suite("test_suite1_ghpf") {
    	init1 = init2 = NULL;
		web_server_  = NULL;	
		rest_server_ = NULL;	
    	
    	// Add WebServer tests 
    	// web_Server_ = new TestWebServer();
		//boost::shared_ptr<TestWebServer> instance1(web_server_);	
        //init1 = BOOST_CLASS_TEST_CASE(&TestWebServer::test_init, instance1);
        //add(init1);

    	// Add RESTServer tests 
    	
		rest_server_ = new TestRESTServer();
		
		boost::shared_ptr<TestRESTServer> instance2(rest_server_);	
        init2 = BOOST_CLASS_TEST_CASE(&TestRESTServer::test_init, instance2);
        add(init2);
    }
	
	~TestSuite1_GHPF() {
		//ghpf::Log::log_DEBUG("test_suite.cpp","About to delete test for web_server_");
		//if(web_server_ != NULL) delete web_server_;	
		
		//ghpf::Log::log_DEBUG("test_suite.cpp","About to delete test for rest_server_");
		//if(rest_server_ != NULL) delete rest_server_;	

		//ghpf::Log::log_DEBUG("test_suite.cpp","About to delete init1");
		//if(init1 != NULL) delete init1;	

		//ghpf::Log::log_DEBUG("test_suite.cpp","About to delete init2");
		//if(init2 != NULL) delete init2;	
	}
	
private:
	TestWebServer  *web_server_;
	TestRESTServer *rest_server_;
	
	test_case *init1, *init2;

};

test_suite* init_unit_test_suite(int argc, char** argv) {

	ghpf::Log::init(_LOG_FILE_);
	
	ghpf::Log::log_DEBUG("test_suite.cpp","Create test_suite");
	test_suite* suite(BOOST_TEST_SUITE("GHPF Master Suite"));
	
	ghpf::Log::log_DEBUG("test_suite.cpp","Create TestSuite1_GHPF");
	TestSuite1_GHPF *ts = new TestSuite1_GHPF();
    suite->add(ts);
    
    
	ghpf::Log::log_DEBUG("test_suite.cpp","Tear down test suite");
    // Not sure if this is appropriate... delete ts;
	ghpf::Log::destroy();

	return suite;
}
