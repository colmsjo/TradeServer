#Fig 2.11
#Comparison on normal and heavy-tailed distributions.

N<-101
p<-0.5          # mix prob

pdf("D:\\S689RC\\Fig2_11.pdf")   
par(mfrow=c(2,2))

t<-seq(-5,10,length.out=N)
npdf<-dnorm(t,0,2)
mpdf<-p*npdf + (1-p)*dt(t,1)         # mix with normal (0,2) with cauchy(0)
ymax<-max(mpdf)

plot(t,npdf,type = 'l',xlab='x',ylab="PDF",ylim=c(0,ymax),main="densities")
lines(t,mpdf,lty=2)
legend(3, ymax,c("N(0,1)", "Mix"), col = c(3,6), lty = c(1, 2), merge = TRUE, bg='gray90')

t<-seq(4.0,12,length.out=N)
npdf<-dnorm(t,0,2)
mpdf<-p*npdf + (1-p)*dt(t,1)         # mix with normal (0,2) with cauchy(0)
ymax<-max(c(mpdf,npdf))

plot(t,npdf,type = 'l',xlab='x',ylab="PDF",ylim=c(0,ymax),main="densities-detail")
lines(t,mpdf,lty=2)
legend(7, ymax,c("N(0,1)", "Mix"), col = c(3,6), lty = c(1, 2), merge = TRUE, bg='gray90')

x<-rnorm(N,0,1)
y<-c(rnorm(N*3/4,0,1),rt((N/4)+1,1))             #3/4 Normal + 1/4 Cauchy

qqnorm(x)
qqline(x)
qqnorm(y)
qqline(y)

dev.off()
