#Fig 2.11
#Comparison on normal and heavy-tailed distributions


#Pareto Parameters
c<-0.25
a<-1.1

sigma<-0.3113
normp<-1/(1-pnorm(c,0,sigma))
lamda<-c/a
expp<-1/exp(-c/lamda)

#require Lib fExtremes
#library(fExtremes)

N<-100
t<-seq(c,4,length.out=N)
pCDF<-(c/t)^a
nCDF<-(1-pnorm(t,0,sigma))*normp
eCDF<-exp(-t/lamda)*expp

#pdf("Fig2_12.pdf")
plot(t,pCDF,type='l',main="Survival Functions",xlab="x",ylab="1-F(x)")
lines(t,nCDF,lty=2)
lines(t,eCDF,lty=3)
legend(3, 0.93,c("Pareto", "Normal","Exp"), col = c(3,4,6), text.col= "green4",lty = c(1, 2, 3), merge = TRUE, bg='gray90')
#dev.off()
