#Fig 2.5 
#CDF

N<-100

t<-seq(-2.5,2.5,length.out=100)

p1<-pnorm(t,0,1)

rx<-sort(rnorm(N,0,1))
max_x<-max(t)
min_x<-min(t)
dist_x<-max_x-min_x
max_y<-max(c(p1,))
stepy<-0:(N-1)
stepy<-(stepy+0.5)/N

pdf("D:\\S689RC\\R\\Fig2_5.pdf")

plot(rx,stepy,type="s",xlab="x",ylab="F(x)",main="Empirical CDF")
lines(t,p1,lty=2)

legend(min_x+0.29,max_y-0.005,c("F(x)","Fn(x)"),lty = c(2,1),merge = TRUE, bg='gray90')
dev.off()