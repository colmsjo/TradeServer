#Binomial Prob distibution with n=10 and p=0.6
#fig2_2

N<-10
i<-0:10
p<-0.6
#ycdf<-pbinom(i,N,p)
#barplot(ycdf)
pdf("D:\\S689RC\\R\\Fig2_2.pdf")
ypdf<-dbinom(i,N,p)
Title<-"Binomial Probability distibution with n=10 and p=0.6"
barplot(ypdf, xlab = "x", ylab = "P(X=x)",main=Title)
dev.off()

