#Example for skewness and kurtosis 
Price<-scan("c:\\S689RC\\dat\\ge.dat", skip = 2)
N<-length(Price)  
Return<-Price[2:N]/Price[1:(N-1)]-1
LogReturn<-log(1+Return)
LogPrice<-log(Price)

library(fBasics)
skw<-skewness(LogReturn)
kurt<-kurtosis (LogReturn)
stat<-basicStats (LogReturn)

qqnorm(LogReturn)
qqline(LogReturn)


