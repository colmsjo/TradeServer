#Fig 4.6
#AR(1) Process with mu=0.4 and a=0.4, the intergral of this AR(1) and second integral of this AR(1). 
# Yt = a * Y(t-1) + et

N<-400
a<-0.4
e<-rnorm((N+1),0,1)
u<-0
ar<-rep(u,(N+1))
arima1<-ar
arima2<-ar

for(i in 2:(N+1))
{
ar[i]<-ar[i-1]*a[1] + e[i]
arima1[i]<-arima1[i-1]+ar[i]
arima2[i]<-arima2[i-1]+arima1[i]
}

pdf("D:\\S689RC\\Fig4_6.pdf")
par(mfrow=c(3,1))
t<-0:N
t1<-paste("ARIMA(1,0,0) with u=0 and a = ",a)

plot(t,ar,type='l',xlab="t",ylab="y",main=t1)
plot(t,arima1,type='l',xlab="t",ylab="y",main="ARIMA(1,1,0)")
plot(t,arima2,type='l',xlab="t",ylab="y",main="ARIMA(1,2,0)")
dev.off()
