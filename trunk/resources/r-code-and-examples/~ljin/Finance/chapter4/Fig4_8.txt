#Fig 4.8
#Tiem series plot of the daily GE log returns with forecasts from an AR(1). 

Price<-scan("D:\\S689RC\\dat\\ge.dat", skip = 2)
N<-length(Price)  
Return<-Price[2:N]/Price[1:(N-1)]-1
LogReturn<-log(1+Return)
LogPrice<-log(Price)

library("fSeries")

#Fit GE Daily log return
fit_ar<-armaFit(formula = LogReturn ~ arima(1,0,0))     #AR (1)
pdf("D:\\S689RC\\Fig4_8.pdf")
sink("D:\\S689RC\\out4_8.txt")
predict(fit_ar, n.ahead = 30,n.back = 120, conf = c(95, 95))
sink()
dev.off()
