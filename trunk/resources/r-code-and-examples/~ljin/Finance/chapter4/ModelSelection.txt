Price<-scan("C:\\S689RC\\chapter4\\ge.txt", skip = 2)
N<-length(Price)  
Return<-Price[2:N]/Price[1:(N-1)]-1
LogReturn<-log(1+Return)

library(tseries)
fit<-ar(LogReturn)
aic<-fit$aic
aic<-aic[2:9]
bic<-rep(0,8)
for(i in 1:8)
{
bic[i]<-aic[i]-2*i+i*log(N-1)
}
difaic<-aic-aic[1]
difbic<-bic-bic[1]
diff<-cbind(difaic,difbic)
diff