
Title:
 ARIMA Modelling 

Call:
 armaFit(formula = LogReturn ~ arima(2, 0, 1))

Model:
 ARIMA(2,0,1) with method: CSS-ML

Coefficient(s):
       ar1         ar2         ma1   intercept  
-5.328e-01   7.855e-02   8.044e-01  -2.466e-05  

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0425790 -0.0109012  0.0003172  0.0109946  0.0444282 

Moments: 
Skewness Kurtosis 
-0.05071 -0.07873 

Coefficient(s):
            Estimate  Std. Error  t value Pr(>|t|)    
ar1       -5.328e-01   1.140e-01   -4.673 2.96e-06 ***
ar2        7.855e-02   7.473e-02    1.051    0.293    
ma1        8.044e-01   9.266e-02    8.681  < 2e-16 ***
intercept -2.466e-05   1.318e-03   -0.019    0.985    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

sigma^2 estimated as: 0.0002842
log likelihood:       671.27
AIC Criterion:        -1332.53

Description:
 Thu Dec 22 20:58:04 2005 

null device 
          1 
