
Title:
 ARIMA Modelling 

Call:
 armaFit(formula = LogReturn ~ arima(0, 0, 2))

Model:
 ARIMA(0,0,2) with method: CSS-ML

Coefficient(s):
       ma1         ma2   intercept  
 2.638e-01  -7.483e-02  -2.530e-05  

Residuals:
       Min         1Q     Median         3Q        Max 
-4.361e-02 -1.160e-02  6.732e-05  1.136e-02  4.483e-02 

Moments: 
Skewness Kurtosis 
-0.07819 -0.09639 

Coefficient(s):
            Estimate  Std. Error  t value Pr(>|t|)    
ma1        0.2638020   0.0631600    4.177 2.96e-05 ***
ma2       -0.0748260   0.0719351   -1.040    0.298    
intercept -0.0000253   0.0012719   -0.020    0.984    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

sigma^2 estimated as: 0.0002879
log likelihood:       669.66
AIC Criterion:        -1331.32

Description:
 Thu Dec 22 20:58:04 2005 

null device 
          1 
