
#Example 7.3 Page 239

CMAT<-scan("D:\\S689RC\\dat\\exp7_3.csv", skip = 1,what = list(Close_tbill="",Close_msft="",Close_sp500="",Close_ge="",Close_ford="",Date=""),sep=",")
Close_tbill<-as.numeric(CMAT$Close_tbill)
Close_msft<-as.numeric(CMAT$Close_msft)
Close_sp500<-as.numeric(CMAT$Close_sp500)

Ep_sp500<-diff(log(Close_sp500))-Close_tbill/(100*253)
Ep_msft<-diff(log(Close_msft))-Close_tbill/(100*253)

sink("D:\\S689RC\\outexp7_3.txt")
fit<-lm(Ep_msft~Ep_sp500)               #example 7_3 with intercept
anova(fit)
summary(fit)


print(" ************************************* ")

fit1<-lm(Ep_msft~0+Ep_sp500)          #w/o intercept
anova(fit1)
summary(fit1)
sink()