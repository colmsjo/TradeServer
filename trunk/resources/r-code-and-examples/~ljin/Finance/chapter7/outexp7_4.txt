Analysis of Variance Table

Response: Ep_msft
          Df  Sum Sq Mean Sq F value    Pr(>F)    
Ep_sp500   1 0.41818 0.41818  32.631 4.234e-07 ***
sptime     1 0.00969 0.00969   0.756    0.3882    
Residuals 57 0.73047 0.01282                      
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

Call:
lm(formula = Ep_msft ~ Ep_sp500 + sptime)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.368680 -0.061856  0.003310  0.058558  0.259079 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  0.01272    0.01532   0.830    0.410    
Ep_sp500     1.68306    0.30648   5.492 9.62e-07 ***
sptime       0.49408    0.56827   0.869    0.388    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

Residual standard error: 0.1132 on 57 degrees of freedom
Multiple R-Squared: 0.3694,	Adjusted R-squared: 0.3473 
F-statistic: 16.69 on 2 and 57 DF,  p-value: 1.965e-06 

