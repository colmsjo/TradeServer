#Fig 8.9
#GE call options 

Vol<-scan("D:\\S689RC\\dat\\ImpVolRegData.txt", what=list(K="",T="",ImpVol=""), skip = 5)
RK<-as.numeric(Vol$K)
K<-RK-mean(RK)
RT<-as.numeric(Vol$T)
T<-RT-mean(RT)
ImpVol<-as.numeric(Vol$ImpVol)

T2<-T*T
K2<-K*K
KT<-K*T
K3<-K*K*K
T3<-T*T*T

N<-30

k = seq(min(K), max(K), length.out=N)
new_K <- data.frame(K=k,K2=k*k,K3=k*k*k,T=0,T2=0,T3=0)
prefit_K<-predict(lm(ImpVol~K+K2+K3+T+T2+T3), new_K, se.fit = TRUE)

t<-seq(min(T),max(T),length.out=N)
new_T <- data.frame(T=t,T2=t*t,T3=t*t*t,K=0,K2=0,K3=0)
prefit_T<-predict(lm(ImpVol~T+T2+T3+K+K2+K3), new_T, se.fit = TRUE)

pdf("D:\\S689RC\\Fig8_9.pdf")   
plot(k+mean(RK),prefit_K$fit,type='l',xlab="Exercise Price",ylab='implied volatility')
dev.off()

pdf("D:\\S689RC\\Fig8_10.pdf")   
plot(t+mean(RT),prefit_T$fit,type='l',xlab="Maturity",ylab='implied volatility')
dev.off()


#Fig 8.11

