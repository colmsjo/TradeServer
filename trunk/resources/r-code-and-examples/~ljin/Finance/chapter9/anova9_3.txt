
Formula: price ~ 100 * exp(-beta0 * time2mat - (beta1 * time2mat^2)/2)

Parameters:
       Estimate Std. Error t value Pr(>|t|)    
beta0 5.319e-02  3.109e-04  171.08   <2e-16 ***
beta1 7.458e-04  3.477e-05   21.45   <2e-16 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

Residual standard error: 0.6482 on 115 degrees of freedom

Correlation of Parameter Estimates:
        beta0
beta1 -0.9273


Formula: price ~ 100 * exp(-beta0 * time2mat - (beta1 * time2mat^2)/2 - 
    (beta2 * time2mat^3)/3)

Parameters:
        Estimate Std. Error t value Pr(>|t|)    
beta0  4.749e-02  1.987e-04  239.02   <2e-16 ***
beta1  2.403e-03  5.163e-05   46.55   <2e-16 ***
beta2 -7.542e-05  2.285e-06  -33.00   <2e-16 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1 

Residual standard error: 0.2024 on 114 degrees of freedom

Correlation of Parameter Estimates:
        beta0   beta1
beta1 -0.9489        
beta2  0.8754 -0.9785

