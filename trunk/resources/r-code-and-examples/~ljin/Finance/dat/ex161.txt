  Date    FYGN3
  Jan-50    1.09
  Feb-50   1.125
  Mar-50   1.138
  Apr-50   1.159
  May-50   1.166
  Jun-50   1.174
  Jul-50   1.172
  Aug-50   1.211
  Sep-50   1.315
  Oct-50   1.329
  Nov-50   1.364
  Dec-50   1.367
  Jan-51   1.387
  Feb-51   1.391
  Mar-51   1.422
  Apr-51    1.52
  May-51   1.578
  Jun-51   1.499
  Jul-51   1.593
  Aug-51   1.644
  Sep-51   1.646
  Oct-51   1.608
  Nov-51   1.608
  Dec-51   1.731
  Jan-52   1.688
  Feb-52   1.574
  Mar-52   1.658
  Apr-52   1.623
  May-52    1.71
  Jun-52     1.7
  Jul-52   1.824
  Aug-52   1.876
  Sep-52   1.786
  Oct-52   1.783
  Nov-52   1.862
  Dec-52   2.126
  Jan-53   2.042
  Feb-53   2.018
  Mar-53   2.082
  Apr-53   2.177
  May-53     2.2
  Jun-53   2.231
  Jul-53   2.101
  Aug-53   2.088
  Sep-53   1.876
  Oct-53   1.402
  Nov-53   1.427
  Dec-53    1.63
  Jan-54   1.214
  Feb-54   0.984
  Mar-54   1.053
  Apr-54   1.011
  May-54   0.782
  Jun-54    0.65
  Jul-54    0.71
  Aug-54   0.892
  Sep-54   1.007
  Oct-54   0.987
  Nov-54   0.948
  Dec-54   1.174
  Jan-55   1.257
  Feb-55   1.177
  Mar-55   1.335
  Apr-55    1.62
  May-55   1.491
  Jun-55   1.432
  Jul-55   1.622
  Aug-55   1.876
  Sep-55   2.086
  Oct-55   2.259
  Nov-55   2.225
  Dec-55   2.564
  Jan-56   2.456
  Feb-56   2.372
  Mar-56    2.31
  Apr-56   2.613
  May-56    2.65
  Jun-56   2.527
  Jul-56   2.334
  Aug-56   2.606
  Sep-56    2.85
  Oct-56   2.961
  Nov-56       3
  Dec-56    3.23
  Jan-57    3.21
  Feb-57   3.165
  Mar-57    3.14
  Apr-57   3.113
  May-57   3.042
  Jun-57   3.316
  Jul-57   3.165
  Aug-57   3.404
  Sep-57   3.578
  Oct-57   3.591
  Nov-57   3.337
  Dec-57   3.102
  Jan-58   2.598
  Feb-58   1.562
  Mar-58   1.354
  Apr-58   1.126
  May-58   1.046
  Jun-58   0.881
  Jul-58   0.962
  Aug-58   1.686
  Sep-58   2.484
  Oct-58   2.793
  Nov-58   2.756
  Dec-58   2.814
  Jan-59   2.837
  Feb-59   2.712
  Mar-59   2.852
  Apr-59    2.96
  May-59   2.851
  Jun-59   3.247
  Jul-59   3.243
  Aug-59   3.358
  Sep-59   3.998
  Oct-59   4.117
  Nov-59   4.209
  Dec-59   4.572
  Jan-60   4.436
  Feb-60   3.954
  Mar-60   3.439
  Apr-60   3.244
  May-60   3.392
  Jun-60   2.641
  Jul-60   2.396
  Aug-60   2.286
  Sep-60   2.489
  Oct-60   2.426
  Nov-60   2.384
  Dec-60   2.272
  Jan-61   2.302
  Feb-61   2.408
  Mar-61    2.42
  Apr-61   2.327
  May-61   2.288
  Jun-61   2.359
  Jul-61   2.268
  Aug-61   2.402
  Sep-61   2.304
  Oct-61    2.35
  Nov-61   2.458
  Dec-61   2.617
  Jan-62   2.746
  Feb-62   2.752
  Mar-62   2.719
  Apr-62   2.735
  May-62   2.694
  Jun-62   2.719
  Jul-62   2.945
  Aug-62   2.837
  Sep-62   2.792
  Oct-62   2.751
  Nov-62   2.803
  Dec-62   2.856
  Jan-63   2.914
  Feb-63   2.916
  Mar-63   2.897
  Apr-63   2.909
  May-63    2.92
  Jun-63   2.995
  Jul-63   3.143
  Aug-63    3.32
  Sep-63   3.379
  Oct-63   3.453
  Nov-63   3.522
  Dec-63   3.523
  Jan-64   3.529
  Feb-64   3.532
  Mar-64   3.553
  Apr-64   3.484
  May-64   3.482
  Jun-64   3.478
  Jul-64   3.479
  Aug-64   3.506
  Sep-64   3.527
  Oct-64   3.575
  Nov-64   3.624
  Dec-64   3.856
  Jan-65   3.828
  Feb-65   3.929
  Mar-65   3.942
  Apr-65   3.932
  May-65   3.895
  Jun-65    3.81
  Jul-65   3.831
  Aug-65   3.836
  Sep-65   3.912
  Oct-65   4.032
  Nov-65   4.082
  Dec-65   4.362
  Jan-66   4.596
  Feb-66    4.67
  Mar-66   4.626
  Apr-66   4.611
  May-66   4.642
  Jun-66   4.539
  Jul-66   4.855
  Aug-66   4.932
  Sep-66   5.356
  Oct-66   5.387
  Nov-66   5.344
  Dec-66   5.007
  Jan-67   4.759
  Feb-67   4.554
  Mar-67   4.288
  Apr-67   3.852
  May-67    3.64
  Jun-67    3.48
  Jul-67   4.308
  Aug-67   4.275
  Sep-67   4.451
  Oct-67   4.588
  Nov-67   4.762
  Dec-67   5.012
  Jan-68   5.081
  Feb-68   4.969
  Mar-68   5.144
  Apr-68   5.365
  May-68   5.621
  Jun-68   5.544
  Jul-68   5.382
  Aug-68   5.095
  Sep-68   5.202
  Oct-68   5.334
  Nov-68   5.492
  Dec-68   5.916
  Jan-69   6.177
  Feb-69   6.156
  Mar-69    6.08
  Apr-69    6.15
  May-69   6.077
  Jun-69   6.493
  Jul-69   7.004
  Aug-69   7.007
  Sep-69   7.129
  Oct-69    7.04
  Nov-69   7.193
  Dec-69    7.72
  Jan-70   7.914
  Feb-70   7.164
  Mar-70    6.71
  Apr-70    6.48
  May-70   7.035
  Jun-70   6.742
  Jul-70   6.468
  Aug-70   6.412
  Sep-70   6.244
  Oct-70   5.927
  Nov-70   5.288
  Dec-70    4.86
  Jan-71   4.494
  Feb-71   3.773
  Mar-71   3.323
  Apr-71    3.78
  May-71   4.139
  Jun-71   4.699
  Jul-71   5.405
  Aug-71   5.078
  Sep-71   4.668
  Oct-71   4.489
  Nov-71   4.191
  Dec-71   4.023
  Jan-72   3.403
  Feb-72    3.18
  Mar-72   3.723
  Apr-72   3.723
  May-72   3.648
  Jun-72   3.874
  Jul-72   4.059
  Aug-72   4.014
  Sep-72   4.651
  Oct-72   4.719
  Nov-72   4.774
  Dec-72   5.061
  Jan-73   5.307
  Feb-73   5.558
  Mar-73   6.054
  Apr-73   6.289
  May-73   6.348
  Jun-73   7.188
  Jul-73   8.015
  Aug-73   8.672
  Sep-73   8.478
  Oct-73   7.155
  Nov-73   7.866
  Dec-73   7.364
  Jan-74   7.755
  Feb-74    7.06
  Mar-74   7.986
  Apr-74   8.229
  May-74    8.43
  Jun-74   8.145
  Jul-74   7.752
  Aug-74   8.744
  Sep-74   8.363
  Oct-74   7.244
  Nov-74   7.585
  Dec-74   7.179
  Jan-75   6.493
  Feb-75   5.583
  Mar-75   5.544
  Apr-75   5.694
  May-75   5.315
  Jun-75   5.193
  Jul-75   6.164
  Aug-75   6.463
  Sep-75   6.383
  Oct-75   6.081
  Nov-75   5.468
  Dec-75   5.504
  Jan-76   4.961
  Feb-76   4.852
  Mar-76   5.047
  Apr-76   4.878
  May-76   5.185
  Jun-76   5.443
  Jul-76   5.278
  Aug-76   5.153
  Sep-76   5.075
  Oct-76    4.93
  Nov-76    4.81
  Dec-76   4.354
  Jan-77   4.597
  Feb-77   4.662
  Mar-77   4.613
  Apr-77    4.54
  May-77   4.942
  Jun-77   5.004
  Jul-77   5.146
  Aug-77     5.5
  Sep-77    5.77
  Oct-77   6.188
  Nov-77    6.16
  Dec-77   6.063
  Jan-78   6.448
  Feb-78   6.457
  Mar-78   6.319
  Apr-78   6.306
  May-78    6.43
  Jun-78   6.707
  Jul-78   7.074
  Aug-78   7.036
  Sep-78   7.836
  Oct-78   8.132
  Nov-78   8.787
  Dec-78   9.122
  Jan-79   9.351
  Feb-79   9.265
  Mar-79   9.457
  Apr-79   9.493
  May-79   9.579
  Jun-79   9.045
  Jul-79   9.262
  Aug-79    9.45
  Sep-79  10.182
  Oct-79  11.472
  Nov-79  11.868
  Dec-79  12.071
  Jan-80  12.036
  Feb-80  12.814
  Mar-80  15.526
  Apr-80  14.003
  May-80    9.15
  Jun-80   6.995
  Jul-80   8.126
  Aug-80   9.259
  Sep-80  10.321
  Oct-80   11.58
  Nov-80  13.888
  Dec-80  15.661
  Jan-81  14.724
  Feb-81  14.905
  Mar-81  13.478
  Apr-81  13.635
  May-81  16.295
  Jun-81  14.557
  Jul-81  14.699
  Aug-81  15.612
  Sep-81  14.951
  Oct-81  13.873
  Nov-81  11.269
  Dec-81  10.926
  Jan-82  12.412
  Feb-82   13.78
  Mar-82  12.493
  Apr-82  12.821
  May-82  12.148
  Jun-82  12.108
  Jul-82  11.914
  Aug-82   9.006
  Sep-82   8.196
  Oct-82    7.75
  Nov-82   8.042
  Dec-82   8.013
  Jan-83    7.81
  Feb-83    8.13
  Mar-83   8.304
  Apr-83   8.252
  May-83   8.185
  Jun-83    8.82
  Jul-83    9.12
  Aug-83    9.39
  Sep-83    9.05
  Oct-83    8.71
  Nov-83    8.71
  Dec-83    8.96
  Jan-84    8.93
  Feb-84    9.03
  Mar-84    9.44
  Apr-84    9.69
  May-84     9.9
  Jun-84    9.94
  Jul-84   10.13
  Aug-84   10.49
  Sep-84   10.41
  Oct-84    9.97
  Nov-84    8.79
  Dec-84    8.16
  Jan-85    7.76
  Feb-85    8.22
  Mar-85    8.57
  Apr-85       8
  May-85    7.56
  Jun-85    7.01
  Jul-85    7.05
  Aug-85    7.18
  Sep-85    7.08
  Oct-85    7.17
  Nov-85     7.2
  Dec-85    7.07
  Jan-86    7.04
  Feb-86    7.03
  Mar-86    6.59
  Apr-86    6.06
  May-86    6.12
  Jun-86    6.21
  Jul-86    5.84
  Aug-86    5.57
  Sep-86    5.19
  Oct-86    5.18
  Nov-86    5.35
  Dec-86    5.49
  Jan-87    5.45
  Feb-87    5.59
  Mar-87    5.56
  Apr-87    5.76
  May-87    5.75
  Jun-87    5.69
  Jul-87    5.78
  Aug-87       6
  Sep-87    6.32
  Oct-87     6.4
  Nov-87    5.81
  Dec-87     5.8
  Jan-88     5.9
  Feb-88    5.69
  Mar-88    5.69
  Apr-88    5.92
  May-88    6.27
  Jun-88     6.5
  Jul-88    6.73
  Aug-88    7.02
  Sep-88    7.23
  Oct-88    7.34
  Nov-88    7.68
  Dec-88    8.09
  Jan-89    8.29
  Feb-89    8.48
  Mar-89    8.83
  Apr-89     8.7
  May-89     8.4
  Jun-89    8.22
  Jul-89    7.92
  Aug-89    7.91
  Sep-89    7.72
  Oct-89    7.59
  Nov-89    7.65
  Dec-89    7.64
  Jan-90    7.64
  Feb-90    7.76
  Mar-90    7.87
  Apr-90    7.78
  May-90    7.78
  Jun-90    7.74
  Jul-90    7.66
  Aug-90    7.44
  Sep-90    7.38
  Oct-90    7.19
  Nov-90    7.07
  Dec-90    6.81
  Jan-91     6.3
  Feb-91    5.95
  Mar-91    5.91
  Apr-91    5.67
  May-91    5.51
  Jun-91     5.6
  Jul-91    5.58
  Aug-91    5.39
  Sep-91    5.25
  Oct-91    5.03
  Nov-91     4.6
  Dec-91    4.12
  Jan-92    3.84
  Feb-92    3.84
  Mar-92    4.05
  Apr-92    3.81
  May-92    3.66
  Jun-92     3.7
  Jul-92    3.28
  Aug-92    3.14
  Sep-92    2.97
  Oct-92    2.84
  Nov-92    3.14
  Dec-92    3.25
  Jan-93    3.06
  Feb-93    2.95
  Mar-93    2.97
  Apr-93    2.89
  May-93    2.96
  Jun-93     3.1
  Jul-93    3.05
  Aug-93    3.05
  Sep-93    2.96
  Oct-93    3.04
  Nov-93    3.12
  Dec-93    3.08
  Jan-94    3.02
  Feb-94    3.21
  Mar-94    3.52
  Apr-94    3.74
  May-94    4.19
  Jun-94    4.18
  Jul-94    4.39
  Aug-94     4.5
  Sep-94    4.64
  Oct-94    4.96
  Nov-94    5.25
  Dec-94    5.64
  Jan-95    5.81
  Feb-95     5.8
  Mar-95    5.73
  Apr-95    5.67
  May-95     5.7
  Jun-95     5.5
  Jul-95    5.47
  Aug-95    5.41
  Sep-95    5.26
  Oct-95     5.3
  Nov-95    5.35
  Dec-95    5.16
  Jan-96    5.02
  Feb-96    4.87
  Mar-96    4.96
