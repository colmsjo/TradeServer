/*--------------------------------------------------
 * NA_PWC cpp 
 *
 * Neural Array Pinto/Wilson/Cowan C++
 *   for inclusion in python
 *
 * subclassed from NA (Neural Array)
 * 3.2.2: Readfromfile
 * 3.2.3: LockNA
 * 3.2.4: Save/Load
 * 3.2.5: UnLock
 * 3.2.6: Copy
 * 3.2.7: Cleaned up as Py/C++ sample
--------------------------------------------------*/

#include 
#include 
#include 
#include 

#include "PyObjectPlus.h"
#include "NA_PWC.h"

version NA_PWC_version = "3.2.7";

/*------------------------------
 * NA_PWC Type							// TYPE structure
------------------------------*/
			
PyTypeObject NA_PWC::Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,				/*ob_size*/
	"NA_PWC",			/*tp_name*/
	sizeof(NA_PWC),			/*tp_basicsize*/
	0,				/*tp_itemsize*/
	/* methods */
	PyDestructor,	  		/*tp_dealloc*/
	0,			 	/*tp_print*/
	__getattr, 			/*tp_getattr*/
	__setattr, 			/*tp_setattr*/
	0,			        /*tp_compare*/
	__repr,		        	/*tp_repr*/
	0,			        /*tp_as_number*/
	0,		 	        /*tp_as_sequence*/
	0,			        /*tp_as_mapping*/
	0,			        /*tp_hash*/
	sPyCycle,			/*tp_call */
};

/*------------------------------
 * NA_PWC Methods						// Methods structure
------------------------------*/
PyMethodDef NA_PWC::Methods[] = {
  {"Lock",         (PyCFunction) sPyLock,         Py_NEWARGS},
  {"UnLock",       (PyCFunction) sPyUnLock,       Py_NEWARGS},
  {"PrintVoltage", (PyCFunction) sPyPrintVoltage, Py_NEWARGS},

  {NULL, NULL}		/* Sentinel */
};

/*------------------------------
 * NA_PWC Parents						// Parents structure
------------------------------*/
PyParentObject NA_PWC::Parents[] = {&NA_PWC::Type, 
				    &NA::Type, 
				    &NS::Type, 
				    NULL};			// Sentinel          

/*------------------------------
 * NA_PWC constructor
------------------------------*/
NA_PWC::NA_PWC(char *name, int n0, 				// C++ constructor
	       float tau0, float gamma0, 
	       PyTypeObject *T) 
 : V(n0), NA(name, n0, tau0, T)
{
  gamma = gamma0;
  locked = false;
}

PyObject *NA_PWC::PyMake(PyObject *ignored, PyObject *args)	// Python wrapper
{
  // expects (name) (n) (tau=0) (gamma=0) 
  char *name;
  int n;
  float tau = 0;
  float gamma = 0;

  Py_Try(PyArg_ParseTuple(args, "si|ff", 
			  &name, &n, &tau, &gamma));		// Read arguments
  Py_Assert(n > 0, PyExc_ValueError, "n <= 0");			// Check values ok
  Py_Assert(tau >= 0, PyExc_ValueError, "tau << 0");		// Check values ok

  return new NA_PWC(name, n, tau, gamma);			// Make new Python-able object
}

/*------------------------------ 
 * NA_PWC destructor 
------------------------------*/ 
NA_PWC::~NA_PWC()						// Everything handled in parent
{} 

/*------------------------------ 
 * NA_PWC Attributes
------------------------------*/ 
PyObject *NA_PWC::_getattr(char *attr)				// __getattr__ function: note only need to handle new state
{ 
  if (streq(attr, "gamma"))					// accessable new state
    return Py_BuildValue("f", gamma); 

  if (streq(attr, "locked")) 					// accessable new state
    return Py_BuildValue("i", int(locked)); 

  _getattr_up(NA); 						// send to parent
} 

int NA_PWC::_setattr(char *attr, PyObject *value) 		// __setattr__ function: note only need to handle new state
{ 
  if (streq(attr, "gamma")) 					// settable new state
    gamma = PyFloat_AsDouble(value); 

  else if (streq(attr, "locked")) 				// settable new state
    { 
      if (PyObject_IsTrue(value)) 
	locked = true;  
      else  
	locked = false; 
    } 
  else  
    return NA::_setattr(attr, value); 				// send up to parent

  return 0;							// never reaches here -- keeps compiler from complaining
} 

/*------------------------------
 * NA_PWC print voltage						// A typical new method
------------------------------*/
void NA_PWC::PrintVoltage(void)					// C++ method
{
  printf("%s(V): ", name);  
  for (int i=0; i < n; i++) 
    printf("%5.2f ", V[i]); 
  printf("\n"); 
}

PyObject *NA_PWC::PyPrintVoltage(PyObject *args) 		// Python wrapper
{ PrintVoltage(); Py_Return; } 


/*------------------------------
 * NA_PWC invert						// C++ method only, does not require python wrapper
------------------------------*/
void NA_PWC::Invert(void)
{
  for (int i=0; i<n; i++)
    V[i] = atanh(2 * F[i] - 1);
}

								// These methods are modifications of methods already
								// available in the parent.  This means that they
								// only need new C++ versions.  The python wrappers
								// do not change.  (Just make sure the C++ methods in
								// the parent are declared virtual.)
/*------------------------------
 * NA_PWC cycle
------------------------------*/
void NA_PWC::Cycle(AgentGlobalState &AGS)
{
  if (!locked)
    for (int i=0; i<n; i++)
      {
	// Firing rate is sigmoidal function of voltage
	F[i] = 0.5 + 0.5 * tanh(V[i] + gamma);
	
	if (!(F[i] >= 0.0 && F[i] <= 1.0))
	  fprintf(stderr, "%s: F[%d]=0.5 + 0.5 * tanh(%f + %f out of range\n",
		  name, i, F[i], V[i], gamma); 
	
	Assert(F[i] >= 0.0 && F[i] <= 1.0, "F[i] out of range"); 
	
	// reset voltage
	V[i] = 0;
      }
  
  NA::Cycle(AGS); 
}

/*------------------------------
 * NA_PWC fill
------------------------------*/
void NA_PWC::FillWithRandom(float min, float max)
{
  if (!locked)
    {
      NA::FillWithRandom(min,max);
      Invert();
    }
}

/*------------------------------
 * NA_PWC fill linearly
------------------------------*/
void NA_PWC::FillLinearly(void)
{
  if (!locked)
    {
      NA::FillLinearly();
      Invert();
    }
}

/*------------------------------
 * NA_PWC FillWithZero
------------------------------*/
void NA_PWC::FillWithZero(void)
{
  if (!locked)
    {
      NA::FillWithZero();
      Invert();
    }
}

/*------------------------------
 * NA_PWC FillWithConst
------------------------------*/
void NA_PWC::FillWithConst(float c)
{
  if (!locked)
    {
      NA::FillWithConst(c);
      Invert();
    }
}

/*------------------------------
 * NA_PWC Fire One Neuron
------------------------------*/
void NA_PWC::FireOneNeuron(int i)
{ 
  if (!locked)
    {
      NA::FireOneNeuron(i);
      Invert();
    }
}

/*------------------------------
 * NA_PWC ReadFromFile
------------------------------*/
void NA_PWC::ReadFromFile(FILE *fp)
{
  if (!locked)
    {
      NA::ReadFromFile(fp);
      Invert();
    }
}

									// This is the module initialization.

/*------------------------------
 * Module Initialization
------------------------------*/

static PyMethodDef FileMethods[] = {					// Only one file method available to python: 
									// make a new NA_PWC object.
  {"new", NA_PWC::PyMake, Py_NEWARGS},

  {NULL, NULL}		// Sentinel
};

extern "C" {								// Python is a C program and wants to call
									// init with a C protocol.
  void initNA_PWC(void)
    {      
      Py_InitModule("NA_PWC", FileMethods);      
    }
}