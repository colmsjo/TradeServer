/*------------------------------	
 * NA_PWC h				
 *					
 * Neural Array C++			
 *  for inclusion in python		
 * 					
 * subclassed from NA (Neural Array)	
 *					
 * adds pinto-wilson-cowan dynamics	
 *					
 * State				
 *   V     -- voltage			
 *   gamma -- voltage leak		
 *					
 *   F = 0.5 + 0.5 * tanh(V + gamma)	
------------------------------*/	
					
					
#ifndef _na_pwc_h_			
#define _na_pwc_h_			
					
#include "array1d.h"	// An array class (only in C++, implements one 	
			// dimensional arrays in a standard C++ manner 	
			// These arrays are not available to python.  	
#include "NA.h"		// The neural array header file			
					
class NA_PWC : public NA {		
					
  Py_Header;			// always start with Py_Header
					
 protected:			// additional state added by this subclass	
  array1d <float> V;	
  float gamma;				
  boolean locked;
  
 public:
  NA_PWC(char *name, int n, float tau, float gamma, 
	 PyTypeObject *T = &Type);			// C++ constructor
  static PyObject *PyMake(PyObject *, PyObject *);	// Python constructor 
							// (called by "new", see below)
  ~NA_PWC();						// C++ destructor

  PyObject *_getattr(char *attr);			// __getattr__ function
  int _setattr(char *attr, PyObject *value);		// __setattr__ function

							// A typical new method.  
  virtual void PrintVoltage(void);			// Actual C++ function, directly callable from C++
  PyObject *PyPrintVoltage(PyObject *args);		// Python wrapper
  static PyObject *sPyPrintVoltage(PyObject *self, 	// static python wrapper
				   PyObject *args, 
				   PyObject *kwd)
    {return ((NA_PWC*)self)->PyPrintVoltage(args);};

							// Another typical new method
  void Lock(void) {locked = true;}				
  PyObject *PyLock(PyObject *args) {Lock(); Py_Return;};
  static PyObject *sPyLock(PyObject *self, 
			   PyObject *args, 
			   PyObject *kwds)
    {return ((NA_PWC*)self)->PyLock(args);};

							// And another
  void UnLock(void) {locked = false;}
  PyObject *PyUnLock(PyObject *args) {UnLock(); Py_Return;};
  static PyObject *sPyUnLock(PyObject *self, 
			     PyObject *args, 
			     PyObject *kwds)
    {return ((NA_PWC*)self)->PyUnLock(args);};

							// Methods that are modifications of parents.  If the 
							// method calls are identical from parent to child, then
							// only the C++ function needs to be rewritten.

  void FillWithRandom(float min, float max);		
  void FillLinearly(void);		
  void FillWithZero(void);		
  void FillWithConst(float c);
  void FireOneNeuron(int i = -1);
  void ReadFromFile(FILE *fp);
  void Cycle(AgentGlobalState &AGS);


							// C++ only methods.  These methods are not required 
							// by any python calls, so they only need C++ versions.
  void Invert(void);		                
  void IncrementVoltage(int i, float dv) 
    {V[i] += dv;};

};

#endif // _na_pwc_h_
