/*------------------------------
 * PyObjectPlus cpp
 *
 * C++ library routines for Crawl 3.2
------------------------------*/

#include "stdlib.h"
#include "PyObjectPlus.h"

/*------------------------------
 * PyObjectPlus Type		-- Every class, even the abstract one should have a Type
------------------------------*/

PyTypeObject PyObjectPlus::Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,				/*ob_size*/
	"PyObjectPlus",			/*tp_name*/
	sizeof(PyObjectPlus),		/*tp_basicsize*/
	0,				/*tp_itemsize*/
	/* methods */
	PyDestructor,	  		/*tp_dealloc*/
	0,			 	/*tp_print*/
	__getattr, 			/*tp_getattr*/
	__setattr, 			/*tp_setattr*/
	0,			        /*tp_compare*/
	__repr,			        /*tp_repr*/
	0,			        /*tp_as_number*/
	0,		 	        /*tp_as_sequence*/
	0,			        /*tp_as_mapping*/
	0,			        /*tp_hash*/
	0,				/*tp_call */
};

/*------------------------------
 * PyObjectPlus Methods 	-- Every class, even the abstract one should have a Methods
------------------------------*/
PyMethodDef PyObjectPlus::Methods[] = {
  {"isA",		 (PyCFunction) sPy_isA,			Py_NEWARGS},
  {NULL, NULL}		/* Sentinel */
};

/*------------------------------
 * PyObjectPlus Parents		-- Every class, even the abstract one should have parents
------------------------------*/
PyParentObject PyObjectPlus::Parents[] = {&PyObjectPlus::Type, NULL};

/*------------------------------
 * PyObjectPlus attributes	-- attributes
------------------------------*/
PyObject *PyObjectPlus::_getattr(char *attr)
{
  if (streq(attr, "type"))
    return Py_BuildValue("s", (*(GetParents()))->tp_name);

  return Py_FindMethod(Methods, this, attr);    
}

int PyObjectPlus::_setattr(char *attr, PyObject *value)
{
  cerr << "Unknown attribute" << endl;
  return 1;
}

/*------------------------------
 * PyObjectPlus repr		-- representations
------------------------------*/
PyObject *PyObjectPlus::_repr(void)
{
  Py_Error(PyExc_SystemError, "Representation not overridden by object.");  
}

/*------------------------------
 * PyObjectPlus isA		-- the isA functions
------------------------------*/
boolean PyObjectPlus::isA(PyTypeObject *T)		// if called with a Type, use "typename"
{
  return isA(T->tp_name);
}

boolean PyObjectPlus::isA(const char *typename)		// check typename of each parent
{
  int i;
  PyParentObject  P;
  PyParentObject *Ps = GetParents();

  for (P = Ps[i=0]; P != NULL; P = Ps[i++])
      if (streq(P->tp_name, typename))
	return true;
  return false;
}

PyObject *PyObjectPlus::Py_isA(PyObject *args)		// Python wrapper for isA
{
  char *typename;
  Py_Try(PyArg_ParseTuple(args, "s", &typename));
  if(isA(typename))
    {Py_INCREF(Py_True); return Py_True;}
  else
    {Py_INCREF(Py_False); return Py_False;};
}

