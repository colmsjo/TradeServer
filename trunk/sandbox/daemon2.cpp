/*
 * Example daemon shell code for all of the requirements of a basic
 * linux daemon written in C.
 *
 * To use this code, search for 'TODO' and follow the directions.
 * 
 * To compile this file:
 *      gcc -o [daemonname] thisfile.c
 *
 * Substitute gcc with cc on some platforms.
 *
 * Peter Lombardo (peter AT lombardo DOT info)
 * 5/1/2006
 *
 */
 
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <assert.h>
#include <signal.h>

#include <sstream>
#include <string>
#include <exception>
#include <iostream>
#include <fstream>


#include "../tools/nvwa-0.8.2/nvwa/debug_new.h"


using namespace std;

bool				exit_process = false;
ofstream 			log_file_;

void init_log(string log_file) {	
	pid_t pid = getpid();
	stringstream log_file2;
	log_file2 << log_file << "." << pid << ".log"; 
	log_file_.open(log_file2.str().c_str());		
}

void destroy_log() {
	log_file_.close();
}	

void mylog(string s) {
	log_file_ << s << "\n";
}

 
// TODO: Change '[daemonname]' to the name of _your_ daemon
#define DAEMON_NAME "test-daemonn"
#define PID_FILE "/var/run/test-daemon.pid"
 
/**************************************************************************
    Function: Print Usage
 
    Description:
        Output the command-line options for this daemon.
 
    Params:
        @argc - Standard argument count
        @argv - Standard argument array
 
    Returns:
        returns void always
**************************************************************************/
void PrintUsage(int argc, char *argv[]) {
    if (argc >=1) {
        printf("Usage: %s -h -nn", argv[0]);
        printf("  Options:n");
        printf("      -ntDon't fork off as a daemon.n");
        printf("      -htShow this help screen.n");
        printf("n");
    }
}
 
/**************************************************************************
    Function: signal_handler
 
    Description:
        This function handles select signals that the daemon may
        receive.  This gives the daemon a chance to properly shut
        down in emergency situations.  This function is installed
        as a signal handler in the 'main()' function.
 
    Params:
        @sig - The signal received
 
    Returns:
        returns void always
**************************************************************************/
void signal_handler(int sig) {
 
    switch(sig) {
        case SIGHUP:
        	exit_process = true;
            mylog("Received SIGHUP signal.");
            break;
        case SIGTERM:
        	exit_process = true;
            mylog("Received SIGTERM signal.");
            break;
        default:
        	stringstream s;
        	s <<  "Unhandled signal: " << strsignal(sig);
            mylog(s.str().c_str());
            break;
    }
    
    return;
}

pid_t mydaemon() { 
    /* Our process ID and Session ID */
    pid_t pid, sid;

    // Setup signal handling
    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGQUIT, signal_handler);


    /* Fork off the parent process, the child gets 0 returned while the parent gets the PID of the child process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0) {
    	mylog("Exiting parent process...");
        exit(EXIT_SUCCESS);
    }

    /* Change the file mode mask */
    umask(0);

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        /* Log the failure */
    	mylog("Failed sid < 0");
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory */
    if ((chdir("/")) < 0) {
        /* Log the failure */
    	mylog("Failed cd /");
        exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	return sid;
} 
 
/**************************************************************************
    Function: main
 
    Description:
        The c standard 'main' entry point function.
 
    Params:
        @argc - count of command line arguments given on command line
        @argv - array of arguments given on command line
 
    Returns:
        returns integer which is passed back to the parent process
**************************************************************************/
int main(int argc, char *argv[]) {

	init_log(DAEMON_NAME);
 
#if defined(DEBUG)
    int daemonize = 0;
#else
    int daemonize = 1;
#endif
  
    int c;
    while( (c = getopt(argc, argv, "nh|help")) != -1) {
        switch(c){
            case 'h':
                PrintUsage(argc, argv);
                exit(0);
                break;
            case 'n':
                daemonize = 0;
                break;
            default:
                PrintUsage(argc, argv);
                exit(0);
                break;
        }
    }
 
    mylog("daemon starting up");
 
	pid_t pid;
 
    if (daemonize) {
        mylog("starting the daemonizing process");

		pid = mydaemon();
			
 		/* Deprecated on OS X, using the homebuilt version instead
 		if (daemon(0,0) != 0) {
    		mylog("daemon() failed!");
        	exit(EXIT_FAILURE);
 		}*/
 
        mylog("Closing stdin, stdout and stderr");
    }

	mylog("Doing execl()");
 	destroy_log();
	if (execl("/Users/jonas/svn/TradeServer/trunk/sandbox/tmpi", "/Users/jonas/svn/TradeServer/trunk/sandbox/tmpi", NULL) == -1) {
		mylog("execl() failed!");
		exit(EXIT_FAILURE);
	}
	
	while(!exit_process) {
		stringstream s;
		s << "Working realy hard, pid:" + pid;
		//mylog(s.str().c_str());
		mylog("Working really hard…");
		sleep(10);
	}
	
    mylog("Daemon exiting");
  
 	destroy_log();
    exit(0);
}