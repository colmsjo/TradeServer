CC = g++

LIBS = -ldl

.cc.o:
	$(CC) -ggdb -c $<

default:
	make testdcl

OBJS = testdcl.o

testdcl: testdcl.o
	$(CC) -rdynamic -o testdcl testdcl.o $(LIBS)

# -Wl,-soname,libcircle.so
libcircle.so:  circle.o
	g++ -shared -Wl -o libcircle.so circle.o

# -Wl,-soname,libsquare.so
libsquare.so:  square.o
	g++ -shared -Wl -o libsquare.so square.o

all: testdcl libcircle.so libsquare.so

clean:
	rm -f *.so *.o testdcl

