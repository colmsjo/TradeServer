#include <iostream> 
#include "circle.hh"

void circle::draw(){
   // simple ascii circle<\n>
   cout << "\n";
   cout << "      ****\n";
   cout << "    *      *\n";
   cout << "   *        *\n";
   cout << "   *        *\n";
   cout << "   *        *\n";
   cout << "    *      *\n";
   cout << "      ****\n";
   cout << "\n";
}

extern "C" {
	
shape *maker(){
   return new circle;
}

// factory_class *fact
void init_shape(void) {
   	  cout << "Instantiating factory…" << endl;
   	
      // register the maker with the factory
      //fact->reg("circle", maker);	
}

class proxy {
public:
   proxy(){
   	  cout << "Instantiating factory…" << endl;
   	
      // register the maker with the factory
      factory["circle"] = maker;
   }
};

// our one instance of the proxy
proxy p;

} // extern "C"
