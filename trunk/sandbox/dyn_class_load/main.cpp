#include <iostream>
#include <dlfcn.h>


int main() {
    using std::cout;
    using std::cerr;

    cout << "C++ dlopen demo\n\n";

#ifdef __APPLE__
   char *command_str = (char*) ".libs/libhello.dylib";  // command
   std::cout << "RUNNNING APPLE" << std::endl;
#elif defined (__WIN32__) || defined(__WIN64__)
   char *command_str = (char*) "./libhello.dll";  // command
#else
   char *command_str = (char*) ".libs//libhello.so";  // command
#endif


    // open the library
    cout << "Opening hello.so...\n";
    void* handle = dlopen(command_str, RTLD_LAZY);


    
    if (!handle) {
        cerr << "Cannot open library: " << dlerror() << '\n';
        return 1;
    }
    
    // load the symbol
    cout << "Loading symbol hello...\n";
    typedef void (*hello_t)();
    hello_t hello = (hello_t) dlsym(handle, "hello");
    if (!hello) {
        cerr << "Cannot load symbol 'hello': " << dlerror() <<
            '\n';
        dlclose(handle);
        return 1;
    }
    
    // use it to do the calculation
    cout << "Calling hello...\n";
    hello();
    
    // close the library
    cout << "Closing library...\n";
    dlclose(handle);
}