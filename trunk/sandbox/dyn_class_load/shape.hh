#ifndef __SHAPE_H
#define __SHAPE_H

#include <map>
#include <string>
#include <dlfcn.h>
#include <ltdl.h> 

using namespace std;

// base class for all shapes
class shape {
public:
   virtual void draw()=0;
};

// typedef to make it easier to set up our factory
typedef shape *maker_t();

class factory_class {
public:
	void reg(char* str, maker_t *maker);
};

// our global factory
// extern map<string, maker_t *, less<string> > factory;
extern map<string, maker_t *, less<string> > factory;

#endif // __SHAPE_H
