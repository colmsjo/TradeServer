#include <iostream>
#include "square.hh"

void square::draw(){
   // simple ascii square
   cout << "\n";
   cout << "    *********\n";
   cout << "    *       *\n";
   cout << "    *       *\n";
   cout << "    *       *\n";
   cout << "    *       *\n";
   cout << "    *********\n";
   cout << "\n";
}

extern "C" {
	
shape *maker(){
   return new square;
}

void init_shape(void) {
   	  cout << "Instantiating factory…" << endl;

      // register the maker with the factory
      //fact->reg("square", maker);	
}

class proxy { 
public:
   proxy(){
      // register the maker with the factory
      factory["square"] = maker;
   }
};

// our one instance of the proxy
proxy p;

} // extern "C"

