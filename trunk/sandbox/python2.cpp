/*
 * Python1.cpp
 *
 * export PYTHONPATH=./
 * ./python2
 *
 *
 * Embedding Python in C++
 *
 */
 
#include <Python.h>

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/types.h>

using namespace std;

class CppClass {
	public:
		CppClass() {
			std::cout << "This is CppClass!" << endl;
		}
		
		~CppClass() {
			std::cout << "CppClass is being destroyed" << endl;			
		}
		
		void message(void) {
			std::cout << "CppClass saying hi!" << endl;			
		}
};



int
main(int argc, char *argv[])
{
	std::cout << "C++: This is process: " <<  getpid() << std::endl;

	
    PyObject *pName, *pModule, *pDict, *pFunc;
    PyObject *pArgs, *pValue;
    int i;

	string module("python2");
	string function("make_cpp_call");

    Py_Initialize();

    pName = PyString_FromString(module.c_str());
    /* Error checking of pName left out */

    pModule = PyImport_Import(pName);

    Py_DECREF(pName);

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, function.c_str());
        /* pFunc is a new reference */

        if (pFunc && PyCallable_Check(pFunc)) {
            pValue = PyObject_CallObject(pFunc, NULL);
 
            if (pValue != NULL) {
                printf("Result of call: %ld\n", PyInt_AsLong(pValue));
                Py_DECREF(pValue);
            }
            else {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return 1;
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
        return 1;
    }
    Py_Finalize();
    return 0;
}