/*
 * Python3.cpp
 *
 * export PYTHONPATH=./
 * ./python3
 *
 *
 * Embedding Python in C++ and use extensions - http://docs.python.org/extending/embedding.html
 * Define new types - http://www.python.org/doc//current/extending/newtypes.html?highlight=pyobject_head_init
 * Extension modules - http://starship.python.net/crew/arcege/extwriting/pyext.html
 * 						http://starship.python.net/crew/mwh/toext/toext.html
 * 						http://www.tutorialspoint.com/python/python_further_extensions.htm
 */
 
#include <Python.h>				// Needs to be first

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/types.h>

#include "../tools/nvwa-0.8.2/nvwa/debug_new.h"

using namespace std;

class CppClass : public PyObject {
	private:
		static PyMethodDef    Methods[];
	
	public:

  		static PyTypeObject   Type;
	

		/*
		  PyObjectPlus(PyTypeObject *T) 				// constructor
		    {this->ob_type = T; _Py_NewReference(this);};
		  
		  virtual ~PyObjectPlus() {};					// destructor
		  static void PyDestructor(PyObject *P)				// python wrapper
		    {  delete ((PyObjectPlus *) P);  };			    
		    
		*/
		
		CppClass() {											// TODO: Should PyTypeObject *T be an argument?s
			std::cout << "This is CppClass!" << endl;
			Py_INCREF(this);									// Not sure this is the right way??
		}
		
		
		static PyObject* PyMake(PyObject *ignored, PyObject *args)	{

			 // TODO: Add some arguments, just to see how it works?
			 
			 // Py_Try(PyArg_ParseTuple(args, "si|ff", &name, &n, &tau, &gamma));		// Read arguments
			 // Py_Assert(n > 0, PyExc_ValueError, "n <= 0");			// Check values ok
			 // Py_Assert(tau >= 0, PyExc_ValueError, "tau << 0");		// Check values ok
		
			// Make new Python-able object
			return new CppClass();			
		}

		/* 
			Should define PyDestructor and reference this from a Type structure
			
			The question is how do I make Python aware of the Type structure??
		*/
		
		virtual ~CppClass() {
			std::cout << "CppClass is being destroyed" << endl;			
		}

		static void PyDestructor(PyObject *P) {  
			delete ((CppClass *) P);  
		}			    
		
		void message(void) {
			std::cout << "CppClass saying hi!" << endl;			
		}

		PyObject *PyMessage(PyObject *args) {
			message(); 
			Py_INCREF(Py_None); return Py_None;		//Py_Return macro
		}
		
		static PyObject* sPyMessage(PyObject *self, PyObject *args, PyObject *kwds) {
			return ((CppClass*)self)->PyMessage(args);
		}

};


/*------------------------------
 * CppClass Methods				
------------------------------*/
PyMethodDef CppClass::Methods[] = {
  {"message", (PyCFunction) sPyMessage, 1},		// Why not CppClass::sPyMessage?

  {NULL, NULL}		/* Sentinel */
};


/*------------------------------
 * Constructor				
------------------------------*/
static PyMethodDef FileMethods[] = { 
  {"new", CppClass::PyMake, 1},

  {NULL, NULL}		// Sentinel
};

/*------------------------------
 * Type				
------------------------------*/
PyTypeObject CppClass::Type = {
	PyObject_HEAD_INIT(NULL)
	0,								// ob_size
	"cpp.CppClass",						// tp_name
	sizeof(CppClass),				// tp_basicsize
	0,								// tp_itemsize
	
									// methods 
	PyDestructor,	  				// tp_dealloc
	0,			 					// tp_print
	0, //__getattr, 						// tp_getattr
	0, //__setattr, 						// tp_setattr
	0,			        			// tp_compare
	0, //__repr,		        			// tp_repr
	0,			        			// tp_as_number
	0,		 	        			// tp_as_sequence
	0,			        			// tp_as_mapping
	0,			        			// tp_hash
	0, //sPyCycle,						// tp_call 
};
	


static int numargs=0;

/* Return the number of arguments of the application command line */
static PyObject* emb_numargs(PyObject *self, PyObject *args) {
    if(!PyArg_ParseTuple(args, ":numargs"))
        return NULL;
    return Py_BuildValue("i", numargs);
}

static PyMethodDef EmbMethods[] = {
    {"numargs", emb_numargs, METH_VARARGS,
     "Return the number of arguments received by the process."},
    {NULL, NULL, 0, NULL}
};

int main(int argc, char *argv[]) {
	std::cout << "C++: This is process: " <<  getpid() << std::endl;
	
    PyObject *pName, *pModule, *pDict, *pFunc;
    PyObject *pArgs, *pValue;
    int i;

	string module("python3");
	string function("make_cpp_call");

	// Initilize Python
    Py_Initialize();
    
    // Initialize embedding of C++ from Python
    numargs = argc;
	Py_InitModule("emb", EmbMethods);

	// Initilize CppClass
	CppClass::Type.tp_new = PyType_GenericNew;
	if (PyType_Ready(&CppClass::Type) < 0) {
		PyErr_Print();
		fprintf(stderr, "Failed to initialize CppClass - PyType_Ready\n");
		return 1;
	}

	PyObject *pCppModule = Py_InitModule("cpp", FileMethods);
    if (!pCppModule) {
		PyErr_Print();
		fprintf(stderr, "Failed to initialize CppClass - Py_InitModule\n");
		return 1;
	}

	Py_INCREF(&CppClass::Type);
	PyModule_AddObject(pCppModule, "CppClass", (PyObject *)&CppClass::Type);
 
    // Load module python2.py
    pName = PyString_FromString(module.c_str()); 					// Error checking of pName left out
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pModule) {
		PyErr_Print();
		fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
		return 1;
	}

	// Get the python fuction to call
    pFunc = PyObject_GetAttrString(pModule, function.c_str()); 		// pFunc is a new reference 
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
	    Py_XDECREF(pFunc);
	    Py_DECREF(pModule);
        return 1;
    }

    // Make the call to the python function
    pValue = PyObject_CallObject(pFunc, NULL);
    if (!pValue) {
        PyErr_Print();
        fprintf(stderr,"Call failed\n");
        Py_DECREF(pFunc);
        Py_DECREF(pModule);
        return 1;
    }

	// Show the result of the python call
    printf("Result of call: %ld\n", PyInt_AsLong(pValue));
    
    // Decrease counters for python garbage collection
    Py_DECREF(pValue);
	std::cout << "Before Py_XDECREF(pFunc);" << std::endl;
    Py_XDECREF(pFunc);
	std::cout << "Before Py_DECREF(pModule);" << std::endl;
    Py_DECREF(pModule);
    
	std::cout << "Before Py_Finalize();" << std::endl;
    // Clean up python
    Py_Finalize();
    
	std::cout << "Before return;" << std::endl;
    return 0;
}