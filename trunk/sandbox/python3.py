import os
import emb
import cpp												# Import C++ class

def make_cpp_call():
    print "Python: This is process: ", os.getpid()
    print "Number of arguments", emb.numargs()
    
    cpp_obj = cpp.new()
    # cpp_obj2 = cpp.cpp()

    cpp_obj.message()										

    return 1