#include <Python.h>

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/types.h>

#include "../tools/nvwa-0.8.2/nvwa/debug_new.h"

/*typedef struct {
	PyObject_HEAD
	// Type-specific fields go here. 
} SimpleObject;
*/


class SimpleObject : public PyObject {
	public:
		static PyTypeObject SimpleObjectType;
};

PyTypeObject SimpleObject::SimpleObjectType = {
	PyObject_HEAD_INIT(NULL)
	0,				/* ob_size        */
	"simpletype.Simple",		/* tp_name        */
	sizeof(SimpleObject),		/* tp_basicsize   */
	0,				/* tp_itemsize    */
	0,				/* tp_dealloc     */
	0,				/* tp_print       */
	0,				/* tp_getattr     */
	0,				/* tp_setattr     */
	0,				/* tp_compare     */
	0,				/* tp_repr        */
	0,				/* tp_as_number   */
	0,				/* tp_as_sequence */
	0,				/* tp_as_mapping  */
	0,				/* tp_hash        */
	0,				/* tp_call        */
	0,				/* tp_str         */
	0,				/* tp_getattro    */
	0,				/* tp_setattro    */
	0,				/* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,		/* tp_flags       */
	"Simple objects are simple.",	/* tp_doc         */
};

PyMODINIT_FUNC
initsimpletype(void) 
{
	PyObject* m;

	SimpleObject::SimpleObjectType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&SimpleObject::SimpleObjectType) < 0)
		return;

	m = Py_InitModule3("simpletype", NULL,
			   "Example module that creates an extension type.");
	if (m == NULL)
		return;

	Py_INCREF(&SimpleObject::SimpleObjectType);
	PyModule_AddObject(m, "Simple", (PyObject *)&SimpleObject::SimpleObjectType);
}



int main(int argc, char *argv[]) {
	std::cout << "C++: This is process: " <<  getpid() << std::endl;

	
    PyObject *pName, *pModule, *pDict, *pFunc;
    PyObject *pArgs, *pValue;
    int i;

	std::string module("python5");
	std::string function("run_now");

	// Initilize Python
    Py_Initialize();
    
	initsimpletype();

    // Load module python2.py
    pName = PyString_FromString(module.c_str()); 					// Error checking of pName left out
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pModule) {
		PyErr_Print();
		fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
		return 1;
	}

	// Get the python fuction to call
    pFunc = PyObject_GetAttrString(pModule, function.c_str()); 		// pFunc is a new reference 
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
	    Py_XDECREF(pFunc);
	    Py_DECREF(pModule);
        return 1;
    }

    // Make the call to the python function
    pValue = PyObject_CallObject(pFunc, NULL);
    if (!pValue) {
        PyErr_Print();
        fprintf(stderr,"Call failed\n");
        Py_DECREF(pFunc);
        Py_DECREF(pModule);
        return 1;
    }

	// Show the result of the python call
    printf("Result of call: %ld\n", PyInt_AsLong(pValue)); 
     
    // Decrease counters for python garbage collection
    Py_DECREF(pValue);
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
        
    // Clean up python
    Py_Finalize();
    
    return 0;
}