/*
 * Python6.cpp
 *
 * export PYTHONPATH=./
 * ./python6
 *
 */

#include "python6.hpp"

/*
 * name		char *			name of the member
 * type		int				the type of the member in the C struct
 * offset	Py_ssize_t		the offset in bytes that the member is located on the type’s object struct
 * flags	int				flag bits indicating if the field should be read-only or writable
 * doc		char *			points to the contents of the docstring
 */


void set_method_def_fields(PyMemberDef* method_def, char* name, int type, Py_ssize_t offset, int flags, char* doc) {
	method_def->name   = name;
	method_def->type   = type;
	method_def->offset = offset;
	method_def->flags  = flags;
	method_def->doc    = doc;	
}


PyMemberDef* create_member_defs(void) {
	PyMemberDef* member_defs;
	PyMemberDef  member_def;
	int			 null_ = NULL;
	
	// Allocate array of PyMethodDef
	member_defs = (PyMemberDef*) malloc(sizeof(PyMemberDef)+1); // one record plus a NULL byte
	
	// First record
	set_method_def_fields(&member_def, "firstname", T_OBJECT_EX, offsetof(NameObject, firstname), 0, "first name");
	memcpy(member_defs, &member_def, sizeof(PyMethodDef));
	
	// Second record
	set_method_def_fields(&member_def, "lastname", T_OBJECT_EX, offsetof(NameObject, lastname), 0, "last name");
	memcpy(member_defs+sizeof(PyMemberDef), &member_def, sizeof(PyMethodDef));
	
	// Ending NULL byte
	memcpy(member_defs+2*sizeof(PyMemberDef), &null_, 1);
	
	return member_defs;
}

/*
PyMemberDef NameObject::members[] = {
	{"firstname", T_OBJECT_EX, offsetof(NameObject, firstname), 0, "first name"},
	{"lastname", T_OBJECT_EX, offsetof(NameObject, lastname), 0, "last name"},
	{NULL}  // Sentinel
};*/


/*
 * PyMethodDef
 * 
 * ml_name	char *		name of the method
 * ml_meth	PyCFunction	pointer to the C implementation
 * ml_flags	int			flag bits indicating how the call should be constructed
 * ml_doc	char *		points to the contents of the docstring
 */
 
void set_method_def_fields(PyMethodDef* method_def, char* name, PyCFunction meth, int flags, char* doc) {
	method_def->ml_name  = name;		// use strcpy instead
	method_def->ml_meth  = meth;
	method_def->ml_flags = flags;
	method_def->ml_doc   = doc;			// use strcpy instead
}

PyMethodDef* create_method_defs(void) {
	PyMethodDef* method_defs;
	PyMethodDef  method_def;
	int			 null_ = NULL;
	
	// Allocate array of PyMethodDef
	method_defs = (PyMethodDef*) malloc(sizeof(PyMethodDef)+1); // one record plus a NULL byte
	
	// First record
	//set_method_def_fields(&method_def, "name", (PyCFunction)NameObject::sPyName, METH_NOARGS, "Return the name, combining the firstname and lastname.");
	set_method_def_fields(&method_def, "name", (PyCFunction)sPyName, METH_NOARGS, "Return the name, combining the firstname and lastname.");
	memcpy(method_defs, &method_def, sizeof(PyMethodDef));
	
	// Ending NULL byte
	memcpy(method_defs+sizeof(PyMethodDef), &null_, 1);
	
	return method_defs;
}

/*
PyMethodDef NameObject::methods[] = {
	{"name", (PyCFunction)NameObject::name, METH_NOARGS, "Return the name, combining the firstname and lastname." },
	{NULL}
};*/

// char NameObject::doc[] = "boo.";


/*
 * http://docs.python.org/release/2.2/ext/dnt-type-methods.html
 * 
 * typedef struct _typeobject {
 *    PyObject_VAR_HEAD					// EXPANDS TO: PyObject_HEAD Py_ssize_t ob_size; => Py_ssize_t ob_refcnt; PyTypeObject *ob_type; Py_ssize_t ob_size;
 *
 *    char *tp_name; 					// For printing
 *    int tp_basicsize, tp_itemsize; 	// For allocation
 *
 * ALTERNATIVE
 * typedef struct _typeobject {
 *    Py_ssize_t ob_refcnt; 
 *    PyTypeObject *ob_type; 
 *    Py_ssize_t ob_size;
 *
 *    char *tp_name; 					// For printing
 *    int tp_basicsize, tp_itemsize; 	// For allocation
 *
 *  
 * typedef struct _typeobject {
 *     PyObject_VAR_HEAD
 *     char 		*tp_name; // For printing, in format "<module>.<name>" 
 *     int 			tp_basicsize, tp_itemsize; // For allocation 
 * 
 *     // Methods to implement standard operations 
 * 
 *     destructor 		tp_dealloc;
 *     printfunc 		tp_print;
 *     getattrfunc 		tp_getattr;
 *     setattrfunc 		tp_setattr;
 *     cmpfunc 			tp_compare;
 *     reprfunc 		tp_repr;
 * 
 *     // Method suites for standard classes 
 * 
 *     PyNumberMethods 	*tp_as_number;
 *     PySequenceMethods *tp_as_sequence;
 *     PyMappingMethods *tp_as_mapping;
 * 
 *     // More standard operations (here for binary compatibility) 
 * 
 *     hashfunc 		tp_hash;
 *     ternaryfunc 		tp_call;
 *     reprfunc 		tp_str;
 *     getattrofunc 	tp_getattro;
 *     setattrofunc 	tp_setattro;
 * 
 *     // Functions to access object as input/output buffer 
 *     PyBufferProcs *	tp_as_buffer;
 * 
 *     // Flags to define presence of optional/expanded features 
 *     long 			tp_flags;
 * 
 *     char 			*tp_doc; // Documentation string
 * 
 *     // Assigned meaning in release 2.0
 *     // call function for all accessible objects 
 *     traverseproc 	tp_traverse;
 * 
 *     // delete references to contained objects 
 *     inquiry 			tp_clear;
 * 
 *     // Assigned meaning in release 2.1
 *     // rich comparisons
 *     richcmpfunc 		tp_richcompare;
 * 
 *     // weak reference enabler 
 *     long 			tp_weaklistoffset;
 *
 *     // Added in release 2.2 
 *     // Iterators 
 *     getiterfunc 		tp_iter;
 *     iternextfunc 	tp_iternext;
 * 
 *     // Attribute descriptor and subclassing stuff
 *     struct PyMethodDef *tp_methods;
 *     struct PyMemberDef *tp_members;
 *     struct PyGetSetDef *tp_getset;
 *     struct _typeobject *tp_base;
 *     PyObject 		*tp_dict;
 *     descrgetfunc 	tp_descr_get;
 *     descrsetfunc 	tp_descr_set;
 *     long 			tp_dictoffset;
 *     initproc 		tp_init;
 *     allocfunc 		tp_alloc;
 *     newfunc 			tp_new;
 *     freefunc 		tp_free; // Low-level free-memory routine
 *     inquiry 			tp_is_gc; // For PyObject_IS_GC
 *     PyObject 		*tp_bases;
 *     PyObject 		*tp_mro; // method resolution order
 *     PyObject 		*tp_cache;
 *     PyObject 		*tp_subclasses;
 *     PyObject 		*tp_weaklist;
 * 
 * } PyTypeObject;
 *
 */


void set_type_object_fields(PyTypeObject* type_object, char* tp_name, int tp_basicsize, long tp_flags, char* tp_doc, 
							struct PyMethodDef* tp_methods, struct PyMemberDef *tp_members, initproc tp_init){
	
	type_object->tp_name 		= tp_name;
	type_object->tp_basicsize 	= tp_basicsize;
	type_object->tp_flags 		= tp_flags;
	type_object->tp_doc 		= tp_doc;
	type_object->tp_methods 	= tp_methods;
	type_object->tp_members		= tp_members;
	type_object->tp_init 		= tp_init;	
}


PyTypeObject* create_type_object(void) {
	PyTypeObject* type_object;
	
	// Allocate array of PyMethodDef
	type_object = (PyTypeObject*) malloc(sizeof(PyTypeObject)); 
	
	// Create PyTypeObject for NameObject
	set_type_object_fields(type_object, 
		"name.Name",					/* tp_name           */
		sizeof(NameObject),				/* tp_basicsize      */
		Py_TPFLAGS_DEFAULT,				/* tp_flags          */
		"boo.",							/* tp_doc            */
		create_method_defs(),	     	/* tp_methods        */
		create_member_defs(),			/* tp_members        */
		//(initproc)NameObject::sPyInit	// tp_init           
		(initproc)sPyInit				/* tp_init           */
	);
	type_object->tp_new = PyType_GenericNew;
	
	return type_object;
}

/*
PyTypeObject NameObjectType = {
	//PyObject_HEAD_INIT(NULL)			// Expands to 1, NULL,
	1, 									// ob_refcnt 
	NULL,								// ob_type 
	0,									// ob_size           
	"name.Name",			// tp_name           
	sizeof(NameObject),		/* tp_basicsize      
	0,				/* tp_itemsize       
	0,				/* tp_dealloc        
	0,				/* tp_print          
	0,				/* tp_getattr        
	0,				/* tp_setattr        
	0,				/* tp_compare        
	0,				/* tp_repr           
	0,				/* tp_as_number      
	0,				/* tp_as_sequence    
	0,				/* tp_as_mapping     
	0,				/* tp_hash           
	0,				/* tp_call           
	0,				/* tp_str            
	0,				/* tp_getattro       
	0,				/* tp_setattro       
	0,				/* tp_as_buffer      
	Py_TPFLAGS_DEFAULT,		/* tp_flags          
	NameObject::doc,			/* tp_doc            
	0,				/* tp_traverse       
	0,				/* tp_clear          
	0,				/* tp_richcompare    
	0,				/* tp_weaklistoffset 
	0,				/* tp_iter           
	0,				/* tp_iternext       
	NameObject::methods,	     		/* tp_methods        
	NameObject::members,			/* tp_members        
	0,				/* tp_getset         
	0,				/* tp_base           
	0,				/* tp_dict           
	0,				/* tp_descr_get      
	0,				/* tp_descr_set      
	0,				/* tp_dictoffset     
	(initproc)NameObject::init,		/* tp_init           
};*/



//int NameObject::sPyInit(NameObject *self, PyObject *args, PyObject *kwds) {
int sPyInit(NameObject *self, PyObject *args, PyObject *kwds) {
	PyObject *firstname = Py_None;
	PyObject *lastname = Py_None;
	PyObject *tmp = NULL;

	// static char *kwlist[] = {"firstname", "lastname", NULL};
	char *kwlist[] = {"firstname", "lastname", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|OO", kwlist,
					 &firstname, &lastname))
		return -1;

	tmp = self->firstname;
	if (firstname == Py_None) {
		self->firstname = PyString_FromString("");
	}
	else {
		Py_INCREF(firstname);
		self->firstname = firstname;
	}
	Py_XDECREF(tmp);

	tmp = self->lastname;
	if (lastname == Py_None) {
		self->lastname = PyString_FromString("");
	}
	else {
		Py_INCREF(lastname);
		self->lastname = lastname;
	}
	Py_XDECREF(tmp);

	return 0;
}


// Static Python wrapper
//PyObject* NameObject::sPyName(NameObject *self, PyObject *unused) {
PyObject* sPyName(NameObject *self, PyObject *unused) {
	return ((NameObject*)self)->PyName(self, unused);
}

// Python function
PyObject* NameObject::PyName(NameObject *self, PyObject *unused)
{
	PyObject *args = NULL;
	PyObject *format = NULL;
	PyObject *result = NULL;

	format = PyString_FromString("%s %s");
	if (format == NULL)
		goto fail;

	if (self->firstname == NULL) {
		PyErr_SetString(PyExc_AttributeError, "firstname");
		goto fail;
	}

	if (self->lastname == NULL) {
		PyErr_SetString(PyExc_AttributeError, "lastname");
		goto fail;
	}

	args = Py_BuildValue("OO", self->firstname, self->lastname);
	if (args == NULL)
		goto fail;

	result = PyString_Format(format, args);

  fail:
	Py_XDECREF(format);
	Py_XDECREF(args);

	return result;
}

void free_type_object(PyTypeObject* type_object) {
    free(type_object->tp_methods);
    free(type_object->tp_members);
    free(type_object);
}

int main(int argc, char *argv[]) {
	std::cout << "C++: This is process: " <<  getpid() << std::endl;

	
    PyObject *pName, *pModule, *pFunc; //, *pDict, ;
    PyObject *pValue; // *pArgs,
    // int i;

	std::string module("python6");
	std::string function("run_now");

	// Initilize Python
    Py_Initialize();
    
	/* NameObjectType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&NameObjectType) < 0) {
		PyErr_Print();
		fprintf(stderr, "Type not ready - PyType_Ready\n");
		return 1;
	} */

 
	PyTypeObject* type_object = create_type_object();
	if (PyType_Ready(type_object) < 0) {
		PyErr_Print();
		fprintf(stderr, "Type not ready - PyType_Ready\n");
		free_type_object(type_object);
		return 1;
	}

	PyObject* m = Py_InitModule3("name", NULL, "XXX");
	if (!m) {
		PyErr_Print();
		fprintf(stderr, "Failed to load initilize module - Py_InitModule3\n");
		free_type_object(type_object);
		return 1;
	}

	/*Py_INCREF(&NameObjectType);
	PyModule_AddObject(m, "Name", (PyObject *)&NameObjectType);*/

	Py_INCREF(type_object);
	PyModule_AddObject(m, "Name", (PyObject*)type_object);
	
	
    // Load module python2.py
    pName = PyString_FromString(module.c_str()); 					// Error checking of pName left out
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pModule) {
		PyErr_Print();
		fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
		free_type_object(type_object);
		return 1;
	}

	// Get the python fuction to call
    pFunc = PyObject_GetAttrString(pModule, function.c_str()); 		// pFunc is a new reference 
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
	    Py_XDECREF(pFunc);
	    Py_DECREF(pModule);
		free_type_object(type_object);
        return 1;
    }

    // Make the call to the python function
    pValue = PyObject_CallObject(pFunc, NULL);
    if (!pValue) {
        PyErr_Print();
        fprintf(stderr,"Call failed\n");
        Py_DECREF(pFunc);
        Py_DECREF(pModule);
		free_type_object(type_object);
        return 1;
    }

	// Show the result of the python call
    printf("Result of call: %ld\n", PyInt_AsLong(pValue)); 
    
    // Decrease counters for python garbage collection
    Py_DECREF(pValue);
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
    
	std::cout << "Before Py_Finalize()" << std::endl;
	
    // Clean up python
    Py_Finalize();
    
	free_type_object(type_object);
    
	std::cout << "Freeing the allocated memory" << std::endl;
		
    return 0;
}