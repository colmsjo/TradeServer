#include <Python.h>
#include <structmember.h>

#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/types.h>

#include "../tools/nvwa-0.8.2/nvwa/debug_new.h"

class NameObject :  PyObject {
	public:
		PyObject *firstname;
		PyObject *lastname;

		PyObject* 			PyName(NameObject *self, PyObject *unused);
		
		
		// PyMethodDef methods[];
		// PyMemberDef members[];
		// char Name_doc[];

		/*static PyObject *name(NameObject *self, PyObject *unused);
		static int init(NameObject *self, PyObject *args, PyObject *kwds);
		static PyMethodDef methods[];
		static PyMemberDef members[];
		static char doc[];*/
};

		static PyObject* 	sPyName(NameObject *self, PyObject *unused);
		static int 			sPyInit(NameObject *self, PyObject *args, PyObject *kwds);
