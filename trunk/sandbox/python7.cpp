/*
 * Python6.cpp
 *
 * export PYTHONPATH=./
 * ./python7
 *
 */

#include "python7.hpp"

/*
 *  offsetof can only be used for POD types (only public methods, no virtuals, no constructor or desctructor etc.)
 * http://www.cplusplus.com/reference/clibrary/cstddef/offsetof/
 *
 */
PyMemberDef NameObject::members[] = {
	{(char*)"firstname", T_OBJECT_EX, offsetof(NameObject, firstname), 0, (char*)"first name"},
	{(char*)"lastname", T_OBJECT_EX, offsetof(NameObject, lastname), 0, (char*)"last name"},
	{NULL}  /* Sentinel */
};

PyMethodDef NameObject::methods[] = {
	{"name", (PyCFunction)NameObject::sPyName, METH_NOARGS, "Return the name, combining the firstname and lastname." },
	{NULL}
};

char NameObject::doc[] = "boo.";

PyTypeObject NameObjectType = {
	//PyObject_HEAD_INIT(NULL)			// Expands to 1, NULL,
	1, 									/* ob_refcnt */
	NULL,								/* ob_type */
	0,									/* ob_size           */
	"name.Name",			/* tp_name           */
	sizeof(NameObject),		/* tp_basicsize      */
	0,				/* tp_itemsize       */
	0,				/* tp_dealloc        */
	0,				/* tp_print          */
	0,				/* tp_getattr        */
	0,				/* tp_setattr        */
	0,				/* tp_compare        */
	0,				/* tp_repr           */
	0,				/* tp_as_number      */
	0,				/* tp_as_sequence    */
	0,				/* tp_as_mapping     */
	0,				/* tp_hash           */
	0,				/* tp_call           */
	0,				/* tp_str            */
	0,				/* tp_getattro       */
	0,				/* tp_setattro       */
	0,				/* tp_as_buffer      */
	Py_TPFLAGS_DEFAULT,		/* tp_flags          */
	NameObject::doc,			/* tp_doc            */
	0,				/* tp_traverse       */
	0,				/* tp_clear          */
	0,				/* tp_richcompare    */
	0,				/* tp_weaklistoffset */
	0,				/* tp_iter           */
	0,				/* tp_iternext       */
	NameObject::methods,	     		/* tp_methods        */
	NameObject::members,			/* tp_members        */
	0,				/* tp_getset         */
	0,				/* tp_base           */
	0,				/* tp_dict           */
	0,				/* tp_descr_get      */
	0,				/* tp_descr_set      */
	0,				/* tp_dictoffset     */
	(initproc)NameObject::init,		/* tp_init           */
};



int NameObject::init(NameObject *self, PyObject *args, PyObject *kwds) {
	PyObject *firstname = Py_None;
	PyObject *lastname = Py_None;
	PyObject *tmp = NULL;

	// static char *kwlist[] = {"firstname", "lastname", NULL};
	char *kwlist[] = {(char*)"firstname", (char*)"lastname", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|OO", kwlist,
					 &firstname, &lastname))
		return -1;

	tmp = self->firstname;
	if (firstname == Py_None) {
		self->firstname = PyString_FromString("");
	}
	else {
		Py_INCREF(firstname);
		self->firstname = firstname;
	}
	Py_XDECREF(tmp);

	tmp = self->lastname;
	if (lastname == Py_None) {
		self->lastname = PyString_FromString("");
	}
	else {
		Py_INCREF(lastname);
		self->lastname = lastname;
	}
	Py_XDECREF(tmp);

	return 0;
}

PyObject* NameObject::sPyName(NameObject *self, PyObject *unused)
{
	return self->PyName(self, unused);
}

PyObject* NameObject::PyName(NameObject *self, PyObject *unused)
{
	PyObject *args = NULL;
	PyObject *format = NULL;
	PyObject *result = NULL;

	format = PyString_FromString("%s %s");
	if (format == NULL)
		goto fail;

	if (self->firstname == NULL) {
		PyErr_SetString(PyExc_AttributeError, "firstname");
		goto fail;
	}

	if (self->lastname == NULL) {
		PyErr_SetString(PyExc_AttributeError, "lastname");
		goto fail;
	}

	args = Py_BuildValue("OO", self->firstname, self->lastname);
	if (args == NULL)
		goto fail;

	result = PyString_Format(format, args);

  fail:
	Py_XDECREF(format);
	Py_XDECREF(args);

	return result;
}


int main(int argc, char *argv[]) {
	std::cout << "C++: This is process: " <<  getpid() << std::endl;

	
    PyObject *pName, *pModule, *pFunc; //, *pDict, ;
    PyObject *pValue; // *pArgs,
    // int i;

	std::string module("python7");
	std::string function("run_now");

	// Initilize Python
    Py_Initialize();
    
	NameObjectType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&NameObjectType) < 0) {
		PyErr_Print();
		fprintf(stderr, "Type not ready - PyType_Ready\n");
		return 1;
	} 

	PyObject* m = Py_InitModule3("name", NULL, "XXX");
	if (!m) {
		PyErr_Print();
		fprintf(stderr, "Failed to load initilize module - Py_InitModule3\n");
		return 1;
	}

	Py_INCREF(&NameObjectType);
	PyModule_AddObject(m, "Name", (PyObject *)&NameObjectType);
	
	
    // Load module python2.py
    pName = PyString_FromString(module.c_str()); 					// Error checking of pName left out
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pModule) {
		PyErr_Print();
		fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
		return 1;
	}

	// Get the python fuction to call
    pFunc = PyObject_GetAttrString(pModule, function.c_str()); 		// pFunc is a new reference 
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
	    Py_XDECREF(pFunc);
	    Py_DECREF(pModule);
        return 1;
    }

    // Make the call to the python function
    pValue = PyObject_CallObject(pFunc, NULL);
    if (!pValue) {
        PyErr_Print();
        fprintf(stderr,"Call failed\n");
        Py_DECREF(pFunc);
        Py_DECREF(pModule);
        return 1;
    }

	// Show the result of the python call
    printf("Result of call: %ld\n", PyInt_AsLong(pValue)); 
    
    // Decrease counters for python garbage collection
    Py_DECREF(pValue);
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
    
    // Clean up python
    Py_Finalize();
  
  
    
    return 0;
}