/* 
 * Pseudo code from the extenind R guide
 * * #include <Rembedded.h>
 *
 * int main(int ac, char **av) { *	// do some setup 
 *	Rf_initEmbeddedR(argc, argv); 
 *
 *	// do some more setup  *	// submit some code to R, which is done interactively via run_Rmainloop(); *		A possible substitute for a pseudo-console is *		R_ReplDLLinit();
 *		
 *		 while(R_ReplDLLdo1() > 0) { 
 *		 	// add user actions here if desired  *		} *	// end psuedo cade
 *	
 *	Rf_endEmbeddedR(0); 
 *	// final tidying up after R is shutdown 
 *	return 0; * } 
 */

/*
 * From R-source/tests/Embedding/tryEval.c
 *
 * export R_HOME=/Library/Frameworks/R.framework/Resources
 * ./r1
 *
 */

#include <string>
#include <exception>
#include <iostream>

#include <R.h>
#include <Rinternals.h>
#include <Rembedded.h>

int eval_R_command(const char *funcName, int argc, char *argv[]);
void init_R(int argc, char **argv);
void end_R();


int
eval_R_command(const char *funcName, int argc, char *argv[])
{
    SEXP e;
    SEXP arg;

    int i;
    int errorOccurred;
    init_R(argc, argv);

    PROTECT(arg = allocVector(INTSXP, 10));
    for(i = 0; i < LENGTH(arg); i++) INTEGER(arg)[i]  = i + 1;

    PROTECT(e = lang2(install(funcName), arg));

    /* Evaluate the call to the R function.
       Ignore the return value.
    */
    R_tryEval(e, R_GlobalEnv, &errorOccurred);

    Rf_endEmbeddedR(0);
    UNPROTECT(2);   
    return(0);
}

void
init_R(int argc, char **argv)
{
    int defaultArgc = 1;
    char *defaultArgv[] = {"Rtest"};

    if(argc == 0 || argv == NULL) {
	argc = defaultArgc;
	argv = defaultArgv;
    }
    Rf_initEmbeddedR(argc, argv);
}

void
end_R()
{
    Rf_endEmbeddedR(0);
}


// Works ok
int main_1(int argc, char *argv[]) {
    eval_R_command("print", argc, argv);
    return(0);
}


// generates memory leak
int main_2(int argc, char *argv[]) {
    /* Evaluates the expression 
       plot(c(1,2,3,4,5,6,7,8,9,10))
    */
    eval_R_command("plot", argc, argv); 
    return(0);
}

/******************************************************/

// Works, but needs foo.r

void bar1() ;
void source(const char *name);
/*
  Creates and evaluates a call 
  to a function giving named arguments
   plot(1:10, pch="+")
 */
int main_3(int argc, char *argv[]) {
    char *localArgs[] = {"R", "--silent"};
    init_R(sizeof(localArgs)/sizeof(localArgs[0]), localArgs);
    source("foo.R");
    bar1();

    end_R();
    return(0);
}


/*
 This arranges for the command source("foo.R")
 to be called and this defines the function we will
 call in bar1.
 */
void
source(const char *name)
{
    SEXP e;

    PROTECT(e = lang2(install("source"), mkString(name)));
    R_tryEval(e, R_GlobalEnv, NULL);
    UNPROTECT(1);
}

/* 
  Call the function foo() with 3 arguments, 2 of which
  are named.
   foo(pch="+", id = 123, c(T,F))

  Note that PrintValue() of the expression seg-faults.
  We have to set the print name correctly.
*/

void
bar1() 
{
    SEXP fun, pch;
    SEXP e;

    PROTECT(e = allocVector(LANGSXP, 4));
    fun = findFun(install("foo"), R_GlobalEnv);
    if(fun == R_NilValue) {
	fprintf(stderr, "No definition for function foo. Source foo.R and save the session.\n");
	UNPROTECT(1);
	exit(1);
    }
    SETCAR(e, fun);

    SETCADR(e, mkString("+"));
    SET_TAG(CDR(e), install("pch"));

    SETCADDR(e, ScalarInteger(123));   
    SET_TAG(CDR(CDR(e)), install("id"));

    pch = allocVector(LGLSXP, 2);
    LOGICAL(pch)[0] = TRUE;
    LOGICAL(pch)[1] = FALSE;
    SETCADDDR(e, pch);   

    PrintValue(e);
    eval(e, R_GlobalEnv);

    SETCAR(e, install("foo"));
    PrintValue(e);
    R_tryEval(e, R_GlobalEnv, NULL);

    UNPROTECT(1);
}


/******************************************************/

// plot seams to result in memory leaks

#include <R_ext/Parse.h>

int main_4(int argc, char *argv[]) {
    SEXP e, tmp;
    int hadError;
    ParseStatus status;

    init_R(argc, argv);

    PROTECT(tmp = mkString("{plot(1:10, pch=\"+\"); print(1:10)}"));
    PROTECT(e = R_ParseVector(tmp, 1, &status, R_NilValue));
    PrintValue(e);
    R_tryEval(VECTOR_ELT(e,0), R_GlobalEnv, &hadError);
    UNPROTECT(2);

    end_R();
    return(0);
}


/******************************************************/

#include <R_ext/Rdynload.h>

// .Call / .External way of calling
SEXP myCall(SEXP a, SEXP b, SEXP c);R_CallMethodDef callMethods[] = { 
	{"myCall", (DL_FUNC) &myCall, 3}, 
	{NULL, NULL, 0}};

// .C way of calling

void myC(double *x, int *n, char **names, int *status);// Had problems, solution - http://biostat.mc.vanderbilt.edu/wiki/Main/WritingRExtensions
/*R_CMethodDef cMethods[] = { 
	{"myC", (DL_FUNC) &myC, 4, {REALSXP, INTSXP, STRSXP, LGLSXP}}, 
	{NULL, NULL, 0}};*/

R_NativePrimitiveArgType hello_youArgs[4] = {REALSXP, INTSXP, STRSXP, LGLSXP};
R_CMethodDef cMethods[] = { 
	{"myC", (DL_FUNC) &myC, 4, hello_youArgs}, 
	{NULL, NULL, 0}};


int main_5(int argc, char *argv[]) {

	R_registerRoutines(NULL, cMethods, callMethods, NULL, NULL);			// C-structure, Call-structure, Fortrat-structure, External-structure	
}

// Doing nothing
SEXP myCall(SEXP a, SEXP b, SEXP c) {
	return NULL;
}

void myC(double *x, int *n, char **names, int *status) {
	std::cout << *x << std::endl;	
}
	
// Used by R when loading DLL (.so)
void R_init_myLib(DllInfo *info) {	R_registerRoutines(info, cMethods, callMethods, NULL, NULL);			// C-structure, Call-structure, Fortrat-structure, External-structure}

/******************************************************/
int main(int argc, char *argv[]) {
    main_4(argc, argv);					

    return(0);
}



