/** 
* A first test application.
*
* This application reads http data using the curls library. It then uses a simple xml parser to parse the contens
* and save the result in CSV-files.
*
* @param void no parameters are used
* @see Test()
* @see testMeToo()
* @see publicVar()
* @return 0 if succesfull, otherwise a error code (yet to be defined)
*
*
* Change log:
* 		101114 Jonas C. - initial version.
*
*/


#include <stdio.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <stdlib.h>
 

/**
*
* Copied from http://www.daniweb.com/forums/thread269500.html
*
*
*/

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  size_t written;
  written = fwrite(ptr, size, nmemb, stream);
  return written;
}
 
void read_data(void) {
  
  CURL *curl;
  CURLcode res;
  FILE *fp;
  long lSize;
  char *buffer;
  
  printf("Start read_data\n");

  // only call once, curl = curl_easy_init();
  if(curl) {
    fp = fopen("output.html","wb");
    curl_easy_setopt(curl, CURLOPT_URL, "http://di.se");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); 
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    fclose(fp);
 }

  fp = fopen("output.html","rb");
  fseek (fp , 0 , SEEK_END);
  lSize = ftell (fp);
  
  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}
  
  rewind (fp);
  fread (buffer,1,lSize,fp);
  buffer[lSize] = 0;
  fclose(fp);

  printf("End read_data\n");

}

int main(void)
{
  CURL *curl;
  CURLcode res;

  printf("Start of main()\n");
  
  
  curl = curl_easy_init();
  //curl = curl_global_init(CURL_GLOBAL_ALL);
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://di.se");
    res = curl_easy_perform(curl);
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }

  printf("Calling read_data\n");
  read_data();

  return 0;

}


