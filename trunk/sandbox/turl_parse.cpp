
#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


// For strtok
#include <string.h>
#include <stddef.h>


// For url_parse
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#include <errno.h>
#include <assert.h>


/* Parse a URL.

   Return a new struct url if successful, NULL on error.  In case of
   error, and if ERROR is not NULL, also set *ERROR to the appropriate
   error code. */
  
/*
 
// Structure containing info on a URL.  
struct url
{
  char *url;			// Original URL 
  enum url_scheme scheme;	// URL scheme

  char *host;			// Extracted hostname
  int port;			// Port number

  // URL components (URL-quoted).
  char *path;
  char *params;
  char *query;
  char *fragment;

  // Extracted path info (unquoted).
  char *dir;
  char *file;

  // Username and password (unquoted).
  char *user;
  char *passwd;
};
   
struct url *
url_parse (const char *url, int *error)
{
  struct url *u;
  const char *p;
  int path_modified, host_modified;

  enum url_scheme scheme;

  const char *uname_b,     *uname_e;
  const char *host_b,      *host_e;
  const char *path_b,      *path_e;
  const char *params_b,    *params_e;
  const char *query_b,     *query_e;
  const char *fragment_b,  *fragment_e;

  int port;
  char *user = NULL, *passwd = NULL;

  char *url_encoded = NULL;

  int error_code;

  scheme = url_scheme (url);
  if (scheme == SCHEME_INVALID)
    {
      error_code = PE_UNSUPPORTED_SCHEME;
      goto error;
    }

  url_encoded = reencode_escapes (url);
  p = url_encoded;

  p += strlen (supported_schemes[scheme].leading_string);
  uname_b = p;
  p += url_skip_credentials (p);
  uname_e = p;

  // scheme://user:pass@host[:port]..
  //                    ^              

  // We attempt to break down the URL into the components path,
  //   params, query, and fragment.  They are ordered like this:

  //     scheme://host[:port][/path][;params][?query][#fragment]  

  params_b   = params_e   = NULL;
  query_b    = query_e    = NULL;
  fragment_b = fragment_e = NULL;

  host_b = p;

  if (*p == '[')
    {
      // Handle IPv6 address inside square brackets.  Ideally we'd
	  //just look for the terminating ']', but rfc2732 mandates
	  //rejecting invalid IPv6 addresses.  

      // The address begins after '['. 
      host_b = p + 1;
      host_e = strchr (host_b, ']');

      if (!host_e)
	{
	  error_code = PE_UNTERMINATED_IPV6_ADDRESS;
	  goto error;
	}

#ifdef ENABLE_IPV6
      // Check if the IPv6 address is valid.
      if (!is_valid_ipv6_address(host_b, host_e))
	{
	  error_code = PE_INVALID_IPV6_ADDRESS;
	  goto error;
	}

      // Continue parsing after the closing ']'. 
      p = host_e + 1;
#else
      error_code = PE_IPV6_NOT_SUPPORTED;
      goto error;
#endif
    }
  else
    {
      p = strpbrk_or_eos (p, ":/;?#");
      host_e = p;
    }

  if (host_b == host_e)
    {
      error_code = PE_EMPTY_HOST;
      goto error;
    }

  port = scheme_default_port (scheme);
  if (*p == ':')
    {
      const char *port_b, *port_e, *pp;

      // scheme://host:port/tralala
      //              ^             
      ++p;
      port_b = p;
      p = strpbrk_or_eos (p, "/;?#");
      port_e = p;

      if (port_b == port_e)
	{
	  // http://host:/whatever 
	  //             ^         
          error_code = PE_BAD_PORT_NUMBER;
	  goto error;
	}

      for (port = 0, pp = port_b; pp < port_e; pp++)
	{
	  if (!ISDIGIT (*pp))
	    {
	      // http://host:12randomgarbage/blah 
	      //               ^                  
              error_code = PE_BAD_PORT_NUMBER;
              goto error;
	    }
	  
	  port = 10 * port + (*pp - '0');
	}
    }

  if (*p == '/')
    {
      ++p;
      path_b = p;
      p = strpbrk_or_eos (p, ";?#");
      path_e = p;
    }
  else
    {
      // Path is not allowed not to exist. 
      path_b = path_e = p;
    }

  if (*p == ';')
    {
      ++p;
      params_b = p;
      p = strpbrk_or_eos (p, "?#");
      params_e = p;
    }
  if (*p == '?')
    {
      ++p;
      query_b = p;
      p = strpbrk_or_eos (p, "#");
      query_e = p;

      // Hack that allows users to use '?' (a wildcard character) in
	 //FTP URLs without it being interpreted as a query string
	 //delimiter.  
      if (scheme == SCHEME_FTP)
	{
	  query_b = query_e = NULL;
	  path_e = p;
	}
    }
  if (*p == '#')
    {
      ++p;
      fragment_b = p;
      p += strlen (p);
      fragment_e = p;
    }
  assert (*p == 0);

  if (uname_b != uname_e)
    {
      // http://user:pass@host
      //        ^         ^  
      //     uname_b   uname_e 
      if (!parse_credentials (uname_b, uname_e - 1, &user, &passwd))
	{
	  error_code = PE_INVALID_USER_NAME;
	  goto error;
	}
    }

  u = (struct url *)xmalloc (sizeof (struct url));
  memset (u, 0, sizeof (*u));

  u->scheme = scheme;
  u->host   = strdupdelim (host_b, host_e);
  u->port   = port;
  u->user   = user;
  u->passwd = passwd;

  u->path = strdupdelim (path_b, path_e);
  path_modified = path_simplify (u->path);
  split_path (u->path, &u->dir, &u->file);

  host_modified = lowercase_str (u->host);

  if (params_b)
    u->params = strdupdelim (params_b, params_e);
  if (query_b)
    u->query = strdupdelim (query_b, query_e);
  if (fragment_b)
    u->fragment = strdupdelim (fragment_b, fragment_e);

  if (path_modified || u->fragment || host_modified || path_b == path_e)
    {
      // If we suspect that a transformation has rendered what
	 //url_string might return different from URL_ENCODED, rebuild
	 //u->url using url_string.  
     // u->url = url_string (u, 0);

      if (url_encoded != url)
	xfree ((char *) url_encoded);
    }
  else
    {
      if (url_encoded == url)
	u->url = xstrdup (url);
      else
	u->url = url_encoded;
    }
  url_encoded = NULL;

  return u;

 error:
  // Cleanup in case of error: 
  if (url_encoded && url_encoded != url)
    xfree (url_encoded);

  // Transmit the error code to the caller, if the caller wants to
  //   know.  
  if (error)
    *error = error_code;
  return NULL;
}

*/

int main(void) {
	
	/*
	struct *url;
	url = url_parse("http://localhost:8080/help/me?parm1=val1;parm2=val2");
	std::cout << "host:" << url->host << std::endl;
	std::cout << "port:" << url->port << std::endl;
	std::cout << "path:" << url->path << std::endl;
	*/
	
	
     //const char string[] = "words separated by spaces -- and, punctuation!";
     const char string[] = "http://localhost:8080/abcde-034k-ldskjf0-lkjdf/start?parm1=val;parm2=val2";
     const char delimiters[] = "/?;&";
     char *token, cp[100];
 
 	 
          
     //cp = strdupa (string);                /* Make writable copy.  */

     strcpy(cp, string);
     
     token = strtok (cp, delimiters);      /* token => "words" */
     std::cout << "token 1:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => "separated" */
     std::cout << "token 2:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => "by" */
     std::cout << "token 3:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => "spaces" */
     std::cout << "token 4:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => "and" */
     std::cout << "token 5:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => "punctuation" */
     std::cout << "token 6:" << token << std::endl;
     
     token = strtok (NULL, delimiters);    /* token => NULL */
     std::cout << "token 7:" << token << std::endl;

	
}