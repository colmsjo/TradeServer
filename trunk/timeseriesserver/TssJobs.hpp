/****************************************************************************/
/**! \mainpage TssJobs Classes used for defining jobs to run in the TSS server
 * \section intro_sec Introduction
 *
 * TssJobs.hpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101219 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef TSS_JOBS
#define TSS_JOBS








#endif //TSS_JOBS

