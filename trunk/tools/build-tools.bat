rem
rem Jonas C. 101212
rem
rem Script for building CppTest and Pantheios
rem

call env.bat

7z x -y cpptest-1.1.1.tar.gz.gz
7z x -y cpptest-1.1.1.tar.gz
7z x -y xmlParser.zip
7z x -y pantheios-1.0.1-beta201.zip
7z x -y nvwa-0.8.2.zip
7z x -y stlsoft-1.9.108-hdrs.zip

7z x -y boost_1_45_0.tar.gz 
7z x -y boost_1_45_0.tar 

rem Did not get this to compile
rem unzip boost-log-1.0.zip
rem cp -r boost-log-1.0/* boost_1_45_0

rem Newver version, still not part of the core boost library
rem unzip boost-log2.zip
rem cp -r logging/* boost_1_45_0


cd boost_1_45_0
call bootstrap --prefix=c:\local\boost --with-libraries=date_time,filesystem,math,iostreams,serialization,mpi,thread,test,regex,signals >> build-tools.log

bjam variant=release link=static threading=multi runtime-link=static stage >> build-tools.log

rem #./bjam --without-python variant=release link=static threading=multi runtime-link=static stage >> build-tools.log
cd ..

rem cd cpptest-1.1.1 
rem ./configure | tee -a build-tools.log
rem make | tee -a build-tools.log
rem cd ..

rem Does not build properly, get error from cl.exe
rem cd pantheios-1.0.1-beta201
rem cd build/vc9/
rem nmake build.libs.core >> build-tools.log
rem nmake build.libs.be >> build-tools.log
rem nmake build.libs.bec >> build-tools.log
rem nmake build.libs.fe >> build-tools.log
rem nmake build.libs.util >> build-tools.log
rem nmake build >> build-tools.log
rem nmake test >> build-tools.log
rem cd ../../../

rem svn checkout https://gnunet.org/svn/libmicrohttpd/
7z x -y libmicrohttpd-0.4.5-w32.zip
rem cd libmicrohttpd
rem autoreconf -fi
rem ./configure
rem make
rem sudo make install

