#
# Jonas C. 101212
#
# Script for building CppTest, Pantheios, Boost, libmicrohttd etc.
#
unzip xmlParser.zip
unzip pantheios-1.0.1-beta211.zip
unzip nvwa-0.8.2.zip
unzip stlsoft-1.9.108-hdrs.zip

gunzip cpptest-1.1.1.tar.gz.gz
tar -xf cpptest-1.1.1.tar.gz
cd cpptest-1.1.1 
./configure | tee -a build-tools.log
make | tee -a build-tools.log
cd ..



gunzip boost_1_45_0.tar.gz 
tar -xvf boost_1_45_0.tar 
cd boost_1_45_0
# sudo ./bootstrap.sh --prefix=/usr/local --with-libraries=date_time,filesystem,math,iostreams,serialization,mpi,thread,test,regex,signals,python | tee -a build-tools.log
# sudo ./bjam variant=release link=static threading=multi runtime-link=static install 2>&1 | tee -a build-tools.log
cd ..

#
# Not used anymore
#
#cd pantheios-1.0.1-beta201
#cd build/gcc42.unix/
#cd build/gcc40.mac
#make build.libs.core | tee -a build-tools.log
#make build.libs.be | tee -a build-tools.log
#make build.libs.bec | tee -a build-tools.log
#make build.libs.fe | tee -a build-tools.log
#make build.libs.util | tee -a build-tools.log
#make build | tee -a build-tools.log
#make test | tee -a build-tools.log
#cd ../../../

svn checkout https://gnunet.org/svn/libmicrohttpd/
cd libmicrohttpd
autoreconf -fi
./configure
make
# sudo make install

echo "Need to install cpptest, boost and libmicrohttpd manually (using sudo make install or sudo bjam …"
