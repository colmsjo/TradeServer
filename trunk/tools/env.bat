@echo off

rem
rem Set root directory for source code
rem

set SRC_ROOT=c:\svn\TradeServer\trunk

rem
rem Path for Boost
rem
set BOOST_ROOT=$SRC_ROOT\tools\boost_1_45_0

rem
rem Setup pantheios
rem

rem set FASTFORMAT_ROOT=$SRC_ROOT/tools/pantheios-1.0.1-beta201
set FASTFORMAT_ROOT=$SRC_ROOT\tools\pantheios-1.0.1-beta211

rem set STLSOFT=$SRC_ROOT/tools/stlsoft-1.9.103
set STLSOFT=$SRC_ROOT\tools\stlsoft-1.9.108

set GLIBCXX_FORCE_NEW=1

rem
rem Setup VC++
rem

call "C:\local\Microsoft Visual Studio 9.0\VC\vcvarsall.bat" x86 >nul



