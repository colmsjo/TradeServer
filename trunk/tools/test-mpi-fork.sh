#!/bin/sh 

have_fork_support=`ompi_info --param btl openib --parsable | grep have_fork_support:value | cut -d: -f7`
if test "$have_fork_support" = "1"; then
    echo "Happiness / world peace / birds are singing"
else
    echo "Despair / time for Haagen Daas"
fi
