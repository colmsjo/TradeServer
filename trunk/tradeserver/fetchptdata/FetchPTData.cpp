/****************************************************************************/
/**
 *
 * FetchPTData.cpp 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Classes that fetch real time ticker data from a PrimeTrader installation.
 * The data is stored in html tables. The URL:s to fetch are stores in a file
 *
 * Pseudo code
 *
 * main: 
 *   open and read file with (URL, file_name) pairs
 *   for each URL, file_name
 *     array = fetch_url(URL)
 *     write_to_file(array, file_name)
 *
 * fetch_url(URL):
 *   read URL into string buffer using curl
 *   parse string buffer and save contents in array
 *   return array
 *
 * write_to_file(array, file_name):
 *   open file_name for writing
 *   for each item in array
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101118 Jonas C. Initial version
 *
 ****************************************************************************/


#include "FetchPTData.h"

//Specify process identity
//const PAN_CHAR_T PANTHEIOS_FE_PROCESS_IDENTITY[] = "fetchptdata.exe";

FetchURL::FetchURL() {
	pStrContents 	= NULL;
	
	// Curl related initialization
	curl 		= curl_easy_init();		// (B) Initilise web query
 	res 		= CURLE_OK;			// Error code 
	szErr[0] 	= '\0';				// String explaining error
	
	// XML Parser related initialization
	this->pXmlResults = new  XMLResults();
	//astrCells 	= NULL;
	pTableRows 	= NULL;
	iRows 		= 0;
}

FetchURL::~FetchURL() {
	if(pStrContents) delete pStrContents;
	
	// cleanup array of parsed contents, this->pTableRows[i].iCols
	for(int i=0; i<this->iRows; i++) {
		for(int j=0; j<this->pTableRows[i]->iCols; j++) { 
			delete this->pTableRows[i]->pstrColValue[j];
		}
		free(this->pTableRows[i]->pstrColValue);
		free(this->pTableRows[i]);
	}	
	free(this->pTableRows);

	// XML parser cleanup
	if(pXmlResults) delete pXmlResults;
	
	// curl cleanup
	curl_easy_cleanup(curl);			// (E) Close the connection
}


/**
 * Functions related to readURL
 *
 *
 *
 *
 *
 *
 */

int writer(char *data, size_t size, size_t nmemb, string *buffer){
	int result = 0;
	if(buffer != NULL) {
		buffer -> append(data, size * nmemb);
		result = size * nmemb;
	}
	return result;
} 
 
  
int FetchURL::fetch_url_main(string strURLToFetch) {
	
	if(pStrContents) delete pStrContents;
	pStrContents = new string();
	
	if ( strURLToFetch.length()<=0 || strURLToFetch.empty() ){
		string s = string("FetchURL::fetch_url_main - Invalid argument:") + strURLToFetch;		
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, s);
		throw  TSException(s);											
	}
	
	/* (C) Set Options of the web query 
	 * See also:  http://curl.haxx.se/libcurl/c/curl_easy_setopt.html  */
	if (curl){
		curl_easy_setopt(curl, CURLOPT_URL, strURLToFetch.c_str());
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);	 		/* No we don't need the Header of the web content. Set to 0 and curl ignores the first line */
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0); 		/* Don't follow anything else than the particular url requested*/
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);		/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, pStrContents ); 	/* Data Pointer &buffer stores downloaded web content */
		
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, szErr );
	}
	else{
		this->logger->log_EMERGENCY(TSERR_CURL_ERROR, "FetchURL::fetch_url_main - curl_easy_init failed");
		return TS_NOT_OK;											
	}
	
	/* (D) Fetch the data */
	res = curl_easy_perform(curl);
	if (res != CURLE_OK ) {
		string s = string("FetchURL::fetch_url_main - curl_easy_perform failed with error:") + szErr;		
		this->logger->log_EMERGENCY(TSERR_CURL_ERROR, s);
		throw  TSException(s);											
	}
 		
	return TS_OK;
}


// Saving for test purposes
void print(string buffer) {
	/* (F) Transform &buffer into a istringstream object */
	std::istringstream iss(buffer);
 
	string line, item;	
	int linenum = 0;
	while (getline (iss, line)){
		linenum++;					/* Move to Next Line */
		cout << "\nLine #" << linenum << ":" << endl;	/* Terminal Printout */
		std::istringstream linestream(line);		/* Read Next Line */
 
		int itemnum = 0;
		while (getline (linestream, item, ',')){
			itemnum++;
			cout << "Item #" << itemnum << ": " << item << endl;	/* Terminal Printout */
		} // End WHILE (items)
	} //End WHILE (lines)	
}

int FetchURL::readURL(string strURL) {	 	 
 	return this->fetch_url_main(strURL);
}


/**
 * saveAsCSV
 *
 *
 *
 *
 *
 *
 */

int FetchURL::saveAsCSV(string strFileName) {

	if ( strFileName.length()<=0 || strFileName.empty() ){
		string s = string("FetchURL::saveAsCSV - Invalid argument:") + strFileName;		
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, s);
		throw  TSException(s);											
	}

	//open file for wring, erase if already existing
	FILE *pFile = fopen(strFileName.c_str(), "w");

	//Make sure the file was opened
	if ( pFile==NULL ){
		string s = string("FetchURL::saveAsCSV - could not open file for writing:") + strFileName;		
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, s);
		throw  TSException(s);											
	}

	
	for(int i=0; i<this->iRows; i++) {
		for(int j=0; j<this->pTableRows[i]->iCols; j++) { 
			fprintf(pFile,"%s", this->pTableRows[i]->pstrColValue[j]->c_str() );
			
			// print ; after all cols but hte last
			if(j != this->pTableRows[i]->iCols - 1) fprintf(pFile,";" );
		}
		// separate lines with newline
		fprintf(pFile,"\n" );
	}	
	
	int ierror =fclose(pFile);
	if( ierror ) {
		std::ostringstream oss;
		oss << "FetchURL::saveAsCSV - fclose with error:" << ierror ;
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, oss.str());
		throw  TSException(oss.str());											
	}
	
 	return TS_OK; 	 
} 


/**
 * parseContents
 *
 *
 *
 *
 *
 *
 */

int FetchURL::parseContents() { 
	return this->parseContents("body", "html/table", "tr", "td");
}
 
 
int FetchURL::parseContents(string strMainNode, string strTableNode,  string strRowNode, string strCellNode) { 
	// parse the XML string
	XMLNode xMainNode 	= XMLNode::parseString((XMLCSTR) this->pStrContents->c_str(), (XMLCSTR) strMainNode.c_str(), this->pXmlResults);

	if (pXmlResults->error != eXMLErrorNone  ) {
		string s = string("FetchURL::parseContents > XMLNode::parseString failed with error") + XMLNode::getError(pXmlResults->error);		
		this->logger->log_EMERGENCY(TSERR_CURL_ERROR, s);
		throw  TSException(s);											
	}
	
	// Get the table
	XMLNode xNodeTable	 = xMainNode.getChildNodeByPath(strTableNode.c_str());

	// Check the number of rows and allocate a pointer for each row
	this->iRows 		= xNodeTable.nChildNode(strRowNode.c_str());
	//this->astrCells 	= (string***) malloc(sizeof(string*) * this->iRows);
	this->pTableRows  	= (struct_table_row**) malloc( sizeof(struct_table_row**) * this->iRows);	
	
	for (int i=0; i<this->iRows; i++) {

		XMLNode xNodeRow = xNodeTable.getChildNode(strRowNode.c_str(), i);

		// Get the number of columns and allocate a string pointer for each
		//this->iCols 				= xNodeRow.nChildNode(strCellNode.c_str());
		//this->astrCells[i] 			= (string**) malloc(sizeof(string*) * this->iCols);
		
		this->pTableRows[i] 			= (struct_table_row*) malloc( sizeof(struct_table_row) );
		this->pTableRows[i]->iCols 		= xNodeRow.nChildNode(strCellNode.c_str());
		this->pTableRows[i]->pstrColValue 	= (string**) malloc( sizeof(string**) * this->pTableRows[i]->iCols);
	
		// Save the contents of each cell
		for (int j=0; j<this->pTableRows[i]->iCols; j++) {
			XMLCSTR cstr 		= xNodeRow.getChildNode(strCellNode.c_str(),j).getText();			
			//this->astrCells[i][j] 	=  new string(cstr!=NULL ? cstr : "");
			this->pTableRows[i]->pstrColValue[j] = new string(cstr!=NULL ? cstr : "");
		}
	}
	
	return TS_OK;
} 


int FetchURL::testPTableRows() { 
	string*** appString;

	appString = (string***) malloc( sizeof(string*) * 3);
	for(int i=0; i<3; i++) {
		//appString[i] = (string**) malloc((size_t) 5);
		appString[i] = (string**) malloc( sizeof(string*) * 5);
		for(int j=0; j<5; j++) {
			appString[i][j] = new string("test string");
		}
	}
	
	for(int i=0; i<3; i++) {
		for(int j=0; j<5; j++) delete appString[i][j];
		free(appString[i]);
	}

	free(appString);
	
	this->iRows = 5;
	
	// use the memeber variable astrCells
	this->pTableRows  = (struct_table_row**) malloc( sizeof(struct_table_row**) * this->iRows);	
	for (int i=0; i<this->iRows; i++) {
		this->pTableRows[i] = (struct_table_row*) malloc( sizeof(struct_table_row) );
		this->pTableRows[i]->iCols = 5;
		this->pTableRows[i]->pstrColValue = (string**) malloc( sizeof(string**) * this->pTableRows[i]->iCols);

		
		for (int j=0; j<this->pTableRows[i]->iCols; j++) {
			this->pTableRows[i]->pstrColValue[j] = new string("test string");
		}
	}
		
	return TS_OK;
} 
			
			

 /// getCell
 /**
 * Get the contents once it has been parsed.
 *
 * @param s a string containing the contents
 *
 * \return Return a string with the HTML contents
 *
 */
string FetchURL::getCell(int row, int col) { 
 	if( this->pTableRows == NULL ) {
		string s = string("FetchURL::getCell - pTableRows==NULL");		
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, s);
		throw  TSException(s);											
	}

 	if( row<0 || row>this->iRows || col<0 || col>this->pTableRows[row]->iCols ) {
		string s = string("FetchURL::getCell - Invalid argument(s) row(max rows):"); // + row + "(" + this->iRows + ") col(max cols):" + col +"(" + this->iCols+ ")";		
		this->logger->log_EMERGENCY(TSERR_INTERNAL_ERROR, s);
		throw  TSException(s);											
	}
	 	 
	return *(this->pTableRows[row]->pstrColValue[col]);
}
  

