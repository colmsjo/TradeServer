/****************************************************************************/
/*! \mainpage FetchPTData Fetch PrimeTrader data and write to CSV-file
 * \section intro_sec Introduction
 *
 * FetchPTData.h 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Classes that fetch real time ticker data from a PrimeTrader installation.
 * The data is stored in html tables. The URL:s to fetch are stores in a file
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101118 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef FETCH_PT_DATA
#define FETCH_PT_DATA

 
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <exception>

#include <curl/curl.h>
#include <curl/easy.h> 

#include "xmlParser.h"
//#include "pantheios/pantheios.hpp"
//#include "pantheios/frontends/stock.h"
//#include "pantheios/backends/bec.file.h" 

#ifdef DEBUG_MODE
#include "../../tools/nvwa-0.8.2/nvwa/debug_new.h"
	#endif

using namespace std;


// Define error codes
// code > 0 everything is ok
// vode < 0 some type of error
#define TS_OK 1				// All good
#define TS_NOT_OK -1			// Generic error

// Logging setup
static const string TS_LOG_FILE = "tradeserver.log";

#define PSTR(x)         PANTHEIOS_LITERAL_STRING(x)

// Define texts for errors used for logging
#define TSERR_INTERNAL_ERROR 		"ERROR 1. Internal error"
#define TSERR_CURL_ERROR 		"ERROR 2. Generic curl error"



/// TSLog
/**
 * Wrapper class for the Pantheios loggin framework. Should always be used
 *
 *
 */

class TSLog {
public:
	TSLog(string strLogFile) {
		//pantheios_be_file_setFilePath(strLogFile.c_str()); 
					      //PANTHEIOS_BE_FILE_F_TRUNCATE, 
					      //PANTHEIOS_BE_FILE_F_TRUNCATE, 
					      //PANTHEIOS_BEID_LOCAL);	
	}
	
	void log_DEBUG(string s1, string s2) {	
		string s = s1 + ":" + s2;
		//pantheios::log_DEBUG(s);
	}
	
	void log_INFORMATIONAL(string s1, string s2) {	
		string s = s1 + ":" + s2;
		//pantheios::log_INFORMATIONAL(s);
	}
	
	void log_NOTICE(string s1, string s2) {	
		string s = s1 + ":" + s2;
		//pantheios::log_NOTICE(s);
	}
	
	void log_WARNING(string s1, string s2) {
		string s = s1 + ":" + s2;
		//pantheios::log_WARNING(s);
	}
	
	void log_ERROR(string s1, string s2) {
		string s = s1 + ":" + s2;
		//pantheios::log_ERROR(s);
	}
	
	void log_CRITICAL(string s1, string s2) {
		string s = s1 + ":" + s2;
		//pantheios::log_CRITICAL(s);
	}
	
	void log_ALERT(string s1, string s2) {
		string s = s1 + ":" + s2;
		//pantheios::log_ALERT(s);
	}
	
	void log_EMERGENCY(string s1, string s2) {
		string s = s1 + ":" + s2;
		//pantheios::log_EMERGENCY(s);
	}	
};

/// TSBase
/**
 * A generic TradeServer Root class that all classes should inherit.
 *
 * Currently empty, can be used for generic constructors, destructors etc.
 *
 */

class TSBase {
protected:
	//static TSLog* logger;
	TSLog* logger;
public:
	TSBase() {
		//if(!TSBase::logger) TSBase::logger = new TSLog(TS_LOG_FILE); 
		logger = new TSLog(TS_LOG_FILE); 
	}

	~TSBase() {
		delete logger; 
	}

};

/// TSException generic exception class
/**
 * A generic exception class. Inherit from this class for spcecialized exceptions
 *
 *
 *
 *
 * Example on how to catch exceptioins, NOT THE & FOLLOWING catch (exception&)
 *
 * int main () {
 *  try
 *  {
 *    throw myex;
 *  }
 * catch (exception& e)
 * {
 *   cout << e.what() << endl;
 * }
 *  return 0;
 * }
 *
 *
 *
 *
 */

/** class TSException : public exception {
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
}; */
 
 
class TSException : public exception {
public:
	TSException(string s) {
		strErrorMsg = s;
	}
	
	~TSException() throw() {};
	
	string getErrorMsg() throw() {
		return strErrorMsg;
	}

private:
	string strErrorMsg;
};


 
/// FetchURL class
/**
 * A class that has functions for fetching URL data in HTML table format and save to a file
 *
 *
 *
 */

// structure for storing rows of the table
typedef struct {
	string ** pstrColValue;
	int       iCols;	
	
} struct_table_row;
 
class FetchURL : public TSBase {
public:	
	
	/// Constructor
	/**
	 * Empty constructor
	 *
	 * @param no params
	 *
	 * \return return a instance of the vlass FetchURL
	 *
	 */
	 FetchURL();

	/// Destructor
	/**
	 * 
	 *
	 *
	 */
	 ~FetchURL();

	 
	 /// readURL
	 /**
	 * Parse string and save the contents as a CSV-file
	 *
	 * @param strURL the URL to fetch
	 *
	 * \return return 1 if successfull, error code <0 if unsuccesfull
 	 *
	 */;
	 int readURL(string strURL);

	 /// parseContents
	 /**
	 * Parse the contets that have been fetched
	 *
	 * Equivalent to parseContents("body", "html/table", "tr", "td")  
	 *
	 * \return return TS_OK if successfull, error code <0 if unsuccesfull
	 *
	 */
 	 int parseContents();

	 /// parseContents
	 /**
	 * Parse the contets that have been fetched
	 *
	 * @param szMainNode 	"body" in case of a HTML  table
	 * @param szTableNode	"html/table" in case of a HTML  table
	 * @param szRowNode	"tr" in case of a HTML  table 
	 * @param szCellNode	"td" in case of a HTML  table
	 *
	 * \return return TS_OK if successfull, error code <0 if unsuccesfull
	 *
	 */
 	 int parseContents(string szMainNode, string szTableNode, string szRowNode, string szCellNode);
 	 
	 /// saveAsCSV
	 /**
	 * Save contents table as CSV file
	 *
	 * @param strFileName file to save contents in
	 *
	 * \return return TS_OK if successfull, error code <0 if unsuccesfull
	 *
	 */
 	 int saveAsCSV(string strFileName);
 	 
	 /// getContents
	 /**
	 * Return the HTML contents
	 *
	 * \return Return a string with the HTML contents
	 *
	 */
 	 string getContents() { return *pStrContents; }
 	 
	 /// setContents
	 /**
	 * Set the HTML contents. This content can then be parsed with parseContents.
	 *
	 * @param s a string containing the contents
	 *
	 * \return Return a string with the HTML contents
	 *
	 */
 	 void setContents(string s) { 
 	 	pStrContents = new string(s); 
 	 }
 	 

	 /// getCell
	 /**
	 * Get the contents once it has been parsed.
	 *
	 * @param s a string containing the contents
	 *
	 * \return Return a string with the HTML contents
	 *
	 */
 	 string getCell(int row, int col); 

	 /// getNumRows
	 /**
	 * Get the contents once it has been parsed.
	 *
	 * \return Return the number of rows in the table
	 *
	 */
 	 int getNumRows() { return this->iRows; } 

	 /// getNumCols
	 /**
	 * Get the contents once it has been parsed.
	 *
	 * \return Return the number of cols in the table
	 *
	 */
 	 int getNumCols(int row) { return this->pTableRows[row]->iCols; } 

 	 int testPTableRows(); 
 	 
private:
 	 string 	*pStrContents;				// string buffer for contents
 	 
	 // curl related attributed
	 CURL 		*curl;					// That is the connection, see: curl_easy_init  
 	 CURLcode 	res;					// Error code 
	 char 		szErr[CURL_ERROR_SIZE];			// String explaining error
 	 
	 // XML parser related attributes
	 XMLResults     *pXmlResults;				// get meaningful explanation with getError
	 
	 // array of string with the contents
	 // [["cell_11", "cell_12", ..., "cell_1n"],
	 // ["cell_21", "cell_22", ..., "cell_2n"],
	 // ...
	 // ["cell_m1", "cell_m2", ..., "cell_mn"]]
	 //
	 //string*** 		astrCells;
	 struct_table_row** 	pTableRows;
	 int 			iRows;
	 //int			iCols;				// TO BE REMOVED
	 
 	 int fetch_url_main(string strURLToFetch);
 	 
};

#endif
