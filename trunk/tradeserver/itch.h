/****************************************************************************/
/*! \mainpage Itch Classes used to parser ITCH data
 * \section intro_sec Introduction
 *
 * itch.h 
 *
 * @version     v0.1
 * @author      Jonas Colmsj�
 *
 * Copyright (c) 2010, Gizur Consulting
 * <a href="http://gizur.com">Gizur Consulting</a>
 * All rights reserved.
 *
 * ---------------------------------------------------------------------------
 *
 * Classes use for parsing itch data.
 *
 * 1. Check the first character of the ITCH string
 * 2. Copy the segments of the string and convert to appropriate data types
 * 3. Create a object of approriate class with the data
 * 4. All messages inherit a ItchMessage base class (: public MessageClass)
 *
 *
 * NasDaq OMX ITCH message types:
 * TIME_SEC_ID				'T'
 * TIME_MSEC_ID				'M'
 * SYSTEM_EVENT_ID			'S'
 * MARKET_SEGMENT_STATE_ID		'O'
 * ORDER_BOOK_DIRECTORY_ID		'R'
 * ORDER_BOOK_TRADING_ACTION_ID		'H'
 * ADD_ORDER_ID				'A'
 * ADD_ORDER_MPID_ID			'F'
 * ORDER_EXECUTED_ID			'E'
 * ORDER_EXECUTED_WITH_PRICE_ID		'C'
 * ORDER_CANCEL_ID			'X'
 * ORDER_DELETE_ID			'D'
 * TRADE_ID				'P'
 * CROSS_TRADE_ID			'Q'
 * BROKEN_TRADE_ID			'B'
 * NOII_ID				'I'
 *
 * 
 * const int TIME_SEC_OFFSET[] = {0,1,6};
 *
 * this->itch_message.message_type = *pSz;
 *	
 * switch(this->itch_message.message_type) {
 *	case TIME_SEC_ID:
 *
 * 	strncpy(szBuffer, pSz+TIME_SEC_OFFSET[1], TIME_SEC_OFFSET[2]);
 *	TimeSecMsg* timeSec = new TimeSecMsg();
 *	timeSec->secs = atol(szBuffer);
 *	return timeSec;
 *
 *
 * ---------------------------------------------------------------------------
 *
 * Change log:
 *
 * 101206 Jonas C. Initial version
 *
 ****************************************************************************/

#ifndef ITCH
#define ITCH

#include "ts_common.h"

typedef char MESSAGE_TYPE;

#define TIME_SEC_ID			'T'
#define TIME_MSEC_ID			'M'
#define SYSTEM_EVENT_ID			'S'
#define MARKET_SEGMENT_STATE_ID		'O'
#define ORDER_BOOK_DIRECTORY_ID		'R'
#define ORDER_BOOK_TRADING_ACTION_ID	'H'
#define ADD_ORDER_ID			'A'
#define ADD_ORDER_MPID_ID		'F'
#define ORDER_EXECUTED_ID		'E'
#define ORDER_EXECUTED_WITH_PRICE_ID	'C'
#define ORDER_CANCEL_ID			'X'
#define ORDER_DELETE_ID			'D'
#define TRADE_ID			'P'
#define CROSS_TRADE_ID			'Q'
#define BROKEN_TRADE_ID			'B'
#define NOII_ID				'I'


/// ItchMessage class
/**
 * Base class for Itch messages
 *
 *
 */

class ItchMessage {
public:	
	/// Indicates the ITCH message type, i.e. 'T', 'M' etc. 	 
	MESSAGE_TYPE message_type;

 	ItchMessage(MESSAGE_TYPE mtype) {
 		this->message_type = mtype;
 	}
};


/// TimeSecMsg class
/**
 * ITCH Seconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int TIME_SEC_OFFSET[] = {0,1,6};

class TimeSecMsg : public ItchMessage {
public:
	TimeSecMsg() : ItchMessage(TIME_SEC_ID){}
	int secs;
};


/// TimeMSecMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int TIME_MSEC_OFFSET[] = {0,1,6};

class TimeMSecMsg : public ItchMessage {
public:
	TimeMSecMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// SystemEventMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int SYSTEM_EVENT_OFFSET[] = {0,1,6};

class SystemEventMsg : public ItchMessage {
public:
	SystemEventMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// MarketSegmentStateMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int MARKET_SEGMENT_STATE_OFFSET[] = {0,1,6};

class MarketSegmentStateMsg : public ItchMessage {
public:
	MarketSegmentStateMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderBookDirectoryMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_BOOK_DIRECTORY_OFFSET[] = {0,1,6};

class OrderBookDirectoryMsg : public ItchMessage {
public:
	OrderBookDirectoryMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderBookTradingActionMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_BOOK_TRADING_ACTION_OFFSET[] = {0,1,6};

class OrderBookTradingActionMsg : public ItchMessage {
public:
	OrderBookTradingActionMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// AddOrderMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ADD_ORDER_OFFSET[] = {0,1,6};

class AddOrderMsg : public ItchMessage {
public:
	AddOrderMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// AddOrderMPIDMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ADD_ORDER_MPID_OFFSET[] = {0,1,6};

class AddOrderMPIDMsg : public ItchMessage {
public:
	AddOrderMPIDMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderExecutedMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_EXECUTED_OFFSET[] = {0,1,6};

class OrderExecutedMsg : public ItchMessage {
public:
	OrderExecutedMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderExecutedWithPriceMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_EXECUTED_WITH_PRICE_OFFSET[] = {0,1,6};

class OrderExecutedWithPriceMsg : public ItchMessage {
public:
	OrderExecutedWithPriceMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderCancelMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_CANCEL_OFFSET[] = {0,1,6};

class OrderCancelMsg : public ItchMessage {
public:
	OrderCancelMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// OrderDeleteMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int ORDER_DELETE_OFFSET[] = {0,1,6};

class OrderDeleteMsg : public ItchMessage {
public:
	OrderDeleteMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// TradeMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int TRADE_OFFSET[] = {0,1,6};

class TradeMsg : public ItchMessage {
public:
	TradeMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};



/// CrossTradeMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int CROSS_TRADE_OFFSET[] = {0,1,6};

class CrossTradeMsg : public ItchMessage {
public:
	CrossTradeMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// BrokenTradeMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int BROKEN_TRADE_OFFSET[] = {0,1,6};

class BrokenTradeMsg : public ItchMessage {
public:
	BrokenTradeMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};


/// BrokenTradeMsg class
/**
 * ITCH Milliseconds since midnight message
 *
 *
 */

// Offset for each field, the last offset point to tne end of the message 
const int NOII_OFFSET[] = {0,1,6};

class NOIIMsg : public ItchMessage {
public:
	NOIIMsg() : ItchMessage(TIME_SEC_ID){}
	int msecs;
};



// Makes it easy to cast pointer to different message types
/** typedef union {
		TIME_SEC 			sec_msg;
		TIME_MSEC 			msec_msg;
		SYSTEM_EVENT			system_event;
		MARKET_SEGMENT_STATE		market_segment;
		ORDER_BOOK_DIRECTORY		order_book_directory;
		ORDER_BOOK_TRADING_ACTION	order_book_trading_action;
		ADD_ORDER			add_order;
		ADD_ORDER_MPID			add_order_mpid;
		ORDER_EXECUTED			order_executed;
		ORDER_EXECUTED_WITH_PRICE	order_executed_with_price;
		ORDER_CANCEL			order_cancel;
		ORDER_DELETE			order_delete;
		TRADE				trade;
		CROSS_TRADE			cross_trade;
		BROKEN_TRADE			broken_trade;
		NOII				noii;
} MESSAGE;


typedef struct {
	MESSAGE_TYPE message_type;
	MESSAGE      *message;
} ITCH_MESSAGE;
*/

/// ItchMessage class
/**
 * Used for parsing ITCH data
 *
 *
 *
 */


class Itch : public TSBase {
public:	
	/// Constructor
	/**
	 * Empty constructor
	 *
	 * @param no params
	 *
	 * \return return a instance of the vlass FetchURL
	 *
	 */
	Itch();

	/// Destructor
	/**
	 * 
	 *
	 *
	 */
	~Itch();

	 /// parse
	 /**
	 * Get the contents once it has been parsed.
	 *
	 * @param msg the message to parse
	 *
	 * \return returns the number of characters the message consisted of
	 *
	 */
 	 ItchMessage* parse(const char *pSz);

	/// Public class member that is used to access the message details 	 
	//ITCH_MESSAGE itch_message;	

private:

};


#endif
